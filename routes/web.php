<?php
//后台登陆路由
Route::get('/admin/login', "Admin\LoginController@login");
//网站关闭提示
Route::get("/switch.html", "Home\IndexController@switch");
//禁止访问提示
Route::get("/banip.html", "Home\IndexController@banip");
//处理后台登录信息
Route::post('/admin/dologin', "Admin\LoginController@dologin");
//Token登录采集
Route::get("/admin/caiji-action/{page}/{id}/{type}", "Admin\ReslistController@action");
//前台开关路由组
Route::group(['middleware' => 'switch'], function () {
    //图形验证码
    Route::get("/code/{name}", "Home\ErrorController@code");
    //登录页面
    Route::get('/login', "Home\LoginController@index");
    //注册的页面
    Route::get('/home/zc', "Home\ZcController@index");
    //注册页面
    Route::get("/reg.html", "Home\LoginController@reg");
    //执行登录
    Route::post("/dologin", "Home\LoginController@dologin");
    //退出登陆
    Route::get("/outLogin", "Home\LoginController@out");
    //执行注册
    Route::post("/doreg", "Home\RegisterController@doreg");
    Route::post("/phoneReg", "Home\RegisterController@phoneReg");
    Route::post("/emailReg", "Home\RegisterController@emailReg");
    //激活邮件
    Route::get("/activation", "Home\RegisterController@emailjh");
    //找回密码
    Route::get("/back.html", "Home\RegisterController@back");
    //找回手机号账号
    Route::post("/phoneBack", "Home\RegisterController@phoneBack");
    //找回邮箱账号
    Route::post("/emailBack", "Home\RegisterController@emailBack");
    //登录注册结束
    //没有权限的显示页面
    Route::get("/admin/unpower.html", function () {
        return view("admin.unpower");
    });
    //图标展示页面
    Route::get("/admin/unicode.html", function () {
        return view("admin.unicode");
    });
    //个人中心登陆判断路由组
    Route::group(["middleware" => 'center'], function () {
        //个人中心
        Route::get("/center", "Home\CenterController@index");
        //个人中心发送邮件
        Route::post("/center-email", "Home\CenterController@email");
        //个人中心发送短信
        Route::post("/center-phone", "Home\CenterController@phone");
        //个人中心修改邮件
        Route::post("/centeremail", "Home\CenterController@centeremail");
        //个人中心修改手机号
        Route::post("/centerphone", "Home\CenterController@centerphone");
        //个人中心修改昵称
        Route::post("/centername", "Home\CenterController@centername");
        //执行个人中心取消收藏操作
        Route::post("/collection-cancel", "Home\CenterController@cancel");
        //执行个人中心假删除订单操作
        Route::post("/recharge-deletee", "Home\CenterController@deletee");
        //继续支付订单操作
        Route::get("/recharge-pay/{id}", "Home\CenterController@rechargepay");
        //用户充值支付宝界面
        Route::get("/center-pay/{level}", "Home\CenterController@pay");
        //支付宝同步页面
        Route::get("/return_url", "Home\CenterController@returnn");
    });
    //支付异步页面
    Route::post("/notify_url", "Home\CenterController@notifyy");
    Route::get("/notify_url", "Home\CenterController@notifys");
    //影片详情页评论
    Route::post("/comments", "Home\DetailController@comments");
    //执行影片详情评论操作
    Route::post("/comments-reply", "Home\DetailController@reply");
    //执行评论赞操作
    Route::post("/comments-praise", "Home\DetailController@praise");
    //执行评论踩操作
    Route::post("/comments-tread", "Home\DetailController@tread");
    //播放器页面
    Route::get("/play/{ids}.html", "Home\PlayController@index");
    //影片详情
    Route::get("/details/{id}.html", "Home\DetailController@index");
    //前台提交报错
    Route::get("/error", "Home\ErrorController@index");
    //执行报错添加
    Route::post("/doError", "Home\ErrorController@add");
    //执行评分
    Route::get("/include/ajax", "Home\DetailController@ajax");
    //留言板页面
    Route::resource("/gbook", "Home\LeaveController");
    //历史记录
    Route::get("/history", "Home\PlayController@history");
    //添加收藏
    Route::get("/collection", "Home\PlayController@collection");
    //搜索自动补全
    Route::get("/ass", "Home\PlayController@ass");
    //申请友情链接
    Route::get("/tolink", "Home\LinksController@index");
    //执行友链申请
    Route::post("/doLinks", "Home\LinksController@add");
    //观看密码处理
    Route::post("/watchPasswd", "Home\PlayController@watchPasswd");
    //用户头像上传
    Route::post("/uploadpic", "Home\CenterController@upload");
    //用户收藏
    Route::get("/collection", "Home\CenterController@collection");
    //公告详情
    Route::get("/gonggao/{id}.html", "Home\IndexController@gonggao");
    //前台首页
    Route::get('/', "Home\IndexController@index");
    //前台首页头部主分类遍历模板页
    Route::get('/list/{tid}', "Home\IndexController@list");
    //前台首页头部主分类遍历模板页条件页
    Route::get('/search', "Home\IndexController@search");
});

/*权限判断路由组*/
Route::group(["middleware" => "power"], function () {
    //后台首页
    Route::get('/admin', "Admin\IndexController@index");
    //处理后台退出
    Route::get('/admin/outlogin', "Admin\LoginController@outlogin");
    //后台欢迎页面
    Route::get('/admin/welcome', "Admin\IndexController@welcome");
    //模板模块
    Route::get('/admin/temlist', "Admin\MobanController@index");
    //列表
    Route::get('/admin/gglist', "Admin\GonggaoController@gonggao");
    //修改
    Route::get('/admin/ggedit/{id}', "Admin\GonggaoController@edit");
    //执行修改
    Route::post('/admin/ggeditdo', "Admin\GonggaoController@editdo");
    //添加
    Route::get('/admin/ggadd', "Admin\GonggaoController@store");
    //执行添加
    Route::post('/admin/doggadd', "Admin\GonggaoController@doadd");
    //删除
    Route::get('/admin/ggdel/{id}', "Admin\GonggaoController@del");
    //微信配置   wxconfig
    Route::get('/admin/wxconfig', "Admin\WxconfigController@index");
    Route::post('/admin/wxconfig/add', "Admin\WxconfigController@edit");
    //百度推送   baidu
    Route::get('/admin/baidu', "Admin\BaiduController@index");
    //邮件配置	mail
    Route::get('/admin/mail', "Admin\MailController@mail");
    //邮件修改
    Route::post('/admin/mail/edit', "Admin\MailController@edit");
    //模板
    Route::get('/admin/moban/moban/{filename}', "Admin\MobanController@index");
    //用户列表展示
    Route::get("/admin/userlist", "Admin\UserController@index");
    //添加用户页面
    Route::get("/admin/user-add", "Admin\UserController@add");
    //执行用户添加操作
    Route::post("/admin/user-doadd", "Admin\UserController@doadd");
    //执行修改用户状态操作
    Route::post("/admin/user-banned", "Admin\UserController@banned");
    //查看用户详情
    Route::get("/admin/user-info/{id}", "Admin\UserController@info");
    //后台收藏管理
    Route::get("/admin/collection", "Admin\UserController@collection");
    //封禁列表
    Route::get("/admin/backlist", "Admin\UserController@backlist");
    //执行解封用户操作
    Route::post("/admin/use-deblocking", "Admin\UserController@deblocking");
    //后台充值记录展示
    Route::get("/admin/recharge", "Admin\UserController@recharge");
    //管理员列表展示
    Route::get("/admin/adminuser", "Admin\AdminController@index");
    //执行修改管理员状态操作
    Route::post("/admin/admin-banned", "Admin\AdminController@banned");
    //执行删除管理员操作
    Route::post("/admin/admin-del", "Admin\AdminController@del");
    //添加管理员页面
    Route::get("/admin/admin-add", "Admin\AdminController@add");
    //执行管理员添加操作
    Route::post("/admin/admin-doadd", "Admin\AdminController@doadd");
    //修改管理员页面
    Route::get("/admin/admin-edit/{id}", "Admin\AdminController@edit");
    //执行管理员修改操作
    Route::post("/admin/admin-doedit", "Admin\AdminController@doedit");
    //角色管理展示页面
    Route::get("/admin/power", "Admin\PowerController@powerindex");
    //执行修改角色状态操作
    Route::post("/admin/power-banned", "Admin\PowerController@powerbanned");
    //添加角色页面
    Route::get("/admin/power-add", "Admin\PowerController@poweradd");
    //执行角色添加操作
    Route::post("/admin/power-doadd", "Admin\PowerController@powerdoadd");
    //修改角色页面
    Route::get("/admin/power-edit/{id}", "Admin\PowerController@poweredit");
    //执行角色修改操作
    Route::post("/admin/power-doedit", "Admin\PowerController@powerdoedit");
    //执行角色删除操作
    Route::post("/admin/power-del", "Admin\PowerController@powerdel");
    //设置管理员角色操作
    Route::get("/admin/admin-power/{id}", "Admin\AdminController@adminpower");
    //执行设置管理员角色操作
    Route::post("/admin/admin-power-do", "Admin\AdminController@doadminpower");
    //权限分类展示页面
    Route::get("/admin/power-type", "Admin\PowerController@powertypeindex");
    //权限分类修改页面
    Route::get("/admin/power-type-edit/{id}", "Admin\PowerController@powertypeedit");
    //执行权限分类修改操作
    Route::post("/admin/power-type-doedit", "Admin\PowerController@powertypedoedit");
    //添加权限分类页面
    Route::get("/admin/power-type-add", "Admin\PowerController@powertypeadd");
    //执行添加权限分类操作
    Route::post("/admin/power-type-doadd", "Admin\PowerController@powertypedoadd");
    //执行删除权限分类操作
    Route::post("/admin/power-type-del", "Admin\PowerController@powertypedel");
    //权限管理展示页面
    Route::get("/admin/power-manage", "Admin\PowerController@powermanageindex");
    //权限规则修改页面
    Route::get("/admin/power-manage-edit/{id}", "Admin\PowerController@powermanageedit");
    //执行权限规则修改操作
    Route::post("/admin/power-manage-doedit", "Admin\PowerController@powermanagedoedit");
    //添加权限规则页面
    Route::get("/admin/power-manage-add", "Admin\PowerController@powermanageadd");
    //执行添加权限规则操作
    Route::post("/admin/power-manage-doadd", "Admin\PowerController@powermanagedoadd");
    //执行删除权限规则操作
    Route::post("/admin/power-manage-del", "Admin\PowerController@powermanagedel");
    //菜单列表展示页面
    Route::get("/admin/menulist", "Admin\MenuController@index");
    //执行修改菜单状态操作
    Route::post("/admin/menu-banned", "Admin\MenuController@banned");
    //添加菜单模块页面
    Route::get("/admin/menu-add", "Admin\MenuController@add");
    //执行添加菜单模块操作
    Route::post("/admin/menu-doadd", "Admin\MenuController@doadd");
    //菜单模块修改页面
    Route::get("/admin/menu-edit/{id}", "Admin\MenuController@edit");
    //执行修改菜单模块操作
    Route::post("/admin/menu-doedit", "Admin\MenuController@doedit");
    //执行删除菜单模块操作
    Route::post("/admin/menu-del", "Admin\MenuController@del");
    //菜单禁用管理展示页面
    Route::get("/admin/permissions", "Admin\MenuController@permissions");
    //设置角色禁用菜单页面
    Route::get("/admin/menu-permissions/{id}", "Admin\MenuController@einrichten");
    //执行角色禁用菜单操作
    Route::post("/admin/menu-permissions", "Admin\MenuController@doeinrichten");
    //评论管理展示页面
    Route::get("/admin/comments", "Admin\CommentsController@index");
    //评论回复展示页面
    Route::get("/admin/comments-reply/{id}", "Admin\CommentsController@reply");
    //执行评论删除处理
    Route::post("/admin/comments-del", "Admin\CommentsController@del");
    //采集列表
    Route::get('/admin/reslist', "Admin\ReslistController@index");
    //绑定分类
    Route::get('/admin/caiji-binding/{id}', "Admin\ReslistController@binding");
    //执行分类绑定
    Route::get("/admin/caiji-dobinding", "Admin\ReslistController@dobinding");
    //添加资源库
    Route::get("/admin/caiji-add", "Admin\ReslistController@add");
    //执行添加资源库
    Route::post("/admin/caiji-doadd", "Admin\ReslistController@doadd");
    //资源库修改
    Route::get("/admin/caiji-edit/{id}", "Admin\ReslistController@edit");
    //执行资源库修改
    Route::post("/admin/caiji-doedit", "Admin\ReslistController@doedit");
    //执行资源库删除
    Route::post("/admin/caiji-del", "Admin\ReslistController@del");
    //清除分类绑定
    Route::get("/admin/caiji-clear", "Admin\ReslistController@clear");
    //断点续采
    Route::get("/admin/caiji-xucai", "Admin\ReslistController@xucai");
    //执行续采
    Route::get("/admin/caiji-doxucai", "Admin\ReslistController@doxucai");
    //资源库管理
    Route::get("/admin/resmanage", "Admin\ResmanageController@index");
    //资源列表
    Route::get("/admin/caiji-detail/{page}/{id}", "Admin\ResmanageController@list");
    //资源api搜索
    Route::get("/admin/caiji-search", "Admin\ResmanageController@search");
    //根据指定id采集
    Route::get("/admin/caiji-ids/{ids}/{id}", "Admin\ResmanageController@ids");
    //播放器来源
    Route::get("/admin/lyname", "Admin\LynameController@index");
    //修改播放器
    Route::get("/admin/lyname-edit", "Admin\LynameController@edit");
    //数据列表
    Route::resource("/admin/datalist", "Admin\DataController");
    //执行数据修改
    Route::post("/admin/data-edit", "Admin\DataController@doedit");
    //执行添加数据
    Route::post("/admin/data-add", "Admin\DataController@add");
    //执行隐藏数据
    Route::get("/admin/data-yc", "Admin\DataController@yc");
    //隐藏数据列表
    Route::get("/admin/hiddendata", "Admin\DataController@hiddendata");
    //显示数据操作
    Route::get("/admin/data-xs", "Admin\DataController@xs");
    //删除数据操作
    Route::get("/admin/data-del", "Admin\DataController@del");
    //批量删除数据
    Route::post("/admin/data-delAll", "Admin\DataController@delAll");
    //批量移动数据操作
    Route::post("/admin/data-removeAll", "Admin\DataController@removeAll");
    //搜索
    Route::get("/admin/data-search", "Admin\DataController@search");
    //添加新地址
    Route::get("/admin/data-addPlay", "Admin\DataController@addPlay");
    //执行新地址添加
    Route::post("/admin/data-doAddPlay", "Admin\DataController@doAddPlay");
    //删除播放来源
    Route::get("/admin/data-delPlay", "Admin\DataController@delPlay");
    //有密码数据
    Route::get("/admin/pwddata", "Admin\DataController@pwddata");
    //幻灯片
    Route::get("/admin/banner", "Admin\DataController@bannerList");
    //修改幻灯片状态
    Route::get("/admin/banner-change", "Admin\DataController@bannerChange");
    //排序修改
    Route::get("/admin/banner-px", "Admin\DataController@px");
    //幻灯片编辑
    Route::get("/admin/banner-edit", "Admin\DataController@bannerEdit");
    //执行幻灯修改
    Route::post("/admin/doBannerEdit", "Admin\DataController@doBannerEdit");
    //添加幻灯
    Route::get("/admin/banner-add", "Admin\DataController@addBanner");
    //执行添加幻灯
    Route::post("/admin/doBannerAdd", "Admin\DataController@doBannerAdd");
    //执行幻灯删除
    Route::get("/admin/banner-del", "Admin\DataController@delBanner");
    //数据批量替换
    Route::resource("/admin/datareplate", "Admin\DataOperationControll");
    //数据库语句操作
    Route::get("/admin/sql", "Admin\DataOperationControll@mysqlTool");
    //数据库语句执行
    Route::post("/admin/dosql", "Admin\DataOperationControll@dosql");
    //文件权限检测
    Route::get("/admin/checkperm", "Admin\DataOperationControll@checkperm");
    //删除指定数据来源
    Route::get("/admin/deldata", 'Admin\DataOperationControll@deldata');
    //指定删除指定数据来源
    Route::get("/admin/operationDel", "Admin\DataOperationControll@operationDel");
    //数据检测
    Route::get("/admin/checkdata", "Admin\DataOperationControll@checkdata");
    //友情链接
    Route::resource("/admin/links", "Admin\LinksController");
    //友情链接状态改变
    Route::get("/admin/links-status", "Admin\LinksController@status");
    //友情链接审核状态改变
    Route::get("/admin/links-audit", "Admin\LinksController@audit");
    //留言管理
    Route::resource("/admin/leave", "Admin\LeaveController");
    //留言审核状态改变
    Route::get("/admin/leave-audit", "Admin\LeaveController@audit");
    //查看回复
    Route::get("/admin/leave-replay", "Admin\LeaveController@replay");
    //报错数据
    Route::resource("/admin/error", "Admin\ErrorController");
    //报错状态改变
    Route::get("/admin/error-audit", "Admin\ErrorController@audit");
    //查看报错处理结果
    Route::get("/admin/error-replay", "Admin\ErrorController@replay");
    //清除缓存
    Route::get("/admin/clear", "Admin\IndexController@clear");
    //切换模版
    Route::get('/admin/switch', "Admin\MobanController@switch");
    //用户修改密码
    Route::get("/admin/updatePwd", "Admin\LoginController@updatepwd");
    Route::post("/admin/doupdatePwd", "Admin\LoginController@doupdate");
    //系统配置里广告
    Route::get('/admin/ad', "Admin\XtController@ad");
    //广告删除
    Route::get('/admin/ad/del', "Admin\XtController@del");
    //广告增加
    Route::get('/admin/ad/add', "Admin\XtController@add");
    //处理广告增加
    Route::post('/admin/ad/doadd', "Admin\XtController@doadd");
    //广告修改页
    Route::get('/admin/ad/edit/{id}', "Admin\XtController@edit");
    //处理广告修改
    Route::post('/admin/ad/doedit', "Admin\XtController@doedit");
    //广告禁用与启用
    Route::get('/admin/ad/jinyong', "Admin\XtController@jinyong");
    //系统基本配置模板页
    Route::get('/admin/jbconfig', "Admin\XtpzController@index");
    //系统基本配置修改
    Route::get('/admin/jbconfig/edit', "Admin\XtpzController@edit");
    //系统基本配置logo图片修改
    Route::post('/admin/api/upload', "Admin\XtpzController@imgedit");
    //系统基本配置二维码图片修改
    Route::post('/admin/api/upload2', "Admin\XtpzController@imgedit2");
    //登录日志模块显示
    Route::get('/admin/loginlog', "Admin\DlrzController@index");
    //清除登录日志
    Route::get('/admin/Dlrz/del', "Admin\DlrzController@del");
    //登录分页
    Route::get('/admin/Dlrz/fenye', "Admin\DlrzController@index");
    //后台分类模板
    Route::get('/admin/type', "Admin\TypeController@index");
    //分类批量设置会员等级
    Route::get('/admin/type/level', "Admin\TypeController@level");
    //后台分类显示与隐藏
    Route::get('/admin/type/xs', "Admin\TypeController@xs");
    //后台分类删除
    Route::post('/admin/type/del', "Admin\TypeController@del");
    //后台分类增加
    Route::get('/admin/type/add', "Admin\TypeController@add");
    //后台处理分类增加
    Route::post('/admin/type/doadd', "Admin\TypeController@doadd");
    //后台分类修改
    Route::get('/admin/type/edit/{id}', "Admin\TypeController@edit");
    //后台处理分类修改
    Route::post('/admin/type/doedit', "Admin\TypeController@doedit");
    //后台分类移动
    Route::get('/admin/type/yd/{id}/{p_id}', "Admin\TypeController@yd");
    //后台处理分类移动
    Route::get('/admin/type/doyd', "Admin\TypeController@doyd");
    //接口配置模板
    Route::get("/admin/apiconfig", "Admin\ApiconfigController@index");
    //手机接口修改模板
    Route::get("/admin/apiconfig/phone", "Admin\ApiconfigController@phone");
    //执行手机接口修改
    Route::post("/admin/apiconfig/pedit", "Admin\ApiconfigController@pedit");
    //支付接口修改模板
    Route::get("/admin/apiconfig/pay", "Admin\ApiconfigController@pay");
    //执行支付接口修改
    Route::post("/admin/apiconfig/payedit", "Admin\ApiconfigController@payedit");
    //安全配置模板
    Route::get("/admin/security", "Admin\SecurityController@index");
    // 新增功能路由(后台)
    Route::get("/admin/imgBatchLocalization", "Admin\DataOperationControll@imgBatchLocalization");
    // 获取远程图片url
    Route::get("/admin/remoteImg", "Admin\DataOperationControll@remoteImg");
    // 执行数据库本地化图片变更
    Route::get("/admin/doremoteImg", "Admin\DataOperationControll@doremoteImg");
});