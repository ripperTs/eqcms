<?php
if (file_exists('install.lock')) {
	header("location:index6.php");
	die;
}

?>
<html>

<head>
	<title>安装向导 - EQCMS</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<link href="static/css/style.css" rel="stylesheet" type="text/css" />
	<script src="static/js/jquery.js" type="text/javascript"></script>
</head>

<body scroll="no">
	<div class="b">
		<div class="main">
			<div class="head">
				<div class="h_right"><a href="//www.eqcms.top" target="_blank">官方网站</a></div>
				<img src="../upload/logo.png" width="134" height="40" style="margin-top:5px;" />
			</div>
			<div class="cont">
				<div class="c_top"></div>
				<div class="c_c">
					<div class="c_c_left">
						<ul>
							<li class="">1、欢迎界面</li>
							<li class="">2、阅读协议</li>
							<li class="on">3、环境检测</li>
							<li class="">4、参数配置</li>
							<li class="">5、安装完成</li>
						</ul>
					</div>
					<div class="c_c_right">

						<div class="content">
							<h2>检测服务器运行环境和文件权限</h2>
							<table class="tb">
								<tr>
									<th width="170"><strong>需开启的配置</strong></th>
									<th width="80"><strong>要求</strong></th>
									<th width="400"><strong>实际状态及建议</strong></th>
								</tr>
								<tr>
									<td>PHP 版本</td>
									<td>7.x</td>
									<td>
										<?php
										$check = array();
										$phpv = explode(".", phpversion())[0];
										if ($phpv < 7) {
											echo "<font color=red>[×]Off</font>";
											array_push($check, '0');
										} else {
											echo  "<font color=green>[√]On</font>";
											array_push($check, '1');
										}
										?>
										<small> (本程序仅支持PHP7.X版本，当前版本为<?php echo phpversion() ?>)</small>
									</td>
								</tr>
								<tr>
									<td>MySQLi 支持</td>
									<td>On</td>
									<td>
										<?php
										$sp_mysql = (function_exists('mysqli_connect') ? '<font color=green>[√]On</font>' : '<font color=red>[×]Off</font>');
										echo $sp_mysql;
										?> <small>(必须开启MySQLi扩展，否则无法连接数据库)</small>
									</td>
								</tr>
								<tr>
									<td>fsockopen</td>
									<td>On </td>
									<td>
										<?php
										if (function_exists('fsockopen') || function_exists('pfsockopen')) {
											echo "<font color=green>[√]On</font>";
										} else {
											echo "<font color=red>[x]Off</font>";
										}
										?>
										<small>(不符合要求将导致采集、远程资料本地化等功能无法应用)</small>
									</td>
								</tr>
								<tr>
									<td>iconv</td>
									<td>On </td>
									<td>
										<?php
										$sp_iconv = (function_exists('iconv') ? '<font color=green>[√]On</font>' : '<font color=red>[×]Off</font>');
										echo $sp_iconv;
										?>
										<small>(不符合要求将导致部分编码问题)</small>
									</td>
								</tr>
								<tr>
									<td>allow_url_fopen</td>
									<td>On </td>
									<td>
										<?php
										$sp_allow_url_fopen = (ini_get('allow_url_fopen') ? '<font color=green>[√]On</font>' : '<font color=red>[×]Off</font>');
										echo $sp_allow_url_fopen;
										?>
										<small>(不符合要求将导致采集、远程资料本地化等功能无法应用)</small>
									</td>
								</tr>
								<tr>
									<td>safe_mode</td>
									<td>Off</td>
									<td>
										<?php
										$sp_safe_mode = (ini_get('safe_mode') ? '<font color=red>[×]On</font>' : '<font color=green>[√]Off</font>');
										echo $sp_safe_mode;
										?>
										<small>(本系统不支持在<span class="STYLE2">非win主机的安全模式</span>下运行)</small>
									</td>
								</tr>

								<tr>
									<td>GD 支持 </td>
									<td>On</td>
									<td>
										<?php
										$sp_gd = (extension_loaded('gd') ? '<font color=green>[√]On</font>' : '<font color=red>[×]Off</font>');
										echo $sp_gd;
										?>
										<small>(不支持将导致与图片相关的大多数功能无法使用或引发警告)</small>
									</td>
								</tr>

								<tr>
									<td>Curl 支持 </td>
									<td>On</td>
									<td>
										<?php
										$sp_curl = (function_exists('curl_init') ? '<font color=green>[√]On</font>' : '<font color=#B8860B>[×]Off</font>');
										echo $sp_curl;
										?>
										<small>(不支持将无法进行百度推送和图片同步等操作)</small>
									</td>
								</tr>

								<tr>
									<td>SSL 支持</td>
									<td>On</td>
									<td>
										<?php
										$ssl = (is_SSL() ? '<font color=green>[√]On</font>' : '<font color=#B8860B>[×]Off</font>');
										echo $ssl;
										function is_SSL()
										{
											if (!isset($_SERVER['HTTPS']))
												return FALSE;
											if ($_SERVER['HTTPS'] === 1) {  //Apache
												return TRUE;
											} elseif ($_SERVER['HTTPS'] === 'on') { //IIS
												return TRUE;
											} elseif ($_SERVER['SERVER_PORT'] == 443) { //其他
												return TRUE;
											}
											return FALSE;
										}
										?>
										<small>(邮件SMTP发信服务要求开启SSL支持)</small>
									</td>
								</tr>
							</table>

							<table class="tb">
								<tr>
									<th width="300"><strong>目录名</strong></th>
									<th width="212"><strong>读取权限</strong></th>
									<th width="212"><strong>写入权限</strong></th>
								</tr>
								<tr>
								<tr>
									<td>/</td>
									<td><?php echo is_readable("./") ? '<font color=green>[√]读</font>' : '<font color=red>[X]读</font>' ?></td>
									<td><?php echo is_writable("./") ? '<font color=green>[√]写</font>' : '<font color=red>[X]写</font>' ?></td>
								</tr>

								<tr>
									<td>/upload</td>
									<td><?php echo is_readable("../upload") ? '<font color=green>[√]读</font>' : '<font color=red>[X]读</font>' ?></td>
									<td><?php echo is_writable("../upload") ? '<font color=green>[√]写</font>' : '<font color=red>[X]写</font>' ?></td>
								</tr>

								<tr>
									<td>/app/Library</td>
									<td><?php echo is_readable("../../app/Library") ? '<font color=green>[√]读</font>' : '<font color=red>[X]读</font>' ?></td>
									<td><?php echo is_writable("../../app/Library") ? '<font color=green>[√]写</font>' : '<font color=red>[X]写</font>' ?></td>
								</tr>

								<tr>
									<td>/vendor</td>
									<td><?php echo is_readable("../../vendor") ? '<font color=green>[√]读</font>' : '<font color=red>[X]读</font>' ?></td>
									<td><?php echo is_writable("../../vendor") ? '<font color=green>[√]写</font>' : '<font color=red>[X]写</font>' ?></td>
								</tr>
								<tr>
									<td>/resources/views/template</td>
									<td><?php echo is_readable("../../resources/views/template") ? '<font color=green>[√]读</font>' : '<font color=red>[X]读</font>' ?></td>
									<td><?php echo is_writable("../../resources/views/template") ? '<font color=green>[√]写</font>' : '<font color=red>[X]写</font>' ?></td>
								</tr>
								<tr>
									<td>/app/Http/Middleware</td>
									<td><?php echo is_readable("../../app/Http/Middleware") ? '<font color=green>[√]读</font>' : '<font color=red>[X]读</font>' ?></td>
									<td><?php echo is_writable("../../app/Http/Middleware") ? '<font color=green>[√]写</font>' : '<font color=red>[X]写</font>' ?></td>
								</tr>
								<tr>
									<td>/database</td>
									<td><?php echo is_readable("../../database") ? '<font color=green>[√]读</font>' : '<font color=red>[X]读</font>' ?></td>
									<td><?php echo is_writable("../../database") ? '<font color=green>[√]写</font>' : '<font color=red>[X]写</font>' ?></td>
								</tr>
								<tr>
									<td>/config</td>
									<td><?php echo is_readable("../../config") ? '<font color=green>[√]读</font>' : '<font color=red>[X]读</font>' ?></td>
									<td><?php echo is_writable("../../config") ? '<font color=green>[√]写</font>' : '<font color=red>[X]写</font>' ?></td>
								</tr>
								<tr>
									<td>/.env</td>
									<td>
										<font color=green>必须给777权限</font>
									</td>
									<td>
										<font color=green>必须给777权限</font>
									</td>
								</tr>
								<tr>
									<td>/public/log</td>
									<td><?php echo is_readable("../../public/log") ? '<font color=green>[√]读</font>' : '<font color=red>[X]读</font>' ?></td>
									<td><?php echo is_writable("../../public/log") ? '<font color=green>[√]写</font>' : '<font color=red>[X]写</font>' ?></td>
								</tr>
							</table>


						</div>
						<div class="button">
							<font color=red>&nbsp;&nbsp;&nbsp;提示：为保证程序功能完整,先修复红色 [×] 错误后刷新本页继续安装！</font><a href="index4.php">下一步</a>
							<a href="index2.php">上一步</a>
						</div>
					</div>
				</div>
				<div class="c_btm"></div>
			</div>
		</div>
	</div>

	<script src="static/js/install.js" type="text/javascript"></script>
	<div style="display:none;">
		<script language="JavaScript" type="text/javascript" charset="utf-8" src="static/js/install.js"></script>
	</div>
</body>

</html>
