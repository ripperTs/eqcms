<?php
if (file_exists('install.lock')) {
	header("location:index6.php");
	die;
}

?>

<html>
<head>
<title>安装向导 - EQCMS</title>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<link href="static/css/style.css"rel="stylesheet" type="text/css"  />
<script src="static/js/jquery.js" type="text/javascript"></script>
</head>

<body scroll="no">
<div class="b">
	<div class="main">
		<div class="head">
			<div class="h_right"><a href="//www.eqcms.top" target="_blank">官方网站</a></div>
			<img src="../upload/logo.png"  width="134" height="40" style="margin-top:5px;"/>
		</div>
		<div class="cont">
			<div class="c_top"></div>
			<div class="c_c">
				<div class="c_c_left">
					<ul>
						<li class="on">1、欢迎界面</li>
						<li class="">2、阅读协议</li>
						<li class="">3、环境检测</li>
						<li class="">4、参数配置</li>
						<li class="">5、安装完成</li>
					</ul>
				</div>
				<div class="c_c_right">

<div class="content">
	<h2>欢迎使用EQCMS开始您的筑梦之旅</h2>
	<div style="font-size:77px; color:#146098;text-align:center;font-weight:bold; margin-top:20%;letter-spacing:6px;">EQCMS</div>
	<div style="font-size:44px; color:#3c89c3;text-align:center;letter-spacing:4px;">安全&nbsp;·&nbsp;快速&nbsp;·&nbsp;稳定&nbsp;·&nbsp;开源</div>
</div>
<div class="button"><a href="index2.php">开始安装</a></div>
				</div>
			</div>
			<div class="c_btm"></div>
		</div>
	</div>
</div>

<script src="static/js/install.js" type="text/javascript"></script>
<div style="display:none;">
<script language="JavaScript" type="text/javascript" charset="utf-8" src="static/js/install.js"></script>
</div>
</body>
</html>
