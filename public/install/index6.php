<?php
if (!file_exists('install.lock')) {
    header("location:index.php");
    die;
}
?>
<html>

<head>
    <title>安装向导 - EQCMS</title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <link href="static/css/style.css" rel="stylesheet" type="text/css" />
    <script src="static/js/jquery.js" type="text/javascript"></script>
</head>

<body scroll="no">
    <div class="b">
        <div class="main">
            <div class="head">
                <div class="h_right"><a href="//www.eqcms.top" target="_blank">官方网站</a></div>
                <img src="../upload/logo.png" width="134" height="40" style="margin-top:5px;" />
            </div>
            <div class="cont">
                <div class="c_top"></div>
                <div class="c_c">
                    <div class="c_c_left">
                        <ul>
                            <li class="">1、欢迎界面</li>
                            <li class="">2、阅读协议</li>
                            <li class="">3、环境检测</li>
                            <li class="">4、参数配置</li>
                            <li class="on">5、安装完成</li>
                        </ul>
                    </div>
                    <div class="c_c_right">

                        <div class="content">
                            <h2>您已经安装过了EQCMS程序</h2>
                            <br><br>
                            <div class="end" style="margin-left:80px;">
                                <h3>EQCMS程序已经安装过,您还可以:</h3>
                                <p>1.如需重新安装,请使用FTP删除./install/install.lock文件即可重新安装!</p>
                            </div>
                    </div>
                </div>
                <div class="c_btm"></div>
            </div>
        </div>
    </div>

    <script src="static/js/install.js" type="text/javascript"></script>
    <div style="display:none;">
        <script language="JavaScript" type="text/javascript" charset="utf-8" src="static/js/install.js"></script>
    </div>
</body>

</html>
