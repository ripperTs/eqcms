<?php
error_reporting(0);
if (file_exists('install.lock')) {
    header("location:index6.php");
    die;
}

if (!isset($_POST['check'])) {
    die("<script>location='../../404.html'</script>");
}
include "sql.php";
$link = mysqli_connect($_POST['DB_HOST'], $_POST['DB_USERNAME'], $_POST['DB_PASSWORD']) or die('数据库连接失败!');
mysqli_set_charset($link, 'utf8');
$row = mysqli_query($link, "CREATE DATABASE IF NOT EXISTS `{$_POST['DB_DATABASE']}`"); //创建数据库
if (!$row) {
    die("数据库连接失败!");
}

mysqli_select_db($link, $_POST['DB_DATABASE']);
foreach ($sql as $v) {
    mysqli_query($link, $v);
}

//数据库安装成功后修改这两个文件
constEdit("../../app/Library/function.php", $_POST);
saveEnv("../../.env", $_POST);

file_put_contents("install.lock","安装锁");

//常量文件修改
function constEdit($file, $arr)
{
    $info = file_get_contents($file);
    foreach ($arr as $k => $v) {
        $info = preg_replace("/define\(\"{$k}\",\".*?\"\)/", "define(\"{$k}\",\"{$v}\")", $info);
    }
    return file_put_contents($file, $info);
}

//数据库信息文件修改
function saveEnv($file, $arr)
{
    $info = file_get_contents($file);
    foreach ($arr as $k => $v) {
        $info = preg_replace("/{$k}=.*?\n/", "{$k}={$v}\n", $info);
    }
    return file_put_contents($file, $info);
}
?>
<html>

<head>
    <title>安装向导 - EQCMS</title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <link href="static/css/style.css" rel="stylesheet" type="text/css" />
    <script src="static/js/jquery.js" type="text/javascript"></script>
</head>

<body scroll="no">
    <div class="b">
        <div class="main">
            <div class="head">
                <div class="h_right"><a href="//www.eqcms.top" target="_blank">官方网站</a></div>
                <img src="../upload/logo.png" width="134" height="40" style="margin-top:5px;" />
            </div>
            <div class="cont">
                <div class="c_top"></div>
                <div class="c_c">
                    <div class="c_c_left">
                        <ul>
                            <li class="">1、欢迎界面</li>
                            <li class="">2、阅读协议</li>
                            <li class="">3、环境检测</li>
                            <li class="">4、参数配置</li>
                            <li class="on">5、安装完成</li>
                        </ul>
                    </div>
                    <div class="c_c_right">

                        <div class="content">
                            <h2>恭喜您已经成功完成了EQCMS的安装</h2>
                            <br><br>
                            <div class="end" style="margin-left:80px;">
                                <h3>恭喜,您的网站已安装完成！</h3>
                                <p>首页地址：<a href="http://<?php echo $_POST['S_REALM'] ?>" target="_blank">http://<?php echo $_POST['S_REALM'] ?></a>
                                    <br>后台地址：<a href="http://<?php echo $_POST['S_REALM'] ?>/admin" target="_blank">http://<?php echo $_POST['S_REALM'] ?>/admin</a>
                                    <br>用户名：admin <br>密　码：admin <br>请牢记以上信息，您可以登陆后台修改密码及网站设置。</p>
                            </div>
                        </div>
                        <div class="c_btm"></div>
                    </div>
                </div>
            </div>

            <script src="static/js/install.js" type="text/javascript"></script>
            <div style="display:none;">
                <script language="JavaScript" type="text/javascript" charset="utf-8" src="static/js/install.js"></script>
            </div>
</body>

</html>
