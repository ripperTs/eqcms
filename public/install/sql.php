<?php

$sql = array(

  "CREATE TABLE IF NOT EXISTS `eq_ad` (
  `id` int(11) NOT NULL,
  `ad_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `ad_time` int(11) unsigned zerofill NOT NULL DEFAULT '00000000001',
  `ad_img` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `ad_depict` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `ad_join` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `ad_state` int(255) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;",

  "INSERT INTO `eq_ad` (`id`, `ad_name`, `ad_time`, `ad_img`, `ad_depict`, `ad_join`, `ad_state`) VALUES",
  "CREATE TABLE IF NOT EXISTS `eq_admin` (
  `id` int(10) unsigned NOT NULL,
  `user` varchar(16) NOT NULL,
  `passwd` varchar(255) NOT NULL,
  `status` int(255) NOT NULL DEFAULT '1',
  `logintime` int(11) DEFAULT NULL,
  `loginip` varchar(255) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;",

  "INSERT INTO `eq_admin` (`id`, `user`, `passwd`, `status`, `logintime`, `loginip`) VALUES
(1, 'admin', '\$2y\$10\$Ehh8b.HQkqUT1g821Qm0Ou5d.ra4LIUF8TTxNwVD6ypkzvInMVBc2', 1, NULL, '12345678');",

  "CREATE TABLE IF NOT EXISTS `eq_admin-power` (
  `uid` int(10) unsigned NOT NULL,
  `pid` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;",

  "INSERT INTO `eq_admin-power` (`uid`, `pid`) VALUES
(4, 1),
(4, 8);",

  "CREATE TABLE IF NOT EXISTS `eq_banner` (
  `id` int(10) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  `b_url` varchar(255) NOT NULL,
  `ishidden` int(11) NOT NULL DEFAULT '1',
  `time` int(11) NOT NULL,
  `sorting` int(11) NOT NULL DEFAULT '1',
  `img` varchar(255) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;",

  "INSERT INTO `eq_banner` (`id`, `title`, `b_url`, `ishidden`, `time`, `sorting`, `img`) VALUES",

  "CREATE TABLE IF NOT EXISTS `eq_blacklist` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `time` int(11) NOT NULL,
  `reason` varchar(255) NOT NULL,
  `operation` varchar(255) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;",

  "INSERT INTO `eq_blacklist` (`id`, `uid`, `time`, `reason`, `operation`) VALUES",

  "CREATE TABLE IF NOT EXISTS `eq_collection` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `d_name` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `time` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;",

  "INSERT INTO `eq_collection` (`id`, `uid`, `d_name`, `url`, `time`) VALUES",

  "CREATE TABLE IF NOT EXISTS `eq_comments` (
  `id` int(11) unsigned NOT NULL,
  `did` int(11) unsigned NOT NULL,
  `uid` int(11) unsigned NOT NULL,
  `content` varchar(255) NOT NULL,
  `time` int(11) unsigned NOT NULL,
  `praise` int(11) unsigned NOT NULL DEFAULT '0',
  `tread` int(11) unsigned NOT NULL DEFAULT '0',
  `cid` int(11) unsigned NOT NULL DEFAULT '0',
  `cname` varchar(255) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;",

  "INSERT INTO `eq_comments` (`id`, `did`, `uid`, `content`, `time`, `praise`, `tread`, `cid`, `cname`) VALUES",

  "CREATE TABLE IF NOT EXISTS `eq_data` (
  `id` int(10) unsigned NOT NULL,
  `d_name` varchar(255) NOT NULL,
  `t_id` int(11) NOT NULL,
  `typename` varchar(255) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `lasttime` varchar(255) DEFAULT NULL,
  `time` int(11) NOT NULL,
  `ishidden` int(255) NOT NULL DEFAULT '1',
  `passwd` varchar(255) DEFAULT NULL,
  `level` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;",

  "CREATE TABLE IF NOT EXISTS `eq_details` (
  `id` int(255) unsigned NOT NULL,
  `d_id` int(11) NOT NULL,
  `vod_name` varchar(255) NOT NULL,
  `img` varchar(255) NOT NULL,
  `director` mediumtext,
  `strring` varchar(600) DEFAULT NULL,
  `num` int(11) DEFAULT '0',
  `score` int(255) DEFAULT NULL,
  `region` varchar(255) DEFAULT NULL,
  `language` varchar(255) DEFAULT NULL,
  `year` varchar(255) NOT NULL,
  `playly` varchar(255) DEFAULT NULL,
  `plot` mediumtext,
  `time` varchar(255) DEFAULT NULL,
  `snum` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;",

  "CREATE TABLE IF NOT EXISTS `eq_email` (
  `id` int(10) unsigned NOT NULL,
  `address` varchar(255) NOT NULL,
  `port` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `nickname` varchar(255) NOT NULL,
  `account` varchar(255) NOT NULL,
  `passwd` varchar(255) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;",

  "INSERT INTO `eq_email` (`id`, `address`, `port`, `email`, `nickname`, `account`, `passwd`) VALUES",

  "CREATE TABLE IF NOT EXISTS `eq_error` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `d_name` varchar(255) NOT NULL,
  `d_id` int(11) NOT NULL,
  `content` varchar(255) NOT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `audit` int(255) NOT NULL DEFAULT '2',
  `replay` int(255) NOT NULL DEFAULT '0',
  `time` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;",

  "INSERT INTO `eq_error` (`id`, `email`, `d_name`, `d_id`, `content`, `ip`, `audit`, `replay`, `time`) VALUES",

  "CREATE TABLE IF NOT EXISTS `eq_gconfig` (
  `id` int(10) unsigned NOT NULL,
  `websites` varchar(255) NOT NULL,
  `domain` varchar(255) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `keyword` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `eqimg` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `statistical` varchar(255) DEFAULT NULL,
  `icp` varchar(255) DEFAULT NULL,
  `copyright` varchar(255) DEFAULT NULL,
  `dondition` int(11) NOT NULL DEFAULT '2',
  `isimg` int(11) NOT NULL DEFAULT '1',
  `iscontent` int(11) NOT NULL DEFAULT '1',
  `isterval` int(11) NOT NULL DEFAULT '3',
  `regintegral` int(11) NOT NULL DEFAULT '0',
  `qdintegral` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;",

  "INSERT INTO `eq_gconfig` (`id`, `websites`, `domain`, `logo`, `title`, `keyword`, `description`, `eqimg`, `email`, `statistical`, `icp`, `copyright`, `dondition`, `isimg`, `iscontent`, `isterval`, `regintegral`, `qdintegral`) VALUES",

  "CREATE TABLE IF NOT EXISTS `eq_gonggao` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_bin NOT NULL,
  `content` varchar(255) COLLATE utf8_bin NOT NULL,
  `s_time` int(11) NOT NULL,
  `e_time` int(11) NOT NULL,
  `status` enum('0','1') COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;",

  "INSERT INTO `eq_gonggao` (`id`, `title`, `content`, `s_time`, `e_time`, `status`) VALUES",

  "CREATE TABLE IF NOT EXISTS `eq_history` (
  `id` int(11) NOT NULL,
  `d_name` varchar(255) NOT NULL,
  `remarks` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `time` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;",

  "CREATE TABLE IF NOT EXISTS `eq_invited` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `u_name` varchar(255) NOT NULL,
  `u_user` varchar(255) NOT NULL,
  `time` int(11) NOT NULL,
  `u_uid` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;",

  "CREATE TABLE IF NOT EXISTS `eq_leave` (
  `id` int(10) unsigned NOT NULL,
  `email` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `content` varchar(255) NOT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `audit` int(11) NOT NULL,
  `replay` int(11) NOT NULL DEFAULT '0',
  `time` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;",

  "INSERT INTO `eq_leave` (`id`, `email`, `name`, `title`, `content`, `ip`, `audit`, `replay`, `time`) VALUES",

  "CREATE TABLE IF NOT EXISTS `eq_link` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `img` varchar(255) DEFAULT NULL,
  `url` varchar(255) NOT NULL,
  `describe` varchar(255) DEFAULT NULL,
  `ishidden` int(255) NOT NULL DEFAULT '1',
  `time` int(11) NOT NULL,
  `audit` int(11) NOT NULL DEFAULT '0',
  `starttime` int(11) DEFAULT NULL,
  `endtime` int(11) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;",

  "INSERT INTO `eq_link` (`id`, `name`, `img`, `url`, `describe`, `ishidden`, `time`, `audit`, `starttime`, `endtime`) VALUES",

  "CREATE TABLE IF NOT EXISTS `eq_menu` (
  `id` int(10) unsigned NOT NULL,
  `m_name` varchar(255) NOT NULL,
  `en_name` varchar(255) NOT NULL,
  `ishidden` int(11) NOT NULL DEFAULT '1',
  `pid` int(11) NOT NULL DEFAULT '0',
  `path` varchar(255) DEFAULT NULL,
  `ico` varchar(255) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=64 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;",

  "INSERT INTO `eq_menu` (`id`, `m_name`, `en_name`, `ishidden`, `pid`, `path`, `ico`) VALUES
(10, '分类管理', 'type', 1, 1, '0,1,', ''),
(1, '数据管理', 'data', 1, 0, '0,', '&#xe6cb;'),
(2, '模板管理', 'template', 1, 0, '0,', '&#xe696;'),
(3, '会员管理', 'user', 1, 0, '0,', '&#xe6b8;'),
(4, '系统配置', 'config', 1, 0, '0,', '&#xe6ae;'),
(5, '评论留言管理', 'message', 1, 0, '0,', '&#xe6ba;'),
(6, '管理员管理', 'admindata', 1, 0, '0,', '&#xe6f5;'),
(7, '菜单管理', 'menu', 1, 0, '0,', '&#xe75c;'),
(8, '采集管理', 'caiji', 1, 0, '0,', '&#xe828;'),
(9, '系统工具', 'tool', 1, 0, '0,', '&#xe6f6;'),
(11, '数据列表', 'datalist', 1, 1, '0,1,', NULL),
(12, '添加数据', 'datalist/create', 1, 1, '0,1,', NULL),
(13, '隐藏数据', 'hiddendata', 1, 1, '0,1,', NULL),
(14, '有密码数据', 'pwddata', 1, 1, '0,1,', NULL),
(15, '幻灯片管理', 'banner', 1, 1, '0,1,', NULL),
(16, '模版列表', 'temlist', 1, 2, '0,2,', NULL),
(17, '会员列表', 'userlist', 1, 3, '0,3,', NULL),
(18, '充值记录', 'recharge', 1, 3, '0,3,', NULL),
(19, '收藏管理', 'collection', 1, 3, '0,3,', NULL),
(20, '封禁列表', 'backlist', 1, 3, '0,3,', NULL),
(21, '基本配置', 'jbconfig', 1, 4, '0,4,', NULL),
(22, '安全配置', 'security', 1, 4, '0,4,', NULL),
(23, '接口配置', 'apiconfig', 1, 4, '0,4,', NULL),
(24, '视频播放接口配置', 'playapi', 1, 23, '0,4,23,', NULL),
(25, '手机验证接口配置', 'phoneapi', 1, 23, '0,4,23,', NULL),
(26, '支付宝接口配置', 'payapi', 1, 23, '0,4,23,', NULL),
(27, '微信公众号配置', 'wxconfig', 1, 4, '0,4,', NULL),
(29, '邮件服务器配置', 'mail', 1, 4, '0,4,', NULL),
(30, '友情链接', 'links', 1, 4, '0,4,', NULL),
(31, '系统公告', 'gonggao', 1, 0, '0,', '&#xe713;'),
(32, '广告', 'ad', 1, 4, '0,4,', NULL),
(33, '留言管理', 'leave', 1, 5, '0,5,', NULL),
(34, '评论管理', 'comments', 1, 5, '0,5,', NULL),
(35, '投票管理', 'vote', 0, 5, '0,5,', NULL),
(36, '管理员列表', 'adminuser', 1, 6, '0,6,', NULL),
(37, '登陆日志', 'loginlog', 1, 6, '0,6,', NULL),
(38, '地图', 'menulist', 1, 7, '0,7,', NULL),
(39, '禁用管理', 'permissions', 1, 7, '0,7,', NULL),
(40, '资源库列表', 'reslist', 1, 8, '0,8,', NULL),
(41, '资源库管理', 'resmanage', 1, 8, '0,8,', NULL),
(42, '数据批量替换', 'datareplate', 1, 9, '0,9,', NULL),
(43, '删除指定数据', 'deldata', 1, 9, '0,9,', NULL),
(44, '文件权限检查', 'checkperm', 1, 9, '0,9,', NULL),
(45, '公告列表', 'gglist', 1, 31, '0,31,', NULL),
(51, '权限分类', 'power-type', 1, 6, '0,6,', NULL),
(50, '角色管理', 'power', 1, 6, '0,6,', NULL),
(48, '数据完整性检测', 'checkdata', 1, 9, '0,9,', NULL),
(49, '播放器来源', 'lyname', 1, 8, '0,8,', NULL),
(52, '权限管理', 'power-manage', 1, 6, '0,6,', NULL),
(60, '报错数据', 'error', 1, 1, '1,0,', NULL),
(65, '图片批量本地化', 'imgBatchLocalization', 1, 9, '0,9,', NULL),
(64, 'SQL助手', 'sql', 1, 9, '0,9,', NULL);",

  "CREATE TABLE IF NOT EXISTS `eq_paddress` (
  `id` int(10) unsigned NOT NULL,
  `r_id` int(11) NOT NULL,
  `psorce` varchar(255) NOT NULL,
  `playurl` mediumtext NOT NULL,
  `ishidden` int(255) NOT NULL DEFAULT '1',
  `time` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;",

  "CREATE TABLE IF NOT EXISTS `eq_pay` (
  `id` int(10) unsigned NOT NULL,
  `partner` varchar(255) NOT NULL,
  `key` varchar(255) NOT NULL,
  `apiurl` varchar(255) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;",

  "INSERT INTO `eq_pay` (`id`, `partner`, `key`,`apiurl`) VALUES
(1, '213456782131', '121132','eqcms.top');",

  "CREATE TABLE IF NOT EXISTS `eq_phoneapi` (
  `id` int(10) unsigned NOT NULL,
  `templateid` varchar(255) NOT NULL,
  `accountid` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `appid` varchar(255) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;",

  "INSERT INTO `eq_phoneapi` (`id`, `templateid`, `accountid`, `token`, `appid`) VALUES
(1, '515910', '8b9f0977eb204e0eab1bd865dc959caa', '6c7da969ca96984243b76937b47a8rd9', '1a783c0b509c445c859e8db286ffe883');",

  "CREATE TABLE IF NOT EXISTS `eq_playapi` (
  `id` int(10) unsigned NOT NULL,
  `p_name` varchar(18) NOT NULL,
  `p_url` varchar(255) NOT NULL,
  `addtime` int(11) NOT NULL,
  `sorting` int(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;",

  "CREATE TABLE IF NOT EXISTS `eq_power` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `describe` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;",

  "INSERT INTO `eq_power` (`id`, `name`, `describe`, `status`) VALUES
(1, '超级管理员', '至高无上的权利', 1),
(7, '公告管理员', '公告的增删改查', 1),
(8, '普通管理员', NULL, 0);",

  "CREATE TABLE IF NOT EXISTS `eq_power-menu` (
  `pid` int(10) unsigned NOT NULL,
  `mid` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;",

  "CREATE TABLE IF NOT EXISTS `eq_power-type` (
  `pid` int(10) unsigned NOT NULL,
  `tid` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;",

  "INSERT INTO `eq_power-type` (`pid`, `tid`) VALUES
(1, 2),
(1, 4),
(2, 10),
(7, 12),
(8, 2),
(8, 8),
(8, 14);",

  "CREATE TABLE IF NOT EXISTS `eq_powermanage` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `rule` varchar(255) CHARACTER SET utf8 NOT NULL,
  `tid` int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;",

  "INSERT INTO `eq_powermanage` (`id`, `name`, `rule`, `tid`) VALUES
(5, '用户列表页', 'UserController@index', 10),
(6, '添加页面', 'GonggaoController@store', 12),
(7, '添加的处理页面', 'GonggaoController@doadd', 12),
(8, '管理员列表', 'AdminController@index', 14);",

  "CREATE TABLE IF NOT EXISTS `eq_powertype` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `pid` int(10) unsigned NOT NULL DEFAULT '0',
  `path` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;",

  "INSERT INTO `eq_powertype` (`id`, `name`, `pid`, `path`) VALUES
(9, '用户相关', 0, '0,'),
(10, '用户列表', 9, '0,9,'),
(11, '公告', 0, '0,'),
(12, '公告的增加', 11, '0,11,'),
(13, '管理员管理', 0, '0,'),
(14, '管理员列表', 13, '0,13,');",

  "CREATE TABLE IF NOT EXISTS `eq_recharge` (
  `id` int(10) unsigned NOT NULL,
  `code` char(16) CHARACTER SET utf8 NOT NULL,
  `uid` int(10) unsigned NOT NULL,
  `money` varchar(50) CHARACTER SET utf8 NOT NULL,
  `integral` varchar(50) CHARACTER SET utf8 NOT NULL,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `hide` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `time` char(10) CHARACTER SET utf8 NOT NULL,
  `mtime` char(10) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;",

  "INSERT INTO `eq_recharge` (`id`, `code`, `uid`, `money`, `integral`, `status`, `hide`, `time`, `mtime`) VALUES
(1, '1234567898765432', 6, '200', '2000', 1, 1, '8529637415', '1568423687');",

  "CREATE TABLE IF NOT EXISTS `eq_reperror` (
  `id` int(11) NOT NULL,
  `e_id` int(11) NOT NULL,
  `user` varchar(255) NOT NULL,
  `rcontent` varchar(255) NOT NULL,
  `rtime` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;",

  "INSERT INTO `eq_reperror` (`id`, `e_id`, `user`, `rcontent`, `rtime`) VALUES",

  "CREATE TABLE IF NOT EXISTS `eq_repleave` (
  `id` int(11) NOT NULL,
  `l_id` int(11) NOT NULL,
  `user` varchar(255) NOT NULL,
  `rcontent` varchar(255) NOT NULL,
  `rtime` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;",

  "INSERT INTO `eq_repleave` (`id`, `l_id`, `user`, `rcontent`, `rtime`) VALUES",

  "CREATE TABLE IF NOT EXISTS `eq_sign` (
  `id` int(11) NOT NULL,
  `u_id` int(11) NOT NULL,
  `num` int(11) NOT NULL DEFAULT '0',
  `time` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;",

  "CREATE TABLE IF NOT EXISTS `eq_type` (
  `id` int(10) unsigned NOT NULL,
  `p_id` int(11) NOT NULL DEFAULT '0',
  `t_name` varchar(20) NOT NULL,
  `tenname` varchar(255) NOT NULL,
  `ishidden` int(10) NOT NULL DEFAULT '1',
  `title` varchar(255) DEFAULT NULL,
  `keyword` varchar(255) DEFAULT NULL,
  `time` int(11) NOT NULL,
  `path` varchar(255) DEFAULT NULL,
  `authority` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=38 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;",

  "INSERT INTO `eq_type` (`id`, `p_id`, `t_name`, `tenname`, `ishidden`, `title`, `keyword`, `time`, `path`,`authority`) VALUES
(1, 0, '电影', 'dy', 1, '电影', '电影', 1576254153, '0,','0'),
(2, 0, '电视剧', 'tv', 1, '电视剧', '电视剧', 1575857386, '0,','0'),
(3, 0, '综艺', 'zy', 1, '综艺', '综艺', 1575857386, '0,','0'),
(4, 0, '动漫', 'dm', 1, '动漫', '动漫', 1575857386, '0,','0'),
(5, 1, '动作片', 'dongzuo', 1, '动作片', '动作片', 1575857386, '0,1,','0'),
(6, 1, '爱情片', 'aiqing', 1, '爱情片', '爱情片', 1575857386, '0,1,','0'),
(7, 1, '科幻片', 'kehuan', 1, '科幻片', '科幻片', 1575857386, '0,1,','0'),
(8, 1, '恐怖片', 'kongpu', 1, '恐怖片', '恐怖片', 1575857386, '0,1,','0'),
(9, 1, '喜剧片', 'xiju', 1, '喜剧片', '喜剧片', 1575857386, '0,1,','0'),
(10, 1, '记录片', 'jilu', 1, '纪录片 ', '纪录片', 1575857386, '0,1,','0'),
(11, 1, '剧情片', 'juqing', 1, '剧情片', '剧情片', 1575857386, '0,1,','0'),
(12, 2, '国产剧', 'dalu', 1, '国产剧', '国产剧', 1575857386, '0,2,','0'),
(13, 2, '港台剧', 'tangtai', 1, '港台剧', '港台剧', 1575857386, '0,2,','0'),
(14, 2, '欧美剧', 'oumei', 1, '欧美剧', '欧美剧', 1575857386, '0,2,','0'),
(15, 2, '日韩剧', 'rihan', 1, '日韩剧', '日韩剧', 1575857386, '0,2,','0'),
(16, 3, '国内综艺', 'gnzy', 1, '国内综艺', '国内综艺', 1575857386, '0,3,','0'),
(17, 3, '台湾综艺', 'twzy', 1, '台湾综艺', '台湾综艺', 1575857386, '0,3,','0'),
(18, 3, '国外综艺', 'gwzy', 1, '国外综艺', '国外综艺', 1575857386, '0,3,','0'),
(19, 4, '国产动漫', 'gcdm', 1, '国产动漫', '国产动漫', 1575857386, '0,4,','0'),
(20, 4, '日本动漫 ', 'rbdm', 1, '日本动漫', '日本动漫', 1575857386, '0,4,','0'),
(21, 4, '欧美动漫', 'omdm', 1, '欧美动漫', '欧美动漫', 1575857386, '0,4,','0');",

  "CREATE TABLE IF NOT EXISTS `eq_union` (
  `id` int(10) unsigned NOT NULL,
  `u_name` varchar(20) NOT NULL,
  `u_api` varchar(100) NOT NULL,
  `ruletype` varchar(255) DEFAULT NULL,
  `notype` varchar(255) DEFAULT NULL,
  `time` int(11) NOT NULL,
  `describe` varchar(255) DEFAULT NULL,
  `history` varchar(255) DEFAULT NULL,
  `apitype` int(255) NOT NULL DEFAULT '1'
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;",

  "INSERT INTO `eq_union` (`id`, `u_name`, `u_api`, `ruletype`, `notype`, `time`, `describe`, `history`,`apitype`) VALUES
(1, 'OK资源', 'https://api.okzy.tv/api.php/provide/vod/', '1_1,2_2,3_3,4_4,7_9,8_6,9_7,10_8,11_11,12_5,13_12,14_13,15_15,16_14,20_10,22_13,23_15,25_16,26_17,27_18,28_18,29_19,30_20,31_21,32_19,5_3', '5', 1575981005, '默认资源库', '/admin/caiji-action/6/1/day','2'),
(2, '卧龙资源', 'http://cj.wlzy.tv/api/macs/vod/', '1_1,2_2,3_3,4_4,5_5,6_9,7_6,8_7,9_8,10_11,11_5,12_12,13_13,14_15,15_14,16_13,17_15,19_10,23_16,24_17,25_18,26_18,27_19,28_20,29_21,30_19,31_21', '', 1575857386, NULL, '','2'),
(8, '158资源库', 'http://cj.158zyz.com:158/inc/apijson_vod.php', '', NULL, 1576118808, '暂无描述', NULL,'2'),
(9, '极速资源', 'https://www.caijizy.vip/api.php/provide/vod/at/json/', NULL, NULL, 1576000298, '暂无描述', NULL,'2');",

  "CREATE TABLE IF NOT EXISTS `eq_user` (
  `level` int(255) NOT NULL DEFAULT '1',
  `integral` int(255) NOT NULL DEFAULT '0',
  `id` int(10) unsigned NOT NULL,
  `user` varchar(255) NOT NULL,
  `passwd` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `num` int(11) DEFAULT '0',
  `time` int(11) NOT NULL,
  `pic` varchar(255) DEFAULT NULL,
  `token` char(32) DEFAULT NULL,
  `activation` int(255) DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=80 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;",

  "INSERT INTO `eq_user` (`level`, `integral`, `id`, `user`, `passwd`, `email`, `name`, `phone`, `status`, `num`, `time`, `pic`, `token`, `activation`) VALUES",

  "CREATE TABLE IF NOT EXISTS `eq_wechat` (
  `id` int(10) unsigned NOT NULL,
  `domain` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `focusmess` varchar(255) DEFAULT NULL,
  `num` int(11) NOT NULL DEFAULT '15',
  `name` varchar(255) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;",

  "INSERT INTO `eq_wechat` (`id`, `domain`, `token`, `focusmess`, `num`, `name`) VALUES",
  "INSERT INTO `eq_wechat` VALUES (1, 'eqcms.top', 'eqcms', '欢迎关注本站微信公众号,10W+电影任你搜!', 15, 'EQCMS内容管理系统');",

  "ALTER TABLE `eq_ad`
  ADD PRIMARY KEY (`id`);",

  "ALTER TABLE `eq_admin`
  ADD PRIMARY KEY (`id`) USING BTREE;",

  "ALTER TABLE `eq_admin-power`
  ADD PRIMARY KEY (`uid`,`pid`);",

  "ALTER TABLE `eq_banner`
  ADD PRIMARY KEY (`id`) USING BTREE;",

  "ALTER TABLE `eq_blacklist`
  ADD PRIMARY KEY (`id`) USING BTREE;",

  "ALTER TABLE `eq_collection`
  ADD PRIMARY KEY (`id`) USING BTREE;",

  "ALTER TABLE `eq_comments`
  ADD PRIMARY KEY (`id`) USING BTREE;",

  "ALTER TABLE `eq_data`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `name` (`d_name`);",

  "ALTER TABLE `eq_details`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `name` (`vod_name`);",

  "ALTER TABLE `eq_email`
  ADD PRIMARY KEY (`id`) USING BTREE;",

  "ALTER TABLE `eq_error`
  ADD PRIMARY KEY (`id`);",

  "ALTER TABLE `eq_gconfig`
  ADD PRIMARY KEY (`id`) USING BTREE;",

  "ALTER TABLE `eq_gonggao`
  ADD PRIMARY KEY (`id`);",

  "ALTER TABLE `eq_history`
  ADD PRIMARY KEY (`id`) USING BTREE;",

  "ALTER TABLE `eq_invited`
  ADD PRIMARY KEY (`id`) USING BTREE;",

  "ALTER TABLE `eq_leave`
  ADD PRIMARY KEY (`id`) USING BTREE;",

  "ALTER TABLE `eq_link`
  ADD PRIMARY KEY (`id`) USING BTREE;",

  "ALTER TABLE `eq_menu`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `en_name` (`en_name`) USING BTREE;",

  "ALTER TABLE `eq_paddress`
  ADD PRIMARY KEY (`id`) USING BTREE;",

  "ALTER TABLE `eq_pay`
  ADD PRIMARY KEY (`id`) USING BTREE;",

  "ALTER TABLE `eq_phoneapi`
  ADD PRIMARY KEY (`id`) USING BTREE;",

  "ALTER TABLE `eq_playapi`
  ADD PRIMARY KEY (`id`) USING BTREE;",

  "ALTER TABLE `eq_power`
  ADD PRIMARY KEY (`id`);",

  "ALTER TABLE `eq_power-menu`
  ADD PRIMARY KEY (`pid`,`mid`);",

  "ALTER TABLE `eq_power-type`
  ADD PRIMARY KEY (`pid`,`tid`);",

  "ALTER TABLE `eq_powermanage`
  ADD PRIMARY KEY (`id`);",

  "ALTER TABLE `eq_powertype`
  ADD PRIMARY KEY (`id`);",

  "ALTER TABLE `eq_recharge`
  ADD PRIMARY KEY (`id`);",

  "ALTER TABLE `eq_reperror`
  ADD PRIMARY KEY (`id`);",

  "ALTER TABLE `eq_repleave`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `lid` (`l_id`);",

  "ALTER TABLE `eq_sign`
  ADD PRIMARY KEY (`id`) USING BTREE;",

  "ALTER TABLE `eq_type`
  ADD PRIMARY KEY (`id`) USING BTREE;",

  "ALTER TABLE `eq_union`
    ADD PRIMARY KEY (`id`) USING BTREE;",

  "ALTER TABLE `eq_user`
  ADD PRIMARY KEY (`id`) USING BTREE;",

  "ALTER TABLE `eq_wechat`
  ADD PRIMARY KEY (`id`) USING BTREE;",

  "ALTER TABLE `eq_ad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=72;",

  "ALTER TABLE `eq_admin`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;",

  "ALTER TABLE `eq_banner`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;",

  "ALTER TABLE `eq_blacklist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;",

  "ALTER TABLE `eq_collection`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;",

  "ALTER TABLE `eq_comments`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;",

  "ALTER TABLE `eq_data`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;",

  "ALTER TABLE `eq_details`
  MODIFY `id` int(255) unsigned NOT NULL AUTO_INCREMENT;",

  "ALTER TABLE `eq_email`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;",

  "ALTER TABLE `eq_error`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;",

  "ALTER TABLE `eq_gconfig`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;",

  "ALTER TABLE `eq_gonggao`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;",

  "ALTER TABLE `eq_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;",

  "ALTER TABLE `eq_invited`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;",

  "ALTER TABLE `eq_leave`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;",

  "ALTER TABLE `eq_link`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;",

  "ALTER TABLE `eq_menu`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=64;",

  "ALTER TABLE `eq_paddress`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;",

  "ALTER TABLE `eq_pay`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;",

  "ALTER TABLE `eq_phoneapi`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;",

  "ALTER TABLE `eq_playapi`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;",

  "ALTER TABLE `eq_power`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;",

  "ALTER TABLE `eq_powermanage`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;",

  "ALTER TABLE `eq_powertype`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;",

  "ALTER TABLE `eq_recharge`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;",

  "ALTER TABLE `eq_reperror`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;",

  "ALTER TABLE `eq_repleave`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;",

  "ALTER TABLE `eq_sign`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;",

  "ALTER TABLE `eq_type`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=38;",

  "ALTER TABLE `eq_union`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;",

  "ALTER TABLE `eq_user`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=80;",

  "ALTER TABLE `eq_wechat`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;",



);
