<?php
if (file_exists('install.lock')) {
	header("location:index6.php");
	die;
}

?>
<html>

<head>
	<title>安装向导 - EQCMS</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<link href="static/css/style.css" rel="stylesheet" type="text/css" />
	<script src="static/js/jquery.js" type="text/javascript"></script>
</head>

<body scroll="no">
	<div class="b">
		<div class="main">
			<div class="head">
				<div class="h_right"><a href="//www.eqcms.top" target="_blank">官方网站</a></div>
				<img src="../upload/logo.png" width="134" height="40" style="margin-top:5px;" />
			</div>
			<div class="cont">
				<div class="c_top"></div>
				<div class="c_c">
					<div class="c_c_left">
						<ul>
							<li class="">1、欢迎界面</li>
							<li class="">2、阅读协议</li>
							<li class="">3、环境检测</li>
							<li class="on">4、参数配置</li>
							<li class="">5、安装完成</li>
						</ul>
					</div>
					<div class="c_c_right">
						<form action="index5.php" method="post" name="form1">
							<input type="hidden" name="step" value="4" />
							<div class="content">
								<h2>填写数据库配置和网站配置信息</h2>
								<table class="tb">
									<tr>
										<th colspan="2">数据库设置</th>
									</tr>

									<tr>
										<td width="120">数据库地址：</td>
										<td><input class="inp" name="DB_HOST" id="dbhost" type="text" value="127.0.0.1" tips="通常为localhost或127.0.0.1" /></td>
									</tr>
									<tr>
										<td width="120">数据库名称：</td>
										<td>
											<input class="inp" name="DB_DATABASE" id="DB_DATABASE" type="text" value="eqcms" tips="数据库名，如数据库已存在，将会被清空" />
											<input type="hidden" name="check" value="1">
										</td>
									</tr>
									<tr>
										<td width="120">数据库用户：</td>
										<td><input class="inp" name="DB_USERNAME" id="DB_USERNAME" type="text" value="root" tips="数据库用户，强烈建议使用非root用户" /></td>
									</tr>
									<tr>
										<td width="120">数据库密码：</td>
										<td>
											<div style='float:left;margin-right:3px;'><input class="inp" name="DB_PASSWORD" id="dbpwd" type="text" tips="数据库用户的连接密码" /><input class="inp" name="dbprefix" type="hidden" value="sea_" /></div>
											<div style='float:left' id='dbpwdsta'></div>
										</td>
									</tr>





									<tr>
										<td width="120">数据库编码：</td>
										<td>
											<input type="radio" name="dblang" id="dblang_utf8" value="utf8" checked="checked" /><label for="dblang_utf8">UTF8</label>
											<!--<input class="inp" type="radio" name="dblang" id="dblang_latin1" value="latin1" /><label for="dblang_latin1">LATIN1</label>
						<small>仅对4.1+以上版本的MySql选择</small>-->
										</td>
									</tr>
								</table>

								<table class="tb">
									<tr>
										<th colspan="2">默认管理员(安装成功后可更改密码)</th>
									</tr>

									<tr>
										<td width="120">用户名：</td>
										<td>
											<input id="adm_user" class="inp" readonly name="adminuser" type="text" value="admin" tips="网站管理员用户名，只能输入使用英文和数字" />
										</td>
									</tr>
									<tr>
										<td width="120">密　码：</td>
										<td><input id="adm_pass" class="inp" readonly name="adminpwd" type="text" value="admin" tips="后台管理员密码" /></td>
									</tr>
								</table>


								<table class="tb">
									<tr>
										<th colspan="2">网站设置</th>
									</tr>

									<tr>
										<td width="120">网站名称：</td>
										<td>
											<input class="inp" name="S_NAME" type="text" value="EQCMS内容管理系统" tips="请填写您的网站名称" />
										</td>
									</tr>
									<tr>
										<td width="120"> 网站网址：</td>
										<td><input class="inp" name="S_REALM" type="text" value="<?php $baseurl = $_SERVER['HTTP_HOST'];
																									echo $baseurl; ?>" tips="网站地址，如：http://www.seacms.net" /></td>
									</tr>
								</table>

							</div>
							<div class="button"><a id="xyb" href="javascript:void(0)" onclick="DoInstall()">下一步</a> <a id="syb" href="index.php?step=2">上一步</a></div>
					</div>
				</div>
				<div class="c_btm"></div>
			</div>
		</div>
	</div>
	</form>
	<script src="static/js/install.js" type="text/javascript"></script>
	<div style="display:none;">
		<script language="JavaScript" type="text/javascript" charset="utf-8" src="static/js/install.js"></script>
	</div>
</body>
<script language="javascript" type="text/javascript">
	function $o(tid) {
		return document.getElementById(tid);
	}

	function DoInstall() {
		var install = document.getElementById("xyb");
		var back = document.getElementById("syb");
		document.form1.submit();
		install.style.pointerEvents="none";
		install.style.background="#aaa";
		install.style.borderRadius="5px";
		install.innerHTML="请稍等...";
		back.style.pointerEvents="none";
		back.style.background="#aaa";
		back.style.borderRadius="5px";
	}

	function integuc() {
		$o('ucurl').style.display = '';
		$o('ucip').style.display = '';
		$o('ucpwd').style.display = '';
	}

	function nointeguc() {
		$o('ucurl').style.display = 'none';
		$o('ucip').style.display = 'none';
		$o('ucpwd').style.display = 'none';

	}
</script>

</html>
