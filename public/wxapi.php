<?php
ob_clean(); //清除缓存
$file = "../.env"; //.env文件位置
$str = file_get_contents($file); //读取文件内容
$prg = "/DB_HOST=(.*?)DB_PORT=(.*?)DB_DATABASE=(.*?)DB_USERNAME=(.*?)DB_PASSWORD=(.*?)\n/s";
preg_match_all($prg, $str, $arr);
define("HOST", rtrim($arr[1][0], "\n\r"));
define("USER", rtrim($arr[4][0], "\n\r"));
define("PWD", rtrim($arr[5][0], "\n\r"));
define("DBNAME", rtrim($arr[3][0], "\n\r"));

$wechatObj = new wechatCallbackapiTest();
$wechatObj->valid();

class wechatCallbackapiTest
{
    public $domain = null;
    public $name = null;
    public $token = null;
    public $focusmess = null;
    public $num = null;

    public function __construct()
    {
        $dsn = 'mysql:host=' . HOST . ';dbname=' . DBNAME;
        $pdo = new PDO($dsn, USER, PWD);
        $sql = "SELECT * FROM eq_wechat";
        $res = $pdo->query($sql)->fetchAll(2)[0]; //取出微信配置表信息
        $this->domain = $res['domain']; //域名
        $this->token = $res['token']; //秘钥
        $this->focusmess = $res['focusmess']; //关注后回复信息
        $this->num = $res['num']; //消息数量
    }

    public function valid()
    {
        $echoStr = $_GET["echostr"];
        if ($echoStr == "") {
            $this->responseMsg();
        } elseif ($this->checkSignature()) {
            echo $echoStr;
            exit;
        }
    }


    public function responseMsg()
    {
        $postStr = addslashes(file_get_contents('php://input'));
        if (!empty($postStr)) {
            libxml_disable_entity_loader(true);
            $postObj = simplexml_load_string($postStr, 'SimpleXMLElement', LIBXML_NOCDATA);
            $fromUsername = $postObj->FromUserName;
            $toUsername = $postObj->ToUserName;
            $keyword = trim($postObj->Content); //用户搜索内容
            $event = $postObj->Event;
            $time = time();
            //加载xml模版
            $textTpl = "<xml>
							<ToUserName><![CDATA[%s]]></ToUserName>
							<FromUserName><![CDATA[%s]]></FromUserName>
							<CreateTime>%s</CreateTime>
							<MsgType><![CDATA[%s]]></MsgType>
							<Content><![CDATA[%s]]></Content>
							<FuncFlag>0</FuncFlag>
							</xml>";
            switch ($postObj->MsgType) {
                case 'event':
                    if ($event == 'subscribe') {
                        //关注后的回复
                        $contentStr = "{$this->focusmess}";
                        $msgType = 'text';
                        $textTpl = sprintf($textTpl, $fromUsername, $toUsername, $time, $msgType, $contentStr);
                        echo $textTpl;
                    }
                    break;
                case 'text':
                    if($data = $this->watchPasswd($keyword)){
                        $str = "您要观看的影片《".$data[0]['d_name']. "》的密码是:\r\n {$data[0]['passwd']}";
                        $contentStr = $str;
                        $msgType = 'text';
                        $textTpl = sprintf($textTpl, $fromUsername, $toUsername, $time, $msgType, $contentStr);
                        echo $textTpl;
                        return;
                    }
                    //问答回复
                    $data = $this->search($keyword, $this->num);
                    if (count($data) > 0) {
                        $str = "我们为您找到以下结果: \r\n";
                        //有返回结果
                        foreach ($data as $v) {
                            $url = $this->domain . "/details/" . $v['id'] . '.html';
                            $str .= "<a href='{$url}'>{$v['d_name']}</a> \r\n";
                        }
                        $contentStr = $str;
                        $msgType = 'text';
                        $textTpl = sprintf($textTpl, $fromUsername, $toUsername, $time, $msgType, $contentStr);
                        echo $textTpl;
                    } else {
                        $contentStr = '未找到关于' . $keyword . '的相关搜索结果!';
                        $msgType = 'text';
                        $textTpl = sprintf($textTpl, $fromUsername, $toUsername, $time, $msgType, $contentStr);
                        echo $textTpl;
                    }
                    break;
                default:
                    break;
            }
        } else {
            echo "微信公众号接口!";
            exit;
        }
    }

    //搜索方法
    private function search($search, $num)
    {
        //数据库连接
        $dsn = 'mysql:host=' . HOST . ';dbname=' . DBNAME;
        $pdo = new PDO($dsn, USER, PWD);
        //查询微信配置
        // $sql = "";
        $sql = "SELECT d.d_name,d.id FROM eq_data AS d,eq_details AS e WHERE d.id=e.d_id AND d.ishidden=1 AND d.d_name LIKE '%{$search}%' LIMIT 0,{$num}";
        //返回查询结果
        $res = $pdo->query($sql)->fetchAll(2);
        return $res;
    }

    //观看密码验证
    private function watchPasswd($id){
        //数据库连接
        $id = intval($id);
        $dsn = 'mysql:host=' . HOST . ';dbname=' . DBNAME;
        $pdo = new PDO($dsn, USER, PWD);
        $sql = "SELECT d_name,passwd FROM eq_data WHERE id={$id}";
        $res = $pdo->query($sql)->fetchAll(2);
        if($res[0]['passwd']!=''){
            return $res;
        }else{
            return false;
        }


    }

    private function checkSignature()
    {


        $signature = $_GET["signature"];
        $timestamp = $_GET["timestamp"];
        $nonce = $_GET["nonce"];

        $token = $this->token;
        $tmpArr = array($token, $timestamp, $nonce);
        // use SORT_STRING rule
        sort($tmpArr, SORT_STRING);
        $tmpStr = implode($tmpArr);
        $tmpStr = sha1($tmpStr);

        if ($tmpStr == $signature) {
            return true;
        } else {
            return false;
        }
    }
}
