<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<meta name="description" content="{{DEPICT}}" />
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="keywords" content="{{KEYWORD}}" />
<link rel="shortcut icon" href="/{{TEM}}images/ico.png" type="image/x-icon" />
<link rel="stylesheet" href="/{{TEM}}css/mytheme-font.css" type="text/css" />
<link rel="stylesheet" href="/{{TEM}}css/mytheme-ui.css" type="text/css" />
<link rel="stylesheet" href="/{{TEM}}css/mytheme-site.css" type="text/css" />
<link rel="stylesheet" href="/{{TEM}}css/mytheme-color.css" type="text/css" name="default" />
<link rel="stylesheet" href="/{{TEM}}css/mytheme-color1.css" type="text/css" name="skin" disabled/>
<link rel="stylesheet" href="/{{TEM}}css/mytheme-color2.css" type="text/css" name="skin" disabled/>
<link rel="stylesheet" href="/{{TEM}}css/mytheme-color3.css" type="text/css" name="skin" disabled/>
<script type="text/javascript" src="/{{TEM}}js/common.js"></script>
<script type="text/javascript" src="/{{TEM}}js/function.js"></script>
<script type="text/javascript">var sitePath='', siteUrl='http://{{S_REALM}}'</script>
<script type="text/javascript" src="/{{TEM}}js/jquery.min.js"></script>
<script type="text/javascript" src="/{{TEM}}js/layer.js"></script>
<script type="text/javascript" src="/{{TEM}}js/mytheme-site.js"></script>
<script type="text/javascript" src="/{{TEM}}js/mytheme-ui.js"></script>
<script type="text/javascript" src="/{{TEM}}js/mytheme-cms.js"></script>
<script type="text/javascript" src="/{{TEM}}js/home.js"></script>
<link rel="stylesheet" href="/css/font.css">
<link rel="stylesheet" href="/lib/layui/css/layui.css">
<script src="/lib/layui/layui.js" charset="utf-8"></script>
<script type="text/javascript" src="/js/xadmin.js"></script>
<style type="text/css">[class*=col-],.myui-panel_hd,.myui-content__list li{ padding: 10px}.btn{ border-radius: 5px;}.myui-vodlist__thumb{ border-radius:5px; padding-top:150%; background: url(/{{TEM}}images/a.gif) no-repeat;}.myui-vodlist__thumb.square{ padding-top: 100%; background: url(/{{TEM}}images/d.gif) no-repeat;}.myui-vodlist__thumb.wide{ padding-top: 60%; background: url(/{{TEM}}images/c.gif) no-repeat;}.myui-vodlist__thumb.actor{ padding-top: 140%;}.flickity-prev-next-button.previous{ left: 10px;}.flickity-prev-next-button.next{ right: 10px;}.myui-sidebar{ padding: 0 0 0 20px;}.myui-panel{ padding: 10px; margin-bottom: 20px; border-radius: 5px;}.myui-layout{ margin: -10px -10px 20px;}.myui-panel-mb{ margin-bottom: 20px;}.myui-panel-box{ padding: 10px;}.myui-panel-box.active{ margin: -10px;}.myui-player__item .fixed{ width: 500px;}.myui-vodlist__text li a{ padding: 10px 15px 10px 0;}.myui-vodlist__media li { padding: 10px 0 10px;}.myui-screen__list{ padding: 10px 10px 0;}.myui-screen__list li{ margin-bottom: 10px; margin-right: 10px;}.myui-page{ padding: 0 10px;}.myui-extra{ right: 20px; bottom: 30px;}@media (min-width: 1200px){.container{ max-width: 1920px;}.container{ padding-left: 120px;  padding-right: 120px;}.container.min{ width: 1200px; padding: 0;}}@media (max-width: 1400px){.myui-layout{ margin: 0;}}@media (max-width: 767px){body,body.active{ padding-bottom: 50px;}[class*=col-],.myui-content__list li{ padding: 5px}.flickity-prev-next-button.previous{ left: 5px;}.flickity-prev-next-button.next{ right: 5px;}.myui-panel{ padding: 0; border-radius: 0;}.myui-vodlist__text li a{ padding: 10px 15px 10px 0;}.myui-vodlist__media li { padding: 5px 0 5px;}.myui-screen__list{ padding: 10px 5px 0;}.myui-screen__list li{ margin-bottom: 5px; margin-right: 5px;}.myui-extra{ right: 20px; bottom: 80px;}.myui-page{ padding: 0 5px;}}.padding-70{padding-top:70px;height:70px;}</style>
