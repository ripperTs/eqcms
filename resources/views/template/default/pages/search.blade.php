@foreach($row as $k=>$v)
								<li class="active  clearfix">
									<div class="thumb">
										<a class="myui-vodlist__thumb img-lg-150 img-xs-100 lazyload" href="/details/{{$v->d_id}}.html" title="{{$v->d_name}}" data-original="{{$v->img}}" style="background-image: url('{{$v->img}}');">
											<span class="play hidden-xs"></span>
												<span class="pic-tag pic-tag-top" style="background-color: #5bb7fe;">{{round($v->score/$v->snum,1)}}分</span>
												<span class="pic-text text-right">HD</span>
										</a>
									</div>
									<div class="detail">
								    	<h4 class="title"><a href="/details/{{$v->id}}.html">{{$v->d_name}}</a></h4>
								    	<p><span class="text-muted">导演：</span><a href='/search?terms={{$v->director}}'>{{$v->director}} </a></p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<p><span class="text-muted">主演：</span>
											@foreach($arr[$k] as $v1)
											<a href='/search?terms={{$v1}}'>{{$v1}}</a>
											@endforeach
										&nbsp;</p>&nbsp;&nbsp;
										<p><span class="text-muted">分类：</span><a href='/list/{{$v->tid}}'>{{$v->typename}}</a><span class="split-line"></span><span class="text-muted hidden-xs">地区：</span><a href='/list/{{$v->tid}}?region={{$v->region}}'>{{$v->region}}</a><span class="split-line"></span><span class="text-muted hidden-xs">年份：</span><a href='/list/{{$v->tid}}?year={{$v->year}}'>{{$v->year}}</p>&nbsp;&nbsp;
										<p class="hidden-xs"><span class="text-muted">简介：</span>{{mb_substr($v->plot,0,60)}}....<a href="/details/{{$v->d_id}}.html">详情 ></a></p>&nbsp;&nbsp;
										<p class="margin-0">
											<a class="btn btn-sm btn-warm" href="/details/{{$v->d_id}}.html"><i class="fa fa-play"></i> 立即播放</a>
											<a class="btn btn-sm btn-default hidden-xs" href="/details/{{$v->d_id}}.html">查看详情 <i class="fa fa-angle-right"></i></a>
										</p>
									</div>
								</li>
							@endforeach
