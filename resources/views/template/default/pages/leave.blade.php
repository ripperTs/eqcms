@foreach ($res as $v)
      <div class="stui-pannel stui-pannel-bg clearfix">
       <div class="stui-pannel-box clearfix">
        <div class="col-pd clearfix">
         <ul>
          <li class="topwords"><strong>{{$v->name}}</strong><span class="text-red pull-right">第{{$v->id}}楼<span></span></span></li>
          <li class="top-line" style="margin-top: 10px; padding: 10px 0;">{{$v->content}}<br />
            @if ($v->replay==1)
                <div look="{{$v->id}}">
                <a onclick="look(this,{{$v->id}})" class="btn btn-default" href="javascript:;"  >查看管理员回复 </a>
                </div>
            @endif
            </li>
          <span class="font-12 text-muted">发表于 {{date('Y-m-d H:i:s',$v->time)}}<span></span></span>
         </ul>
        </div>
       </div>
      </div>
      @endforeach
