<div class="myui-panel-box clearfix">
					<div class="myui-panel_bd">
						<ul class="myui-vodlist clearfix">
                        @foreach ($data as $l)
<li class="col-lg-8 col-md-6 col-sm-4 col-xs-3">
								<div class="myui-vodlist__box">
			<a class="myui-vodlist__thumb lazyload" href="/details/{{$l->id}}.html" title="{{$l->d_name}}"  data-original="{{$l->img}}" style="background-image: url('{{$l->img}}');">
				<span class="play hidden-xs"></span>
			<span class="pic-tag pic-tag-top" style="background-color: #5bb7fe;">
				{{$l->score}}分
			</span>
<span class="pic-text text-right">
		</span>
	</a>
	<div class="myui-vodlist__detail">
		<h4 class="title text-overflow"><a href="/details/{{$l->id}}.html" title="{{$l->d_name}}">{{$l->d_name}}</a></h4>
		<p class="text text-overflow text-muted hidden-xs">
		{{$l->year}}/{{$l->region}}/{{$l->typename}}</p>
	</div>
</div>
</li>
@endforeach


													</ul>
					</div>
									</div>
