<!DOCTYPE html>
<html>
<head>
        <title>{{S_NAME}} - {{$typeOne->t_name}}_列表_{{$typeOne->title}}!</title>
        <!-- 引入头样式 -->
		@include("template/default/head")
		<style>
			.myui-panel{
				border: 1px solid #eee;
			}
			#box{
            position:absolute;
            top:0px;
            left:0px;
            width:100%;
            height:100%;
            z-index:99999;
            background: rgba(0, 0, 0, 0.4);
            display: none;
        }
		</style>
</head>
<body>
    <!-- 引入导航栏样式 -->
    @include("template/default/header")
    <div class="container">
		<div id="box">
			<div style="width:100%;margin-top:400px;text-align:center;">

			</div>
		</div>
        <div class="row">
        	<div class="myui-panel_hd"><div style="height:70px;"></div></div>
        	<div class="myui-panel myui-panel-bg clearfix" style="background-color: #3b3939;">
	<div class="myui-panel-box clearfix">
		<div class="myui-panel_bd">
			<div class="flickity clearfix" data-align="left" data-next="1">
                    @foreach($clickTop as $c)
						<div class="col-lg-8 col-sm-5 col-xs-3">
                            <div class="myui-vodlist__box">
                                <a href="/details/{{$c->id}}.html" class="myui-vodlist__thumb lazyload" title="{{$c->d_name}}" data-original="{{$c->img}}">
                                    <span class="play hidden-xs"></span>
                                        <span class="pic-tag pic-tag-top" style="background-color: #5bb7fe;">
                                        {{$c->score}}分
                                        </span>
                                    <span class="pic-text text-center">{{$c->d_name}}</span>
                                </a>
                            </div>
				        </div>
                @endforeach



			</div>
    	</div>
   </div>
</div>
<!-- 推荐 -->
        	<div class="myui-panel myui-panel-bg2 clearfix">
	<div class="myui-panel-box clearfix">
		<div class="myui-panel_hd">
			<div class="myui-panel__head active bottom-line clearfix">
				<a class="slideDown-btn more pull-right" href="javascript:;">收起 <i class="fa fa-angle-up"></i></a>
				<h3 class="title">
                {{$typeOne->t_name}}				</h3>
				<a class="more text-muted" href="/list/1">重置筛选</a>
			</div>
		</div>

		<div class="myui-panel_bd">
			<div class="slideDown-box">

								<ul class="myui-screen__list nav-slide clearfix" data-align="left">
					<li>
						<a class="text-muted btn">类型</a>
					</li>

						@foreach ($type as $kv=>$v)
							@if ($kv<=12)
								<li><a class="btn {{$v->id==$typeOne->id?'btn-warm':''}}" href="/list/{{$v->id}}?{{$request}}">{{$v->t_name}}</a></li>
							@endif
                        @endforeach
				    					</ul>

								<ul class="myui-screen__list nav-slide clearfix" data-align="left">
					<li>
						<a class="btn text-muted">地区</a>
					</li>
                    <li><a class="btn {{'regionAll'==$regionOne?'btn-warm':''}}" href="/list/{{$typeOne->id}}?{{$request}}&region=regionAll">全部</a></li>
					@foreach ($region as $ks=>$j)
					@if ($ks<=10)
						@if($j->region != "其它")
						<li><a class="btn {{$j->region == ' '.$regionOne?'btn-warm':''}}" href="/list/{{$typeOne->id}}?{{$request}}&region={{$j->region}}">{{$j->region}}</a></li>
						@endif
					@endif
                    @endforeach
				        				</ul>
								<ul class="myui-screen__list nav-slide clearfix" data-align="left">
					<li>
						<a class="btn text-muted">年份</a>
					</li>
                    <li><a class="btn {{'yearAll'==$yearOne?'btn-warm':''}}" href="/list/{{$typeOne->id}}?{{$request}}&year=yearAll">全部</a></li>
                    @foreach ($year as $y)
                        <li><a class="btn {{$y->year==$yearOne?'btn-warm':''}}" href="/list/{{$typeOne->id}}?{{$request}}&year={{$y->year}}">{{$y->year}}</a></li>
                        @endforeach

				</ul>
								<ul class="myui-screen__list nav-slide clearfix" data-align="left">
					<li>
						<a class="btn text-muted">语言</a>
					</li>
                    <li><a class="btn {{'langAll'==$langAllOne?'btn-warm':''}}" href="/list/{{$typeOne->id}}?{{$request}}&langAll=langAll">全部</a></li>
					@foreach ($language as $kk=>$q)
					@if ($kk <=10)
						@if($q->language != "其它")
						<li><a class="btn  {{$q->language==$langAllOne?'btn-warm':''}}" href="/list/{{$typeOne->id}}?{{$request}}&langAll={{$q->language}}">{{$q->language}}</a></li>
					@endif
					@endif

                    @endforeach


				</ul>

			</div>
			<ul class="myui-screen__list nav-slide clearfix" data-align="left">
				<li id="ajaxPages">
					<a class="btn text-muted">排序</a>
				</li>
						<li> <a class="btn {{'time'==$ansOne?'btn-warm':''}}" href="/list/{{$typeOne->id}}?{{$request}}&qubie=time">按时间</a></li>
						<li> <a class="btn {{'man'==$ansOne?'btn-warm':''}}" href="/list/{{$typeOne->id}}?{{$request}}&qubie=man">按人气</a></li>
						<li> <a class="btn {{'grade'==$ansOne?'btn-warm':''}}" href="/list/{{$typeOne->id}}?{{$request}}&qubie=grade">按评分</a></li>
			</ul>
		</div>
	</div>
</div>
<!-- 筛选 -->

            <div class="myui-panel myui-panel-bg clearfix" id="ajaxPage">
				<div class="myui-panel-box clearfix">
					<div class="myui-panel_bd">
						<ul class="myui-vodlist clearfix">
                        @foreach ($data as $l)
<li class="col-lg-8 col-md-6 col-sm-4 col-xs-3">
								<div class="myui-vodlist__box">
			<a class="myui-vodlist__thumb lazyload" href="/details/{{$l->id}}.html" title="{{$l->d_name}}"  data-original="{{$l->img}}">
				<span class="play hidden-xs"></span>
			<span class="pic-tag pic-tag-top" style="background-color: #5bb7fe;">
				{{round($l->score/$l->snum,1)}}分
			</span>
<span class="pic-text text-right">
		</span>
	</a>
	<div class="myui-vodlist__detail">
		<h4 class="title text-overflow"><a href="/details/{{$l->id}}.html" title="{{$l->d_name}}">{{$l->d_name}}</a></h4>
		<p class="text text-overflow text-muted hidden-xs">
		{{$l->year}}/{{$l->region}}/{{$l->typename}}</p>
	</div>
</div>
</li>
@endforeach


													</ul>
					</div>
									</div>
			</div>

			<ul class="myui-page text-center clearfix">						 -->
</ul>
        </div>
    </div>
 			<!-- 引入尾 -->
		@include("template/default/foot")

</body>
<script>
      var type;
      layui.use(['laydate','form','laypage'], function(){
        var laydate = layui.laydate;
        var  form = layui.form;
        var laypage = layui.laypage;
        //执行分页ajax
        laypage.render({
            elem: document.getElementsByClassName("myui-page")[0] //获取div元素(就是将分页按钮插入到哪个DIV中)
            ,count: {{$count}} //数据总数，从服务端得到
            ,limit:{{$pageNum}} //每页显示数量
            ,theme:'#FF9E12'
            ,jump: function(obj, first){
            var url = "/list/{{$typeOne->id}}?{{$request}}"; //拼接跳转路由地址
            //首次不执行
            if(!first){
                //ajax分页
				$("#box").css("display","block");
				scrollTo("#ajaxPages",300);//自动回到顶部
                $.get(url,{'page':obj.curr},function(data){
                    $("#ajaxPage").html(data);
					$("#box").css("display","none");
                });
                }
            }
        });
      });
	  function scrollTo(ele, speed){
        if(!speed) speed = 300;
        if(!ele){
                $("html,body").animate({scrollTop:0},speed);
        }else{
                if(ele.length>0) $("html,body").animate({scrollTop:$(ele).offset().top},speed);
        }
        return false;
	}


    </script>
</html>
