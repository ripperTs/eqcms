<!DOCTYPE html>
<html>
<head>

<title>用户注册-{{S_NAME}}</title>
@include("template/default/head")
<style type="text/css">[class*=col-],.myui-panel_hd,.myui-content__list li{ padding: 10px}.btn{ border-radius: 5px;}.myui-vodlist__thumb{ border-radius:5px; padding-top:150%; background: url(/templets/default/images/a.gif) no-repeat;}.myui-vodlist__thumb.square{ padding-top: 100%; background: url(/templets/default/images/d.gif) no-repeat;}.myui-vodlist__thumb.wide{ padding-top: 60%; background: url(/templets/default/images/c.gif) no-repeat;}.myui-vodlist__thumb.actor{ padding-top: 140%;}.flickity-prev-next-button.previous{ left: 10px;}.flickity-prev-next-button.next{ right: 10px;}.myui-sidebar{ padding: 0 0 0 20px;}.myui-panel{ padding: 10px; margin-bottom: 20px; border-radius: 5px;}.myui-layout{ margin: -10px -10px 20px;}.myui-panel-mb{ margin-bottom: 20px;}.myui-panel-box{ padding: 10px;}.myui-panel-box.active{ margin: -10px;}.myui-player__item .fixed{ width: 500px;}.myui-vodlist__text li a{ padding: 10px 15px 10px 0;}.myui-vodlist__media li { padding: 10px 0 10px;}.myui-screen__list{ padding: 10px 10px 0;}.myui-screen__list li{ margin-bottom: 10px; margin-right: 10px;}.myui-page{ padding: 0 10px;}.myui-extra{ right: 20px; bottom: 30px;}@media (min-width: 1200px){.container{ max-width: 1920px;}.container{ padding-left: 120px;  padding-right: 120px;}.container.min{ width: 1200px; padding: 0;}}@media (max-width: 1400px){.myui-layout{ margin: 0;}}@media (max-width: 767px){body,body.active{ padding-bottom: 50px;}[class*=col-],.myui-content__list li{ padding: 5px}.flickity-prev-next-button.previous{ left: 5px;}.flickity-prev-next-button.next{ right: 5px;}.myui-panel{ padding: 0; border-radius: 0;}.myui-vodlist__text li a{ padding: 10px 15px 10px 0;}.myui-vodlist__media li { padding: 5px 0 5px;}.myui-screen__list{ padding: 10px 5px 0;}.myui-screen__list li{ margin-bottom: 5px; margin-right: 5px;}.myui-extra{ right: 20px; bottom: 80px;}.myui-page{ padding: 0 5px;}}</style>
<style type="text/css">.myui-user__head .btn-default{padding:15px}.myui-user__head .fa{font-size:16px;vertical-align:-1px}.myui-user__name{border-radius:5px;margin-bottom:20px;position:relative;padding:50px 30px;background-image:linear-gradient(135deg,#f761a1 10%,#8c1bab 100%)}.myui-user__name dt{float:left}.myui-user__name dd{margin-left:70px;padding-top:5px}.myui-user__name .logout{position:absolute;top:0;right:15px;margin-top:20px}.myui-user__form p{margin:0;padding:15px 0}.myui-user__form .xiang{display:inline-block;width:120px;padding-right:15px;text-align:right;color:#999}
.myui-user__form input
.form-control{display:inline-block;
	width:200px;
	margin-right:10px}


.myui-login__form{
	width:430px;
	padding:30px;
	margin: 80px auto;
	margin-top: 50px;
	height:550px;
	box-shadow:0 2px 5px rgba(0,0,0,.1)
	}
.myui-login__form li{margin:20px 0}@media (max-width:767px){.myui-user__head .btn-default{padding:15px 5px}.myui-user__name{padding:20px}.myui-user__name h3{margin:3px 0 5px;font-size:15px}.myui-user__head li a,.myui-vodlist__text li a{font-size:14px}.stui_login__form,.stui_login__form.active{width:100%;margin:0;padding:0}.myui-user__form p{padding:10px;font-size:14px}.myui-user__form .xiang{display:none}.myui-user__form .xiang.active{display:inline-block;width:auto;text-align:left}.myui-user__form input.form-control{width:100%;height:40px}.myui-user__form p .btn{width:120px}.myui-user__form .btn_unbind{display:inline-block;margin-top:5px}}
	.box{width:100px;
		height:40px;
		 float:left; margin-left:10px;
		 text-align:center;
		  line-height:50px;
		  cursor:pointer;
		}
	.show{width:300px;
		height:200px;
		  margin-top:10px;
		  text-align:center;
		   line-height:50px;}
</style>
<link rel="stylesheet" href="/{{TEM}}images/stui_block_color.css" type="text/css" />
		<link rel="stylesheet" href="/{{TEM}}images/stui_block.css" type="text/css" />
</head>
<body>
	@include("template/default/header")
	<div class="myui-panel_hd"><div style="height:30px;"></div></div>
<div class="myui-login__form clearfix" style="height: 600px;width:593px;">
	<div class="myui-panel myui-panel-bg clearfix" style="height: 500px;width:536px;">

		<div class="myui-panel-box clearfix" style="height: 600px;">
			<div class="myui-panel_bd">
				<div class="head text-center">
					<a href="/"><img class="img-responsive" style="width: 120px;height: 40px;margin-bottom: 10px;" src="{{S_LOGO}}"/></a>
				</div>
					<div class='box' data='1' style='background:#BFBFBF;color:#fff;margin-left: 150px;border-radius:10px;height:35px;margin-top: 5px;margin-bottom: 10px;line-height: 35px;'>手机号注册</div>
					<div class='box' data='2' style="background: #F0F0F0;border-radius:10px;height:35px;margin-top: 5px;margin-bottom: 10px;line-height: 35px;">邮箱注册</div>
					<div class='show' id="show1" style="margin-left:70px;">
				@if ($phone->templateid  !=  0)
				<form class="layui-form" style="margin-top:20px;">
					<ul>
 					<li>
						<input  type="input" name="user" id="phone" lay-verify="tellphone" autofocus class="form-control" placeholder="填写您的手机号" />
						<span id="yonghu1" style="color: red;float: right;margin-top: -40px;"></span>
					</li>

					<li>
						<input type="password" name="passwd" id="mima" lay-verify="required" class="form-control" placeholder="输入密码" />
						<span id="mm" style="color: red"></span>
					</li>
					<li>
						<a onclick="member_phone(this,1)" style="float:right;" id="phyzm" class="layui-btn" href="javascript:;"  >
                                        发送手机验证码
						</a>
						<input  type="text" id="yzm" name="vcode" lay-verify="required" placeholder="验证码" style='width:50%;text-transform:uppercase;' class="form-control" />
						<span id="phyzmspan" style="color: red"></span>
					</li>
					<li>
						<button lay-submit value="注册账号" lay-filter="phoneReg" class="layui-btn layui-btn-warm">
						注册账号
						</button>
					</li>
					<li class="text-center" style="margin-top: -20px;">
						<a class="text-muted" href="/back.html" style="float:left;">找回密码</a>
						<a class="text-muted" href="/login" style="float:right;">有账号?去登陆</a>
					</li>
				</ul>
			</form>
			@else
				<div style="width:100%;height:140px;margin-top:70px;margin-left:40px;padding: 10px;">
					<p style="color:#F5BA41;">手机号注册功能已关闭<br>请使用邮箱注册!</p>
				</div>
				<li class="text-center" style="margin-left:60px;">
						<a class="text-muted" href="/back.html" style="float:left;">找回密码</a>
						<a class="text-muted" href="/login" style="float:right;">有账号?去登陆</a>
					</li>
			@endif
			</div>
		</div>
				<div class='show' id='show2' style="display:none;height: 350px;margin-left: 73px;" >
					<form class="layui-form" >
				<li>
						<input  type="input" name="user" id="email" lay-verify="email" autofocus class="form-control" placeholder="填写您的邮箱" />
						<span id="yonghu2" style="color: red;float: right;margin-top: -40px;"></span>
					</li>

					<li>
						<input type="password" name="passwd" id="mima" lay-verify="required" class="form-control" placeholder="输入密码" />
						<span id="mm" style="color: red"></span>
					</li>
					<li>
						<a onclick="member_email(this,1)" style="float:right;" id="mailyzm" class="layui-btn" href="javascript:;"  >
                                        发送邮箱验证码
                                      </a>
						<input  type="text" id="emailyzm" name="vcode" lay-verify="required" placeholder="验证码" style='width:50%;text-transform:uppercase;' class="form-control" />
						<span id="yzmspan" style="color: red"></span>
					</li>
					<li>
						<button lay-submit value="注册账号" lay-filter="emailReg" class="layui-btn layui-btn-warm">
							注册账号
						</button>
					</li>
					<li class="text-center" style="margin-top: -20px;">
						<a class="text-muted" href="/back.html" style="float:left;">找回密码</a>
						<a class="text-muted" href="/login" style="float:right;">有账号?去登陆</a>
					</li>
				</form>
			</div>
		</div>
	</div>
</div>
					</div>
					<script>
					var boxs = document.getElementsByClassName('box');  //得到 数组对象
					var shows = document.getElementsByClassName('show');  //得到 数组对象
					for (var i=0; i < boxs.length; i++) {
						boxs[i].onclick = function() {
							//不管对应的id 让所有的show 都变没
							for (var j=0; j < shows.length; j++) {
								shows[j].style.display = "none";
							}

							//获得data的内容 getAttribute
							var id = this.getAttribute('data');
							//alert(id); 点的是第几个 获得 show+第几个
							var show = document.getElementById('show'+id);
							show.style.display = "block";

							//接下来做按钮样式的切换
							//让所有人都没有样式
							for (var j=0; j < boxs.length; j++) {
								boxs[j].style.background = "#F0F0F0";
								boxs[j].style.color = "";
							}
							//点谁有样式
							this.style.background = "#BFBFBF";
							this.style.color = "#FFF";
						}

					}

					</script>
										<!-- <h5>登录账号</h5> -->

			<ul>

 		@include("template/default/foot")

</body>
<script>
//Demo
var yzm = false;
var phoneyz = false;
var token = "{{csrf_token()}}";
var emailyz = false;
layui.use('form', function(){
  var form = layui.form;

  $("#yzm").blur(function(){
		var yzm = $(this).val();
		$.post("/doreg",{'_token':token,'yzm':yzm,'type':'yzm','phone':$("#phone").val()},function(data){
			if(data==2){
				$("#phyzmspan").html("验证码不正确!");
				yzm = false;
			}else{
				yzm = true;
				$("#phyzmspan").html("");
			}
		})
	});

	$("#emailyzm").blur(function(){
		var yzm = $(this).val();
		$.post("/doreg",{'_token':token,'yzm':yzm,'type':'emailyzm','email':$("#email").val()},function(data){
			if(data==2){
				$("#yzmspan").html("验证码不正确!");
				yzm = false;
			}else{
				yzm = true;
				$("#yzmspan").html("");
			}
		})
	});

	$("#phone").blur(function(){
		var phone = $(this).val();
		$.post("/doreg",{'_token':token,'type':'phones','phone':$("#phone").val()},function(data){
			if(data==2 && $("#phone").val()!=''){
				$("#yonghu1").html("手机号已存在!");
				phoneyz = false;
			}else{
				phoneyz = true;
				$("#yonghu1").html("");
			}
		})
	});

	$("#email").blur(function(){
		$.post("/doreg",{'_token':token,'type':'emails','email':$("#email").val()},function(data){
			if(data==2 && $("#email").val()!=''){
				$("#yonghu2").html("邮箱已存在!");
				emailyz = false;
			}else{
				emailyz = true;
				$("#yonghu2").html("");
			}
		})
	});

var mobile = /^1[3|4|5|7|8]\d{9}$/,phone = /^0\d{2,3}-?\d{7,8}$/;
        form.verify({
            tellphone: function(value){
            	var flag = mobile.test(value) || phone.test(value);
                if(!flag){
                	return '请输入正确手机号';
                }
            }
        });
	//监听提交手机号
  form.on('submit(phoneReg)', function(data){
	// $('input').trigger('blur');
        // if(yzm && phone){
        //     return true;
        // }else{
        //     return false;
        // }
		$.post("/phoneReg",{'_token':token,'data':data.field},function(data){
			if(data==200){
				alert('注册成功!请登陆');
				location="/login";
			}else if(data==2){
				alert('注册失败!');
			}
		});

    return false;
  });


  //监听提交邮箱
  form.on('submit(emailReg)', function(data){
	$.post("/emailReg",{'_token':token,'data':data.field},function(data){
			if(data==1){
				alert('注册账号成功,激活邮件已发送至您的邮箱,请激活后登陆!');
				location="/login";
			}else if(data==2){
				alert('注册失败!')
			}
		});

    return false;
  });

});
/*隐藏*/
      function member_email(obj,id){
		var mail = $("#email").val();
		var n=60;
		$("#mailyzm").html('');
		$("#mailyzm").css("pointerEvents","none");
		$("#mailyzm").attr("class","layui-btn layui-btn-disabled");
		var t = setInterval(() => {
			$("#mailyzm").html(n+"秒");
			if(n<1){
				$("#mailyzm").css("pointerEvents","auto");
				$("#mailyzm").attr("class","layui-btn");
				$("#mailyzm").html("重新获取验证码");
				clearInterval(t);
			}
			n--;
		}, 1000);
		$.post("/doreg",{'_token':token,'email':$("#email").val(),'type':'email'},function(data){
				if(data==1){

				}else{
					alert('邮件发送失败!');
				}
			})


      }

	  /*隐藏*/
      function member_phone(obj,id){
		  if(!phoneyz){
			  return false;
		  }
		var phone = $("#phone").val();
		var reg=/^[1][3-9][0-9]{9}$/;
		if(reg.test($("#phone").val())){
			var n=60;
			$("#phyzm").html('');
			$("#phyzm").css("pointerEvents","none");
			$("#phyzm").attr("class","layui-btn layui-btn-disabled");
			var t = setInterval(() => {
				$("#phyzm").html(n+"秒");
				if(n<1){
					$("#phyzm").css("pointerEvents","auto");
					$("#phyzm").attr("class","layui-btn");
					$("#phyzm").html("重新获取验证码");
					clearInterval(t);
				}
				n--;
			}, 1000);
			$.post("/doreg",{'_token':token,'phone':$("#phone").val(),'type':'phone'},function(data){
				if(data==1){

				}else{
					alert('手机验证码发送失败!');
				}
			})

        }else{
            $("#yonghu1").html("手机号不正确");
        }

      }
</script>
</html>
