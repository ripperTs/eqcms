<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <div id="contentDiv0" style="background:#fff;padding-bottom:20px;zoom:1;position:relative;z-index:1;"
        class="qm_bigsize qm_converstaion_body body qmbox qqmail_webmail_only">
        <table width="710" style="margin:0 auto;border-collapse: collapse;">
            <tbody>
                <tr>
                    <td style="padding:0">
                        <table width="680" height="80" bgcolor="#ffffff"
                            style="border-collapse: collapse;margin:0 auto;box-shadow: 0 0 15px rgba(3,68,174,0.13);">
                            <tbody>
                                <tr>
                                    <td style="padding-left: 35px"><a
                                            href="http://{{S_REALM}}"
                                            rel="noopener" target="_blank"><img
                                                src="http://{{S_REALM}}{{S_LOGO}}" width="120" height="40"></a></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="padding:20px 0;background-color: #0bb9eb;position: relative">
                        <div style="width: 710px;margin:0 auto;">
                            <div style="width:645px;margin:0 auto">
                                <div style="padding:0 18px;">
                                    <p style="color:#ffffff;line-height: 30px;font-size: 14px;font-weight: bold">尊敬的会员：
                                    </p>
                                    <div
                                        style="text-indent: 2em;color:#ffffff;line-height: 30px;font-size: 12px;margin-bottom: 40px;">
                                        <p>您在{{S_NAME}}网站申请激活新邮箱:</p>
                                        <p></p>
                                        <p><b>您的验证码为</b></p>
                                        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        {{$code}}</p>
                                        <p></p>
                                        <p><b>为了您的账号安全,请勿随意告诉他人</b></p>

                                    </div>
                                    <p style="text-align: right;color:#ffffff;font-size: 12px;font-weight: bold">
                                        {{S_NAME}}</p>
                                </div>
                                <div style="border-top:1px dashed #85dcf5">
                                    <div style="margin:0 auto;font-size: 12px;padding:10px 0 0 18px;">
                                        <p style="color: #ffffff;">邮件由系统自动发出，请勿回复</p>
                                    </div>
                                </div>
                            </div>
                        </div><span
                            style="width:0;height:0;border-width:0 0 15px 15px;border-style:solid;border-color:transparent transparent #0394c1 transparent;position:absolute;left: 0;top:-15px"></span><span
                            style="width:0;height:0;border-width:15px 15px 0 0;border-style:solid;border-color:#0394c1 transparent transparent transparent;position:absolute;right: 0;bottom: -15px"></span>
                    </td>
                </tr>
                <tr>
                    <td style="height: 170px;border-collapse: collapse;">
                        <div
                            style="width: 709px;margin:0 auto;padding: 20px 0;box-shadow: 0 0 15px rgba(3,68,174,0.13);text-align:center">
                            <img src="http://{{S_REALM}}{{TWO_IMG}}" height="88">
                            <p style="color:#707070;font-size: 12px;text-align: center">Copyright © <span
                                    style="border-bottom:1px dashed #ccc;z-index:1" t="7" onclick="return false;"
                                    data="2007-2019">2007-2020</span> {{S_NAME}} All Right Reserved</p>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table><br><br>
        </div>
        <style type="text/css">
            .qmbox style,
            .qmbox script,
            .qmbox head,
            .qmbox link,
            .qmbox meta {
                display: none !important;
            }
        </style> <!-- -->
    </div>
</body>
</html>
