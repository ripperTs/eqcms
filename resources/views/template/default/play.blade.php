
<!DOCTYPE html>
<html>
<head>
<title>《{{$data->d_name}}》由{{S_NAME}}提供全集在线播放_最新电视剧电影在线观看_10W+影视资源任你搜</title>
@include("template/default/head")
<link rel="stylesheet" href="/{{TEM}}images/stui_block_color.css" type="text/css" />
<link rel="stylesheet" href="/{{TEM}}images/stui_block.css" type="text/css" />
<script type="text/javascript" src="/{{TEM}}js/play.js"></script>
<script type="text/javascript">
    var vod_name='{{$data->d_name}}',vod_url=window.location.href,vod_part="第{{$blues+1}}集";
</script>
</head>
<body class="active">
@include("template/default/header")
	<div class="myui-player clearfix" style="background-color: #27272f;">
		<div class="container">
        	<div class="row">
        		<div class="myui-player__item clearfix" style="background-color: #1f1f1f;">
        								<div class="col-md-wide-75 clearfix padding-0 relative" id="player-left">
										<div class="myui-player__box player-fixed">
	<a class="player-fixed-off" href="javascript:;" style="display: none;"><i class="fa fa-close"></i></a>
	<div class="embed-responsive clearfix">
		<div class="passwd">
                @foreach ($playurl as $k=>$pv)
                    @if($k==$vfrom)
                        @foreach ($pv as $g=>$h)
                            <li>
                                @if ($g==$vpart)
                                    <iframe id='cciframe' scrolling='no' src="/dplayer/index.html?url={{explode('$',$h)[1]}}" frameborder='0' oallowfullscreen msallowfullscreen mozallowfullscreen allowfullscreen webkitallowfullscreen></iframe>
                                @endif
                            </li>
                        @endforeach
                    @endif
				@endforeach
		</div>
			@if (U_WATCH==1)
				<script>
					var passDiv = `
						<div style="font-size:12px; width:100%;height:100%;">
							<div style="width:200px; height:50px;text-align:left; margin:40px auto;">
								<span style="color:#47A1F8;">请先登录后继续观看此视频!</span><br>
								<br>
								<span style="color:red;font-size:16px;">
								<a style="color:red;" href="/login">去登陆</a>
								</span>
								<span style="color:red;font-size:16px;"> |
								<a style="color:red;" href="/reg.html">去注册</a>
								</span>
						<br>
							</div>
						</div>
					`;
					$(".passwd").html(passDiv);
				</script>
			@endif
			@if ($authority == 1 && $ulevel == '')
				<script>
					var passDiv = `
						<div style="font-size:12px; width:100%;height:100%;">
							<div style="width:200px; height:50px;text-align:left; margin:40px auto;">
								<span style="color:#47A1F8;">当前影片限制会员观看!</span><br>
								<br>
								<span style="color:red;font-size:16px;">
								<a style="color:red;" href="/login">去登陆</a>
								</span>
								<span style="color:red;font-size:16px;"> |
								<a style="color:red;" href="/reg.html">去注册</a>
								</span>
						<br>
							</div>
						</div>
					`;
					$(".passwd").html(passDiv);
				</script>
			@endif
			@if ($data->level != '' && $ulevel < $data->level))
				<script>
					var passDiv = `
						<div style="font-size:12px; width:100%;height:100%;">
							<div style="width:200px; height:50px;text-align:left; margin:40px auto;">
								<span style="color:#47A1F8;">您的会员等级不支持观看该影片!</span><br>
								<br>
								<span style="color:#47A1F8;">您需要升级到:</span>
								<span style="color:red;font-size:16px;">
								@if($data->level == '1')普通会员@elseif($data->level == '2')铜牌会员@elseif($data->level == '3')银牌会员@elseif($data->level == '4')金牌会员@elseif($data->level == '5')钻石会员@endif
								</span>
								<span style="color:#47A1F8;">才可以继续观看!</span>
						<br>
							</div>
						</div>
					`;
					$(".passwd").html(passDiv);
				</script>
				@endif
				@if ($data->passwd != '' && $quanxian != MD5($data->passwd))
				<script>
					var passDiv = `
						<div style="font-size:12px; width:100%;height:100%;">
							<div style="width:200px; height:50px;text-align:left; margin:40px auto;">
								<span style="color:#47A1F8;">请输入观看密码后继续：</span><br>
								<form action="/watchPasswd" method="post">
									{{csrf_field()}}
									<input style="border:1px solid #3374b4;height:33px;line-height:33px;padding-left:5px" type="password" name="password">
									<input type="hidden" name="ids" value="{{$vid}}-{{$vfrom}}-{{$blues}}">
									<input style="border:1px solid #3374b4;background:#3374b4;padding:7px 10px;color:#fff;text-decoration:none;vertical-align:top" type="submit" value="播 放">
								</form>
								<br>
						<img style="margin:15px 0 5px 0" src="{{TWO_IMG}}" height="100" width="100">
						<br><span style="color:#47A1F8;">扫描二维码关注微信<br>
						回复: <font color="red"><b>{{$vid}}</b></font> 获取播放口令</span>
							</div>
						</div>
					`;
					$(".passwd").html(passDiv);
				</script>
				@endif
                <script>
                    var pn=pn;
                    var forcejx1=forcejx;
                    var forcejx2="no";
                    var forcejx3=forcejx;if(forcejx1!=forcejx2 && contains(unforcejxARR,pn)==false){pn=forcejx3;}else{pn=pn;}document.getElementById("cciframe").width = playerw;document.getElementById("cciframe").height = playerh;
                </script>
 	</div>
</div>
<div class="clearfix" style="background-color: #1b1a25;">
	<div class="col-md-wide-7">
	<ul class="myui-player__operate">
			<li>
		<a href="javascript:AddFav('{{$vid}}',{{Session::get('user')['id']}})"><i class="fa fa-star"></i>收藏</a>
	</li>
		<li>
		<a  href="javascript:void()" onclick="reportErr({{$vid}})"><i class="fa fa-close"></i> 报错</a>
	</li>
		<li>
		<a href="javascript:;" onclick="window.location.reload()"><i class="fa fa-spinner"></i> 刷新</a>
	</li>

    <li><a href="/play/{{$vid}}-{{$vfrom}}-{{($vpart)<=0?'0':$vpart-1}}.html"><i class="fa fa-caret-up"></i> 上集</a></li>
    @if ($vpart>=($numm-1))
        <li><a href="/play/{{$vid}}-{{$vfrom}}-{{($numm-1)}}.html"><i class="fa fa-caret-down"></i> 下集</a></li>
    @else
        <li><a href="/play/{{$vid}}-{{$vfrom}}-{{$vpart+1}}.html"><i class="fa fa-caret-down"></i> 下集</a></li>
    @endif
    <li><a href="javascript:void(0);" onclick='xj();'><i class="fa fa-eye"></i> 选集</a></li>
		<li class="dropdown-hover hidden-xs">
		<a href="javascript:;"><i class="fa fa-qrcode font14"></i> 关注公众号</a>
		<div class="dropdown-box top fadeInDown clearfix">
			<div class="item text-center">
				<div class="qrcode-box active" style="background-color: #ffffff;">
	<img class="icon" src="{{TWO_IMG}}" width="196"/>
</div>
<p>扫码关注，观影更方便</p>
			</div>
		</div>
	</li>
</ul>
</div>
	<div class="col-md-wide-3">
		<div class="myui-player-links">
			<font size="2" face="arial" class="wjts" color="red">切勿相信视频内广告 以免造成财产损失</font><br/>
			<font size="2" face="arial" class="wjts1" color="red">播放卡顿/异常等，请尝试在下方切换线路</font>
		</div>
	</div>
</div>
<script>
	var date=new Date();
	if(date.getHours()>=20){
		$(".wjts").html("当前是晚间高峰时期 请耐心等待片刻");
		$(".wjts1").html("如果长时间无法播放 点击报错留言给我们");
	}else{
		$(".wjts").html("切勿相信视频内广告 以免造成财产损失");
		$(".wjts1").html("播放卡顿/异常等，请尝试在下方切换线路");
	}

</script>
<style type="text/css"> .embed-responsive{ padding-bottom: 56.25%;} </style>
										<a class="is-btn hidden-sm hidden-xs" id="player-sidebar-is" href="javascript:;"><i class="fa fa-angle-right"></i></a>
										</div>
										<div class="col-md-wide-25 padding-0" id="player-sidebar">
						<div class="myui-panel margin-0 clearfix">
	<div class="myui-panel-box clearfix">
		<div class="myui-panel__head active clearfix">
			<h3 class="title text-fff">
				{{$data->d_name}}			</h3>
		</div>
		<div class="tab-content mb10">
<div><ul class="myui-content__list playlist clearfix" id="playlist"><li class="col-md-2 col-sm-5 col-xs-3 active"> </li></ul></div>
		</div>
<!-- <div class="col-pd mb10">
			<div style="padding: 80px  35px; background-color: #000; border-radius: 5px; text-align: center;"><h5 class="margin-0 text-muted">广告位02</h5></div>		</div> -->
				<div class="myui-panel_bd">
			<h5 class="text-center text-muted">—— 同主演推荐 ——</h5>
			<ul class="myui-vodlist__bd clearfix">

                {{-- 开始 --}}
                @foreach ($strrTop as $stv)


								<li class="col-md-2 col-sm-4 col-xs-3">
					<div class="myui-vodlist__box">
	<a href="/details/{{$stv->id}}.html" class="myui-vodlist__thumb lazyload" title="{{$stv->d_name}}" style="background: url({{$stv->img}});">
		<span class="play hidden-xs"></span>
			<span class="pic-tag pic-tag-top" style="background-color: #5bb7fe;">
				{{round($stv->score/$stv->snum,1)}}分
			</span>
		<span class="pic-text text-center">{{$stv->d_name}}</span>
	</a>
</div>
                </li>
                @endforeach

                {{-- 结束 --}}


							</ul>
		</div>
	</div>
</div>

					</div>
									</div>
				<div class="myui-player__data hidden-xs clearfix">
					<h3>
						<a class="text-fff" href="/details/{{$vid}}.html">{{$data->d_name}}</a>
						<small class="text-muted">{{$data->note}}</small>
					</h3>
					<p class="text-muted margin-0">
						{{date('Y-m-d H:i',$data->time)}}更新 / <span class="text-red">{{round($data->score/$data->snum,1)}}分</span> / <a class="text-muted" href="/list/{{$data->t_id}}?region={{$data->region}}">{{$data->region}}</a> / <a class="text-muted" href="/list/{{$data->t_id}}?year={{$data->year}}">{{$data->year}}	</a>
					</p>
				</div>
			</div>
		</div>
	</div>

    <div class="container">
        <div class="row">

			<div class="row">
                                <div class="col-md-wide-7 col-xs-1 padding-0">
						<div class="myui-panel myui-panel-bg clearfix">
			<div class="myui-panel-box clearfix">
				<div class="myui-panel_hd">
					<div class="myui-panel__head active bottom-line clearfix" id="abcde">
						<a class="more sort-button pull-right" href="javascript:;"><i class="fa fa-sort-amount-asc"></i> 排序</a>						<h3 class="title">
								播放地址						</h3>
						<ul id="abcde" class="nav nav-tabs active">
                            @foreach ($paddress as $k=>$p)
                                <li><a href="#playlist{{$p->id}}" data-toggle="tab">线路{{$k+1}}</a></li>
                            @endforeach
						</ul>
					</div>
				</div>
				<div class="tab-content myui-panel_bd">
            @foreach ($playurl as $k=>$j)
            <div id="playlist{{$k}}" class="tab-pane fade in clearfix">
                <ul class="stui-content__playlist  sort-list column8 clearfix">
                    @foreach ($j as $s=>$m)
                    <li>
                        @if ($s==$vpart)
                            <a title='{{explode('$',$m)[0]}}' style="color:red" href='/play/{{$data->did}}-{{$k}}-{{$s}}.html' target="_self">{{explode('$',$m)[0]}}</a>
                        @else
                            <a title='{{explode('$',$m)[0]}}' href='/play/{{$data->did}}-{{$k}}-{{$s}}.html' target="_self">{{explode('$',$m)[0]}}</a>
                        @endif
                    </li>
                    @endforeach
                </ul>
            </div>
            @endforeach

</div>
</div>
</div>
		<script type="text/javascript">
			$(".tab-pane:first,.nav-tabs li:first").addClass("active");
			//选集跳转指定处

			function xj(){
				scrollTo('#abcde',300);
			}

			//跳转函数
			function scrollTo(ele, speed){
		        if(!speed) speed = 300;
		        if(!ele){
		                $("html,body").animate({scrollTop:0},speed);
		        }else{
		                if(ele.length>0) $("html,body").animate({scrollTop:$(ele).offset().top},speed);
		        }
		        return false;
			}

		</script>

					<div class="myui-panel myui-panel-bg clearfix">
	<div class="myui-panel-box clearfix">
		<div class="myui-panel_hd">
			<div class="myui-panel__head clearfix">
				<h3 class="title">
					剧情简介
				</h3>
			</div>
		</div>
		<div class="myui-panel_bd">
			<div class="col-pd text-collapse content">
                <p><span class="text-muted">导演：</span>
                    <a href='/search?terms={{$data->director}}'>{{$data->director}}</a>&nbsp;&nbsp; </p>
                <p><span class="text-muted">主演：</span>
                    @foreach ($strring as $sv)
                        <a href='/search?terms={{$sv}}'>{{$sv}}</a>&nbsp;&nbsp;
                    @endforeach
                </p>
				<span class="sketch content">{{mb_substr($data->plot,0,50)}}</span>
				<span class="data" style="display: none;">{{$data->plot}}</span>
				<a class="details" href="javascript:;">详情 <i class="fa fa-angle-down"></i></a>
			</div>
		</div>
	</div>
</div>

				<div class="myui-panel myui-panel-bg clearfix">
	<div class="myui-panel-box clearfix">
		<div class="myui-panel_hd">
			<div class="myui-panel__head clearfix">
				<h3 class="title">
					猜你喜欢				</h3>
			</div>
		</div>
		<div class="myui-panel_bd">
			<ul class="myui-vodlist__bd clearfix">
@foreach ($like as $l)
								<li class="col-lg- col-md-6 col-sm-4 col-xs-3">
						<div class="myui-vodlist__box">
			<a class="myui-vodlist__thumb lazyload" href="/details/{{$l->id}}.html" title="{{$l->d_name}}" data-original="{{$l->img}}">
				<span class="play hidden-xs"></span>
			<span class="pic-tag pic-tag-top" style="background-color: #5bb7fe;">
            {{round($l->score/$l->snum,1)}}分
			</span>
<span class="pic-text text-right">
		{{$l->note}}</span>
	</a>
	<div class="myui-vodlist__detail">
		<h4 class="title text-overflow"><a href="/details/{{$l->id}}.html" title="{{$l->d_name}}">{{$l->d_name}}</a></h4>
		<p class="text text-overflow text-muted hidden-xs">
		{{$l->year}}/{{$l->region}}/{{$l->typename}}</p>
	</div>
</div>
                </li>
                @endforeach



							</ul>
		</div>
	</div>
</div>
				<div class="myui-panel myui-panel-bg clearfix">
	<div class="myui-panel-box clearfix">
		<div class="myui-panel_bd">
			<div class="myui-panel_hd">
				<div class="myui-panel__head clearfix">
					<h3 class="title">
						影片评论
					</h3>
				</div>
			</div>
			<div class="myui-panel_bd" id="comments">
<div  id="comment_list">评论加载中...</div>
			</div>
		</div>
	</div>
</div>
			</div>
                        <div class="col-md-wide-3 col-xs-1 myui-sidebar hidden-sm hidden-xs">
				<div class="myui-panel myui-panel-bg clearfix">
	<div class="myui-panel-box clearfix">
		<div class="myui-panel_hd">
			<div class="myui-panel__head clearfix">
				<h3 class="title">
					今日新增				</h3>
			</div>
		</div>
		<div class="myui-panel_bd">

			<ul class="myui-vodlist__media active col-pd clearfix">

								@if (count($day)>0)
                    <li>
	<div class="thumb">
		<a class="myui-vodlist__thumb img-xs-70" style="background: url({{$day[0]->img}});" href="/details/{{$day[0]->id}}.html" title="{{$day[0]->d_name}}" data-original="{{$day[0]->img}}"></a>
	</div>
	<div class="detail detail-side">
		<h4 class="title"><a href="/details/{{$day[0]->id}}.html"><i class="fa fa-angle-right text-muted pull-right"></i> {{$day[0]->d_name}}</a></h4>
		<p class="font-12"><span class="text-muted">类型：</span>{{$day[0]->typename}} <span class="text-muted">地区：</span>大陆</p>
		<p class="font-12 margin-0"><span class="text-muted">状态/集数：</span>{{$day[0]->note}}</p>
	</div>
</li>
                @endif
                            </ul>



			<ul class="myui-vodlist__text col-pd clearfix">

 @if (count($day)>0)
                    @foreach ($day as $k=>$v)
                    @if ($k>0)
                        <li>
                        <a href="/details/{{$v->id}}.html" title="{{$v->d_name}}">
                            <span class="pull-right  text-muted hidden-md" style="color: ;">
                            {{$v->note}}</span>
                            @if ($k==1)
                                <span class="badge badge-1">{{$k}}</span>{{mb_substr($v->d_name,0,10)}}</a>
                            @elseif($k==2)
                                <span class="badge badge-2">{{$k}}</span>{{mb_substr($v->d_name,0,10)}}</a>
                            @elseif($k==3)
                                <span class="badge badge-3">{{$k}}</span>{{mb_substr($v->d_name,0,10)}}</a>
                            @else
                                <span class="badge">{{$k}}</span>{{mb_substr($v->d_name,0,10)}}</a>
                            @endif

                    </li>
                    @endif
                    @endforeach
                @endif

							</ul>
		</div>
	</div>
</div>

				<div class="myui-panel myui-panel-bg clearfix">
	<div class="myui-panel-box clearfix">
		<div class="myui-panel_hd">
			<div class="myui-panel__head clearfix">
				<h3 class="title">
					正在热播				</h3>
			</div>
		</div>
		<div class="myui-panel_bd">
			<ul class="myui-vodlist__media active col-pd clearfix">
@foreach ($hot as $h)
									<li>
	<div class="thumb">
		<a class="myui-vodlist__thumb wide img-xs-120" style="background: url({{$h->img}});" href="/details/{{$h->id}}.html" title="{{$h->d_name}}"></a>
	</div>
	<div class="detail detail-side">
		<h4 class="title" style="margin-top: 0;"><a href="/details/{{$h->id}}.html">{{mb_substr($h->d_name,0,10)}}</a></h4>
		<p class="font-12 text-muted"><i class="fa fa-youtube-play"></i> {{round($h->score/$h->snum,1)}}分</p>
	</div>
</li>
@endforeach
							</ul>
		</div>
	</div>
</div>
			</div></div>
        </div>
    </div>
<script type="text/javascript">
    	$(".MacPlayer").addClass("embed-responsive").css({"padding-bottom":"56.25%","z-index":"99"});
		$("#playleft,.dplayer-video-wrap").css({"position":"inherit","overflow":"initial"});
    </script>
	<span class="mac_hits hits hide" data-mid="1" data-id="{{$vid}}" data-type="hits"></span>
	<span class="mac_ulog_set hide" data-type="4" data-mid="1" data-id="{{$vid}}" data-sid="1" data-nid="1"></span>
	<span class="vod_history hide" data-name="{{$data->d_name}}" data-link="/play/{{$vid}}-{{$vfrom}}-{{$vpart}}.html"  data-pic="/play/{{$vid}}-{{$vfrom}}-{{$vpart}}.html" data-part="第{{$blues+1}}集" data-limit="10"></span>

        <!-- 引入尾 -->
		@include("template/default/foot")

</body>
</html>
@if (Y_SWITCH==1)
	<script>
window.onload = function(){
	$.post("/comments",{"_token":"{{csrf_token()}}","did":"{{$data->did}}"},function(data){
		$("#comments").html(data);
	})
}
</script>
@endif
