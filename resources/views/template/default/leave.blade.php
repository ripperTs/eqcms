
<!DOCTYPE html>
<html>
<head>
<title>留言求片-{{S_NAME}}</title>
@include("template/default/head")
<link rel="stylesheet" href="/{{TEM}}images/stui_block_color.css" type="text/css" />
<link rel="stylesheet" href="/{{TEM}}images/stui_block.css" type="text/css" />
</head>
<body>
 		@include("template/default/header")
  <div class="container">
   <div class="row">
    <div class="myui-panel_hd"><div style="height:70px;"></div></div>
    <div class="stui-pannel stui-pannel-x">
     <div class="col-lg-wide-3 col-xs-1">
      <div class="stui-pannel stui-pannel-bg clearfix">
       <div class="stui-pannel-box clearfix">
        <div class="stui-pannel_hd">
         <div class="stui-pannel__head clearfix">
          <h3 class="title">我要留言</h3>
         </div>
        </div>
        <form id="f_leaveword"  class="layui-form layui-col-space5">
         <div class="col-pd clearfix">
          <ul class="gbook-form">
           <li><input class="form-control" type="text" lay-verify="email" placeholder="请输入联系邮箱(必填)" name="email" id="m_author" size="20" /></li>
           <li><input class="form-control" type="text" value="匿名" name="name" id="m_author" size="20" /></li>
           <li><input class="form-control" type="text" placeholder="标题" lay-verify="required" name="title" id="m_author" size="20" /></li>
           <li><textarea class="form-control" onblur="fun(this);" lay-verify="required" placeholder="请输入留言内容..." cols="40" name="content" id="m_content" rows="5"></textarea></li>
            <input type="hidden" name="code_leave" value="leave">
           <li><input name="code" type="text" placeholder="验证码" class="form-control" id="vdcode" style="width: 80px; display: inline-block; margin-right: 10px;text-transform:uppercase" tabindex="3" /> <img id="vdimgck" src="/code/leave" alt="看不清？点击更换" align="absmiddle" style="height: 33px; cursor:pointer" onclick="this.src=this.src+'?get=' + new Date()" /></li>
           <li><button lay-submit="" lay-filter="add" value="提交留言" class="btn btn-primary pull-right" >提交留言</button><input type="reset" value="重新留言" class="btn btn-default" /> </li>
          </ul>
         </div>
        </form>
       </div>
      </div>
     </div>
     <div class="col-lg-wide-7 col-xs-1" id="ajaxpage">
         {{-- 遍历开始 --}}
        @foreach ($res as $v)
      <div class="stui-pannel stui-pannel-bg clearfix">
       <div class="stui-pannel-box clearfix">
        <div class="col-pd clearfix">
         <ul>
          <li class="topwords"><strong>{{$v->name}}</strong><span class="text-red pull-right">第{{$v->id}}楼<span></span></span></li>
          <li class="top-line" style="margin-top: 10px; padding: 10px 0;">{{$v->content}}<br />
            @if ($v->replay==1)
                <div look="{{$v->id}}">
                <a onclick="look(this,{{$v->id}})" class="btn btn-default" href="javascript:;"  >查看管理员回复 </a>
                </div>
            @endif
            </li>
          <span class="font-12 text-muted">发表于 {{date('Y-m-d H:i:s',$v->time)}}<span></span></span>
         </ul>
        </div>
       </div>
      </div>
      @endforeach
      {{-- 遍历结束 --}}

      </div>
      <ul class="stui-page text-center page">

      </ul>

     </div>
    </div>
    <style type="text/css">
					.gbook-form li{ margin-top: 15px;}
					.gbook-form li:first-child{ margin-top: 0;}
				</style>
   </div>
  </div>

        <!-- 引入尾 -->
		@include("template/default/foot")

</body>
<script type="text/javascript">
    function fun(obj){
        var text = '{{C_WORDS}}';
        var list = text.split("|");
        var str=obj.value;
        list.forEach(function(ele){
            var reg=new RegExp(ele,"g");
            str=str.replace(reg,"**");
        })
        document.getElementById("m_content").value=str;
    }
</script>
<script>
        var sh = "{{S_SWITCH}}";
      var token = "{{csrf_token()}}";
      layui.use(['laydate','form','upload','layedit','laypage'], function(){
        var laydate = layui.laydate;
        var  form = layui.form;
        var upload = layui.upload;
        var layedit = layui.layedit;
        var laypage = layui.laypage;

        //执行分页ajax
        laypage.render({
            elem: document.getElementsByClassName("page")[0] //获取div元素(就是将分页按钮插入到哪个DIV中)
            ,count: {{$count}} //数据总数，从服务端得到
            ,limit: {{$num}} //每页显示数量
            ,theme:'#FF9E12'
            ,jump: function(obj, first){
            var url = "/gbook/?page="+obj.curr; //拼接跳转路由地址
            //首次不执行
            if(!first){
                //ajax分页
                var index = layer.load(2, {time: 10*1000}); //加载等待动画
                $.get(url,{},function(data){
                    $("#ajaxpage").html(data);
                    layer.close(index); //数据显示为完毕,关闭加载动画
                    $(document).scrollTop(0); //自动回到顶部
                });
                }
            }
        });

        //自定义验证规则
        form.verify({
            nikename: function(value) {
                if (value.length < 1) {
                    return '至少得1个字符啊';
                }
            },
            repass: function(value) {
                if ($('#L_pass').val() != $('#L_repass').val()) {
                    return '两次密码不一致';
                }
            }
        });



        //监听提交
        form.on('submit(add)',
        function(data) {
            $.post("/gbook",{'data':data.field,'_token':token},function(data){
                if(data==1){
                    if(sh==1){
                        alert("留言成功,管理员审核后才会显示");
                        location.reload();
                    }else{
                        alert("留言成功");
                        location.reload();
                    }

                }else if(data==2){
                    alert("留言失败");
                }else{
                    alert("验证码不正确");
                }
            });

            return false;
        });





      });

      /*编辑*/
      function look(obj,id){
        // $("[look="+id+"]").remove();
        var url  = "/gbook/"+id;
        $.getJSON(url,{},function(data){
            console.log(data);
            if(data.msg==1){
                $("[look="+id+"]").html("<font color='red'>"+data.content+"</font>");
            }
        });
      }



    </script>
</html>
