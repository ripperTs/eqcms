<!DOCTYPE HTML>
<html>
<head>
<title>会员中心</title>
<script src="/cen/jquery.min.js"></script>
@include("template/default/head")
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="robots" content="noindex,nofollow" />
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0;" name="viewport" />
<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<link href="/cen/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="/cen/swiper.min.css" rel="stylesheet" type="text/css" >
<link href="/cen/iconfont.css" rel="stylesheet" type="text/css" />
<link href="/cen/color.css" rel="stylesheet" type="text/css" />
<link href="/cen/style.min.css" rel="stylesheet" type="text/css" />
<style>
    .recharge{
        width:600px;
        height: 300px;
        border-radius: 10px;
        box-shadow: 0 0 10px #aaa;
        position:absolute;
        top:46%;
        left:28%;
        z-index: 9999;
        background: #fff;
        display: none;
    }
    .recharge .title{
        width: 600px;
        height: 20px;
        text-align: left;
        padding: 20px;
        font-size: 18px;
    }
    .recharge .prompt{
        width: 580px;
        height: 60px;
        margin-left: 10px;
        margin-top: 30px;
        border-radius: 5px;
        background:#67c23a;
        padding: 3px;
        padding-left: 30px;
        color: #fff;
    }
    .recharge ul li{
        width: 100px;
        float: left;
        margin-top: 30px;
        margin-left: 75px;
        padding: 20px;
        border-radius: 5px;
        border: 1px solid #aaa;
        text-align: center;

    }
    .recharge .cancels{
        width: 580px;
        text-align: right;
        float: left;
        margin-top: 40px;
        font-size: 20px;
    }
    .recharge .cancels:hover{
        color:#47A1F8;
        cursor:pointer;
    }
    .recharge .xz:hover{
        border:1px solid #FF8D6D;
        cursor:pointer;
    }
</style>
<script type="text/javascript" src="/cen/bootstrap.min.js"></script>
</head>
<body>
@include("template/default/header")
	<div class="container">
	    <div class="row">
            <div class="recharge">
                <div class="title">充值会员</div>
                <div class="prompt">
                    <p>请在下方选择适合自己的付款方式哦~</p>
                    <p>如果开通异常,请及时联系客服!</p>
                </div>
                <ul>
                    <li class="xz" onclick="pay('alipay')">支付宝</li>
                    <li class="xz" onclick="pay('wxpay')">微信</li>
                    <li class="xz" onclick="pay('qqpay')">QQ钱包</li>
                </ul>
                <div class="cancels" onclick="cancels();">取消</div>
            </div>
        <div class="myui-panel_hd"><div style="height:70px;"></div></div>
	    	<div class="hy-member-user hy-layout clearfix" style="background-image:url(/images/a.jpg);">
    			<div class="item">
    				<dl class="margin-0 clearfix">
    					<dt><span class="user" id="pic" style="cursor:pointer;background-image:url(@if($res->pic) {{$res->pic}} @else /images/timg.jfif @endif)"></span></dt>
    					<dd>
    						<span class="name">{{$res->user}}<span>
    						<span class="group">@if($res->level == '1')普通会员@elseif($res->level == '2')铜牌会员@elseif($res->level == '3')银牌会员@elseif($res->level == '4')金牌会员@elseif($res->level == '5')钻石会员@endif<span>
    					</dd>
    			   </dl>
    			</div>
	    	</div>
		    <div class="hy-member hy-layout clearfix" style="margin-top:5px;padding:0 15px;">
                <div class="layui-tab layui-tab-brief">
                    <ul class="layui-tab-title">
						<li class="layui-this"><a href="javascript:void(0)" title="播放线路">基本资料</a></li>
						<li><a href="javascript:void(0)"title="修改密保邮箱">修改密保邮箱</a></li>
						<li><a href="javascript:void(0)"title="修改密保手机">修改密保手机</a></li>
						<li><a href="javascript:void(0)"title="我的收藏">我的收藏</a></li>
						<li><a href="javascript:void(0)" title="购买记录">购买记录</a></li>
						<li><a href="/" title="返回首页">返回首页</a></li>
						<li><a href="/outLogin" title="退出">退出</a></li>
					</ul>
                    <div class="layui-tab-content">
                        <div class="layui-tab-item layui-show"><div style="height:500px;">
                            <div class="col-md-9 col-sm-12 col-xs-12">
                                <ul class="user">
                                    <li><span class="text-muted">会员编号：</span>{{$res->id}}</li>
                                    <li id="resname"><span class="text-muted">您的昵称：</span><a>{{$res->name}}</a><span style="color:#aaa;font-size:14px;">(双击可修改昵称)</span></li>
                                    <li><span class="text-muted">您的账户：</span>{{$res->user}}</li>
                                    <li><span class="text-muted">您的邮箱：</span>{{$res->email}}</li>
                                    <li><span class="text-muted">联系电话：</span>{{$res->phone}}</li>
                                    <li><span class="text-muted">注册时间：</span>{{date('Y-m-d H:i:s',$res->time)}}</li>
                                    <li><span class="text-muted">用户级别：</span>@if($res->level == '1')普通会员@elseif($res->level == '2')铜牌会员@elseif($res->level == '3')银牌会员@elseif($res->level == '4')金牌会员@elseif($res->level == '5')钻石会员@endif</li>
                                    <li><span class="text-muted">升级会员: </span><input type="button" name="level" lid="2" class="btn btn-default btn-sm opacity" value="铜牌会员 ({{LEVEL2}}元/永久)" @if($res->level > '1') disabled="disabled" @endif> <input type="button" name="level" lid="3" class="btn btn-default btn-sm" value="银牌会员 ({{LEVEL3}}元/永久)" @if($res->level > '2') disabled="disabled" @endif> <input type="button" name="level" lid="4" class="btn btn-default btn-sm" value="金牌会员 ({{LEVEL4}}元/永久)" @if($res->level > '3') disabled="disabled" @endif> <input type="button" name="level"  lid="5" class="btn btn-default btn-sm" value="钻石会员 ({{LEVEL5}}元/永久)" @if($res->level > '4') disabled="disabled" @endif></li>
                                </ul>
                            </div>
                        </div></div>
                        <div class="layui-tab-item"><div style="height:500px;padding:50px 10px;">
                            <form class="layui-form" action="">
                            <div class="layui-form-item layui-form-pane">
                                <label class="layui-form-label">登录密码</label>
                                <div class="layui-input-block">
                                <input type="password" name="passwde" required  lay-verify="required" placeholder="请输入原密码" autocomplete="off" class="layui-input">
                                </div>
                            </div>
                            <div class="layui-form-item layui-form-pane">
                                <label class="layui-form-label">新密保邮箱</label>
                                <div class="layui-input-block">
                                <input type="text" name="email" required  lay-verify="required|email" placeholder="请输入新密保邮箱" autocomplete="off" class="layui-input">
                                </div>
                            </div>
                            <div class="layui-form-item  layui-form-pane">
                            <label class="layui-form-label"><button class="layui-btn  layui-btn-normal" id="sandemail" onclick="sendemail()" type="button" style="position:relative;top:-9px;left:-14px">发送验证码</button></label>
                                <div class="layui-input-block">
                                <input type="text" name="email_code" required  lay-verify="required" placeholder="请输入验证码" autocomplete="off" class="layui-input">
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <div class="layui-input-block">
                                <button class="layui-btn" lay-submit lay-filter="formemail" style="float:right;margin-right:30px">立即修改</button>
                                </div>
                            </div>
                            </form>
                        </div></div>
                        <div class="layui-tab-item"><div style="height:500px;padding:50px 10px;">
                            <form class="layui-form" action="">
                            <div class="layui-form-item layui-form-pane">
                                <label class="layui-form-label">登录密码</label>
                                <div class="layui-input-block">
                                <input type="password" name="passwdp" required  lay-verify="required" placeholder="请输入原密码" autocomplete="off" class="layui-input">
                                </div>
                            </div>
                            <div class="layui-form-item layui-form-pane">
                                <label class="layui-form-label">新密保手机</label>
                                <div class="layui-input-block">
                                <input type="text" name="phone" required  lay-verify="required|phone" placeholder="请输入新密保手机" autocomplete="off" class="layui-input">
                                </div>
                            </div>
                            <div class="layui-form-item  layui-form-pane">
                            <label class="layui-form-label"><button class="layui-btn layui-btn-normal" id="sandphone" onclick="sendphone()" type="button" style="position:relative;top:-9px;left:-14px">发送验证码</button></label>
                                <div class="layui-input-block">
                                <input type="text" name="phone_code" required  lay-verify="required" placeholder="请输入验证码" autocomplete="off" class="layui-input">
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <div class="layui-input-block">
                                <button class="layui-btn" lay-submit lay-filter="formphone" style="float:right;margin-right:30px">立即修改</button>
                                </div>
                            </div>
                            </form>
                        </div></div>
                        <div class="layui-tab-item"><div style="height:500px;">
                        <table class="layui-table" style="text-align:center;">
                            <colgroup>
                                <col width="40%">
                                <col width="35%">
                                <col>
                            </colgroup>
                            <thead>
                                <tr>
                                <th style="text-align:center;">影片名</th>
                                <th style="text-align:center;">收藏时间</th>
                                <th style="text-align:center;">操作</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($collection as $v)
                                <tr id="{{$v->id}}">
                                <td><a href="{{$v->url}}" target="_blank">{{$v->d_name}}</td>
                                <td>{{date('Y-m-d H:i:s',$v->time)}}</td>
                                <td><i class="layui-icon layui-icon-star-fill" title="取消收藏" style="cursor:pointer" onclick="cancel('{{$v->id}}')"></i></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        </div></div>
                        <div class="layui-tab-item"><div style="height:500px;">
                        <table class="layui-table" style="text-align:center;">
                            <colgroup>
                                <col width="20%">
                                <col width="15%">
                                <col width="15%">
                                <col width="15%">
                                <col width="20%">
                                <col>
                            </colgroup>
                            <thead>
                                <tr>
                                <th style="text-align:center;">订单号</th>
                                <th style="text-align:center;">消费金额</th>
                                <th style="text-align:center;">购买内容</th>
                                <th style="text-align:center;">订单状态</th>
                                <th style="text-align:center;">订单时间</th>
                                <th style="text-align:center;">操作</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($recharge as $v)
                                <tr id="re{{$v->id}}">
                                <td>{{$v->code}}</td>
                                <td>¥{{$v->money}}</td>
                                <td>{{$v->integral}}</td>
                                <td>
                                @if($v->status == 0)
                                {{-- <a href ="/recharge-pay/{{$v->id}}" onclick="goPay();">未付款</a> --}}
                                未付款
                                @elseif($v->status == 1)
                                已付款
                                @endif
                                </td>
                                <td>{{$v->mtime?date('Y-m-d H:i:s',$v->mtime):date('Y-m-d H:i:s',$v->time)}}</td>
                                <td><i class="layui-icon layui-icon-delete" title="删除" style="cursor:pointer" onclick="deletee('{{$v->id}}')"></i></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        </div></div>
                    </div>
                </div>
		    </div>
	    </div>
	</div>
	<div class="container">
		<div class="row">
			<div class="hy-footer clearfix">
				<p class="text-muted">Copyright ©2020 eqcms</p>
			</div>
		</div>
	</div>
</body>
</html>
<script>
    var token = "{{csrf_token()}}";
    var uid = "{{$res->id}}";
//注意：选项卡 依赖 element 模块，否则无法进行功能性操作
layui.use(['element','form','upload'], function(){
  var element = layui.element;
  var form = layui.form;
  var upload = layui.upload;

  //执行文件上传实例
  var uploadInst = upload.render({
    elem: '#pic' //绑定元素
    ,url: '/uploadpic' //上传接口
    ,size: '1024' //上传大小
    ,data:{'_token':token,'uid':uid}
    ,accept:'images'
    ,before: function(obj){ //obj参数包含的信息，跟 choose回调完全一致，可参见上文。
        layer.load(); //上传loading
    }
    ,choose: function(obj){
        //将每次选择的文件追加到文件队列
        var files = obj.pushFile();
        //预读本地文件，如果是多文件，则会遍历。(不支持ie8/9)
        obj.preview(function(index, file, result){
        // console.log(index); //得到文件索引
        // console.log(file); //得到文件对象
        // console.log(result); //得到文件base64编码，比如图片
        //obj.resetFile(index, file, 'logo.jpg'); //重命名文件名，layui 2.3.0 开始新增
        var picult = 'cursor:pointer;background-image:url('+result+')';
        $("#pic").attr('style',picult);
        });
    }
    ,done: function(res){
        //上传完毕回调
        if(res==1){
                layer.msg("上传头像成功",{icon:1});
            }else{
                layer.msg("上传头像失败",{icon:2});
            }
            layer.closeAll('loading'); //关闭loading
    }
    ,error: function(){
      //请求异常回调
       layer.closeAll('loading'); //关闭loading
    }
  });
  //邮件
    form.on('submit(formemail)',function(data){
        $.post("{{url('centeremail')}}",{'_token':"{{csrf_token()}}",'passwd':data.field.passwde,'email':data.field.email,'email_code':data.field.email_code,'id':"{{$res->id}}"},function(data){
            if (data == '1') {
                alert('修改成功');
                location.reload(true);
            } else if (data == '0') {
                alert('修改失败');
                $('input[name=passwde]').val('');
                $('input[name=email]').val('');
                $('input[name=email_code]').val('');
            } else if (data == '2') {
                alert('验证码不正确');
                $('input[name=email_code]').val('');
            } else if (data == '3') {
                alert('密码错误');
                $('input[name=passwde]').val('');
                $('input[name=email_code]').val('');
            }
        });
        return false;
    });
  //手机
    form.on('submit(formphone)',function(data){
        $.post("{{url('centerphone')}}",{'_token':"{{csrf_token()}}",'passwd':data.field.passwdp,'phone':data.field.phone,'phone_code':data.field.phone_code,'id':"{{$res->id}}"},function(data){
            if (data == '1') {
                alert('修改成功');
                location.reload(true);
            } else if (data == '0') {
                alert('修改失败');
                $('input[name=passwdp]').val('');
                $('input[name=phone]').val('');
                $('input[name=phone_code]').val('');
            } else if (data == '2') {
                alert('验证码不正确');
                $('input[name=phone_code]').val('');
            } else if (data == '3') {
                alert('密码错误');
                $('input[name=passwdp]').val('');
                $('input[name=phone_code]').val('');
            }
        });
        return false;
    });
});
function sendemail(){
    var email = $("input[name=email]").val();
    var re = new RegExp("^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$");
    if(re.test(email)){
        var but = document.getElementById('sandemail');
            but.innerHTML = '('+59+')  请稍等';
            but.setAttribute("class",'layui-btn layui-btn-disabled');
            but.disabled = true;
            var i = 59;
            var button = setInterval(function(){
                i--;
                but.innerHTML = '('+i+')  请稍等';
                if (i == 0){
                    clearInterval(button);
                    but.innerHTML = '发送验证码';
                    but.setAttribute("class",'layui-btn layui-btn-normal');
                    but.disabled = false;
                }
            },1000)
    $.post("/center-email",{'_token':"{{csrf_token()}}",'email':email},function(data){
        if (data == '1') {
            alert('邮件已发送');
        } else if (data == '0') {
            alert('邮件发送失败');
        }
    });
    }else{
        alert('邮箱格式不正确');
    }
}
function sendphone(){
    var phone = $("input[name=phone]").val();
    var re = new RegExp("^[1]([3-9])[0-9]{9}$");
    if(re.test(phone)){
        var butt = document.getElementById('sandphone');
            butt.innerHTML = '('+59+')  请稍等';
            butt.setAttribute("class",'layui-btn layui-btn-disabled');
            butt.disabled = true;
            var i = 59;
            var button = setInterval(function(){
                i--;
                butt.innerHTML = '('+i+')  请稍等';
                if (i == 0){
                    clearInterval(button);
                    butt.innerHTML = '发送验证码';
                    butt.setAttribute("class",'layui-btn layui-btn-normal');
                    butt.disabled = false;
                }
            },1000)
    $.post("/center-phone",{'_token':"{{csrf_token()}}",'phone':phone},function(data){
        if (data == '1') {
            alert('短信发送成功');
        } else if (data == '0') {
            alert('短信发送失败');
        }
    });
    } else {
        alert('手机号不正确');
    }
}
$('#resname').dblclick(function(){
    var com = $('#resname a').html();
    if (com != undefined) {
    $(this).html("<span class='text-muted'>您的昵称：</span><input name=username onblur='usname()' style='height:30px;border:1px solid #e6e6e6;border-radius:4px;' type='text' value="+com+">");
    }
});
function usname(){
    var id = {{$res->id}};
    var com = $('input[name=username]').val();
    if (com.length < 10) {
    $('#resname').html("<span class='text-muted'>您的昵称：</span><a>"+com+"</a>");
    $.post("/centername",{'id':id,'name':com,'_token':'{{csrf_token()}}'},function(data){});
    } else {
        alert('昵称超出字数限制');
    }
}
$("input[name=level]").click(function(){
    lid = $(this).attr('lid');
    $('.recharge').css('display','block');
})

function pay(type) {
    location.href="/center-pay/"+lid+"-"+type;
}

function deletee(id){
    $.post("/recharge-deletee",{'id':id,'_token':'{{csrf_token()}}'},function(data){
        if(data == '1'){
            $('#re'+id).empty();
        }else if(data == '0'){
            alert('好像出了什么问题失败了..');
        }
    });
}
function cancel(id){
    $('#'+id+' '+'i').attr('class','layui-icon layui-icon-star');
    $.post("/collection-cancel",{'id':id,'_token':'{{csrf_token()}}'},function(data){
        if(data == '1'){
            $('#'+id).empty();
        }else if(data == '0'){
            alert('好像出了什么问题失败了..');
        }
    });
}
function cancels() {
    $(".recharge").css('display','none');
}

</script>
