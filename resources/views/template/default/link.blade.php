

<html>
    <head>
        <title>申请友情链接</title>
        <script src="/js/jquery.min.js"></script>
        <script src="/lib/layui/layui.js" charset="utf-8"></script>
        <link rel="stylesheet" href="/css/font.css">
        <link rel="stylesheet" href="/css/xadmin.css">
        <script src="/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/js/xadmin.js"></script>
        <script>
			function checkReportErr(){if (document.getElementById('author').value.length<1){}; if (document.getElementById('errtxt').value.length<1){alert('请填写您的网站信息,如网站域名/网站链接');return false;}}
        </script>
		<style>
		h2,p{padding:0; margin:0;}
		h2{font-size:14px;height:25px;color:#027DB9;line-height:25px;background:#B4E5FE;text-align:center;margin-bottom:10px;}
		.err{width:380px;height:215px;background:#F5FBFE;border:1px solid #B0DCF5;margin: 0 auto}
		.err p{margin-left:10px;}
		</style>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    </head>
    <body  style="font-size:12px;background-color:#D7EDFA;height:22px;line-height:22px;">
        <form id="reporterr" action="/doLinks" method="post" class="layui-form layui-col-space5" onSubmit="return checkReportErr()">
        {{csrf_field()}}
            <div class="err">
			<h2>请填写您的网站信息,并将我们的网站添加到贵站友链中</h2>
                <p style="padding-bottom:5px;">
                    网站名:<input type="text" lay-verify="required" id="author" name="name"  size="30"><font color="#FF0000">*必填</font>
                </p>
                <p style="padding-bottom:5px;">
                    网站地址:<input type="text" lay-verify="required" id="author" name="url"  size="30"><font color="#FF0000">*必填</font>
                </p>
                <input type="hidden" name="session_code" value="link_code">
                <p>
                    网站描述:<textarea id="errtxt"  name="content" lay-verify="required" style="width:270px;height:88px" rows=5 cols=30></textarea><font color="#FF0000">*必填</font>
                </p>
                <p>
                    验证码：<input name="code" type="text" lay-verify="required" id="vdcode" style="width:60px;text-transform:uppercase;" class="text" tabindex="3"/>
                    <img id="vdimgck" src="/code/link_code" alt="看不清？点击更换"  align="absmiddle"  style="cursor:pointer" height="27" onClick="this.src=this.src+'?get=' + new Date()"/>
                    <span class="red"></span>
                </p>
                <br>
                <input type="submit" value="申请友链" lay-filter="add" style="margin:5px 0 0 130px;">
            </div>
        </form>
    </body>
</html>
