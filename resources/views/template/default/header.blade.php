<header class="myui-header__top clearfix" id="header-top">
   <div class="container">
    <div class="row">
     <div class="myui-header_bd clearfix">
      <div class="myui-header__logo">
       <a class="logo" href="/"> <img class="img-responsive hidden-xs" style="width:120px;height:40px;margin-top:10px;" src="{{S_LOGO}}"/> <img style="width:120px;height:40px;margin-top:10px;"  class="img-responsive visible-xs" style="width:120px;height:60px;" src="{{S_LOGO}}" /> </a>
      </div>
      <ul class="myui-header__menu nav-menu">
       <li class="hidden-xs"><a href="/">首页</a></li>
        @foreach($header as $v)
        @if($v->p_id==0)
       <li class="hidden-sm hidden-xs" style="cursor:pointer;"><a href="/list/{{$v->id}}">{{$v->t_name}}</a></li>
        @endif
	   @endforeach
       <li class="dropdown-hover"> <a href="javascript:;">更多 <i class="fa fa-angle-down"></i></a>
        <div class="dropdown-box bottom fadeInDown clearfix">
         <ul class="item nav-list clearfix">
          <li class="col-lg-5 col-md-5 col-sm-5 col-xs-3"><a class="btn btn-sm btn-block btn-warm" href="/">首页</a></li>
          @foreach($header as $v1)
          @if($v1->p_id!=0)
          <li class="col-lg-5 col-md-5 col-sm-5 col-xs-3"><a class="btn btn-sm btn-block btn-" href="/list/{{$v1->id}}">{{$v1->t_name}}</a></li>
          @endif
	      @endforeach
         </ul>
        </div> </li>
      </ul>
      <script type="text/javascript" src="/{{TEM}}js/autocomplete.js"></script>
      <div class="myui-header__search search-box">
       <form id="search" name="search" method="get" target="_blank" action="/search" onsubmit="return qrsearch();">
        <input type="text" id="wd" name="terms" class="search_wd form-control" value="" placeholder="输入影片关键词..." autocomplete="off" style=" padding-left: 12px; " />
        <button class="submit search_submit" id="searchbutton" type="submit"><i class="fa fa-search"></i></button>
       </form>
       <div id="word" class="autocomplete-suggestions active hidden-xs" style="display: none;"></div>
       <a class="search-close visible-xs" href="javascript:;"><i class="fa fa-close"></i></a>
       <div class="search-dropdown-hot dropdown-box bottom fadeInDown">
        <div class="item">
         <p class="text-muted"> 热门搜索 </p>
         @foreach ($dataTop as $k=>$v)
            <p><a class="text-333" href="/details/{{$v->id}}.html" title="{{$v->d_name}}"><span class="badge badge-{{($k>2)?'':$k+1}}">{{$k+1}}</span> {{$v->d_name}}</a></p>
         @endforeach

        </div>
       </div>
      </div>
      <ul class="myui-header__user">
       <li class="visible-xs"> <a class="open-search" href="javascript:;"><i class="fa fa-search"></i></a> </li>
       <li class="dropdown-hover"> <a href="javascript:;" title="播放记录"> <i class="fa fa-clock-o"></i></a>
        <div class="dropdown-box fadeInDown">
         <div class="item clearfix">
          <p class="text-muted"> 播放记录 </p>
          <div class="history-list clearfix">
           <script type="text/javascript">
										var history_get = MyTheme.Cookie.Get("history");
										if(history_get){
										    var json=eval("("+history_get+")");
										    for(i=0;i<json.length;i++){
										        document.write("<p><a class='text-333' href='"+json[i].link+"' title='"+json[i].name+"'><span class='pull-right text-red'>"+json[i].part+"</span>"+json[i].name+"</a></p>");
										    }
										} else {
											document.write("<p style='padding: 80px 0; text-align: center'>您还没有看过影片哦</p>");
									    }
									</script>
          </div>
         </div>
        </div>
      </li>
       <li><a href="/gbook" title="留言反馈"><i class="fa fa-commenting"></i></a></li>
        <li class="dropdown-hover"> <a href="/center">
         <i class="fa fa-user"></i></a>
          </div>
         </div>
        </div>
      </li>

      </ul>
     </div>
    </div>
   </div>
  </header>
