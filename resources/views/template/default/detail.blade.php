<!DOCTYPE html>
<html>
    <head>
        <title>{{S_NAME}} - {{$data->d_name}}_详情列表_最新电影电视剧分享!</title>
        <!-- 引入头样式 -->
        @include("template/default/head")
        <!-- 引入头样式 -->
        <link rel="stylesheet" href="/{{TEM}}images/stui_block_color.css" type="text/css" />
		<link rel="stylesheet" href="/{{TEM}}images/stui_block.css" type="text/css" />
    </head>
<body>
    <!-- 引入导航栏样式 -->
    @include("template/default/header")
    <!-- 引入导航栏样式 -->
    <div class="container">
        <div class="row">
        	<div class="myui-panel_hd"><div style="height:70px;"></div></div>
            <div class="col-md-wide-7 col-xs-1 padding-0">
            	<!-- 详细信息-->
<div class="myui-panel myui-panel-bg clearfix">
	<div class="myui-panel-box clearfix">

		<div class="myui-rumbs col-pd clearfix">
    		<span class="text-muted">当前位置：</span>
    		<a href="/">首页</a> <i class="fa fa-angle-right text-muted"></i>
    		<a href="/list/{{$data->t_id}}">{{$data->typename}}</a> <i class="fa fa-angle-right text-muted"></i>
    		<span class="text-muted">{{$data->d_name}}</span>
    	</div>

		<div class="col-xs-1">
			<div class="myui-content__thumb">
				<a class="myui-vodlist__thumb img-lg-220 img-xs-130 picture" href="/details/{{$data->did}}.html" title="{{$data->d_name}}">
					<img class="lazyload" src="//{{S_REALM}}/template/default/images/a.gif" data-original="{{$data->img}}"/>
					<span class="play active hidden-xs"></span>
                                    </a>
			</div>
			<div class="myui-content__detail">

				<h1 class="title">{{$data->d_name}}</h1>

				<!-- 评分 -->
			    <div class="score">
			    	<ul class="rating">
				        <script type="text/javascript">markVideo({{$data->did}},0,0,0,5,1);markVideo2({{$data->did}},1,5);</script>
				    </ul>
				</div>

				<p class="data">
					<span class="text-muted">分类：</span><a href="/list/{{$data->t_id}}">{{$data->typename}}</a>
					<span class="split-line"></span>
					<span class="text-muted hidden-xs">地区：</span><a href="/list/{{$data->t_id}}?region={{$data->region}}">{{$data->region}}</a>
					<span class="split-line"></span>
					<span class="text-muted hidden-xs">年份：</span><a href="/list/{{$data->t_id}}?year={{$data->year}}">{{$data->year}}	</a>
				</p>
                <p class="data"><span class="text-muted">主演：</span>
                    @foreach ($strring as $v)
                        <a href='/search?terms={{$v}}'>{{$v}}</a>&nbsp;&nbsp;
                    @endforeach
                </p>
                <p class="data"><span class="text-muted">导演：</span>
                    <a href='/search?terms={{$data->director}}'>{{$data->director}}</a>&nbsp;&nbsp;
                </p>
				<p class="data hidden-sm hidden-xs"><span class="text-muted">更新：</span>{{date('Y-m-d H:i:s',$data->time)}}</p>
				<p class="desc text-collapse hidden-xs">
                    <span class="left text-muted">简介：</span>
					<span class="sketch">{{mb_substr($data->plot,0,30)}}...</span>
					<span class="data" style="display: none;">{{$data->plot}}</span>
					<a class="details" href="javascript:;">详情 <i class="fa fa-angle-down"></i></a>
				</p>
				<div class="operate clearfix">

										<a id="abcd" class="btn btn-warm" href="javascript:void(0)" ><i class="fa fa-play"></i> 立即播放</a>
										<div  class="btn btn-default favorite"><i class="fa fa-star"></i> <a  href="javascript:viod()" onclick="reportErr({{$data->did}})">报错</a></div>
										<a class="btn btn-default dropdown-hover hidden-xs" href="javascript:;">
						<div class="dropdown-box top fadeInDown clearfix">
							<div class="item text-center">
<div class="qrcode-box active" style="background-color: #ffffff;">
	<img class="icon" src="{{TWO_IMG}}" width="196" />
</div>
<p>扫一扫用手机观看</p>
							</div>
						</div>
						<i class="fa fa-qrcode font14"></i> 手机观看
					</a>

				</div>
			</div>
		</div>
	</div>
</div>
<div class="myui-panel myui-panel-bg clearfix">
			<div class="myui-panel-box clearfix">
				<div class="myui-panel_hd">
					<div class="myui-panel__head active bottom-line clearfix">
						<a class="more sort-button pull-right" href="javascript:;"><i class="fa fa-sort-amount-asc"></i> 排序</a>						<h3 class="title">
							播放地址						</h3>
						<ul id="abcde" class="nav nav-tabs active">
                            @foreach ($paddress as $k=>$p)
                                <li><a href="#playlist{{$p->id}}" data-toggle="tab">线路{{$k+1}}</a></li>
                            @endforeach
						</ul>
					</div>
				</div>
				<div class="tab-content myui-panel_bd">
                    @foreach ($playurl as $k=>$j)
                        <div id="playlist{{$k}}" class="tab-pane fade in clearfix">
                            <ul class="stui-content__playlist  sort-list column8 clearfix" id="abc">
                                @foreach ($j as $s=>$m)
                                    <li><a title='{{explode('$',$m)[0]}}' href='/play/{{$data->did}}-{{$k}}-{{$s}}.html' target="_self">{{explode('$',$m)[0]}}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    @endforeach
</div>
</div>
</div>
		<script type="text/javascript">
			$(".tab-pane:first,.nav-tabs li:first").addClass("active");

			resetUrl();

			$("#abcde").click(function(){
				$("#abcd").html('<i class="fa fa-play"></i> 请选集播放');
				$('#abcd').removeAttr('href');
				$('#abcd').click(function(){
					scrollTo('#abcde',300);
				})

			})

			function resetUrl(){
				//重新设置点击播放键默认跳转，避免出现404
				var fistBf=document.getElementById("abc");
				var fistChild=fistBf.firstElementChild;
				var url=fistChild.firstElementChild.getAttribute("href");
				document.getElementById('abcd').setAttribute('href',url);
			}

			//默认点击滑动到指定距离
			function scrollTo(ele, speed){
				if(!speed) speed = 300;
				if(!ele){
					$("html,body").animate({scrollTop:0},speed);
				}else{
					if(ele.length>0) $("html,body").animate({scrollTop:$(ele).offset().top},speed);
				}
				return false;
			}
		</script>

				<div class="myui-panel myui-panel-bg clearfix">
	<div class="myui-panel-box clearfix">
		<div class="myui-panel_hd">
			<div class="myui-panel__head clearfix">
				<h3 class="title">
					猜你喜欢				</h3>
				<a class="more text-muted" href="/frim/index13.html">更多  <i class="fa fa-angle-right"></i></a>
			</div>
		</div>
		<div class="myui-panel_bd">
			<ul class="myui-vodlist__bd clearfix">
                @foreach ($like as $l)
								<li class="col-lg- col-md-6 col-sm-4 col-xs-3">
						<div class="myui-vodlist__box">
			<a class="myui-vodlist__thumb lazyload" href="/details/{{$l->id}}.html" title="{{$l->d_name}}" data-original="{{$l->img}}">
				<span class="play hidden-xs"></span>
			<span class="pic-tag pic-tag-top" style="background-color: #5bb7fe;">
            {{round($l->score/$l->snum,1)}}分
			</span>
<span class="pic-text text-right">
		{{$l->note}}</span>
	</a>
	<div class="myui-vodlist__detail">
		<h4 class="title text-overflow"><a href="/details/{{$l->id}}.html" title="{{$l->d_name}}">{{$l->d_name}}</a></h4>
		<p class="text text-overflow text-muted hidden-xs">
		{{$l->year}}/{{$l->region}}/{{$l->typename}}</p>
	</div>
</div>
                </li>
                @endforeach


							</ul>
		</div>
	</div>
</div>
				<div class="myui-panel myui-panel-bg clearfix">
	<div class="myui-panel-box clearfix">
		<div class="myui-panel_bd">
			<div class="myui-panel_hd">
				<div class="myui-panel__head clearfix">
					<h3 class="title">
						影片评论
					</h3>
				</div>
			</div>
			<div class="myui-panel_bd" id="comments">
<div  id="comment_list">评论加载中...</div>
			</div>
		</div>
	</div>
</div>
			</div>
                        <div class="col-md-wide-3 col-xs-1 myui-sidebar hidden-sm hidden-xs" >
				<div class="myui-panel myui-panel-bg clearfix">
	<div class="myui-panel-box clearfix">
		<div class="myui-panel_hd">
			<div class="myui-panel__head clearfix">
				<h3 class="title">
					今日新增				</h3>
			</div>
		</div>
		<div class="myui-panel_bd">

			<ul class="myui-vodlist__media active col-pd clearfix">
                @if (count($day)>0)
                    <li>
	<div class="thumb">
		<a class="myui-vodlist__thumb img-xs-70" style="background: url({{$day[0]->img}});" href="/details/{{$day[0]->id}}.html" title="{{$day[0]->d_name}}" data-original="{{$day[0]->img}}"></a>
	</div>
	<div class="detail detail-side">
		<h4 class="title"><a href="/details/{{$day[0]->id}}.html"><i class="fa fa-angle-right text-muted pull-right"></i> {{$day[0]->d_name}}</a></h4>
		<p class="font-12"><span class="text-muted">类型：</span>{{$day[0]->typename}} <span class="text-muted">地区：</span>大陆</p>
		<p class="font-12 margin-0"><span class="text-muted">状态/集数：</span>{{$day[0]->note}}</p>
	</div>
</li>
                @endif


                            </ul>


			<ul class="myui-vodlist__text col-pd clearfix">
                @if (count($day)>0)
                    @foreach ($day as $k=>$v)
                    @if ($k>0)
                        <li>
                        <a href="/details/{{$v->id}}.html" title="{{$v->d_name}}">
                            <span class="pull-right  text-muted hidden-md" style="color: ;">
                            {{$v->note}}</span>
                            @if ($k==1)
                                <span class="badge badge-1">{{$k}}</span>{{mb_substr($v->d_name,0,10)}}</a>
                            @elseif($k==2)
                                <span class="badge badge-2">{{$k}}</span>{{mb_substr($v->d_name,0,10)}}</a>
                            @elseif($k==3)
                                <span class="badge badge-3">{{$k}}</span>{{mb_substr($v->d_name,0,10)}}</a>
                            @else
                                <span class="badge">{{$k}}</span>{{mb_substr($v->d_name,0,10)}}</a>
                            @endif

                    </li>
                    @endif
                    @endforeach
                @endif
            </ul>
		</div>
	</div>
</div>

				<div class="myui-panel myui-panel-bg clearfix">
	<div class="myui-panel-box clearfix">
		<div class="myui-panel_hd">
			<div class="myui-panel__head clearfix">
				<h3 class="title">
					正在热播				</h3>
			</div>
		</div>
		<div class="myui-panel_bd">
			<ul class="myui-vodlist__media active col-pd clearfix">
@foreach ($hot as $h)
									<li>
	<div class="thumb">
		<a class="myui-vodlist__thumb wide img-xs-120" style="background: url({{$h->img}});" href="/details/{{$h->id}}.html" title="{{$h->d_name}}"></a>
	</div>
	<div class="detail detail-side">
		<h4 class="title" style="margin-top: 0;"><a href="/details/{{$h->id}}.html">{{mb_substr($h->d_name,0,10)}}</a></h4>
		<p class="font-12 text-muted"><i class="fa fa-youtube-play"></i> {{round($h->score/$h->snum,1)}}分</p>
	</div>
</li>
@endforeach

							</ul>
		</div>
	</div>
</div>
			</div>
        </div>
    </div>
 <!-- 引入尾部样式 -->
 		@include("template/default/foot")
</body>
</html>
@if (Y_SWITCH==1)
	<script>
window.onload = function(){
	$.post("/comments",{"_token":"{{csrf_token()}}","did":"{{$data->did}}"},function(data){
		$("#comments").html(data);
	})
}
</script>
@endif

