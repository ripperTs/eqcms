

<html>
    <head>
        <title>影片报错</title>
        <script src="/js/jquery.min.js"></script>
        <script src="/lib/layui/layui.js" charset="utf-8"></script>
        <link rel="stylesheet" href="/css/font.css">
        <link rel="stylesheet" href="/css/xadmin.css">
        <script src="/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/js/xadmin.js"></script>
        <script>
			function checkReportErr(){if (document.getElementById('author').value.length<1){alert('请填写联系邮箱');return false;}; if (document.getElementById('errtxt').value.length<1){alert('请填写报错内容');return false;}}
        </script>
		<style>
		h2,p{padding:0; margin:0;}
		h2{font-size:14px;height:25px;color:#027DB9;line-height:25px;background:#B4E5FE;text-align:center;margin-bottom:10px;}
		.err{width:380px;height:185px;background:#F5FBFE;border:1px solid #B0DCF5;margin: 0 auto}
		.err p{margin-left:10px;}
		</style>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    </head>
    <body  style="font-size:12px;background-color:#D7EDFA;height:22px;line-height:22px;">
        <form id="reporterr" action="/doError" method="post" class="layui-form layui-col-space5" onSubmit="return checkReportErr()">
        {{csrf_field()}}
            <div class="err">
			<h2>请填写您遇到的问题,我们会在第一时间处理</h2>
               <p style="padding-bottom:5px;">联系邮箱:<input type="text" lay-verify="required" id="author" name="email"  size="30"><font color="#FF0000">*必填</font></p>
                <input type="hidden" name="did" value="{{$id}}">
                <input type="hidden" name="session_code" value="error_code">
                <p>问题描述:<textarea id="errtxt"  name="content" lay-verify="required" style="width:270px;height:88px" rows=5 cols=30></textarea>
                <font color="#FF0000">*必填</font></p>
                <p>验证码：<input name="code" type="text" lay-verify="required" id="vdcode" style="width:60px;text-transform:uppercase;" class="text" tabindex="3"/> <img id="vdimgck" src="/code/error_code" alt="看不清？点击更换"  align="absmiddle"  style="cursor:pointer" height="27" onClick="this.src=this.src+'?get=' + new Date()"/><span class="red"></span></p>
                <input type="submit" value="影片报错" lay-filter="add" style="margin:5px 0 0 130px;">
            </div>
        </form>
    </body>
</html>
