
<!DOCTYPE html>
<html>
@include("template/default/head")
<body>
@include("template/default/header")
	<div class="wrap">
	    <div class="container min">
	        <div class="row">
	        	<div class="myui-panel_hd"><div style="height:70px;"></div></div>
			   	<div class="myui-panel myui-panel-bg clearfix">
					<div class="myui-panel-box clearfix">
						<div class="myui-panel_hd">
							<div class="myui-panel__head active bottom-line clearfix">
								<h3 class="title">
									与 <span class="text-red"></span> 相关的搜索结果
								</h3>
							</div>
						</div>
						@if($judge==0)
							<div class="myui-panel_bd col-pd clearfix">
							<ul class="myui-vodlist__media clearfix" id="searchList">
 对不起，没有找到任何记录,您可以<a target='_blank' href='/gbook'><font color='red'><b>在此留言</b></font></a>给我们。
															</ul>
						</div>
						@endif

						<div class="myui-panel_bd col-pd clearfix">
							<ul class="myui-vodlist__media clearfix" id="searchList">
							@foreach($row as $k=>$v)
								<li class="active  clearfix">
									<div class="thumb">
										<a class="myui-vodlist__thumb img-lg-150 img-xs-100 lazyload" href="/details/{{$v->d_id}}.html" title="{{$v->d_name}}" data-original="{{$v->img}}">
											<span class="play hidden-xs"></span>
												<span class="pic-tag pic-tag-top" style="background-color: #5bb7fe;">{{round($v->score/$v->snum,1)}}分</span>
												<span class="pic-text text-right">HD</span>
										</a>
									</div>
									<div class="detail">
								    	<h4 class="title"><a href="/details/{{$v->id}}.html">{{$v->d_name}}</a></h4>
								    	<p><span class="text-muted">导演：</span>
										@foreach($dir[$k] as $v2)
										<a href='/search?terms={{$v2}}'>{{$v2}} </a>
										@endforeach
										</p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<p><span class="text-muted">主演：</span>
											@foreach($arr[$k] as $v1)
											<a href='/search?terms={{$v1}}'>{{$v1}}</a>
											@endforeach
										&nbsp;</p>&nbsp;&nbsp;
										<p><span class="text-muted">分类：</span><a href='/list/{{$v->tid}}'>{{$v->typename}}</a><span class="split-line"></span><span class="text-muted hidden-xs">地区：</span><a href='/list/{{$v->tid}}?region={{$v->region}}'>{{$v->region}}</a><span class="split-line"></span><span class="text-muted hidden-xs">年份：</span><a href='/list/{{$v->tid}}?year={{$v->year}}'>{{$v->year}}</p>&nbsp;&nbsp;
										<p class="hidden-xs"><span class="text-muted">简介：</span>{{mb_substr($v->plot,0,60)}}....<a href="/details/{{$v->d_id}}.html">详情 ></a></p>&nbsp;&nbsp;
										<p class="margin-0">
											<a class="btn btn-sm btn-warm" href="/details/{{$v->d_id}}.html"><i class="fa fa-play"></i> 立即播放</a>
											<a class="btn btn-sm btn-default hidden-xs" href="/details/{{$v->d_id}}.html">查看详情 <i class="fa fa-angle-right"></i></a>
										</p>
									</div>
								</li>
							@endforeach
								</ul>
						</div>
											</div>
				</div>

				<ul class="myui-page text-center clearfix">
</ul>
	        </div>
	    </div>
    </div>
 		<!-- 引入尾 -->
		@include("template/default/foot")
<script>
	$(".text-red").html('{{$terms}}')
	var type;
      layui.use(['laydate','form','laypage'], function(){
        var laydate = layui.laydate;
        var  form = layui.form;
        var laypage = layui.laypage;


        //执行分页ajax
        laypage.render({
            elem: document.getElementsByClassName("myui-page")[0] //获取div元素(就是将分页按钮插入到哪个DIV中)
            ,count: {{$count}} //数据总数，从服务端得到
            ,limit:{{$pageNum}} //每页显示数量
            ,theme:'#FF9E12'
            ,jump: function(obj, first){
            //obj包含了当前分页的所有参数，比如：
            // console.log(obj.curr); //得到当前页，以便向服务端请求对应页的数据。
            // console.log(obj.limit); //得到每页显示的条数
            var url = "/search"; //拼接跳转路由地址
            //首次不执行
            if(!first){
                //ajax分页
                var index = layer.load(2, {time: 10*1000}); //加载等待动画
                $.get(url,{'page':obj.curr,"terms":"{{$terms}}"},function(data){
                    $("#searchList").html(data);
                    layer.close(index); //数据显示为完毕,关闭加载动画
                    $(document).scrollTop(0); //自动回到顶部
                });
                }
            }
        });
      });
</script>
</body>
</html>
