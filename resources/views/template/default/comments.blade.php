<style>
    #submitt{
        color:#777;
        width:130px;
        height:30px;
        border-radius:10px;
        cursor:pointer;
        position:absolute;
        bottom:12px;
        right:32px;
    }
    #submitt:hover{
        color:#000;
    }
    .pinglun{
        height:90px;
        width:100%;
        margin-top:20px;
        padding-bottom:10px;
        border-bottom:1px solid #EEEEEE;
    }
    .pinglunr{
        height:90px;
        width:100%;
        margin-top:20px;
        padding-bottom:10px;
        border-bottom:1px solid #EEEEEE;
    }
    .touxiang{
        float:left;
        width:45px;
        height:45px;
        border-radius:50%;
        overflow:hidden;
        line-height:0px;
    }
    .neirong{
        float:right;
        height:100%;
        position:relative;
    }
    .neirongr{
        float:right;
        height:100%;
        position:relative;
    }
    .zhanghao{
        float:left;
        font-size:14px;
        font-weight:700;
        color: rgba(0, 0, 0, 0.87);
    }
    .riqi{
        margin:2px 0px 0px 5px;
        float:left;
        font-size:12px;
        color: rgba(0, 0, 0, 0.4);
    }
    .hfren{
        float:left;
        margin-top:-13px;
        color: #1e70bf;
    }
    .hfneirong{
        float:left;
        margin-left:5px;
    }
    .hfan{
        font-size:12px;
        color:rgba(0, 0, 0, 0.4);
        cursor:pointer;
        float:left;
    }
    .hfan:hover{
        color:rgba(0, 0, 0, 0.87);
    }
    .praise{
        float:left;
        color:rgba(0, 0, 0, 0.4);
        cursor:pointer;
        margin-left:20px;
        margin-right:5px;
    }
    .tread{
        float:left;
        color:rgba(0, 0, 0, 0.4);
        cursor:pointer;
    }
    .praise:hover{
        color:rgba(0, 0, 0, 0.87);
    }
    .tread:hover{
        color:rgba(0, 0, 0, 0.87);
    }
    #reply{
        position:absolute;
        top:12px;
        left:80px;
    }
</style>
<form class="layui-form" action="/comments-reply" method="post" style="position:relative">
<div class="hfren" id="reply"></div>
<textarea id="demo" style="display: none;"></textarea>
<input type="submit" value="发     布" id="submitt" style="border:1px solid #aaa;" lay-filter="add" lay-submit="">
</form>
<div style="width:100%;margin-top:20px;" id="zuji">
    @foreach($res as $v)
    @if($v->cid == '0')
    <div class="pinglun">
        <div class="touxiang"><img src="
        @if($v->pic)
        .{{$v->pic}}
        @else
        /images/timg.jfif
        @endif" style="width:45px;height:45px;"></div>
        <div class="neirong">
            <div style="height:20px;"><div class="zhanghao">{{$v->user?$v->name:"游客"}}</div><div class="riqi">{{date('Y-m-d H:i:s',$v->time)}}</div></div>
            <div style="height:60px;clear:both;padding-top:10px;"><div class="hfneirong">{!!$v->content!!}</div></div>
            <div style="position:absolute;bottom:0px;left:0px;">
                <div class="hfan" onclick="reply('{{$v->id}}','{{$v->user?$v->name:"游客"}}')"><i class="layui-icon layui-icon-dialogue"></i> 回复</div>
                <div class="praise" onclick="praise('{{$v->id}}')"><i class="layui-icon layui-icon-praise"></i><span id="{{$v->id}}praise">{{$v->praise}}</span></div>
                <div class="tread" onclick="tread('{{$v->id}}')"><i class="layui-icon layui-icon-tread"></i><span id="{{$v->id}}tread">{{$v->tread}}</span></div>
            </div>
        </div>
    </div>
    @endif
    @foreach($res as $V)
    @if($V->cid == $v->id)
    <div class="pinglunr" style="padding-left:50px">
        <div class="touxiang"><img src="
        @if($V->pic)
        .{{$V->pic}}
        @else
        /images/timg.jfif
        @endif" style="width:45px;height:45px;"></div>
        <div class="neirongr">
            <div style="height:20px;"><div class="zhanghao">{{$V->user?$V->name:"游客"}}</div><div class="riqi">{{date('Y-m-d H:i:s',$V->time)}}</div></div>
            <div style="height:60px;clear:both;padding-top:10px;"><div class="hfren" style="margin-top:2px;">{{'@'.$V->cname}}</div><div class="hfneirong">{!!$V->content!!}</div></div>
            <div style="position:absolute;bottom:0px;left:0px;">
                <div class="hfan" onclick="reply('{{$v->id}}','{{$V->user?$V->name:"游客"}}')"><i class="layui-icon layui-icon-dialogue"></i> 回复</div>
                <div class="praise" onclick="praise('{{$V->id}}')"><i class="layui-icon layui-icon-praise"></i><span id="{{$V->id}}praise">{{$V->praise}}</span></div>
                <div class="tread" onclick="tread('{{$V->id}}')"><i class="layui-icon layui-icon-tread"></i><span id="{{$V->id}}tread">{{$V->tread}}</span></div>
            </div>
        </div>
    </div>
    @endif
    @endforeach
    @endforeach
</div>
<script>
layui.use(['form','layer','jquery','layedit'], function(){
        var  form = layui.form;
		layer = layui.layer;
		$ = layui.jquery;
		var layedit = layui.layedit;
		var index = layedit.build('demo',{
			tool:['face'],
			height:120
        });
        var sum = -60 + parseInt($(".pinglun").css('width'))+'px';
        $(".neirong").css('width',sum);
        var sumr = -110 + parseInt($(".pinglunr").css('width'))+'px';
        $(".neirongr").css('width',sumr);
        window.onresize = function(){
            var sum = -60 + parseInt($(".pinglun").css('width'))+'px';
            $(".neirong").css('width',sum);
            var sumr = -110 + parseInt($(".pinglunr").css('width'))+'px';
            $(".neirongr").css('width',sumr);
        }
        //监听提交
        form.on('submit(add)',
            function(data) {
                var contents = "";
                contents = layedit.getContent(index);
                contents = contents.replace(/{{C_WORDS}}/g,"**");
                contents = contents.replace(/\&nbsp\;/g,"");
                contents = contents.replace(/<p>|<br>|<\/p>/g,"");
                if (contents.length > 3) {
                    if (contents.length < 255) {
                $.post("{{url('comments-reply')}}",{'_token':"{{csrf_token()}}",'field':data.field,'content':contents,'did':"{{$did}}"},function(data){
                    if(data == '1'){
                        var but = document.getElementById('submitt');
                            but.value = '('+9+')请稍等一会';
                            but.disabled = true;
                            var i = 9;
                            var button = setInterval(function(){
                                i--;
                                but.value = '('+i+')请稍等一会';
                                but.disabled = true;
                                if (i == 0){
                                    clearInterval(button);
                                    but.value = '发     布';
                                    but.disabled = false;
                                }
                            },1000)
                        alert('发布成功');
                        $('#reply').html('');
                        $("#LAY_layedit_1").contents().find('body').html('');
                    }else if(data == '0'){
                        alert('好像出现了点错误');
                    }
                });
            }else{
                alert('评论内容超出字数上限');
            }
            }else{
                alert('评论的内容太少了');
            }
                return false;
            });
    });
    function emptyy(){
        $('#reply').html('');
    }
    function praise(id){
        $.post("/comments-praise",{'_token':"{{csrf_token()}}",'id':id},function(data){
            if(data == '2'){
                alert('别赞了,别赞了,再赞人就飘了');
            }else if(data == '1'){
               var p = parseInt($('#'+id+'praise').html())+1;
               $('#'+id+'praise').html(p);
            }else if(data == '0'){
                alert('不知道发生了什么,好像出了点问题');
            }
        });
    }
    function tread(id){
        $.post("/comments-tread",{'_token':"{{csrf_token()}}",'id':id},function(data){
            if(data == '2'){
                alert('别踩了,别踩了,再踩人就傻了');
            }else if(data == '1'){
                var t = parseInt($('#'+id+'tread').html())+1;
                $('#'+id+'tread').html(t);
            }else if(data == '0'){
                alert('不知道发生了什么,好像出了点问题');
            }
        });
    }
    function reply(id,name){
        $('#reply').html('@'+name+" <span style='color:rgba(0, 0, 0, 0.87);cursor:pointer;font-size:30px;position:relative;top:7px;' onclick='emptyy()'>×</span><input type='hidden' name='cid' value='"+id+"'>"+"<input type='hidden' name='cname' value='"+name+"'>");
    }
</script>
