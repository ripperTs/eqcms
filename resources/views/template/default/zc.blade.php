
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>用户登录-8848私人影院</title>
<meta name="keywords" content="用户登录-8848私人影院">
<meta name="description" content="用户登录-8848私人影院">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

<style type="text/css">[class*=col-],.myui-panel_hd,.myui-content__list li{ padding: 10px}.btn{ border-radius: 5px;}.myui-vodlist__thumb{ border-radius:5px; padding-top:150%; background: url(/templets/default/images/a.gif) no-repeat;}.myui-vodlist__thumb.square{ padding-top: 100%; background: url(/templets/default/images/d.gif) no-repeat;}.myui-vodlist__thumb.wide{ padding-top: 60%; background: url(/templets/default/images/c.gif) no-repeat;}.myui-vodlist__thumb.actor{ padding-top: 140%;}.flickity-prev-next-button.previous{ left: 10px;}.flickity-prev-next-button.next{ right: 10px;}.myui-sidebar{ padding: 0 0 0 20px;}.myui-panel{ padding: 10px; margin-bottom: 20px; border-radius: 5px;}.myui-layout{ margin: -10px -10px 20px;}.myui-panel-mb{ margin-bottom: 20px;}.myui-panel-box{ padding: 10px;}.myui-panel-box.active{ margin: -10px;}.myui-player__item .fixed{ width: 500px;}.myui-vodlist__text li a{ padding: 10px 15px 10px 0;}.myui-vodlist__media li { padding: 10px 0 10px;}.myui-screen__list{ padding: 10px 10px 0;}.myui-screen__list li{ margin-bottom: 10px; margin-right: 10px;}.myui-page{ padding: 0 10px;}.myui-extra{ right: 20px; bottom: 30px;}@media (min-width: 1200px){.container{ max-width: 1920px;}.container{ padding-left: 120px;  padding-right: 120px;}.container.min{ width: 1200px; padding: 0;}}@media (max-width: 1400px){.myui-layout{ margin: 0;}}@media (max-width: 767px){body,body.active{ padding-bottom: 50px;}[class*=col-],.myui-content__list li{ padding: 5px}.flickity-prev-next-button.previous{ left: 5px;}.flickity-prev-next-button.next{ right: 5px;}.myui-panel{ padding: 0; border-radius: 0;}.myui-vodlist__text li a{ padding: 10px 15px 10px 0;}.myui-vodlist__media li { padding: 5px 0 5px;}.myui-screen__list{ padding: 10px 5px 0;}.myui-screen__list li{ margin-bottom: 5px; margin-right: 5px;}.myui-extra{ right: 20px; bottom: 80px;}.myui-page{ padding: 0 5px;}}</style>
<style type="text/css">.myui-user__head .btn-default{padding:15px}.myui-user__head .fa{font-size:16px;vertical-align:-1px}.myui-user__name{border-radius:5px;margin-bottom:20px;position:relative;padding:50px 30px;background-image:linear-gradient(135deg,#f761a1 10%,#8c1bab 100%)}.myui-user__name dt{float:left}.myui-user__name dd{margin-left:70px;padding-top:5px}.myui-user__name .logout{position:absolute;top:0;right:15px;margin-top:20px}.myui-user__form p{margin:0;padding:15px 0}.myui-user__form .xiang{display:inline-block;width:120px;padding-right:15px;text-align:right;color:#999}.myui-user__form input.form-control{display:inline-block;width:200px;margin-right:10px}.myui-login__form{width:350px;padding:30px;margin: 80px auto; box-shadow:0 2px 5px rgba(0,0,0,.1)}.myui-login__form li{margin:20px 0}@media (max-width:767px){.myui-user__head .btn-default{padding:15px 5px}.myui-user__name{padding:20px}.myui-user__name h3{margin:3px 0 5px;font-size:15px}.myui-user__head li a,.myui-vodlist__text li a{font-size:14px}.stui_login__form,.stui_login__form.active{width:100%;margin:0;padding:0}.myui-user__form p{padding:10px;font-size:14px}.myui-user__form .xiang{display:none}.myui-user__form .xiang.active{display:inline-block;width:auto;text-align:left}.myui-user__form input.form-control{width:100%;height:40px}.myui-user__form p .btn{width:120px}.myui-user__form .btn_unbind{display:inline-block;margin-top:5px}}</style>

</head>
<body>
 		@include("template/default/head");

<div class="myui-login__form clearfix">
	<div class="myui-panel myui-panel-bg clearfix">
		<div class="myui-panel-box clearfix">		
			<div class="myui-panel_bd">
				<div class="head text-center">
					<a href="/index"><img class="img-responsive" style="width: 120px;height: 48px;" src="{{S_LOGO}}"/></a>
										<h5>注册账号</h5>
				</div>
			<ul>
				<form id="f_login"   action="/dozc" method="post">
					{{csrf_field()}}
<!-- 					<input type="hidden" value="login" name="dopost" />
 -->				<li>
						<input  type="input" name="user" id="user" autofocus class="form-control" placeholder="用户名" />
						<span id="ts" style="color: red"></span>
					</li>
					<li>
						<input type="password" name="passwd" id="pass" class="form-control" placeholder="密码" />
						<span id="ts1" style="color: red"></span>
					</li>
					<li>
						<input type="password" name="qrpasswd" id="qrpass" class="form-control" placeholder="确认密码" />
						<span id="ts2" style="color: red"></span>
					</li>
					<li>
						<input type="text" name="email" id="email" class="form-control" placeholder="邮箱地址" />
						<span id="ts3" style="color: red"></span>
					</li>
					<li>
						<img id="vdimgck" src="./include/vdimgck.php" alt="看不清？点击更换" align="absmiddle" class="pull-right" style='width:70px; height:32px;' onClick="this.src=this.src+'?'"/>
						<input name="yzm" type="text" id="yzm"  placeholder="验证码" style='width:50%;text-transform:uppercase;' class="form-control" /></li>
					<li>
						<input type="submit" value="注册" class="btn btn-block btn-warning"/>
					</li>
					<li class="text-center">
						<a class="text-muted" href="./reg.php">已有账号,直接登录?</a>
						
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>

 		@include("template/default/foot")

</body>
<script>
	var user = document.getElementById('user');
	var ts = document.getElementById('ts');
	var ts1 = document.getElementById('ts1');
	var ts2 = document.getElementById('ts2');   //确认密码
	var ts3 = document.getElementById('ts3');   //邮箱地址
	var pass = document.getElementById('pass');
	var qrpass = document.getElementById('qrpass');
	var email = document.getElementById('email');
	var yzm = document.getElementById('yzm');
	
	//ts.innerHTML = "用户名不能为空";
	user.onblur = function(){
			//先获得用户输入内容 value 
			var zhi = this.value;
			if(zhi == ''){
				ts.innerHTML = "!用户名不能为空";
			} else{
				ts.innerHTML = "";
				//alert(11);
			}
	pass.onblur = function(){
			var z = this.value;
			if (z == ''){
				ts1.innerHTML = "!密码不能为空";
			} else{
				ts1.innerHTML = "";
			}

	}
	qrpass.onblur = function(){
			var q = this.value;
			if (q == ''){
				ts2.innerHTML = "!确认密码不能为空";
			} else{
				ts2.innerHTML = "";
			}

	}
	email.onblur = function(){
			var yx = this.value;
			if (yx == ''){
				ts3.innerHTML = "!邮箱不能为空";
			} else{
				ts3.innerHTML = "";
			}

	}
	user.onfocus = function(){
		var u = this.value;
		if (u != ''){
			ts.innerHTML = "";
		}
	}
	pass.onfocus = function(){
		var p = this.value;
		if (p != ''){
			ts.innerHTML = "";
		}
	}
	qrpass.onfocus = function(){
		var qrp = this.value;
		if (qrp != ''){
			ts.innerHTML = "";
		}
	}		
	email.onfocus = function(){
		var em = this.value;
		if (em != ''){
			ts.innerHTML = "";
		}
	}

		}
</script>
</html>