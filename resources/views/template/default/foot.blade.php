<div class="myui-foot clearfix">
	<div class="container min">
		<div class="row">
			<div class="col-pd text-center">
				<p class="margin-0"><div style="width:70%;margin:0 auto;">{{COPYRIGHT}}</div><br />
投诉邮箱：<a target="_blank" href="#" style="text-decoration:none;">{{EMAIL}}</a></p>
<p>{{ICP}}</p>
<p style="font-size:14px;">Powered by <a href="//eqcms.top" target="_blank"><u>EQCMS</u></a> © 2019-<span class="year"></span> All Rights Reserved</p>
			</div>
		</div>
	</div>
</div>

<ul class="myui-extra clearfix">
	<li>
		<a class="backtop" href="javascript:scroll(0,0)" title="返回顶部" style="display: none;"><i class="fa fa-angle-up"></i></a>
	</li>
		<li>
		<a class="btnskin" href="javascript:;" title="切换皮肤"><i class="fa fa-windows"></i></a>
	</li>
		<li class="visible-xs">
		<a class="open-share" href="javascript:;" title="分享给朋友"><i class="fa fa-share-alt"></i></a>
	</li>
		<li class="dropdown-hover hidden-xs">
		<a href="javascript:;" title="手机观看" onclick="MyTheme.Layer.Img('手机观看','{{TWO_IMG}}','扫一扫手机观看')"><i class="fa fa-android"></i></a>
		<div class="dropdown-box right fadeInDown clearfix">
			<div class="item text-center">
				<div class="qrcode-box">
					<img src="{{TWO_IMG}}" width="160" />
				</div>
				<p>扫一扫手机观看</p>
			</div>
		</div>
	</li>
	<li><a href="/gbook" title="留言反馈"><i class="fa fa-commenting"></i></a></li></ul>
<script type="text/javascript">
	MyTheme.Images.Qrcode.Init();MyTheme.Link.Short();MyTheme.Other.Skin();MyTheme.Other.Share();
</script>
<div class="hide"></div>
<script>
	//延时加载，提升页面速度
	window.onload=function(){
		 //百度主动推送
		var bp = document.createElement('script');
		bp.src = 'https://zz.bdstatic.com/linksubmit/push.js';
		var s = document.getElementsByTagName("script")[0];
		 s.parentNode.insertBefore(bp, s);
	}
	// 当前年
	var today = new Date();
	var year = today.getFullYear();
	$(".year").html(year);
</script>
