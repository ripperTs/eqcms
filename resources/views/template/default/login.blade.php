<!DOCTYPE html>
<html>
<head>

<title>用户登录-{{S_NAME}}</title>
@include("template/default/head")
<style type="text/css">[class*=col-],.myui-panel_hd,.myui-content__list li{ padding: 10px}.btn{ border-radius: 5px;}.myui-vodlist__thumb{ border-radius:5px; padding-top:150%; background: url(/templets/default/images/a.gif) no-repeat;}.myui-vodlist__thumb.square{ padding-top: 100%; background: url(/templets/default/images/d.gif) no-repeat;}.myui-vodlist__thumb.wide{ padding-top: 60%; background: url(/templets/default/images/c.gif) no-repeat;}.myui-vodlist__thumb.actor{ padding-top: 140%;}.flickity-prev-next-button.previous{ left: 10px;}.flickity-prev-next-button.next{ right: 10px;}.myui-sidebar{ padding: 0 0 0 20px;}.myui-panel{ padding: 10px; margin-bottom: 20px; border-radius: 5px;}.myui-layout{ margin: -10px -10px 20px;}.myui-panel-mb{ margin-bottom: 20px;}.myui-panel-box{ padding: 10px;}.myui-panel-box.active{ margin: -10px;}.myui-player__item .fixed{ width: 500px;}.myui-vodlist__text li a{ padding: 10px 15px 10px 0;}.myui-vodlist__media li { padding: 10px 0 10px;}.myui-screen__list{ padding: 10px 10px 0;}.myui-screen__list li{ margin-bottom: 10px; margin-right: 10px;}.myui-page{ padding: 0 10px;}.myui-extra{ right: 20px; bottom: 30px;}@media (min-width: 1200px){.container{ max-width: 1920px;}.container{ padding-left: 120px;  padding-right: 120px;}.container.min{ width: 1200px; padding: 0;}}@media (max-width: 1400px){.myui-layout{ margin: 0;}}@media (max-width: 767px){body,body.active{ padding-bottom: 50px;}[class*=col-],.myui-content__list li{ padding: 5px}.flickity-prev-next-button.previous{ left: 5px;}.flickity-prev-next-button.next{ right: 5px;}.myui-panel{ padding: 0; border-radius: 0;}.myui-vodlist__text li a{ padding: 10px 15px 10px 0;}.myui-vodlist__media li { padding: 5px 0 5px;}.myui-screen__list{ padding: 10px 5px 0;}.myui-screen__list li{ margin-bottom: 5px; margin-right: 5px;}.myui-extra{ right: 20px; bottom: 80px;}.myui-page{ padding: 0 5px;}}</style>
<style type="text/css">.myui-user__head .btn-default{padding:15px}.myui-user__head .fa{font-size:16px;vertical-align:-1px}.myui-user__name{border-radius:5px;margin-bottom:20px;position:relative;padding:50px 30px;background-image:linear-gradient(135deg,#f761a1 10%,#8c1bab 100%)}.myui-user__name dt{float:left}.myui-user__name dd{margin-left:70px;padding-top:5px}.myui-user__name .logout{position:absolute;top:0;right:15px;margin-top:20px}.myui-user__form p{margin:0;padding:15px 0}.myui-user__form .xiang{display:inline-block;width:120px;padding-right:15px;text-align:right;color:#999}
.myui-user__form input
.form-control{display:inline-block;
	width:200px;
	margin-right:10px}


.myui-login__form{
	width:430px;
	padding:30px;
	margin: 80px auto;
	margin-top: 50px;
	height:550px;
	box-shadow:0 2px 5px rgba(0,0,0,.1)
	}
.myui-login__form li{margin:20px 0}@media (max-width:767px){.myui-user__head .btn-default{padding:15px 5px}.myui-user__name{padding:20px}.myui-user__name h3{margin:3px 0 5px;font-size:15px}.myui-user__head li a,.myui-vodlist__text li a{font-size:14px}.stui_login__form,.stui_login__form.active{width:100%;margin:0;padding:0}.myui-user__form p{padding:10px;font-size:14px}.myui-user__form .xiang{display:none}.myui-user__form .xiang.active{display:inline-block;width:auto;text-align:left}.myui-user__form input.form-control{width:100%;height:40px}.myui-user__form p .btn{width:120px}.myui-user__form .btn_unbind{display:inline-block;margin-top:5px}}
	.box{width:100px;
		height:40px;
		 float:left; margin-left:10px;
		 text-align:center;
		  line-height:50px;
		  cursor:pointer;
		}
	.show{width:300px;
		height:200px;
		  margin-top:10px;
		  text-align:center;
		   line-height:50px;}
</style>
<link rel="stylesheet" href="/{{TEM}}images/stui_block_color.css" type="text/css" />
		<link rel="stylesheet" href="/{{TEM}}images/stui_block.css" type="text/css" />
</head>
<body>
	@include("template/default/header")
	<div class="myui-panel_hd"><div style="height:30px;"></div></div>
<div class="myui-login__form clearfix" style="height: 550px;width:593px;">
	<div class="myui-panel myui-panel-bg clearfix" style="height: 500px;width:536px;">

		<div class="myui-panel-box clearfix" style="height: 400px;">
			<div class="myui-panel_bd">
				<div class="head text-center">
					<a href="/"><img class="img-responsive" style="width: 120px;height: 40px;margin-bottom: 10px;" src="{{S_LOGO}}"/></a>
				</div>

					<div class='show' id="show1" style="margin-left:70px;">
				<form  class="layui-form" style="margin-top:20px; ">
 					<li>
						<input  type="input" name="user" id="yonghu" autofocus class="form-control" placeholder="手机号/邮箱" />
						<span id="yonghu1" style="color: red;float: right;margin-top: -40px;"></span>
					</li>

					<li>
						<input type="password" name="passwd" id="mima" lay-verify="required" class="form-control" placeholder="密码" />
						<input type="hidden" name="code" value="login">
						<span id="mm" style="color: red;float: right;margin-top: -40px;"></span>
					</li>
					<li>
						<img id="vdimgck" src="/code/loginyzm" alt="看不清？点击更换" align="absmiddle" class="pull-right" style='width:70px; height:32px;' onClick="this.src=this.src+'?'"/>
						<input  type="text" id="yzm" name="vcode" lay-verify="required" placeholder="验证码" style='width:50%;text-transform:uppercase;' class="form-control" />
						<span id="yzmspan" style="color: red;float: right;margin-top: -40px;margin-right:80px;"></span>
					</li>
					<li>
						<button lay-submit value="登录" lay-filter="login" class="layui-btn layui-btn-warm">
							登陆
						</button>
					</li>
					<li class="text-center" style="margin-top: -20px;">
						<a class="text-muted" href="/reg.html" style="float:left;">没有账号?去注册!</a>
						<a class="text-muted" href="/back.html" style="float:right;">找回密码</a>
					</li>
				</ul>
			</form>
			</div>
		</div>
				</form>
				</ul>

			</div>
		</div>
	</div>
</div>
					</div>

			<ul>

 		@include("template/default/foot")

</body>
<script>
//Demo
var token = "{{csrf_token()}}";
var checkUser = false;
var yzm = false;
layui.use('form', function(){
  var form = layui.form;
	$("[name=user]").blur(function(){
		var user = $(this).val();
		$.post("/dologin",{'_token':token,'user':user,'type':'user'},function(data){
			if(data==2){
				$("#yonghu1").html("账号不存在!");
				checkUser = false;
			}else{
				checkUser = true;
				$("#yonghu1").html("");
			}
		})
	});

	$("[name=vcode]").blur(function(){
		var yzm = $(this).val();
		$.post("/dologin",{"_token":token,'yzm':yzm,'type':'yzm'},function(data){
			if(data==2){
				$("#yzmspan").html("验证码不符");
				yzm = false;
			}else{
				yzm = true;
				$("#yzmspan").html("");
			}
		});
	});

var mobile = /^1[3|4|5|7|8]\d{9}$/,phone = /^0\d{2,3}-?\d{7,8}$/;
        form.verify({
            tellphone: function(value){
            	var flag = mobile.test(value) || phone.test(value);
                if(!flag){
                	return '请输入正确手机号';
                }
            }
        });

  //监听提交
  form.on('submit(login)', function(data){
	$.post("/dologin",{"_token":token,"data":data.field,'type':'login'},function(data){
		if(data==200){
			alert('登陆成功!');
				location="/center";
		}else if(data==201){
			alert('密码错误!当天登陆超过5次将会临时冻结账号!');
		}else if(data==301){
			alert("该账户已冻结!");
		}else if(data==401){
				alert("该账户已封禁!");
		}else if(data==501){
			alert("该账户未激活!");
		}else if(data==601){
			alert("登陆失败!");
		}else if(data==701){
			alert("您今天登陆失败错误次数过多,请联系管理员!");
		}

	})
    return false;
  });

});
</script>
</html>
