<!DOCTYPE html>
<html>

<!-- 引入头样式 -->
<head>
<title>{{S_NAME}} - 首页_最新电影电视剧分享!</title>
@include("template/default/head")
<link rel="stylesheet" type="text/css" href="/{{TEM}}css/style2.css" />
<script type="text/javascript" src="/{{TEM}}js/myslideup.js"></script>
<style>
	#show{
		display:none;
	}
	#hide:hover #show{
		display:block;
	}
</style>
<body class="active">
@include("template/default/header")
<div id="home_slide" class="carousel slide clearfix" data-ride="carousel">

	<div class="carousel-inner">
			@foreach($banner as $k=>$b)
				@if($k==0)

				<div class="item text-center active" style="background-color: ;">
					<a title="{{$b->title}}" href="{{$b->b_url}}">
					<img class="img-responsive" src="{{$b->img}}" width="100%" height="100%"/></a>
				</div>
				@elseif($k>0)
				<div class="item text-center" style="background-color: ;">
					<a title="{{$b->title}}" href="{{$b->b_url}}">
					<img class="img-responsive" src="{{$b->img}}" width="100%" height="100%"/></a>
				</div>
				@endif
			@endforeach
    </div>
    	<ul class="carousel-indicators carousel-indicators-thumb hidden-md hidden-sm hidden-xs">
		@foreach($banner as $k1=>$b1)
			@if($k1==0)
                <li data-target="#home_slide" data-slide-to="{{$k1}}" class="active" style="width: 160px; height: 90px;">
<img class="img-responsive" src="{{$b1->img}}" width="100%" height="100%"/>
    		</li>
			@elseif($k1>0)
				<li data-target="#home_slide" data-slide-to="{{$k1}}" class style="width: 160px; height: 90px;">
	<img class="img-responsive" src="{{$b1->img}}" width="100%" height="100%"/>
				</li>
			@endif
		@endforeach
		</ul>

        <a class="carousel-control left" href="#home_slide" data-slide="prev"><i class="fa fa-angle-left"></i></a>
	<a class="carousel-control right" href="#home_slide" data-slide="next"><i class="fa fa-angle-right"></i></a>

</div>
<div class="myui-panel radius-0 myui-panel-bg clearfix">
	<div class="myui-panel-box clearfix">
		<div class="container">
	        <div class="row">
        		<div class="clearfix" data-align="left">
				@foreach($row4 as $v4)
                                       <li class="col-md-9 col-xs-3">
						<a class="btn btn-block btn-default" href="/list/{{$v4->id}}">
							<span class="spot"></span>{{$v4->t_name}}
						</a>
					</li>
				@endforeach
				</div>
			</div>
		</div>
	</div>
</div>
	    <div class="container">
	   	<div class="row">

	    			<!-- <marquee width="100%" behavior="1" direction="1" style="font-size:14px;color:#47A1F8;">关注我们微信公众号:《8848私人影院》在线观影不迷路!发送你想看的影片名即可在线观看!
	    			</marquee> -->
					<ul class="line">
						@foreach($notice as $v)
						@if($v->e_time > time())
						<li style="margin-top: 0px; ">{{$v->content}}</li>
						@endif
						@endforeach
					</ul>

	    	</div>
	        <div class="row">
             <div class="myui-panel myui-panel-bg clearfix">
	<div class="myui-panel-box clearfix">
		<div class="myui-panel_hd">
			<div class="myui-panel__head clearfix">

				<span class="text-muted more pull-right">今日更新：“{{$dayUpdate}}”条影片</span>
				{{-- @include("template/default/countman") --}}
				<h3 class="title">
					今日推荐				</h3>

			</div>
		</div>
		<div class="myui-panel_bd clearfix">
			<ul class="myui-vodlist clearfix">
				@foreach($today as $to)
								<li class="col-lg-8  col-md-8 col-sm-4 col-xs-3">
					<div class="myui-vodlist__box">
			<a class="myui-vodlist__thumb lazyload" href="/details/{{$to->d_id}}.html" title="{{$to->d_name}}" data-original="{{$to->img}}">
				<span class="play hidden-xs"></span>
			<span class="pic-tag pic-tag-top" style="background-color: #5bb7fe;">
			{{round($to->score/$to->snum,1)}}分
			</span>
<span class="pic-text text-right">
{{$to->note}}</span>
	</a>
	<div class="myui-vodlist__detail">
		<h4 class="title text-overflow"><a href="/movie/index74019.html" title="{{$to->d_name}}">{{$to->d_name}}</a></h4>
		<p class="text text-overflow text-muted hidden-xs">
		{{$to->year}}/{{$to->region}}/{{$to->typename}}</p>
	</div>
</div>
				</li>
			@endforeach

							</ul>
		</div>
	</div>
</div>

<div class="myui-panel myui-panel-bg clearfix">
	<div class="myui-panel-box clearfix">
		<div class="myui-panel_bd clearfix">
			<div class="col-lg-wide-75 col-md-wide-75 col-xs-1 padding-0">
				<div class="myui-panel_hd">
					<div class="myui-panel__head clearfix">
						<h3 class="title">
							最新电影						</h3>
                            </ul>
						<a class="more text-muted" href="/list/1">更多 <i class="fa fa-angle-right"></i></a>
					</div>
				</div>
				<ul class="myui-vodlist clearfix">
				@foreach($newcine as $v)
								<li class="col-lg-6 col-md-6 col-sm-4 col-xs-3">
						<div class="myui-vodlist__box">
			<a class="myui-vodlist__thumb lazyload" href="/details/{{$v->id}}.html" title="{{$v->d_name}}" data-original="{{$v->img}}">
				<span class="play hidden-xs"></span>
			<span class="pic-tag pic-tag-top" style="background-color: #5bb7fe;">
			{{round($v->score/$v->snum,1)}}分
			</span>
<span class="pic-text text-right">
{{$v->note}}</span>
	</a>
	<div class="myui-vodlist__detail">
		<h4 class="title text-overflow"><a href="/details/{{$v->id}}.html" title="{{$v->d_name}}">{{$v->d_name}}</a></h4>
		<p class="text text-overflow text-muted hidden-xs">
		{{$v->year}}/{{$v->region}}/{{$v->typename}}</p>
	</div>
</div>
				</li>
			@endforeach


				</ul>
			</div>
						<div class="col-lg-wide-25 col-md-wide-25 hidden-sm hidden-xs">
				<div class="myui-panel_hd">
					<div class="myui-panel__head clearfix">
						<h3 class="title">
							最热电影						</h3>
						<a class="more text-muted" href="/list/1">更多  <i class="fa fa-angle-right"></i></a>
					</div>
				</div>
				<ul class="myui-vodlist__text active clearfix" style="padding: 0 10px;">
				@foreach($newcines as $ks=>$vs)
											<li>
	<a href="/details/{{$vs->id}}.html" title="{{$vs->d_name}}">
		<span class="pull-right  text-muted visible-lg" style="color: ;">
		{{$vs->note}}高清</span>
		<span  class="badge badge-{{($ks>2)?'':$ks+1}}">{{$ks+1}}</span>{{mb_substr($vs->d_name,0,10)}}</a>
</li>
@endforeach

									</ul>
			</div>
					</div>
	</div>
</div>

<div class="myui-panel myui-panel-bg clearfix">
	<div class="myui-panel-box clearfix">
		<div class="myui-panel_bd clearfix">
			<div class="col-lg-wide-75 col-md-wide-75 col-xs-1 padding-0">
				<div class="myui-panel_hd">
					<div class="myui-panel__head clearfix">
						<h3 class="title">
							最新电视剧						</h3>
                            </ul>
						<a class="more text-muted" href="/list/2">更多 <i class="fa fa-angle-right"></i></a>
					</div>
				</div>
				<ul class="myui-vodlist clearfix">

				@foreach($newTV as $v1)
								<li class="col-lg-6 col-md-6 col-sm-4 col-xs-3">
						<div class="myui-vodlist__box">
			<a class="myui-vodlist__thumb lazyload" href="/details/{{$v1->id}}.html" title="{{$v1->d_name}}" data-original="{{$v1->img}}">
				<span class="play hidden-xs"></span>
			<span class="pic-tag pic-tag-top" style="background-color: #5bb7fe;">
			{{round($v1->score/$v1->snum,1)}}分
			</span>
<span class="pic-text text-right">
{{$v1->note}}</span>
	</a>
	<div class="myui-vodlist__detail">
		<h4 class="title text-overflow"><a href="/details/{{$v1->id}}.html" title="{{$v1->d_name}}">{{$v1->d_name}}</a></h4>
		<p class="text text-overflow text-muted hidden-xs">
		{{$v1->year}}/{{$v1->region}}/{{$v1->typename}}</p>
	</div>
</div>
				</li>
			@endforeach



				</ul>
			</div>
						<div class="col-lg-wide-25 col-md-wide-25 hidden-sm hidden-xs">
				<div class="myui-panel_hd">
					<div class="myui-panel__head clearfix">
						<h3 class="title">
							最热电视剧						</h3>
						<a class="more text-muted" href="/list/2">更多  <i class="fa fa-angle-right"></i></a>
					</div>
				</div>
				<ul class="myui-vodlist__text active clearfix" style="padding: 0 10px;">

				@foreach($newTVs as $k1=>$v1s)
											<li>
	<a href="/details/{{$v1s->id}}.html" title="{{$v1s->d_name}}">
		<span class="pull-right  text-muted visible-lg" style="color: ;">
		{{$v1s->note}}</span>
		<span  class="badge badge-{{($k1>2)?'':$k1+1}}">{{$k1+1}}</span>{{mb_substr($v1s->d_name,0,10)}}</a>
</li>
@endforeach


									</ul>
			</div>
					</div>
	</div>
</div>

<div class="myui-panel myui-panel-bg clearfix">
	<div class="myui-panel-box clearfix">
		<div class="myui-panel_bd clearfix">
			<div class="col-lg-wide-75 col-md-wide-75 col-xs-1 padding-0">
				<div class="myui-panel_hd">
					<div class="myui-panel__head clearfix">
						<h3 class="title">
							最新综艺						</h3>
                            </ul>
						<a class="more text-muted" href="/list/3">更多 <i class="fa fa-angle-right"></i></a>
					</div>
				</div>
				<ul class="myui-vodlist clearfix">

				@foreach($newvariety as $v3)
								<li class="col-lg-6 col-md-6 col-sm-4 col-xs-3">
						<div class="myui-vodlist__box">
			<a class="myui-vodlist__thumb lazyload" href="/details/{{$v3->id}}.html" title="{{$v3->d_name}}" data-original="{{$v3->img}}">
				<span class="play hidden-xs"></span>
			<span class="pic-tag pic-tag-top" style="background-color: #5bb7fe;">
			{{round($v3->score/$v3->snum,1)}}分
			</span>
<span class="pic-text text-right">
{{$v3->note}}</span>
	</a>
	<div class="myui-vodlist__detail">
		<h4 class="title text-overflow"><a href="/details/{{$v3->id}}.html" title="{{$v3->d_name}}">{{$v3->d_name}}</a></h4>
		<p class="text text-overflow text-muted hidden-xs">
		{{$v3->year}}/{{$v3->region}}/{{$v3->typename}}</p>
	</div>
</div>
				</li>
			@endforeach



				</ul>
			</div>
						<div class="col-lg-wide-25 col-md-wide-25 hidden-sm hidden-xs">
				<div class="myui-panel_hd">
					<div class="myui-panel__head clearfix">
						<h3 class="title">
							最热综艺						</h3>
						<a class="more text-muted" href="/list/3">更多  <i class="fa fa-angle-right"></i></a>
					</div>
				</div>
				<ul class="myui-vodlist__text active clearfix" style="padding: 0 10px;">
				@foreach($newvarietys as $k2s=>$v2s)
											<li>
	<a href="/details/{{$v2s->id}}.html" title="{{$v2s->d_name}}">
		<span class="pull-right  text-muted visible-lg" style="color: ;">
		{{$v2s->note}}高清</span>
		<span  class="badge badge-{{($k2s>2)?'':$k2s+1}}">{{$k2s+1}}</span>{{mb_substr($v2s->d_name,0,10)}}</a>
</li>
@endforeach



									</ul>
			</div>
					</div>
	</div>
</div>

<div class="myui-panel myui-panel-bg clearfix">
	<div class="myui-panel-box clearfix">
		<div class="myui-panel_bd clearfix">
			<div class="col-lg-wide-75 col-md-wide-75 col-xs-1 padding-0">
				<div class="myui-panel_hd">
					<div class="myui-panel__head clearfix">
						<h3 class="title">
							最新动漫						</h3>
                            </ul>
						<a class="more text-muted" href="/list/4">更多 <i class="fa fa-angle-right"></i></a>
					</div>
				</div>
				<ul class="myui-vodlist clearfix">

				@foreach($newmanga as $v2)
								<li class="col-lg-6 col-md-6 col-sm-4 col-xs-3">
						<div class="myui-vodlist__box">
			<a class="myui-vodlist__thumb lazyload" href="/details/{{$v2->id}}.html" title="{{$v2->d_name}}" data-original="{{$v2->img}}">
				<span class="play hidden-xs"></span>
			<span class="pic-tag pic-tag-top" style="background-color: #5bb7fe;">
			{{round($v2->score/$v2->snum,1)}}分
			</span>
<span class="pic-text text-right">
{{$v2->note}}</span>
	</a>
	<div class="myui-vodlist__detail">
		<h4 class="title text-overflow"><a href="/details/{{$v2->id}}.html" title="{{$v2->d_name}}">{{$v2->d_name}}</a></h4>
		<p class="text text-overflow text-muted hidden-xs">
		{{$v2->year}}/{{$v2->region}}/{{$v2->typename}}</p>
	</div>
</div>
				</li>
			@endforeach



				</ul>
			</div>
						<div class="col-lg-wide-25 col-md-wide-25 hidden-sm hidden-xs">
				<div class="myui-panel_hd">
					<div class="myui-panel__head clearfix">
						<h3 class="title">
							最热动漫						</h3>
						<a class="more text-muted" href="/list/4">更多  <i class="fa fa-angle-right"></i></a>
					</div>
				</div>
				<ul class="myui-vodlist__text active clearfix" style="padding: 0 10px;">

				@foreach($newmangas as $k3s=>$v3s)
											<li>
	<a href="/details/{{$v3s->id}}.html" title="{{$v3s->d_name}}">
		<span class="pull-right  text-muted visible-lg" style="color: ;">
		{{$v3s->note}}高清</span>
		<span  class="badge badge-{{($k3s>2)?'':$k3s+1}}">{{$k3s+1}}</span>{{mb_substr($v3s->d_name,0,10)}}</a>
</li>
@endforeach

									</ul>
			</div>
					</div>
	</div>
</div>






	        	<div class="myui-panel myui-panel-bg  clearfix">
	<div class="myui-panel-box clearfix">
		<div class="myui-panel_hd">
			<div class="myui-panel__head clearfix">
				<h3 class="title">
					友情链接 <div  class="btn btn-default favorite" style="padding: 1px 10px;"><a  href=" " onclick="toLink();return false;" style="font-size:16px;">申请</a></div>
				</h3>
			</div>
		</div>
		<div class="myui-panel_bd clearfix">
			<div class="col-xs-1">
				<ul class="myui-link__text clearfix">
					@foreach($link as $li)
					@if($li->endtime > time())
					<li id="hide">
						{{-- <div style="width:50px;height:50px;" id="show"><img src="{{$li->img}}" width="100%" height="100%"></div> --}}
						<a class="text-muted" href="{{$li->url}}" title="{{$li->name}}" target="_blank">{{$li->name}}</a>
					</li>
					@endif
					@endforeach
				</ul>
			</div>
		</div>
	</div>
</div>
<!-- 友链 -->

	        </div>
		</div>


		<!-- 引入尾 -->
		@include("template/default/foot")

</body>
<script type="text/javascript">
$(function(){
	$(".line").slideUp();
})
</script>
</html>
