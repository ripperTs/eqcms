<!DOCTYPE html>
<html class="x-admin-sm">
    <head>
        <meta charset="UTF-8">
        <title>EQCMS后台管理系统 - 收藏管理</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="/css/font.css">
        <link rel="stylesheet" href="/css/xadmin.css">
        <script src="/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/js/xadmin.js"></script>
        <!--[if lt IE 9]>
          <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
          <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="x-nav">
          
          <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload(true)" title="刷新">
            <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i></a>
        </div>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card">

                        <div class="layui-card-header">
                            <form action="/admin/collection" class="layui-form layui-col-space5" style="float:right;margin-right:50px">

                                <div class="layui-inline layui-show-xs-block">
                                    <input type="text" name="keywords"  placeholder="请输入影片名" autocomplete="off" class="layui-input">
                                </div>
                                <div class="layui-inline layui-show-xs-block">
                                    <button class="layui-btn"  lay-submit="" lay-filter="sreach"><i class="layui-icon">&#xe615;</i></button>
                                </div>
                            </form>
                            
                        </div>
                        <div class="layui-card-body layui-table-body layui-table-main">
                            <table class="layui-table layui-form">
                                <thead>
                                  <tr>
                                    <th>ID</th>
                                    <th>用户ID</th>
                                    <th>影片名</th>
                                    <th>影片地址</th>
                                    <th>收藏时间</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($res as $v)
                                  <tr>
                                    <td>{{$v->id}}</td>
                                    <td><a onclick="xadmin.open('用户详情','/admin/user-info/{{$v->uid}}',500,500)" title="用户详情" href="javascript:;">{{$v->uid}}</a></td>
                                    <td>{{$v->d_name}}</td>
                                    <td><a onclick="xadmin.open('{{$v->d_name}}','{{$v->url}}',500,500)" title="影片地址" href="javascript:;">{{$v->url}}</a></td>
                                    <td>{{date('Y-m-d',$v->time)}}</td>
                                  </tr>
                                  @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="layui-card-body ">
                            <div class="page">
                                <div>
                                    {{$res->appends($request)->render()}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    
</html>
