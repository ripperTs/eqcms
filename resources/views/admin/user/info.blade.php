<!DOCTYPE html>
<html class="x-admin-sm">

    <head>
        <meta charset="UTF-8">
        <title>EQCMS管理系统 - 会员详情</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="/css/font.css">
        <link rel="stylesheet" href="/css/xadmin.css">
        <script type="text/javascript" src="/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/js/xadmin.js"></script>
        <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
        <!--[if lt IE 9]>
            <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
            <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="layui-fluid">
            <div class="layui-row">
                <form class="layui-form">
                    <div class="layui-form-item">
                        <label for="L_repass" class="layui-form-label">
                            账号</label>
                        <div class="layui-input-inline">
                            <input type="text" value="{{$res->user}}" required=""  autocomplete="off" class="layui-input" disabled></div>
                    </div>
                    <div class="layui-form-item">
                        <label for="L_repass" class="layui-form-label">
                            昵称</label>
                        <div class="layui-input-inline">
                            <input type="text" value="{{$res->name}}" required=""  autocomplete="off" class="layui-input" disabled></div>
                    </div>
                    <div class="layui-form-item">
                        <label for="L_repass" class="layui-form-label">
                            邮箱</label>
                        <div class="layui-input-inline">
                            <input type="text" value="{{$res->email}}" required=""  autocomplete="off" class="layui-input" disabled></div>
                    </div>
                    <div class="layui-form-item">
                        <label for="L_repass" class="layui-form-label">
                            手机号</label>
                        <div class="layui-input-inline">
                            <input type="text" value="{{$res->phone}}" required=""  autocomplete="off" class="layui-input" disabled></div>
                    </div>
                    <div class="layui-form-item">
                        <label for="L_repass" class="layui-form-label">
                            登录错误次数</label>
                        <div class="layui-input-inline">
                            <input type="text" value="{{$res->num}}" required=""  autocomplete="off" class="layui-input" disabled></div>
                    </div>
                    <div class="layui-form-item">
                        <label for="L_repass" class="layui-form-label">
                            注册时间</label>
                        <div class="layui-input-inline">
                            <input type="text" value="{{date('Y-m-d H:i:s',$res->time)}}" required=""  autocomplete="off" class="layui-input" disabled></div>
                    </div>
                    <div class="layui-form-item">
                        <label for="L_repass" class="layui-form-label">
                            状态</label>
                        <div class="layui-input-inline">
                            <input type="text" value="{{$res->status}}" required=""  autocomplete="off" class="layui-input" disabled></div>
                    </div>
                    <div class="layui-form-item">
                        <label for="L_repass" class="layui-form-label">
                            邮箱激活状态</label>
                        <div class="layui-input-inline">
                            <input type="text" value="{{$res->activation}}" required=""  autocomplete="off" class="layui-input" disabled></div>
                    </div>
                </form>
                <div style="width:130px;height:130px;position:absolute;top:20px;left:350px;line-height:130px;">
                <img src="
                @if($res->pic)
                ../../../../{{$res->pic}}
                @else
                /images/timg.jfif
                @endif" style="max-height:130px;max-width:130px;display:inlin-block;margin:0 auto"></div>
            </div>
        </div>
        
    </body>

</html>
