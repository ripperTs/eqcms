<!doctype html>
<html  class="x-admin-sm">
<head></head>
	<meta charset="UTF-8">
	<title>EQCMS后台管理系统</title>
	<meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <link rel="stylesheet" href="/css/font.css">
    <link rel="stylesheet" href="/css/login.css">
	  <link rel="stylesheet" href="/css/xadmin.css">
    <script type="text/javascript" src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
    <script src="/lib/layui/layui.js" charset="utf-8"></script>
    <!--[if lt IE 9]>
      <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
      <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="login-bg">

    <div class="login layui-anim layui-anim-up">
        <div class="message">EQCMS后台管理系统</div>
        <div id="darkbannerwrap"></div>

        <form method="post" class="layui-form" action="{{url('admin/dologin')}}">
            {{csrf_field()}}
            <input name="user" placeholder="用户名"  type="text" lay-verify="required" class="layui-input" >
            <hr class="hr15">
            <input name="passwd" lay-verify="required" placeholder="密码"  type="password" class="layui-input">
            <hr class="hr15">
            <input value="登录" lay-submit lay-filter="login" style="width:100%;" type="submit">
            <input type="hidden" name="adminip" value="{{$ip}}">
            <hr class="hr20" >
        </form>
        <div class="ip" style="float:right;margin-top:50px;color:#aaa;">当前访问IP:{{$ip}}</div>
    </div>

    <script>
        $(function  () {
            layui.use('form', function(){
              var form = layui.form;

              //监听提交
              form.on('submit(login)', function(data){
                // alert(888)
                $.post("/admin/dologin",{"user":data.field.user,"passwd":data.field.passwd,"adminip":data.field.adminip,"_token":"{{csrf_token()}}"},function(data){
                  console.log(data);
                   if(data==1){
                          layer.alert("登录成功",{icon:1},function(){
                             location.href="/admin"
                          });
                   }else{
                           layer.alert("登录失败",{icon:2},function(){
                          });
                   }
                 })

                return false;
              });
            });
        })

    </script>
    <!-- 底部结束 -->
</body>
</html>
