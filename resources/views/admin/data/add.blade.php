
<!DOCTYPE html>
<html class="x-admin-sm">
    <head>
        <meta charset="UTF-8">
        <title>EQCMS后台管理系统 - 数据修改</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="/css/font.css">
        <link rel="stylesheet" href="/css/xadmin.css">
        <script src="/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/js/xadmin.js"></script>
        <!--[if lt IE 9]>
          <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
          <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="x-nav">
          <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
            <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i></a>
        </div>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-body ">
                            <form class="layui-form layui-col-space5">
                                影片名:
                                <div class="layui-inline layui-show-xs-block">
                                    <input class="layui-input" lay-verify="nikename"  autocomplete="off" name="d_name" id="start">
                                </div>
                                影片类别:
                                <div class="layui-inline layui-show-xs-block">
                                    <select name="type">
                                        <option>请选择类别...</option>
                                        @foreach ($type as $v)
                                            <option value="{{$v->id}}">{{$v->t_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                集数:
                                <div class="layui-inline layui-show-xs-block">
                                    <input type="text" name="note" lay-verify="nikename" autocomplete="off" class="layui-input">
                                </div>
                                影片密码(为空则不加密):
                                <div class="layui-inline layui-show-xs-block">
                                    <input type="text" name="passwd"  autocomplete="off" class="layui-input">
                                </div>
                                <br><br>
                                封面图片:
                                <div class="layui-inline layui-show-xs-block">
                                    <input type="text" name="img" lay-verify="required" readonly id="pictext" autocomplete="off" class="layui-input">
                                </div>
                                <a type="button" class="layui-btn" id="pic">
                                    <i class="layui-icon">&#xe67c;</i>上传封面
                                    </a>
                                导演:
                                <div class="layui-inline layui-show-xs-block">
                                    <input type="text" name="director" lay-verify="nikename" autocomplete="off" class="layui-input">
                                </div>
                                主演:
                                <div class="layui-inline layui-show-xs-block">
                                    <input type="text" name="strring" lay-verify="nikename" autocomplete="off" class="layui-input">
                                </div>

                                是否隐藏:
                                <div class="layui-inline layui-show-xs-block">
                                    <input type="radio" name="ishidden" autocomplete="off" value="0" class="layui-input" >
                                    <input type="radio" name="ishidden" autocomplete="off" value="1" class="layui-input" checked>
                                </div>
                                <br><br>
                                点击次数:
                                <div class="layui-inline layui-show-xs-block">
                                    <input type="number" name="num" style="width:50px;" lay-verify="required" autocomplete="off" class="layui-input">
                                </div>
                                评分:
                                <div class="layui-inline layui-show-xs-block">
                                    <input type="number" name="score" style="width:50px;" lay-verify="required" autocomplete="off" class="layui-input">
                                </div>

                                地区:
                                <div class="layui-inline layui-show-xs-block">
                                    <input type="text" name="region"  style="width:100px;" lay-verify="required" autocomplete="off" class="layui-input">
                                </div>
                                语言:
                                <div class="layui-inline layui-show-xs-block">
                                    <input type="text" name="language" style="width:100px;" lay-verify="required" autocomplete="off" class="layui-input">
                                </div>
                                年份:
                                <div class="layui-inline layui-show-xs-block">
                                    <input type="number" name="year" style="width:80px;" lay-verify="required" autocomplete="off" class="layui-input">
                                </div>
                                观看限制(会员等级):
                                <div class="layui-inline layui-show-xs-block">
                                    <select name="level">
                                            <option value="">不限制</option>
                                            <option value="1">普通会员</option>
                                            <option value="2">铜牌会员</option>
                                            <option value="3">银牌会员</option>
                                            <option value="4">金牌会员</option>
                                            <option value="5">钻石会员</option>
                                        </select>
                                </div>
                                <br><br>
                                播放来源:
                                        <div class="layui-inline layui-show-xs-block">
                                            <select name="psorce">
                                                @foreach ($lyname as $item)
                                                    <option value="{{$item->psorce}}" >{{$item->psorce}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                <br>
                                <div class="layui-inline layui-show-xs-block" style="width:70%;">
                                    <textarea name="playurl" class="layui-textarea"></textarea>
                                </div>
                                <br><br>
                                <div class="layui-inline layui-show-xs-block" style="width:70%;">
                                    影片简介:
                                </div>
                                <div class="layui-inline layui-show-xs-block" style="width:70%;">
                                    <textarea id="plot" lay-verify="required" name="plot" class="layui-textarea"></textarea>
                                </div>
                                <div class="layui-inline layui-show-xs-block" style="width:70%;">
                                    @if (B_SWITCH==1)
                                    <input type="radio" name="baidu" value="1" class="layui-form-label">
                                    <div class="layui-form-mid layui-word-aux">百度主动推送此片</div>
                                    @endif
                                    <button  class="layui-btn" lay-filter="add" lay-submit="">
                                        添加数据
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script>
      var token = "{{csrf_token()}}";
      layui.use(['laydate','form','upload','layedit'], function(){
        var laydate = layui.laydate;
        var  form = layui.form;
        var upload = layui.upload;
        var layedit = layui.layedit;
        //文件上传实例
        var upload = layui.upload; //实例化模块
        //执行实例
        var uploadInst = upload.render({
            elem: '#pic' //绑定元素
            ,url: '/admin/datalist' //上传处理路由
            ,size: '1024*5' //上传大小
            ,data:{'_token':token,'type':'add'}
            ,accept:'images'
            ,exts:'jpg|png|gif'
            ,before: function(obj){ //obj参数包含的信息，跟 choose回调完全一致，可参见上文。
                layer.load(); //上传loading
            }
            ,choose: function(obj){
                //将每次选择的文件追加到文件队列
                var files = obj.pushFile();
                //预读本地文件，如果是多文件，则会遍历。(不支持ie8/9)
                obj.preview(function(index, file, result){
                });
            }
            ,done: function(res){
            //上传完毕回调
            if(res.status==1){
                layer.msg("上传封面成功",{icon:1});
                $("#pictext").val(res.path);
            }else{
                layer.msg("上传封面失败",{icon:2});
            }
            layer.closeAll('loading'); //关闭loading
            }
            ,error: function(){
            //请求异常回调
            layer.closeAll('loading'); //关闭loading
            }
        });

        //自定义验证规则
        form.verify({
            nikename: function(value) {
                if (value.length < 1) {
                    return '至少得1个字符啊';
                }
            },
            url: [/^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\*\+,;=.]+/s, '播放地址格式错误'],
            repass: function(value) {
                if ($('#L_pass').val() != $('#L_repass').val()) {
                    return '两次密码不一致';
                }
            }
        });

        // 监听全选
        form.on('checkbox(checkall)', function(data){

          if(data.elem.checked){
            $('tbody input').prop('checked',true);
          }else{
            $('tbody input').prop('checked',false);
          }
          form.render('checkbox');
        });


        //监听提交
        form.on('submit(add)',
        function(data) {
            $.post("/admin/data-add",{'data':data.field,'_token':token},function(data){
                if(data!=1){

                    //发异步，把数据提交给php
                    layer.alert("添加失败", {
                        icon: 2
                    },
                    function() {
                        //关闭当前frame
                        xadmin.close();

                        // 可以对父窗口进行刷新
                        xadmin.father_reload();
                    });

                }else{
                    //发异步，把数据提交给php
                    layer.alert("添加成功", {
                        icon: 6
                    },
                    function() {
                        //关闭当前frame
                        xadmin.close();

                        // 可以对父窗口进行刷新
                        xadmin.father_reload();
                    });

                }
            });

            return false;
        });





      });








    </script>
</html>
