@foreach ($res as $v)
<tr>
<td>
    <input type="checkbox" name="id" value="{{$v->id}}"   lay-skin="primary">
</td>
<td>{{$v->id}}</td>
<td><a href="/details/{{$v->id}}.html" target="_blank">{{$v->d_name}}</a></td>
<td>{{$v->typename}}</td>
<td>{{$v->note}}</td>
<td>{{date('Y-m-d H:i:s',$v->time)}}</td>
<td class="td-manage">
    <a onclick="member_edit(this,{{$v->id}})" class="layui-btn layui-btn-normal" href="javascript:;"  >
    <i class="iconfont">&#xe74e;</i> 编辑
    </a>
    @if ($v->ishidden==1)
    <a onclick="member_stop(this,{{$v->id}})" class="layui-btn layui-btn-warm" href="javascript:;"  >
    <i class="iconfont">&#xe69c;</i> 隐藏
    </a>
    @else
    <a onclick="member_acc(this,{{$v->id}})" class="layui-btn layui-btn-warm" href="javascript:;"  >
    <i class="iconfont">&#xe6af;</i> 显示
    </a>

    @endif

    @if ($v->ishidden==1)
         <a onclick="xadmin.open('添加新播放地址','data-addPlay?id={{$v->id}}',600,400)" class="layui-btn" href="javascript:;"  >
                                        <i class="iconfont">&#xe719;</i> 添加地址
                                      </a>
    @endif

    <a onclick="member_del(this,{{$v->id}})" class="layui-btn layui-btn-danger" href="javascript:;"  >
    <i class="iconfont">&#xe69d;</i> 删除
    </a>

</td>
</tr>
@endforeach
