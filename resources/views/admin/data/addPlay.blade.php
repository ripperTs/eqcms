
<!DOCTYPE html>
<html class="x-admin-sm">
    <head>
        <meta charset="UTF-8">
        <title>EQCMS后台管理系统 - 数据修改</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="/css/font.css">
        <link rel="stylesheet" href="/css/xadmin.css">
        <script src="/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/js/xadmin.js"></script>
        <!--[if lt IE 9]>
          <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
          <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-body ">
                            <form class="layui-form layui-col-space5">
                                影片名:
                                <div class="layui-inline layui-show-xs-block">
                                    <input class="layui-input" lay-verify="nikename" readonly  autocomplete="off" name="d_name" value="{{$res->d_name}}" id="start">
                                    <input type="hidden" name="id" value="{{$res->id}}">
                                </div>
                                <br><br>
                                选择播放来源:
                                        <div class="layui-inline layui-show-xs-block">
                                            <select name="psorce">
                                                @foreach ($lyname as $item)
                                                    <option value="{{$item}}" >{{$item}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                <br>
                                <div class="layui-inline layui-show-xs-block" style="width:100%;">
                                    <textarea name="playurl" placeholder="如果添加多个地址,每行一个." lay-verify="url" class="layui-textarea"></textarea>
                                </div>
                                <br><br>
                                <div class="layui-inline layui-show-xs-block" style="width:70%;">
                                    <button  class="layui-btn" lay-filter="add" lay-submit="">
                                        添加数据
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script>
      var token = "{{csrf_token()}}";
      layui.use(['laydate','form','upload','layedit'], function(){
        var laydate = layui.laydate;
        var  form = layui.form;

        //自定义验证规则
        form.verify({
            nikename: function(value) {
                if (value.length < 1) {
                    return '至少得1个字符啊';
                }
            },
            url: [/^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\*\+,;=.]+/s, '播放地址格式错误'],
            repass: function(value) {
                if ($('#L_pass').val() != $('#L_repass').val()) {
                    return '两次密码不一致';
                }
            }
        });

        // 监听全选
        form.on('checkbox(checkall)', function(data){

          if(data.elem.checked){
            $('tbody input').prop('checked',true);
          }else{
            $('tbody input').prop('checked',false);
          }
          form.render('checkbox');
        });


        //监听提交
        form.on('submit(add)',
        function(data) {
            $.post("/admin/data-doAddPlay",{'data':data.field,'_token':token},function(data){
                if(data!=1){

                    //发异步，把数据提交给php
                    layer.alert("添加失败", {
                        icon: 2
                    },
                    function() {
                        //关闭当前frame
                        xadmin.close();

                        // 可以对父窗口进行刷新
                        xadmin.father_reload();
                    });

                }else{
                    //发异步，把数据提交给php
                    layer.alert("添加成功", {
                        icon: 6
                    },
                    function() {
                        //关闭当前frame
                        xadmin.close();

                        // 可以对父窗口进行刷新
                        xadmin.father_reload();
                    });

                }
            });

            return false;
        });





      });








    </script>
</html>
