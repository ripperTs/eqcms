
<!DOCTYPE html>
<html class="x-admin-sm">
    <head>
        <meta charset="UTF-8">
        <title>EQCMS后台管理系统 - 数据修改</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="/css/font.css">
        <link rel="stylesheet" href="/css/xadmin.css">
        <script src="/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/js/xadmin.js"></script>
        <!--[if lt IE 9]>
          <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
          <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-body ">
                            @if ($res=='')
                                无详情数据,可能采集过程中入库失败导致,请重新采集!
                            @else


                            <form class="layui-form layui-col-space5">
                                影片名:
                                <div class="layui-inline layui-show-xs-block">
                                    <input class="layui-input"  autocomplete="off" name="d_name" value="{{$res->d_name}}">
                                </div>
                                影片类别:
                                <div class="layui-inline layui-show-xs-block">
                                    <select name="type">
                                        <option>请选择类别...</option>
                                        @foreach ($type as $v)
<option value="{{$v->id}}" {{($res->t_id == $v->id) ?"selected":''}} >{{$v->t_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                集数:
                                <div class="layui-inline layui-show-xs-block">
                                    <input type="text" name="note" autocomplete="off" value="{{$res->note}}" class="layui-input">
                                </div>
                                影片密码(为空则不加密):
                                <div class="layui-inline layui-show-xs-block">
                                    <input type="text" name="passwd" value="{{$res->passwd}}" autocomplete="off" class="layui-input">
                                </div>
                                 是否隐藏:
                                <div class="layui-inline layui-show-xs-block">
                                    <input type="radio" name="ishidden" autocomplete="off" {{($res->ishidden == 0)?'checked':''}} value="0" class="layui-input">
                                    <input type="radio" name="ishidden" autocomplete="off" {{($res->ishidden == 1)?'checked':''}} value="1" class="layui-input">
                                </div>
                                <br><br>
                                封面图片:
                                <div class="layui-inline layui-show-xs-block">
                                    <input type="text" name="img" id="pictext" size="40" autocomplete="off" value="{{$res->img}}" class="layui-input">
                                </div>
                                <a type="button" class="layui-btn" id="pic">
                                    <i class="layui-icon">&#xe67c;</i>上传封面
                                    </a>
                                导演:
                                <div class="layui-inline layui-show-xs-block">
                                    <input type="text" name="director" autocomplete="off" value="{{$res->director}}" class="layui-input">
                                </div>
                                主演:
                                <div class="layui-inline layui-show-xs-block">
                                    <input type="text" name="strring" size="30" autocomplete="off" value="{{$res->strring}}" class="layui-input">
                                </div>


                                <br><br>
                                点击次数:
                                <div class="layui-inline layui-show-xs-block">
                                    <input type="number" name="num" style="width:50px;" autocomplete="off" value="{{$res->num}}" class="layui-input">
                                </div>
                                评分:
                                <div class="layui-inline layui-show-xs-block">
                                    <input type="number" name="score" style="width:50px;" autocomplete="off" value="{{$res->score}}" class="layui-input">
                                </div>

                                地区:
                                <div class="layui-inline layui-show-xs-block">
                                    <input type="text" name="region" style="width:100px;" autocomplete="off" value="{{$res->region}}" class="layui-input">
                                </div>
                                语言:
                                <div class="layui-inline layui-show-xs-block">
                                    <input type="text" name="language" style="width:100px;" autocomplete="off" value="{{$res->language}}" class="layui-input">
                                </div>
                                年份:
                                <div class="layui-inline layui-show-xs-block">
                                    <input type="number" name="year" style="width:80px;" autocomplete="off" value="{{$res->year}}" class="layui-input">
                                </div>
                                观看限制(会员等级):
                                <div class="layui-inline layui-show-xs-block">
                                    <select name="level">
                                            <option value="" {{$res->level==''?'selected':''}}>不限制</option>
                                            <option value="1" {{$res->level=='1'?'selected':''}}>普通会员</option>
                                            <option value="2" {{$res->level=='2'?'selected':''}}>铜牌会员</option>
                                            <option value="3"  {{$res->level=='3'?'selected':''}}>银牌会员</option>
                                            <option value="4" {{$res->level=='4'?'selected':''}}>金牌会员</option>
                                            <option value="5" {{$res->level=='5'?'selected':''}}>钻石会员</option>
                                        </select>
                                </div>
                                <br><br>
                                <div class="playurl">
                                    @foreach ($paddress as $k=>$j)
                                    <div class="padnum">
                                        播放来源:
                                        <div class="layui-inline layui-show-xs-block">
                                            <select name="psorce{{$k}}">
                                                @foreach ($lyname as $item)
                                                    <option value="{{$item->psorce}}" {{$item->psorce==$j->psorce?'selected':''}} disabled>{{$item->psorce}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <b class="layui-btn" onclick="data_del(this,'{{$item->psorce}}')" href="javascript:;">删除</b>

                                        <br><br>
                                        <div class="layui-inline layui-show-xs-block" style="width:70%;">
                                            <textarea name="playurl{{$k}}" class="layui-textarea">{{str_replace("#","\n",$j->playurl)}}</textarea>
                                        </div>
                                        <br><br>
                                    </div>
                                    @endforeach
                                </div>
                                <div class="layui-inline layui-show-xs-block" style="width:70%;">
                                    影片简介:
                                </div>
                                <div class="layui-inline layui-show-xs-block" style="width:70%;">
                                    <textarea id="plot" name="plot" class="layui-textarea">{{$res->plot}}</textarea>
                                </div>
                                <div class="layui-inline layui-show-xs-block" style="width:70%;">
                                    @if (B_SWITCH==1)
                                    <input type="radio" name="baidu" value="1" class="layui-form-label">
                                    <div class="layui-form-mid layui-word-aux">百度主动推送此片</div>
                                    @endif
                                   <button  class="layui-btn" lay-filter="edit" lay-submit="">
                                        确定修改数据
                                    </button>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script>
    var token = "{{csrf_token()}}";
    var id = {{$res->id}};
    var did = {{$res->d_id}};

      layui.use(['laydate','form','upload','layedit'], function(){
        var laydate = layui.laydate;
        var  form = layui.form;
        var upload = layui.upload;
        var layedit = layui.layedit;


        //文件上传实例
        var upload = layui.upload; //实例化模块
        //执行实例
        var uploadInst = upload.render({
            elem: '#pic' //绑定元素
            ,url: '/admin/datalist' //上传处理路由
            ,size: '1024*50' //上传大小
            ,data:{'_token':token,'id':id}
            ,accept:'images'
            ,before: function(obj){ //obj参数包含的信息，跟 choose回调完全一致，可参见上文。
                layer.load(); //上传loading
            }
            ,choose: function(obj){
                //将每次选择的文件追加到文件队列
                var files = obj.pushFile();
                //预读本地文件，如果是多文件，则会遍历。(不支持ie8/9)
                obj.preview(function(index, file, result){
                });
            }
            ,done: function(res){
            //上传完毕回调
            if(res.status==1){
                layer.msg("修改封面成功",{icon:1});
                $("#pictext").val(res.path);
            }else{
                layer.msg("修改封面失败",{icon:2});
            }
            layer.closeAll('loading'); //关闭loading
            }
            ,error: function(){
            //请求异常回调
            layer.closeAll('loading'); //关闭loading
            }
        });

            //自定义验证规则
        form.verify({
            nikename: function(value) {
                if (value.length < 1) {
                    return '至少得1个字符啊';
                }
            },
            url: [/^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\*\+,;=.]+/s, '播放地址格式错误'],
            repass: function(value) {
                if ($('#L_pass').val() != $('#L_repass').val()) {
                    return '两次密码不一致';
                }
            }
        });
        //监听提交
        form.on('submit(edit)',
        function(data) {
            var padnum = document.getElementsByClassName("padnum").length;
            $.post("/admin/data-edit",{'data':data.field,'_token':token,'id':id,'did':did,'padnum':padnum},function(data){
                if(data==1){

                    //发异步，把数据提交给php
                    layer.alert("修改成功", {
                        icon: 6
                    },
                    function() {
                        //关闭当前frame
                        xadmin.close();

                        // 可以对父窗口进行刷新
                        xadmin.father_reload();
                    });

                }else{
                    //发异步，把数据提交给php
                    layer.alert("修改失败", {
                        icon: 2
                    },
                    function() {
                        //关闭当前frame
                        xadmin.close();

                        // 可以对父窗口进行刷新
                        xadmin.father_reload();
                    });

                }
            });

            return false;
        });

        // 监听全选
        form.on('checkbox(checkall)', function(data){

          if(data.elem.checked){
            $('tbody input').prop('checked',true);
          }else{
            $('tbody input').prop('checked',false);
          }
          form.render('checkbox');
        });

        //执行一个laydate实例
        laydate.render({
          elem: '#start' //指定元素
        });

        //执行一个laydate实例
        laydate.render({
          elem: '#end' //指定元素
        });


      });

       /*删除播放来源*/
      function data_del(obj,id){
          layer.confirm('确认要删除这个播放来源下全部播放地址吗？',function(index){
            $.get("/admin/data-delPlay",{'id':did,'lyname':id},function(data){
                if(data!=1){
                    //发异步删除数据
                    layer.msg('删除失败',{icon:2,time:1000});
                }else{
                        //发异步删除数据
                    $(obj).parent().remove();
                    layer.msg('已删除!',{icon:1,time:1000});
                }
            });

          });
      }

      /*用户-删除*/
      function member_del(obj,id){
          layer.confirm('确认要删除吗？',function(index){
              //发异步删除数据
              $(obj).parents("tr").remove();
              layer.msg('已删除!',{icon:1,time:1000});
          });
      }



      function delAll (argument) {
        var ids = [];

        // 获取选中的id
        $('tbody input').each(function(index, el) {
            if($(this).prop('checked')){
               ids.push($(this).val())
            }
        });

        layer.confirm('确认要删除吗？'+ids.toString(),function(index){
            //捉到所有被选中的，发异步进行删除
            layer.msg('删除成功', {icon: 1});
            $(".layui-form-checked").not('.header').parents('tr').remove();
        });
      }
    </script>
</html>
@endif
