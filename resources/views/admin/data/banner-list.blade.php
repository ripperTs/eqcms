
<!DOCTYPE html>
<html class="x-admin-sm">
    <head>
        <meta charset="UTF-8">
        <title>EQCMS后台管理系统 - 幻灯列表</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="/css/font.css">
        <link rel="stylesheet" href="/css/xadmin.css">
        <script src="/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/js/xadmin.js"></script>
        <!--[if lt IE 9]>
          <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
          <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="x-nav">
          <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
            <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i></a>
        </div>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-body ">
                            <blockquote class="layui-elem-quote">排序默认从大到小排序,数值越大越靠前!!!</blockquote>
                        </div>
                        <div class="layui-card-header">

                            <button class="layui-btn" onclick="xadmin.open('添加幻灯片','banner-add')"><i class="layui-icon"></i>添加幻灯片</button>
                        </div>
                        <div class="layui-card-body layui-table-body layui-table-main">
                            <table class="layui-table layui-form">
                                <thead>
                                  <tr>

                                    <th width="200">图片</th>
                                    <th width="200">标题</th>
                                    <th width="40">排序</th>
                                    <th width="50">显示/隐藏</th>
                                    <th>时间</th>
                                    <th>操作</th></tr>
                                </thead>
                                <tbody>
                                    @if ($count==0)
                                    <tr>
                                        <td colspan="9" align="center">暂无数据</td>
                                    </tr>
                                    @endif
                                    @foreach ($res as $v)
                                  <tr>
                                    <td>
                                        <img src="{{$v->img}}" alt="" width="100" height="60" style="width:100px;height:60px;border:1px solid #ddd;">
                                    </td>
                                    <td>{{$v->title}}</td>
                                    <td>
                                        <input type="text" class="layui-input" pid="{{$v->id}}" size="1" value="{{$v->sorting}}" style="width:40px;">
                                    </td>
                                    <td>
                                        @if ($v->ishidden==1)
                                            <input type="checkbox" name="zzz" value="{{$v->id}}" checked lay-filter="apitype" lay-skin="switch" lay-text="显示|隐藏">
                                        @else
                                            <input type="checkbox" name="zzz" value="{{$v->id}}" lay-filter="apitype" lay-skin="switch" lay-text="显示|隐藏">
                                        @endif

                                    </td>
                                    <td>{{date('Y-m-d H:i:s',$v->time)}}</td>
                                    <td>
                                      <a onclick="member_edit(this,'{{$v->id}}')" class="layui-btn layui-btn-normal" href="javascript:;">
                                        <i class="iconfont"></i> 编辑
                                      </a>
                                      <a onclick="member_del(this,'{{$v->id}}');" class="layui-btn layui-btn-danger" href="javascript:;">
                                        <i class="iconfont"></i> 删除
                                      </a>

                                    </td>
                                    </td>
                                  </tr>
                                   @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </body>
    <script>
      layui.use(['laydate','form'], function(){
        var laydate = layui.laydate;
        var  form = layui.form;
        $ = layui.jquery;


        //开关状态改变
         form.on('switch(apitype)', function(data){
            var checked = data.elem.checked;
            $.get("/admin/banner-change",{'id':data.elem.value,'zhi':checked},function(data){
                if(data==1){
                    layer.msg('状态已改变!',{icon:1,time:1000});
                }else{
                    layer.msg('状态未改变!',{icon:2,time:1000});
                }
            });
        });

        $("[type=text]").blur(function(){
            $.get("/admin/banner-px",{'id':$(this).attr("pid"),'zhi':$(this).val()},function(data){
                if(data==1){
                    layer.msg('排序已改变!',{icon:1,time:1000});
                    location.reload()
                }else{
                    layer.msg('排序修改失败!',{icon:1,time:1000});
                }
            });
        })

        // 监听全选
        form.on('checkbox(checkall)', function(data){

          if(data.elem.checked){
            $('tbody input').prop('checked',true);
          }else{
            $('tbody input').prop('checked',false);
          }
          form.render('checkbox');
        });


        //执行一个laydate实例
        laydate.render({
          elem: '#start' //指定元素
        });

        //执行一个laydate实例
        laydate.render({
          elem: '#end' //指定元素
        });


      });



      /*删除*/
      function member_del(obj,id){
          layer.confirm('确认要删除吗？',function(index){
              //发异步删除数据
              $.get("/admin/banner-del",{'id':id},function(data){
                  if(data==1){
                        $(obj).parents("tr").remove();
                        layer.msg('已删除!',{icon:1,time:1000});
                  }else{
                        layer.msg('删除失败!',{icon:2,time:1000});
                  }
              });

          });
      }

      /*编辑*/
      function member_edit(obj,id){
        xadmin.open('编辑Banner图信息','/admin/banner-edit?id='+id,500,500);
      }



      function delAll (argument) {
        var ids = [];

        // 获取选中的id
        $('tbody input').each(function(index, el) {
            if($(this).prop('checked')){
               ids.push($(this).val())
            }
        });

        layer.confirm('确认要删除吗？'+ids.toString(),function(index){
            //捉到所有被选中的，发异步进行删除
            layer.msg('删除成功', {icon: 1});
            $(".layui-form-checked").not('.header').parents('tr').remove();
        });
      }
    </script>
</html>
