
<!DOCTYPE html>
<html class="x-admin-sm">

    <head>
        <meta charset="UTF-8">
        <title>删除指定数据来源</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="/css/font.css">
        <link rel="stylesheet" href="/css/xadmin.css">
        <script src="/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/js/xadmin.js"></script>
    </head>

    <body>
        <div class="x-nav">
            <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
                <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i>
            </a>
        </div>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-body ">
                            <form class="layui-form layui-col-space5">
                                <div class="layui-input-inline layui-show-xs-block">

                                <div class="layui-input-inline layui-show-xs-block">
                                    <span>删除指定来源:</span>
                                <div class="layui-input-inline layui-show-xs-block">

                                    <select name="lyname">
                                        <option>请选择...</option>
                                        @foreach ($res as $v)
                                            <option value="{{$v->psorce}}">{{$v->psorce}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <br><br>
                                匹配特征字符:
                                <div class="layui-input-inline layui-show-xs-block">
                                    <input type="text" name="ytihuan" placeholder="按特征字符删除" autocomplete="off" size="30" readonly class="layui-input"></div>
                                    提示：留空为不限制，可以输入网址、域名、包含的字符特征
                                    <br><br>
                                <div class="layui-input-inline layui-show-xs-block">
                                    <button class="layui-btn" lay-submit="" lay-filter="del">
                                        <i class="iconfont">&#xe69d;</i> 执行删除</button>

                                </div>
                            </form>
                        </div>


                    </div>
                    <br><br>
                    <blockquote class="layui-elem-quote"><font color="red"><b>注意：为了防止意外、请操作前务必先备份数据库</b></font></blockquote>
                </div>
            </div>
        </div>
    </body>
    <script>layui.use(['laydate', 'form'],
        function() {
            var laydate = layui.laydate;
            var  form = layui.form;
            //执行一个laydate实例
            laydate.render({
                elem: '#start' //指定元素
            });

            //执行一个laydate实例
            laydate.render({
                elem: '#end' //指定元素
            });

             //监听提交
            form.on('submit(del)',
            function(data) {
                var index = layer.load(3, {time: 10*1000}); //加载等待动画
                $.get("/admin/operationDel",{'data':data.field},function(data){
                    if(data==1){
                        layer.close(index); //数据显示为完毕,关闭加载动画
                        layer.msg("操作成功.",{icon:1});
                    }else{
                        layer.close(index); //数据显示为完毕,关闭加载动画
                        layer.msg("失败了",{icon:2});
                    }

                });

                return false;
            });

        });





        </script>

</html>
