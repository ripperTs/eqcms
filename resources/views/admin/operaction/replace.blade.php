
<!DOCTYPE html>
<html class="x-admin-sm">

    <head>
        <meta charset="UTF-8">
        <title>数据替换</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="/css/font.css">
        <link rel="stylesheet" href="/css/xadmin.css">
        <script src="/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/js/xadmin.js"></script>
    </head>

    <body>
        <div class="x-nav">
            <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
                <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i>
            </a>
        </div>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-body ">
                            <form class="layui-form layui-col-space5">
                                <div class="layui-input-inline layui-show-xs-block">

                                <div class="layui-input-inline layui-show-xs-block">
                                    <span>准备将</span>
                                <div class="layui-input-inline layui-show-xs-block">

                                    <select name="tihuan">
                                        <option>请选择...</option>
                                        <option value="1">封面图片</option>
                                        <option value="2">数据地址</option>
                                        <option value="3">数据点击量</option>
                                        <option value="4">数据简介</option>
                                        <option value="5">数据名称</option>
                                    </select>
                                </div>
                                字段中的:
                                <div class="layui-input-inline layui-show-xs-block">
                                    <input type="text" name="ytihuan" placeholder="请输入要替换的字符" autocomplete="off" size="30" class="layui-input"></div>
                                    替换成:
                                    <div class="layui-input-inline layui-show-xs-block">
                                    <input type="text" size="30" name="tihuanh" placeholder="请输入替换后的字符" autocomplete="off" class="layui-input"></div>
                                <div class="layui-input-inline layui-show-xs-block">
                                    <button class="layui-btn" lay-submit="" lay-filter="th">
                                        <i class="iconfont">&#xe6fd;</i> 确认替换</button>
                                </div>
                            </form>
                        </div>


                    </div>
                    <br><br>
                    <blockquote class="layui-elem-quote"><font color="red"><b>请注意!该操作不可逆,请替换前注意数据备份!</b></font></blockquote>
                </div>
            </div>
        </div>
    </body>
    <script>layui.use(['laydate', 'form'],
        function() {
            var laydate = layui.laydate;
            var  form = layui.form;
            //执行一个laydate实例
            laydate.render({
                elem: '#start' //指定元素
            });

            //执行一个laydate实例
            laydate.render({
                elem: '#end' //指定元素
            });

             //监听提交
            form.on('submit(th)',
            function(data) {
                var index = layer.load(3, {time: 10*1000}); //加载等待动画
                $.getJSON("/admin/datareplate/create",{'data':data.field},function(data){
                    if(data.status==1){
                        layer.close(index); //数据显示为完毕,关闭加载动画
                        layer.msg("操作成功,本次替换数据 "+data.row+" 条.",{icon:1});
                    }else{
                        layer.close(index); //数据显示为完毕,关闭加载动画
                        layer.msg("失败了",{icon:2});
                    }

                });

                return false;
            });

        });





        </script>

</html>
