@foreach ($res as $v)
<tr>
<td>
    <input type="checkbox" name="id" value="{{$v->id}}"   lay-skin="primary">
</td>
<td>{{$v->id}}</td>
<td>{{$v->d_name}}</td>
<td>{{$v->note}}</td>
<td>{{date('Y-m-d H:i:s',$v->time)}}</td>
<td class="td-manage">
    @if ($v->ishidden==1)
    <a onclick="member_stop(this,{{$v->id}})" class="layui-btn layui-btn-warm" href="javascript:;"  >
    <i class="iconfont">&#xe69c;</i> 隐藏
    </a>
    @else
    <a onclick="member_acc(this,{{$v->id}})" class="layui-btn layui-btn-warm" href="javascript:;"  >
    <i class="iconfont">&#xe6af;</i> 显示
    </a>

    @endif

    <a onclick="member_del(this,{{$v->id}})" class="layui-btn layui-btn-danger" href="javascript:;"  >
    <i class="iconfont">&#xe69d;</i> 删除
    </a>

</td>
</tr>
@endforeach
