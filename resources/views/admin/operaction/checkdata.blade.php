
<!DOCTYPE html>
<html class="x-admin-sm">
    <head>
        <meta charset="UTF-8">
        <title>EQCMS后台管理系统 - 数据列表</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="/css/font.css">
        <link rel="stylesheet" href="/css/xadmin.css">
        <script src="/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/js/xadmin.js"></script>
        <!--[if lt IE 9]>
          <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
          <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="x-nav">
          <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
            <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i></a>
        </div>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-body ">
                            <blockquote class="layui-elem-quote">本页面用于检测采集到的数据是否完整?主要检测对应数据是否有播放地址?</blockquote>
                            <form class="layui-form layui-col-space5">
                              <button class="layui-btn layui-btn-danger" onclick="delAll();return false;"><i class="layui-icon"></i>批量删除</button>
                              <button class="layui-btn" onclick="xadmin.open('添加新数据','datalist/create');return false"><i class="layui-icon"></i>添加新数据</button>
                            </form>
                        </div>

                        <div class="layui-card-body layui-table-body layui-table-main">
                            <table class="layui-table layui-form">
                                <thead>
                                  <tr>
                                    <th width="20">
                                      <input type="checkbox" title="全选" lay-filter="checkall" name="" lay-skin="primary">
                                    </th>
                                    <th width="20">ID</th>
                                    <th width="130">影片名</th>
                                    <th>集数</th>
                                    <th>更新时间</th>
                                    <th width="200">操作</th></tr>
                                </thead>
                                <tbody>
                                    @foreach ($res as $v)
                                  <tr>
                                    <td>
                                      <input type="checkbox" name="id" value="{{$v->id}}"   lay-skin="primary">
                                    </td>
                                    <td>{{$v->id}}</td>
                                    <td>{{$v->d_name}}</td>
                                    <td>{{$v->note==''?'未知':$v->note}}</td>
                                    <td>{{date('Y-m-d H:i:s',$v->time)}}</td>
                                    <td class="td-manage">
                                      @if ($v->ishidden==1)
                                        <a onclick="member_stop(this,{{$v->id}})" class="layui-btn layui-btn-warm" href="javascript:;"  >
                                        <i class="iconfont">&#xe69c;</i> 隐藏
                                      </a>
                                      @else
                                        <a onclick="member_acc(this,{{$v->id}})" class="layui-btn layui-btn-warm" href="javascript:;"  >
                                        <i class="iconfont">&#xe6af;</i> 显示
                                      </a>

                                      @endif



                                      <a onclick="member_del(this,{{$v->id}})" class="layui-btn layui-btn-danger" href="javascript:;"  >
                                        <i class="iconfont">&#xe69d;</i> 删除
                                      </a>

                                    </td>
                                  </tr>
                                  @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="layui-card-body ">
                            <div class="page">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script>
      var type;
      layui.use(['laydate','form','laypage'], function(){
        var laydate = layui.laydate;
        var  form = layui.form;
        var laypage = layui.laypage;
        //监控select改变事件,取出要移动到哪个分类的id
        form.on('select(type)', function(data){
            type = data.value;
        });
        //执行分页ajax
        laypage.render({
            elem: document.getElementsByClassName("page")[0] //获取div元素(就是将分页按钮插入到哪个DIV中)
            ,count: {{$count}} //数据总数，从服务端得到
            ,limit:15 //每页显示数量
            ,jump: function(obj, first){
            //obj包含了当前分页的所有参数，比如：
            // console.log(obj.curr); //得到当前页，以便向服务端请求对应页的数据。
            // console.log(obj.limit); //得到每页显示的条数
            var url = "/admin/checkdata/?page="+obj.curr; //拼接跳转路由地址
            //首次不执行
            if(!first){
                //ajax分页
                var index = layer.load(2, {time: 10*1000}); //加载等待动画
                $.get(url,{},function(data){
                    $("tbody").html(data);
                    layer.close(index); //数据显示为完毕,关闭加载动画
                    $(document).scrollTop(0); //自动回到顶部
                });
                }
            }
        });


        // 监听全选
        form.on('checkbox(checkall)', function(data){

          if(data.elem.checked){
            $('tbody input').prop('checked',true);
          }else{
            $('tbody input').prop('checked',false);
          }
          form.render('checkbox');
        });

        //执行一个laydate实例
        laydate.render({
          elem: '#start' //指定元素
        });

        //执行一个laydate实例
        laydate.render({
          elem: '#end' //指定元素
        });


      });


      /*隐藏*/
      function member_stop(obj,id){
          layer.confirm('确定要隐藏吗？',function(index){
              //发异步删除数据
              $.get("/admin/data-yc",{'id':id},function(data){
                if(data==1){
                  $(obj).parents("tr").remove();
                  layer.msg('已隐藏!',{icon:1,time:1000});
                }else{
                  layer.msg('隐藏失败!',{icon:2,time:1000});
                }
              });

          });
      }

      /*显示*/
      function member_acc(obj,id){
          layer.confirm('确定要显示数据吗？',function(index){
              //发异步删除数据
              $.get("/admin/data-xs",{'id':id},function(data){
                if(data==1){
                  $(obj).parents("tr").remove();
                  layer.msg('已显示数据!',{icon:1,time:1000});
                }else{
                  layer.msg('数据显示失败!',{icon:2,time:1000});
                }
              });

          });
      }

      /*删除*/
      function member_del(obj,id){
          layer.confirm('会直接删除数据,确认要删除吗？',function(index){
              //发异步删除数据
              $.get("/admin/data-del",{'id':id},function(data){
                if(data==1){
                  $(obj).parents("tr").remove();
                  layer.msg('已删除!',{icon:1,time:1000});
                }else{
                  $(obj).parents("tr").remove();
                  layer.msg('删除失败!',{icon:2,time:1000});
                }
              });

          });
      }

      //批量删除数据
      function delAll (argument) {
        var ids = [];

        // 获取选中的id
        $('tbody input').each(function(index, el) {
            if($(this).prop('checked')){
               ids.push($(this).val())
            }
        });
        //未选中数据不弹框
        if(ids.toString()==''){
          return false;
        }
        layer.confirm('确认要删除所有被选中的数据吗？',function(index){
            //捉到所有被选中的，发异步进行删除
            $.post("/admin/data-delAll",{'ids':ids.toString(),'_token':"{{csrf_token()}}"},function(data){
              if(data==1){
                layer.msg('删除成功', {icon: 1});
                $(".layui-form-checked").not('.header').parents('tr').remove();
              }else{
                layer.msg('删除失败', {icon: 2});
              }
            });

        });
      }


      //批量移动数据
      function removeAll (argument) {
        var ids = [];

        // 获取选中的id
        $('tbody input').each(function(index, el) {
            if($(this).prop('checked')){
               ids.push($(this).val())
            }
        });
        //未选中数据不弹框
        if(ids.toString()==''){
          return false;
        }
        layer.confirm('确认要移动数据吗？',function(index){
            //捉到所有被选中的，发异步进行删除
            $.post("/admin/data-removeAll",{'ids':ids.toString(),'_token':"{{csrf_token()}}",'type':type},function(data){
              if(data==1){
                layer.msg('移动数据成功', {icon: 1});
                location.reload();
              }else{
                layer.msg('移动数据失败', {icon: 2});
              }
            });
        });
      }
    </script>
</html>
