
<!DOCTYPE html>
<html class="x-admin-sm">
    <head>
        <meta charset="UTF-8">
        <title>EQCMS后台管理系统 - 会员列表</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="/css/font.css">
        <link rel="stylesheet" href="/css/xadmin.css">
        <script src="/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/js/xadmin.js"></script>
        <!--[if lt IE 9]>
          <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
          <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="x-nav">
          <span class="layui-breadcrumb">
          </span>
          <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
            <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i></a>
        </div>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-body ">
                        </div>
                        <div class="layui-card-body layui-table-body layui-table-main">
                            <blockquote class="layui-elem-quote">在本页可以隐藏播放来源,使前台隐藏相关的播放地址.</blockquote>
                            <table class="layui-table layui-form">
                                <thead>
                                  <tr>
                                    <th>序号</th>
                                    <th>播放来源</th>
                                    <th width="270">操作</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach ($res as $k=>$v)
                                  <tr>
                                  <td>{{$k+1}}</td>
                                    <td>{{$v->psorce}}</td>
                                    <td class="td-manage">
                                    <input type="hidden" name="psorce" value="{{$v->psorce}}">
                                    <button class="layui-btn layui-btn layui-btn-xs" id="xs" lay-filter="xs"><i class="iconfont">&#xe6e6;</i> 显示</button>
                                    <button class="layui-btn layui-btn-warm layui-btn-xs" id="yc" lay-filter="yc"><i class="iconfont">&#xe69c;</i> 隐藏</button>

                                  </td>

                                  </tr>
                                  @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </body>
    <script>
      layui.use(['laydate','form'], function(){
        var laydate = layui.laydate;
        var  form = layui.form;
        layer = layui.layer,
        $=layui.jquery;
        element=layui.element,
        $(document).on('click','#xs',function(data){
            var a = $(this).prev().val();
            $.get("/admin/lyname-edit",{'name':a,'statue':'1'},function(data){
                console.log(data);
                if(data == 1){
                    layer.msg('操作成功!',{icon:1,time:1000});
                }else{
                    layer.msg('操作失败!',{icon:2,time:1000});
                }
            });

        });
        $(document).on('click','#yc',function(data){
            var a = $(this).prev().prev().val();
             $.get("/admin/lyname-edit",{'name':a,'statue':'0'},function(data){
                if(data == 1){
                    layer.msg('操作成功!',{icon:1,time:1000});
                }else{
                    layer.msg('操作失败!',{icon:2,time:1000});
                }
            });
        });




      });

    </script>
</html>
