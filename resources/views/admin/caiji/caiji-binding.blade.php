<!DOCTYPE html>
<html class="x-admin-sm">

    <head>
        <meta charset="UTF-8">
        <title>EQCM后台管理系统 - 分类绑定</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="/css/font.css">
        <link rel="stylesheet" href="/css/xadmin.css">
        <script type="text/javascript" src="/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/js/xadmin.js"></script>
        <script type="text/javascript" src="/js/jquery.min.js"></script>
        <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
        <!--[if lt IE 9]>
            <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
            <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="layui-fluid">
            <div class="layui-row">
                <span class="layui-fluid" style="color:red;width:100%;text-align:center;">Tips:采集前务必绑定分类,否则无法采集入库成功!解除绑定表示不采集该分类下所有内容</span>
                <div class="layui-form-item" style="margin-top:10px;">
                      <label for="L_repass" class="layui-form-label">
                      </label>
                      <button class="layui-btn" lay-filter="clear" lay-submit="clear">
                          清除分类绑定
                      </button>
                  </div>
                <form class="layui-form">
                  <div class="layui-form-item x-city" id="start">
                      @foreach ($data['class'] as $v)
                              <label class="layui-form-label" style="border:1px solid #ccc;background:#FBFBFB;width:50px;height:10px;line-height: 10px;margin-top:20px;">{{$v['type_name']}}</label>
                              <div class="layui-input-inline" style="margin-top:20px;">
                                <select name="apitype" lay-filter="apitype" typeid="{{$v['type_id']}}" dataId="{{$dataId}}">
                                    <option value="">请绑定分类</option>
                                    <option value="ban">解除(禁止)绑定</option>
                                    @foreach ($type as $value)

        <option value="{{$value->id}}" {{in_array($v['type_id'].'_'.$value->id,$ruletype)?'selected':''}} >{{$value->t_name}}</option>
                                    @endforeach
                                    {{-- 遍历分类 --}}
                                </select>
                              </div>
                      @endforeach


              </form>
            </div>
        </div>
        <script>
        var ids = {{$dataId}};
        layui.use(['form', 'layer'],
            function() {
                $ = layui.jquery;
                var form = layui.form,
                layer = layui.layer;

                form.on('select(apitype)', function(data){
                    var apiid = ($(data.elem).attr("typeid"));
                    id = ($(data.elem).attr("dataId"));
                    var typeid = data.value;

                    $.get("{{url('admin/caiji-dobinding')}}",{'typeid':typeid,'apiid':apiid,'id':id},function(data){
                        if(data==1){
                            layer.msg('绑定成功', {icon: 1});
                        }else if(data==0){
                            layer.msg('绑定失败', {icon: 2});
                        }else{
                            layer.msg('请先解除绑定,然后在绑定该分类', {icon: 2});
                        }
                    });

                });

                form.on('submit(clear)',function(){
                    $.get("{{url('admin/caiji-clear')}}",{'id':ids},function(data){
                        if(data==1){
                            //发异步，把数据提交给php
                            layer.alert("清除成功", {
                                icon: 6
                            },
                            function() {
                                //关闭当前frame
                                xadmin.close();

                                // 可以对父窗口进行刷新
                                xadmin.father_reload();
                            });
                        }else{
                            //发异步，把数据提交给php
                            layer.alert("清除失败", {
                                icon: 2
                            },
                            function() {
                                //关闭当前frame
                                xadmin.close();

                                // 可以对父窗口进行刷新
                                xadmin.father_reload();
                            });
                        }
                    });
                })

                //layer.msg('绑定成功', {icon: 1});
                //layer.msg('绑定失败', {icon: 2});

            });</script>
    </body>

</html>
