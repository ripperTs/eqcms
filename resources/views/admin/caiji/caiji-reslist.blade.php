
<!DOCTYPE html>
<html class="x-admin-sm">
    <head>
        <meta charset="UTF-8">
        <title>EQCMS后台管理系统 - 资源数据列表</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="/css/font.css">
        <link rel="stylesheet" href="/css/xadmin.css">
        <script src="/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/js/xadmin.js"></script>
        <!--[if lt IE 9]>
          <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
          <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="x-nav">
          <span class="layui-breadcrumb">
          </span>
          <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
            <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i></a>
        </div>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card">

                        <div class="layui-card-body ">
                            <blockquote class="layui-elem-quote">
                                <b>

                                每页显示:{{$pageSize}} 条&nbsp;&nbsp;
                                总页数:{{$pageCount}} &nbsp;&nbsp;
                                影片总数据:{{$recordCount}} &nbsp;&nbsp;
                                </b>
                            </blockquote>
                            <form class="layui-form layui-col-space5">
                                <a class="layui-btn layui-btn-normal" onclick="delAll();"><i class="iconfont">&#xe6b3;</i> 采集选中</a>
                                <div class="layui-inline layui-show-xs-block">
                                    <input type="text" name="ss" size="30"  placeholder="请输入影片名" autocomplete="off" class="layui-input">
                                </div>
                                <div class="layui-inline layui-show-xs-block">
                                    <button class="layui-btn"  lay-submit="sreach" lay-filter="sreach"><i class="layui-icon">&#xe615;</i></button>
                                </div>
                            </form>
                        </div>

                        <div class="layui-card-body layui-table-body layui-table-main">
                            <table class="layui-table layui-form">
                                <thead>
                                  <tr>
                                    <th>
                                      <input type="checkbox" title="全选" lay-filter="checkall" name="" lay-skin="primary">
                                    </th>
                                    <th>ID</th>
                                    <th>影片名</th>
                                    <th>所属分类</th>
                                    <th>集数</th>
                                    <th>最后更新时间</th>
                                </thead>
                                <tbody>
                                  @foreach ($list as $v)
                                  <tr>
                                    <td>
                                      <input type="checkbox" name="id" value="{{$v['vod_id']}}" lay-skin="primary">
                                    </td>
                                    <td>{{$v['vod_id']}}</td>
                                    <td>{{$v['vod_name']}}</td>
                                    <td>{{$v['type_name']}}</td>
                                    <td>{{is_string($v['vod_remarks'])?$v['vod_remarks']:''}}</td>
                                    <td>{{$v['vod_time']}}</td>
                                  </tr>
                                  @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="layui-card-body ">
                            <div class="page">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script>
      layui.use(['laydate','form','laypage'], function(){
        var laydate = layui.laydate;
        var  form = layui.form;
        var laypage = layui.laypage;
        var id = {{$id}}
        //执行一个laypage实例
        laypage.render({
            elem: document.getElementsByClassName("page")[0] //注意，这里的 test1 是 ID，不用加 # 号
            ,count: {{$recordCount}} //数据总数，从服务端得到
            ,limit:{{$pageSize}}
            ,jump: function(obj, first){
            //obj包含了当前分页的所有参数，比如：
            // console.log(obj.curr); //得到当前页，以便向服务端请求对应页的数据。
            // console.log(obj.limit); //得到每页显示的条数
            var url = "/admin/caiji-detail/"+obj.curr+"/"+id;
            //首次不执行
            if(!first){
                //ajax分页
                var index = layer.load(2, {time: 10*1000}); //加载等待动画
                $.get(url,{},function(data){
                    $("tbody").html(data);
                    layer.close(index); //数据显示为完毕,关闭加载动画
                    $(document).scrollTop(0); //自动回到顶部
                });
                }
            }
        });

        form.on('submit(sreach)',
                function(data) {
                    console.log(data.field);
                    $("tbody").html(' ');
                    $(".page").html(' ');
                    var ses = layer.load(2, {time: 10*1000});
                    $.getJSON("{{url('admin/caiji-search')}}",{'ss':data.field.ss,'id':id},function(data){
                        // console.log(data.list);
                        for(var i=0;i<=data.list.length;i++){
                            var elem = $("<tr><td><input type='checkbox' name='id' value='"+data.list[i].vod_id+"' lay-skin='primary'></td><td>"+data.list[i].vod_id+"</td><td>"+data.list[i].vod_name+"</td><td>"+data.list[i].type_name+"</td><td>"+data.list[i].vod_remarks+"</td><td>"+data.list[i].vod_time+"</td></tr>");
                            $("tbody").append(elem);
                            layer.close(ses);
                        }
                    });
                    return false;
                });

        // 监听全选
        form.on('checkbox(checkall)', function(data){

          if(data.elem.checked){
            $('tbody input').prop('checked',true);
          }else{
            $('tbody input').prop('checked',false);
          }
          form.render('checkbox');
        });

        //执行一个laydate实例
        laydate.render({
          elem: '#start' //指定元素
        });

        //执行一个laydate实例
        laydate.render({
          elem: '#end' //指定元素
        });


      });


      function delAll (argument) {
        var ids = [];

        // 获取选中的id
        $('tbody input').each(function(index, el) {
            if($(this).prop('checked')){
               ids.push($(this).val())
            }
        });

        //将id传到php准备采集
        var url = "/admin/caiji-ids/"+ids.toString()+"/"+{{$id}};

        window.location.href=url;

      }
    </script>
</html>
