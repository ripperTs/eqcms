<!DOCTYPE html>
<html class="x-admin-sm">
    <head>
        <meta charset="UTF-8">
        <title>EQCM后台管理系统 - 资源库列表</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="/css/font.css">
        <link rel="stylesheet" href="/css/xadmin.css">
        <script src="/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/js/xadmin.js"></script>
        <!--[if lt IE 9]>
          <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
          <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
      <div class="x-nav">
          <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
            <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i></a>
        </div>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">

                <div class="layui-col-md12">
                    <div class="layui-card">

                        <div class="layui-card-header">

                            <button class="layui-btn" onclick="xadmin.open('添加资源库','caiji-add',500,500)"><i class="layui-icon"></i>添加资源库</button>
                        </div>
                        <div class="layui-card-body ">
                            <table class="layui-table layui-form">
                              <thead>
                                <tr>

                                  <th>ID</th>
                                  <th>资源库名</th>
                                  <th>详情描述</th>
                                  <th>绑定分类</th>
                                  <th>采集当天</th>
                                  <th>采集全部</th>
                                  <th>添加时间</th>
                                  <th>操作</th>
                              </thead>
                              <tbody>
                                @foreach ($res as $v)


                                <tr>
                                  <td>{{$v->id}}</td>
                                  <td>{{$v->u_name}}</td>
                                  <td>{{$v->describe}}</td>
                                  <td>
                                    <button class="layui-btn layui-btn-normal layui-btn-mini" onclick="xadmin.open('绑定分类','caiji-binding/{{$v->id}}',900,600)">绑定分类</button>
                                  </td>
                                  <td>
                                    <span class="layui-btn layui-btn layui-btn-xs" onclick="xadmin.open('采集当天更新','/admin/caiji-xucai/?page=1&id={{$v->id}}&type=day')" href="javascript:;">采集当天更新</span></td>
                                  </td>
                                  <td>
                                    <span class="layui-btn layui-btn-warm layui-btn-xs" onclick="xadmin.open('采集全部更新','/admin/caiji-xucai/?page=1&id={{$v->id}}&type=all')" href="javascript:;">采集全部更新</span></td>
                                  </td>
                                  <td class="td-status">
                                    {{date('Y-m-d H:i',$v->time)}}
                                  <td class="td-manage">
                                    <a title="编辑资源库" class="layui-btn layui-btn layui-btn-xs"  onclick="xadmin.open('编辑资源库','caiji-edit/{{$v->id}}',500,500)" href="javascript:;">
                                      <i class="layui-icon">&#xe642; 编辑</i>
                                    </a>
                                    <a title="删除资源库" class="layui-btn-danger layui-btn layui-btn-xs" onclick="member_del(this,'{{$v->id}}')" href="javascript:;">
                                      <i class="layui-icon">&#xe640; 删除</i>
                                    </a>
                                  </td>
                                </tr>
                                @endforeach
                              </tbody>
                            </table>
                            <blockquote class="layui-elem-quote">
                              <font color="red"><b>如果中途采集中断,不必担心.采集工具支持断点续采功能!!!</b></font>
                            </blockquote>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </body>
    <script>
      layui.use(['laydate','form'], function(){
        var laydate = layui.laydate;
        var form = layui.form;

        //执行一个laydate实例
        laydate.render({
          elem: '#start' //指定元素
        });

        //执行一个laydate实例
        laydate.render({
          elem: '#end' //指定元素
        });
      });

       /*用户-停用*/
      function member_stop(obj,id){
          layer.confirm('确认要停用吗？',function(index){

              if($(obj).attr('title')=='启用'){

                //发异步把用户状态进行更改
                $(obj).attr('title','停用')
                $(obj).find('i').html('&#xe62f;');

                $(obj).parents("tr").find(".td-status").find('span').addClass('layui-btn-disabled').html('已停用');
                layer.msg('已停用!',{icon: 5,time:1000});

              }else{
                $(obj).attr('title','启用')
                $(obj).find('i').html('&#xe601;');

                $(obj).parents("tr").find(".td-status").find('span').removeClass('layui-btn-disabled').html('已启用');
                layer.msg('已启用!',{icon: 5,time:1000});
              }

          });
      }

      /*用户-删除*/
      function member_del(obj,id){
          layer.confirm('确认要删除吗？',function(index){
              //发异步删除数据
              $.post("{{url('admin/caiji-del')}}",{'id':id,'_token':"{{csrf_token()}}"},function(data){
                if(data==1){
                  $(obj).parents("tr").remove();
                  layer.msg('已删除!',{icon:1,time:1000});
                }else{
                  layer.msg('删除失败!',{icon:1,time:1000});
                }
              });

          });
      }



      function delAll (argument) {

        var data = tableCheck.getData();

        layer.confirm('确认要删除吗？'+data,function(index){
            //捉到所有被选中的，发异步进行删除
            layer.msg('删除成功', {icon: 1});
            $(".layui-form-checked").not('.header').parents('tr').remove();
        });
      }
    </script></html>
