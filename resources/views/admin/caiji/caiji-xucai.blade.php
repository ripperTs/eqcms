<!DOCTYPE html>
<html class="x-admin-sm">

    <head>
        <meta charset="UTF-8">
        <title>EQCM后台管理系统 - 断点续采</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="/css/font.css">
        <link rel="stylesheet" href="/css/xadmin.css">
        <script type="text/javascript" src="/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/js/xadmin.js"></script>
        <script type="text/javascript" src="/js/jquery.min.js"></script>
        <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
        <!--[if lt IE 9]>
            <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
            <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
    </body>
<script>
var id = {{$id}};
var as = '{{$as}}';
layui.use(['form', 'layer'],
            function() {
                $ = layui.jquery;
                var form = layui.form,
                layer = layui.layer;
                layer.open({
                    content: '你上次从采集没有完成,是否继续上次采集?'
                    ,title:'是否继续上次采集?'
                    ,btn: ['继续采集', '取消记录']
                    ,yes: function(index, layero){
                        //按钮【按钮一】的回调
                        var url = "/admin/caiji-doxucai/?type=1&id="+id;
                        var ses = layer.load(2, {time: 10*1000});
                        window.location.href=url;
                    }
                    ,btn2: function(index, layero){
                        //按钮【按钮二】的回调
                        $.get("/admin/caiji-doxucai",{'type':2,'id':id,'as':as},function(data){
                            console.log(data);
                            if(data==1){

                            //发异步，把数据提交给php
                            layer.alert("取消记录成功", {
                                icon: 6
                            },
                            function() {
                                //关闭当前frame
                                xadmin.close();

                                // 可以对父窗口进行刷新
                                xadmin.father_reload();
                            });

                            }else{
                                //发异步，把数据提交给php
                                layer.alert("取消记录失败", {
                                    icon: 2
                                },
                                function() {
                                    //关闭当前frame
                                    xadmin.close();

                                    // 可以对父窗口进行刷新
                                    xadmin.father_reload();
                                });

                            }
                        });
                        //return false 开启该代码可禁止点击该按钮关闭
                    }
                    ,cancel: function(){
                        //右上角关闭回调

                        //return false 开启该代码可禁止点击该按钮关闭
                    }
                });

                //监听提交
                form.on('submit(add)',
                function(data) {
                    // console.log(data.field);
                    $.post("{{url('admin/caiji-doadd')}}",{'u_name':data.field.u_name,'u_api':data.field.u_api,'describe':data.field.describe,'_token':"{{csrf_token()}}"},function(data){
                        if(data==1){

                            //发异步，把数据提交给php
                            layer.alert("添加成功", {
                                icon: 6
                            },
                            function() {
                                //关闭当前frame
                                xadmin.close();

                                // 可以对父窗口进行刷新
                                xadmin.father_reload();
                            });

                        }else{
                            //发异步，把数据提交给php
                            layer.alert("添加失败", {
                                icon: 2
                            },
                            function() {
                                //关闭当前frame
                                xadmin.close();

                                // 可以对父窗口进行刷新
                                xadmin.father_reload();
                            });

                        }
                    });

                    return false;
                });

            });</script>
</html>
