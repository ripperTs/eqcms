
<!DOCTYPE html>
<html class="x-admin-sm">

    <head>
        <meta charset="UTF-8">
        <title>EQCM后台管理系统 - 资源库添加</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="/css/font.css">
        <link rel="stylesheet" href="/css/xadmin.css">
        <script type="text/javascript" src="/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/js/xadmin.js"></script>
        <script type="text/javascript" src="/js/jquery.min.js"></script>
        <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
        <!--[if lt IE 9]>
            <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
            <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="layui-fluid">
            <div class="layui-row">

                <blockquote class="layui-elem-quote" style="color:red">Tips:使用XML类型接口不支持使用综合资源采集API</blockquote>
                <form class="layui-form">
                  <div class="layui-form-item">
                      <label for="username" class="layui-form-label">
                          资源库名称:
                      </label>
                      <div class="layui-input-inline">
                          <input type="text" id="username" name="u_name" required="" lay-verify="required"
                          autocomplete="off" class="layui-input">
                      </div>
                  </div>

                  <div class="layui-form-item">
                      <label for="username" class="layui-form-label">
                          资源库API:
                      </label>
                      <div class="layui-input-inline">
                          <input type="text" id="api" name="u_api" required="" lay-verify="required"
                          autocomplete="off" class="layui-input">
                      </div>
                  </div>

                  <div class="layui-form-item">
                      <label for="username" class="layui-form-label">
                          API接口类型:
                      </label>
                      <div class="layui-input-block">
                          <input type="radio" name="apitype" value="1" title="XML" checked>
                        <input type="radio" name="apitype" value="2" title="JSON">
                        </div>
                  </div>

                  <div class="layui-form-item">
                      <label for="username" class="layui-form-label">
                          资源库描述:
                      </label>
                      <div class="layui-input-inline">
                          <input type="text" id="describe" name="describe" required="" lay-verify="required"
                          autocomplete="off" class="layui-input">
                      </div>
                  </div>

                  <div class="layui-form-item">
                      <label for="L_repass" class="layui-form-label">
                      </label>
                      <button  class="layui-btn" lay-filter="add" lay-submit="">
                          添加资源库
                      </button>
                  </div>
              </form>
            </div>
        </div>
        <script>layui.use(['form', 'layer'],
            function() {
                $ = layui.jquery;
                var form = layui.form,
                layer = layui.layer;

                //监听提交
                form.on('submit(add)',
                function(data) {
                    // console.log(data.field);
                    $.post("{{url('admin/caiji-doadd')}}",{'u_name':data.field.u_name,'u_api':data.field.u_api,'describe':data.field.describe,'apitype':data.field.apitype,'_token':"{{csrf_token()}}"},function(data){
                        if(data==1){

                            //发异步，把数据提交给php
                            layer.alert("添加成功", {
                                icon: 6
                            },
                            function() {
                                //关闭当前frame
                                xadmin.close();

                                // 可以对父窗口进行刷新
                                xadmin.father_reload();
                            });

                        }else{
                            //发异步，把数据提交给php
                            layer.alert("添加失败", {
                                icon: 2
                            },
                            function() {
                                //关闭当前frame
                                xadmin.close();

                                // 可以对父窗口进行刷新
                                xadmin.father_reload();
                            });

                        }
                    });

                    return false;
                });

            });</script>
    </body>

</html>
