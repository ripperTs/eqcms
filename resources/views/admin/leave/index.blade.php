
<!DOCTYPE html>
<html class="x-admin-sm">
    <head>
        <meta charset="UTF-8">
        <title>EQCMS后台管理系统 - 留言列表</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="/css/font.css">
        <link rel="stylesheet" href="/css/xadmin.css">
        <script src="/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/js/xadmin.js"></script>
        <!--[if lt IE 9]>
          <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
          <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="x-nav">
          <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
            <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i></a>
        </div>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-body ">
                        </div>
                        <div class="layui-card-header">
                            <button class="layui-btn layui-btn-danger" onclick="delAll()"><i class="layui-icon"></i>批量删除</button>
                        </div>
                        <div class="layui-card-body layui-table-body layui-table-main">
                            <table class="layui-table layui-form">
                                <thead>
                                  <tr>
                                    <th>
                                      <input type="checkbox" title="全选" lay-filter="checkall" name="" lay-skin="primary">
                                    </th>
                                    <th>昵称</th>
                                    <th>联系邮箱</th>
                                    <th>留言主题</th>
                                    <th>留言内容</th>
                                    <th>留言IP</th>
                                    <th>留言时间</th>
                                    <th>是否审核</th>
                                    <th>操作</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if ($num==0)
                                    <tr>
                                        <td colspan="9" align="center">暂无数据</td>
                                    </tr>
                                @endif
                                @foreach ($res as $v)
                                  <tr>
                                    <td>
                                      <input type="checkbox" name="id" value="{{$v->id}}" lay-skin="primary">
                                    </td>
                                    <td>{{$v->name}}</td>
                                    <td>{{$v->email}}</td>
                                    <td>{{$v->title}}</td>
                                    <td>{{$v->content}}</td>
                                    <td>{{$v->ip}}</td>
                                    <td>{{date('Y-m-d H:i',$v->time)}}</td>
                                    <td>
                                        @if ($v->audit==1)
                                        <input type="checkbox" name="audit" value="{{$v->id}}" checked lay-filter="audit" lay-skin="switch" lay-text="已审核|待审核">
                                        @else
                                        <input type="checkbox" name="audit" value="{{$v->id}}" lay-filter="audit" lay-skin="switch" lay-text="已审核|待审核">
                                        @endif
                                    </td>
                                    <td class="td-manage">
                                        @if ($v->replay==1)
                                            <a title="查看回复内容" onclick="xadmin.open('查看回复内容','/admin/leave-replay/?id={{$v->id}}',600,400)" href="javascript:;">
                                            <span class="layui-btn"><i class="iconfont">&#xe6e6;</i> 查看</span>
                                        @else
                                            <a title="回复" onclick="xadmin.open('回复留言','/admin/leave/{{$v->id}}',600,400)" href="javascript:;">
                                            <span class="layui-btn layui-btn-normal"><i class="iconfont">&#xe69b;</i> 回复</span>
                                        @endif
                                      </a>
                                      <a title="删除" onclick="member_del(this,{{$v->id}})" href="javascript:;">
                                        <span class="layui-btn layui-btn-danger"><i class="iconfont">&#xe69d;</i> 删除</span>
                                      </a>
                                    </td>

                                  </tr>
                                  @endforeach

                                </tbody>
                            </table>
                        </div>
                        <div class="layui-card-body ">
                            <div class="page">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script>
        var token = "{{csrf_token()}}";
      layui.use(['laydate','form','laypage'], function(){
        var laydate = layui.laydate;
        var  form = layui.form;
        var laypage = layui.laypage;

        //审核开关状态改变
         form.on('switch(audit)', function(data){
            var checked = data.elem.checked;
            $.get("/admin/leave-audit",{'id':data.elem.value,'zhi':checked},function(data){
                if(data==1){
                    layer.msg('审核状态已改变!',{icon:1,time:1000});
                }else{
                    layer.msg('状态未改变!',{icon:2,time:1000});
                }
            });
        });

        //执行分页ajax
        laypage.render({
            elem: document.getElementsByClassName("page")[0] //获取div元素(就是将分页按钮插入到哪个DIV中)
            ,count: {{$count}} //数据总数，从服务端得到
            ,limit:{{$numm}} //每页显示数量
            ,jump: function(obj, first){
            //首次不执行
            if(!first){
                //ajax分页
                var index = layer.load(2, {time: 10*1000}); //加载等待动画
                $.get("/admin/leave",{'page':obj.curr},function(data){
                    $("tbody").html(data);
                    layer.close(index); //数据显示为完毕,关闭加载动画
                    $(document).scrollTop(0); //自动回到顶部
                });
                }
            }
        });

        // 监听全选
        form.on('checkbox(checkall)', function(data){

          if(data.elem.checked){
            $('[name=id]').prop('checked',true);
          }else{
            $('[name=id]').prop('checked',false);
          }
          form.render('checkbox');
        });



      });

       /*用户-停用*/
      function member_stop(obj,id){
          layer.confirm('确认要停用吗？',function(index){

              if($(obj).attr('title')=='启用'){

                //发异步把用户状态进行更改
                $(obj).attr('title','停用')
                $(obj).find('i').html('&#xe62f;');

                $(obj).parents("tr").find(".td-status").find('span').addClass('layui-btn-disabled').html('已停用');
                layer.msg('已停用!',{icon: 5,time:1000});

              }else{
                $(obj).attr('title','启用')
                $(obj).find('i').html('&#xe601;');

                $(obj).parents("tr").find(".td-status").find('span').removeClass('layui-btn-disabled').html('已启用');
                layer.msg('已启用!',{icon: 5,time:1000});
              }

          });
      }

      /*删除*/
      function member_del(obj,id){
          layer.confirm('确认要删除吗？',function(index){
              //发异步删除数据
              var url = "/admin/leave/"+id;
              $.post(url,{'type':'one','_method':"DELETE",'_token':token},function(data){
                  if(data==1){
                    $(obj).parents("tr").remove();
                    layer.msg('已删除!',{icon:1,time:1000});
                  }else{
                        layer.msg('删除失败!',{icon:2,time:1000});
                  }
              });

          });
      }



      function delAll (argument) {
        var ids = [];

        // 获取选中的id
        $('[name=id]').each(function(index, el) {
            if($(this).prop('checked')){
               ids.push($(this).val())
            }
        });

        layer.confirm('确认要删除吗？',function(index){
            //捉到所有被选中的，发异步进行删除
            var url = "/admin/leave/"+ids.toString();
            $.post(url,{'type':'all','_method':"DELETE",'_token':token},function(data){
                  if(data==1){
                        layer.msg('删除成功', {icon: 1});
                        $(".layui-form-checked").not('.header').parents('tr').remove();
                        location.reload();
                  }else{
                        layer.msg('删除失败!',{icon:2,time:1000});
                  }
              });
        });
      }
    </script>
</html>
