@foreach ($res as $v)
<tr>
<td>
    <input type="checkbox" name="id" value="{{$v->id}}" lay-skin="primary">
</td>
<td>{{$v->name}}</td>
<td>{{$v->email}}</td>
<td>{{$v->title}}</td>
<td>{{$v->content}}</td>
<td>{{$v->ip}}</td>
<td>{{date('Y-m-d H:i',$v->time)}}</td>
<td>
    @if ($v->audit==1)
    <input type="checkbox" name="audit" value="{{$v->id}}" checked lay-filter="audit" lay-skin="switch" lay-text="已审核|待审核">
    @else
    <input type="checkbox" name="audit" value="{{$v->id}}" lay-filter="audit" lay-skin="switch" lay-text="已审核|待审核">
    @endif
</td>
<td class="td-manage">
    @if ($v->replay==1)
        <a title="查看回复内容" onclick="xadmin.open('查看回复内容','/admin/leave-replay/?id={{$v->id}}',600,400)" href="javascript:;">
        <span class="layui-btn"><i class="iconfont">&#xe6e6;</i> 查看回复</span>
    @else
        <a title="回复" onclick="xadmin.open('回复留言','/admin/leave/{{$v->id}}',600,400)" href="javascript:;">
        <span class="layui-btn layui-btn-normal"><i class="iconfont">&#xe69b;</i> 回复</span>
    @endif
    </a>
    <a title="删除" onclick="member_del(this,{{$v->id}})" href="javascript:;">
    <span class="layui-btn layui-btn-danger"><i class="iconfont">&#xe69d;</i> 删除</span>
    </a>
</td>

</tr>
@endforeach
