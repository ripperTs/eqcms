
<!DOCTYPE html>
<html class="x-admin-sm">
    <head>
        <meta charset="UTF-8">
        <title>EQCMS后台管理系统 - 友情链接列表</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="/css/font.css">
        <link rel="stylesheet" href="/css/xadmin.css">
        <script src="/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/js/xadmin.js"></script>
        <!--[if lt IE 9]>
          <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
          <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="x-nav">
          <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
            <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i></a>
        </div>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-body ">
                        </div>
                        <div class="layui-card-header">
                            <button class="layui-btn" onclick="xadmin.open('添加友情链接','/admin/links/create',600,550)"><i class="layui-icon"></i>添加友情链接</button>
                        </div>
                        <div class="layui-card-body layui-table-body layui-table-main">
                            <table class="layui-table layui-form">
                                <thead>
                                  <tr>
                                    <th>网站名</th>
                                    <th>网站地址</th>
                                    <th>网站描述</th>
                                    <th>是否显示</th>
                                    <th>是否审核</th>
                                    <th>开始时间</th>
                                    <th>结束时间</th>
                                    <th>操作</th>
                                </tr>
                                </thead>
                                @if ($count==0)
                                    <tr>
                                        <td colspan="9" align="center">暂无数据</td>
                                    </tr>
                                @endif
                                @foreach ($res as $v)
                                <tbody>
                                  <tr>
                                    <td>{{$v->name}}</td>
                                    <td>{{$v->url}}</td>
                                    <td>{{$v->describe}}</td>
                                    <td>
                                        @if ($v->ishidden==1)
                                            <input type="checkbox" name="ishidden" value="{{$v->id}}" checked lay-filter="ishidden" lay-skin="switch" lay-text="显示|隐藏">
                                        @else
                                            <input type="checkbox" name="ishidden" value="{{$v->id}}" lay-filter="ishidden" lay-skin="switch" lay-text="显示|隐藏">
                                        @endif
                                    </td>
                                    <td>
                                        @if ($v->audit==1)
                                        <input type="checkbox" name="audit" value="{{$v->id}}" checked lay-filter="audit" lay-skin="switch" lay-text="已审核|待审核">
                                        @else
                                        <input type="checkbox" name="audit" value="{{$v->id}}" lay-filter="audit" lay-skin="switch" lay-text="已审核|待审核">
                                        @endif
                                    </td>
                                    <td>{{date("Y-m-d",$v->starttime)}}</td>
                                    <td>{{date("Y-m-d",$v->endtime)}}</td>
                                    <td>
                                        <a title="编辑" onclick="xadmin.open('编辑友情链接','/admin/links/{{$v->id}}/edit',600,600)" href="javascript:;">
                                        <i class="layui-icon"></i>
                                      </a>
                                      <a title="删除" onclick="member_del(this,{{$v->id}})" href="javascript:;">
                                        <i class="layui-icon"></i>
                                      </a>
                                    </td>
                                  </tr>
                                  @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script>
      var token = "{{csrf_token()}}";
      layui.use(['laydate','form'], function(){
        var laydate = layui.laydate;
        var  form = layui.form;

        //显示/隐藏开关状态改变
         form.on('switch(ishidden)', function(data){
            var checked = data.elem.checked;
            $.get("/admin/links-status",{'id':data.elem.value,'zhi':checked},function(data){
                if(data==1){
                    layer.msg('状态已改变!',{icon:1,time:1000});
                }else{
                    layer.msg('状态未改变!',{icon:2,time:1000});
                }
            });
        });
        //审核开关状态改变
         form.on('switch(audit)', function(data){
            var checked = data.elem.checked;
            $.get("/admin/links-audit",{'id':data.elem.value,'zhi':checked},function(data){
                if(data==1){
                    layer.msg('审核状态已改变!',{icon:1,time:1000});
                }else{
                    layer.msg('状态未改变!',{icon:2,time:1000});
                }
            });
        });

        // 监听全选
        form.on('checkbox(checkall)', function(data){

          if(data.elem.checked){
            $('tbody input').prop('checked',true);
          }else{
            $('tbody input').prop('checked',false);
          }
          form.render('checkbox');
        });

        //执行一个laydate实例
        laydate.render({
          elem: '#start' //指定元素
        });

        //执行一个laydate实例
        laydate.render({
          elem: '#end' //指定元素
        });


      });



      /*删除*/
      function member_del(obj,id){
          layer.confirm('确认要删除吗？',function(index){
              //发异步删除数据
              var url = "/admin/links/" + id;
              $.post(url,{'_token':token,'_method':'DELETE'},function(data){
                  if(data!=1){
                    layer.msg('删除失败!',{icon:1,time:1000});
                    }else{
                    //发异步，把数据提交给php
                    $(obj).parents("tr").remove();
                    layer.msg('已删除!',{icon:1,time:1000});
                    }
              });
          });
      }



    </script>
</html>
