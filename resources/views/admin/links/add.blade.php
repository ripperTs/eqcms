
<!DOCTYPE html>
<html class="x-admin-sm">
    <head>
        <meta charset="UTF-8">
        <title>EQCMS后台管理系统 - 添加友情链接</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="/css/font.css">
        <link rel="stylesheet" href="/css/xadmin.css">
        <script src="/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/js/xadmin.js"></script>
        <!--[if lt IE 9]>
          <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
          <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-body ">
                            <blockquote class="layui-elem-quote">添加成功后需要进行审核才可以显示哦~</blockquote>
                            <form class="layui-form layui-col-space5">
                                网站名:
                                <div class="layui-inline layui-show-xs-block">
                                    <input class="layui-input" lay-verify="nikename"  autocomplete="off" name="name" >
                                </div>

                                网站URL地址:
                                <div class="layui-inline layui-show-xs-block">
                                    <input type="text" name="url" lay-verify="url" size="40" autocomplete="off" class="layui-input">
                                </div>
                                <div style="display:none;">
                                <br><br>
                                网站LOGO:
                                <div class="layui-inline layui-show-xs-block">
                                    <input type="text" name="img" value="0" readonly id="pictext" autocomplete="off" class="layui-input">
                                </div>
                                <a type="button" class="layui-btn" id="pic">
                                    <i class="layui-icon">&#xe67c;</i>上传LOGO
                                    </a>
                                </div>
                                <br><br>
                                开始时间:
                                <div class="layui-inline">
                                    <input class="layui-input" type="text" name="starttime" id="start">
                                </div>
                                结束时间:
                                <div class="layui-inline">
                                    <input class="layui-input" type="text" name="endtime" id="end">
                                </div>
                                <div class="layui-inline layui-show-xs-block" style="width:70%;">
                                    网站描述:
                                </div>
                                <div class="layui-inline layui-show-xs-block" style="width:70%;">
                                    <textarea id="plot" name="describe" class="layui-textarea"></textarea>
                                </div>
                                <div class="layui-inline layui-show-xs-block" style="width:70%;">
                                    <button  class="layui-btn" lay-filter="add" lay-submit="">
                                        添加友链
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script>
      var token = "{{csrf_token()}}";
      layui.use(['laydate','form','upload','layedit','laydate'], function(){
        var laydate = layui.laydate;
        var  form = layui.form;
        var upload = layui.upload;
        var layedit = layui.layedit;
        //执行一个laydate实例
        laydate.render({
            elem: '#start' //指定元素
            ,format:"MM-dd-yyyy"
        });
        laydate.render({
            elem: '#end' //指定元素
            ,format:"MM-dd-yyyy"
        });
        //文件上传实例
        var upload = layui.upload; //实例化模块
        //执行实例
        var uploadInst = upload.render({
            elem: '#pic' //绑定元素
            ,url: '/admin/datalist' //上传处理路由
            ,size: '1024*5' //上传大小
            ,data:{'_token':token,'type':'add'}
            ,accept:'images'
            ,before: function(obj){ //obj参数包含的信息，跟 choose回调完全一致，可参见上文。
                layer.load(); //上传loading
            }
            ,choose: function(obj){
                //将每次选择的文件追加到文件队列
                var files = obj.pushFile();
                //预读本地文件，如果是多文件，则会遍历。(不支持ie8/9)
                obj.preview(function(index, file, result){
                });
            }
            ,done: function(res){
            //上传完毕回调
            if(res.status==1){
                layer.msg("上传封面成功",{icon:1});
                $("#pictext").val(res.path);
            }else{
                layer.msg("上传封面失败",{icon:2});
            }
            layer.closeAll('loading'); //关闭loading
            }
            ,error: function(){
            //请求异常回调
            layer.closeAll('loading'); //关闭loading
            }
        });

        //自定义验证规则
        form.verify({
            nikename: function(value) {
                if (value.length < 1) {
                    return '至少得1个字符啊';
                }
            },
            url: [/^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\*\+,;=.]+/s, '播放地址格式错误'],
            repass: function(value) {
                if ($('#L_pass').val() != $('#L_repass').val()) {
                    return '两次密码不一致';
                }
            }
        });

        // 监听全选
        form.on('checkbox(checkall)', function(data){

          if(data.elem.checked){
            $('tbody input').prop('checked',true);
          }else{
            $('tbody input').prop('checked',false);
          }
          form.render('checkbox');
        });


        //监听提交
        form.on('submit(add)',
        function(data) {
            $.post("/admin/links",{'data':data.field,'_token':token},function(data){
                if(data!=1){

                    //发异步，把数据提交给php
                    layer.alert("添加失败", {
                        icon: 2
                    },
                    function() {
                        //关闭当前frame
                        xadmin.close();

                        // 可以对父窗口进行刷新
                        xadmin.father_reload();
                    });

                }else{
                    //发异步，把数据提交给php
                    layer.alert("添加成功", {
                        icon: 6
                    },
                    function() {
                        //关闭当前frame
                        xadmin.close();

                        // 可以对父窗口进行刷新
                        xadmin.father_reload();
                    });

                }
            });

            return false;
        });

      });



    </script>
</html>
