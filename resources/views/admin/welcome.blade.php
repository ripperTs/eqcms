<!DOCTYPE html>
<html class="x-admin-sm">
    <head>
        <meta charset="UTF-8">
        <title>欢迎页面</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="/css/font.css">
        <link rel="stylesheet" href="/css/xadmin.css">
        <script src="/lib/layui/layui.js" charset="utf-8"></script>
        <script src="/js/jquery.min.js" charset="utf-8"></script>
        <script type="text/javascript" src="/js/xadmin.js"></script>
        <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
        <!--[if lt IE 9]>
          <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
          <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-body ">
                            <blockquote class="layui-elem-quote">欢迎管理员：
                                <span class="x-red">{{$admin_name}}</span>！当前时间:  <span class="dqsj"></span>
                            </blockquote>
                        </div>
                    </div>
                </div>
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-header">数据统计</div>
                        <div class="layui-card-body ">
                            <ul class="layui-row layui-col-space10 layui-this x-admin-carousel x-admin-backlog">
                                <li class="layui-col-md2 layui-col-xs6">
                                    <a href="javascript:;" class="x-admin-backlog-body">
                                        <h3>采集总数据</h3>
                                        <p>
                                            <cite>{{$cjz}}</cite></p>
                                    </a>
                                </li>
                                 <li class="layui-col-md2 layui-col-xs6">
                                    <a href="javascript:;" class="x-admin-backlog-body">
                                        <h3>今日更新数据</h3>
                                        <p>
                                            <cite>{{$dayTime}}</cite></p>
                                    </a>
                                </li>
                                <li class="layui-col-md2 layui-col-xs6">
                                    <a href="javascript:;" class="x-admin-backlog-body">
                                        <h3>会员数</h3>
                                        <p>
                                            <cite>{{$userNum}}</cite></p>
                                    </a>
                                </li>
                                <li class="layui-col-md2 layui-col-xs6">
                                    <a href="javascript:;" class="x-admin-backlog-body">
                                        <h3>留言数</h3>
                                        <p>
                                            <cite>{{$leave}}</cite></p>
                                    </a>
                                </li>
                                <li class="layui-col-md2 layui-col-xs6">
                                    <a href="javascript:;" class="x-admin-backlog-body">
                                        <h3>黑名单人数</h3>
                                        <p>
                                            <cite>{{$backlist}}</cite></p>
                                    </a>
                                </li>
                                <li class="layui-col-md2 layui-col-xs6 ">
                                    <a href="javascript:;" class="x-admin-backlog-body">
                                        <h3>评论数</h3>
                                        <p>
                                            <cite>{{$comment}}</cite></p>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-header">系统信息</div>
                        <div class="layui-card-body ">
                            <table class="layui-table">
                                <tbody>
                                    <tr>
                                        <th>EQCMS版本</th>
                                        <td>1-1.228</td></tr>
                                    <tr>
                                        <th>服务器地址</th>
                                        <td>{{$url}}</td></tr>
                                    <tr>
                                        <th>操作系统</th>
                                        <td>{{$system}}</td></tr>
                                    <tr>
                                        <th>PHP版本</th>
                                        <td>{{$php_v}}</td></tr>
                                    <tr>
                                        <th>PHP运行方式</th>
                                        <td>{{$mod}}</td></tr>
                                    <tr>
                                        <th>Laraevl</th>
                                        <td>{{$laravel}}</td></tr>
                                    <tr>
                                        <th>上传附件限制</th>
                                        <td>{{$maxFile}}</td></tr>
                                    <tr>
                                        <th>执行时间限制</th>
                                        <td>{{$maxTime}}</td></tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-header">开发团队</div>
                        <div class="layui-card-body ">
                            <table class="layui-table">
                                <tbody>
                                    <tr>
                                        <th width="100">版权所有</th>
                                        <td>EQCMS官方团队
                                            <a href="http://eqcms.top/" target="_blank">访问官网</a></td>
                                    </tr>
                                    <tr>
                                        <th>开发者</th>
                                        <td>
                                            <a href="http://wslmf.com/" target="_blank">WSLMF</a>  (wangyifani@foxmail.com) <br>
                                            <span>ZJM  (zjm0305@foxmail.com)</span> <br>
                                            <span>XHH (1293937416@qq.com)</span> <br>
                                            <span>HYX (412332955@qq.com)</span>
                                        </td></tr>
                                    <tr>
                                        <th>鸣谢</th>
                                        <td><span>DZ(duzheng@163.com)</span></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <style id="welcome_style"></style>

        </div>
        </div>
    </body>
    <script>
        var date=new Date();
        $(".dqsj").html(date);
    </script>
</html>
