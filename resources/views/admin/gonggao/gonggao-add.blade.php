<!DOCTYPE html>
<html class="x-admin-sm">
    <head>
        <meta charset="UTF-8">
        <title>EQCM后台管理系统 - 列表</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="/css/font.css">
        <link rel="stylesheet" href="/css/xadmin.css">
        <script src="/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/js/xadmin.js"></script>
       
    </head>
    <body>
      
        <div class="layui-fluid">
            <div class="layui-row">
              <!--  method="post" action="{{url('/admin/doggadd')}}"  -->
                <form class="layui-form">
                   {{csrf_field()}}
                  <div class="layui-form-item">
                      <label for="username" class="layui-form-label">
                          <span class="x-red"></span>标题
                      </label>
                      <div style="width: 380px;margin-left: 110px;">

                          <input type="text" id="username" name="title" required="" lay-verify="required"
                          autocomplete="off" class="layui-input">
                      </div>
                      <div class="layui-form-mid layui-word-aux">
                          <span class="x-red"></span>
                      </div>
                  </div>
                  <div class="layui-form-item">
                      <label for="phone" class="layui-form-label">
                          <span class="x-red"></span>内容
                      </label>
                      <div class="layui-input-inline">
                         <textarea  name="content" style="height:180px;width: 380px;" required lay-verify="required" placeholder="请输入" class="layui-textarea"></textarea>
                      </div>
                  </div>
                  <div class="layui-inline layui-show-xs-block" style="margin-left: 0px;width: 500px;">
                    <label for="phone" class="layui-form-label">
                          <span class="x-red"></span>发布时间
                    </label>
                    <div class="layui-input-inline" style="width: 380px;">
                        <input class="layui-input" id="start" name="s_time" autocomplete="off"  >
                    </div>
                  </div>
                  <br>
                  <div class="layui-inline layui-show-xs-block" style="margin-left: 0px;width: 380px;">
                    <label for="phone" class="layui-form-label">
                          <span class="x-red"></span>显示
                    </label>
                    <div class="layui-input-inline" style="width: 200px;">
                            <select name="gg_time" id="shipping">
                                  <option value="1">1周</option>
                                  <option value="2">2周</option>
                                  <option value="3">3周</option>
                                  <option value="4">4周</option>
                            </select>
                   
                    </div>
                  </div>
                  <br>


                   <div class="layui-inline layui-show-xs-block" style="margin-left: 0px;">
                   <label for="phone" class="layui-form-label">
                          <span class="x-red"></span>状态
                    </label>
                    &nbsp;&nbsp;&nbsp;
                    显示&nbsp;
                                    <input  name="status" class="layui-input"  autocomplete="off" type="radio"  value="1" checked>
                                    隐藏
                                    <input  name="status" class="layui-input"  autocomplete="off" type="radio" value="0">
                    </div>
                 
                   <div class="layui-form-item">
                      <label for="L_repass" class="layui-form-label">
                      </label>
                      <button  class="layui-btn" lay-filter="formDemo" lay-submit="">添加
                      </button>
                  </div>
                  
              </form>
            </div>
        </div>
    </body>
    <script>
      layui.use('form', function(){
      var form = layui.form;
      //监听提交
      form.on('submit(formDemo)', function(data){
          
          $.post("{{url('admin/doggadd')}}",{'title':data.field.title,'content':data.field.content,'s_time':data.field.s_time,'gg_time':data.field.gg_time,'status':data.field.status,'_token':"{{csrf_token()}}"},function(data){
           
                        if(data==1){
                            //发异步，把数据提交给php
                            layer.alert("添加成功", {
                                icon: 6
                            },
                            function() {
                                //关闭当前frame
                                xadmin.close();
                                // 可以对父窗口进行刷新
                                xadmin.father_reload();
                            });

                        }else{
                            //发异步，把数据提交给php
                            layer.alert("添加失败", {
                                icon: 2
                            },
                            function() {
                                //关闭当前frame
                                xadmin.close();

                                // 可以对父窗口进行刷新
                                xadmin.father_reload();
                            });

                        }
                    });

                    return false;
       
      });
       
    });

      layui.use(['laydate','form'], function(){
        var laydate = layui.laydate;
        var form = layui.form;

        //执行一个laydate实例
        laydate.render({
          elem: '#start' //指定元素
        });

        //执行一个laydate实例
        laydate.render({
          elem: '#end' //指定元素
        });
      });

     
    </script></html>

