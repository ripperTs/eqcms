<!DOCTYPE html>
<html class="x-admin-sm">
    <head>
        <meta charset="UTF-8">
        <title>EQCM后台管理系统 - 列表</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="/css/font.css">
        <link rel="stylesheet" href="/css/xadmin.css">
        <script src="/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/js/xadmin.js"></script>
        <!--[if lt IE 9]>
          <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
          <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="x-nav">
          <span class="layui-breadcrumb">
            <a href="">首页</a>
            <a href="">系统公告</a>
            <a>
              <cite>公告列表</cite></a>
          </span>
          <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
            <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i></a>
        </div>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-body ">
                            <form class="layui-form layui-col-space5">
                                
                                
                               <!--  <div class="layui-inline layui-show-xs-block" style="width: 400px;margin-left: 633px;">
                                    <input type="text" name="title"  placeholder="请输入标题名 内容的关键词" autocomplete="off" class="layui-input">
                                </div> -->
                               <!--  <div class="layui-inline layui-show-xs-block">
                                    <button class="layui-btn"  lay-submit="" lay-filter="sreach"><i class="layui-icon">&#xe615;</i></button>
                                </div> -->
                            </form>
                        </div>
                        <div class="layui-card-header">
                           <!--  <button class="layui-btn layui-btn-danger" onclick="delAll()"> <i class="layui-icon"></i>批量删除</button>
                            <input type="checkbox" name="" class="all" > -->
                            <button class="layui-btn" onclick="xadmin.open('添加公告','/admin/ggadd',580,550)">
                              <i class="layui-icon"></i>
                              <!-- <a href="/admin/ggadd"> -->
                              添加
                              <!-- </a> -->
                            </button>
                        </div>
                        <div class="layui-card-body ">
                            <table class="layui-table layui-form">
                              <thead>
                                <tr>
                                <!--   <th>
                                    <input type="checkbox" name="" class="" lay-skin="primary">
                                  </th> -->
                                  <th>ID</th>
                                  <th>标题</th>
                                  <th>内容</th>
                                  <th>状态</th>
                                  <th>发布时间</th>
                                  <th>撤销时间</th>
                                  <th>操作</th>
                              </thead>
                              @if ($count==0)
                                    <tr>
                                        <td colspan="9" align="center">暂无数据</td>
                                    </tr>
                                @endif
                              <tbody>
                                @foreach($row as $v)
                                <tr>
                                  <!-- <td>
                                    <input type="checkbox" name=""  lay-skin="primary">
                                  </td> -->
                                  <td>{{$v->id}}</td>
                                  <td>{{$v->title}}</td>
                                  <td>{{$v->content}}</td>
                                  <td>{{($v->status == 1)?'显示':'隐藏'}}</td>
                                  <td>{{date("Y-m-d",$v->s_time)}}</td>
                                  <td>{{date("Y-m-d",$v->e_time)}}</td>
                                  <!-- <td class="td-status">
                                    <span class="layui-btn layui-btn-normal layui-btn-mini">已启用</span></td> -->
                                  <td class="td-manage">
                                    <!-- <a onclick="member_stop(this,'10001')" href="javascript:;"  title="启用">
                                      <i class="layui-icon">&#xe601;</i>
                                    </a> -->
                                    &nbsp; &nbsp;
<!-- 
                                    <button href="/admin/ggedit/{{$v->id}}"
                                     class="layui-btn" onclick="xadmin.open('添加公告','/admin/ggadd',580,550)">
                                      <i class="layui-icon"></i>
                                      添加
                                    </button> -->

                                    <a title="编辑" onclick="xadmin.open('添加公告','/admin/ggedit/{{$v->id}}',600,570)" >
                                      <i class="layui-icon">&#xe642;</i>
                                    </a>
                                      &nbsp; &nbsp; &nbsp; &nbsp; 
                                    <!-- <a title="删除" onclick="member_del(this,'{{$v->id}}')" href="javascript:;" >
                                      <i class="layui-icon">&#xe640;</i>
                                    </a> -->
                                    <a title="删除"  href="/admin/ggdel/{{$v->id}}">
                                      <i class="layui-icon">&#xe640;</i>
                                    </a>
                                  </td>
                                </tr>
                                @endforeach

                              </tbody>
                            </table>
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script>
                                      $(.all).click(function(){
                                        $(.didi).attr("checked",$(".all")[0].checked);

                                      })

                                    </script>
    <script>
      layui.use(['laydate','form'], function(){
        var laydate = layui.laydate;
        var form = layui.form;

        //执行一个laydate实例
        laydate.render({
          elem: '#start' //指定元素
        });

        //执行一个laydate实例
        laydate.render({
          elem: '#end' //指定元素
        });
      });

       /*用户-停用*/
      function member_stop(obj,id){
          layer.confirm('确认要停用吗？',function(index){

              if($(obj).attr('title')=='启用'){

                //发异步把用户状态进行更改
                $(obj).attr('title','停用')
                $(obj).find('i').html('&#xe62f;');

                $(obj).parents("tr").find(".td-status").find('span').addClass('layui-btn-disabled').html('已停用');
                layer.msg('已停用!',{icon: 5,time:1000});

              }else{
                $(obj).attr('title','启用')
                $(obj).find('i').html('&#xe601;');

                $(obj).parents("tr").find(".td-status").find('span').removeClass('layui-btn-disabled').html('已启用');
                layer.msg('已启用!',{icon: 5,time:1000});
              }

          });
      }

      /*用户-删除*/
      // function member_del(obj,id){
      //     layer.confirm('确认要删除吗？',function(index){
      //       document.write(id);
      //       console.log(id);
      //         //发异步删除数据
      //         $(obj).parents("tr").remove();
      //         layer.msg('已删除!',{icon:1,time:1000});
      //     });
      // }



      function delAll (argument) {

        var data = tableCheck.getData();

        layer.confirm('确认要删除吗？'+data,function(index){
            //捉到所有被选中的，发异步进行删除
            layer.msg('删除成功', {icon: 1});
            $(".layui-form-checked").not('.header').parents('tr').remove();
        });
      }
    </script></html>
