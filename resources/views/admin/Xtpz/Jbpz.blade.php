<!DOCTYPE html>
<html class="x-admin-sm">
    <head>
        <meta charset="UTF-8">
        <title>基本配置</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="/css/font.css">
        <link rel="stylesheet" href="/css/xadmin.css">
        <link rel="stylesheet" href="/lib/layui/css/layui.css">
        <script src="/lib/layui/layui.js" charset="utf-8"></script>
        <script src="/js/jquery.min.js" charset="utf-8"></script>
        <script type="text/javascript" src="/js/xadmin.js"></script>
        <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
        <!--[if lt IE 9]>
          <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
          <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
    <div class="x-nav">
          <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
            <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i></a>
        </div>
<form class="layui-form" action="" enctype="multipart/form-data">
                        <div class="layui-card-body ">
                            <table class="layui-table">
                                <tbody>
                                    <tr>
                                        <th>网站名</th>
                                        <td>
                                        <input type="text" name="S_NAME" style="width:300px;" required lay-verify="required" placeholder="" value="{{S_NAME}}" autocomplete="off" class="layui-input">
                                        </td></tr>
                                    <tr>
                                        <th>网站域名</th>
                                        <td>
                                        <input type="text" name="S_REALM" style="width:200px;" required lay-verify="required" placeholder="" value="{{S_REALM}}" autocomplete="off" class="layui-input">
                                        </td></tr>
                                    <tr>
                                        <th>网站LOGO</th>
                                        <td>

                                        <img src="{{S_LOGO}}" alt="" id="logo" logo="logo">
                                        <button type="button" class="layui-btn" id="test1" tupian="1">
                                        <i class="layui-icon">&#xe67c;</i>上传LOGO
                                        </button>
                                        <!-- 隐藏域传老图片路径 -->
                                        <input type="hidden" name ="old_logo" value="{{S_LOGO}}">
                                        </td></tr>
                                    <tr>
                                        <th>SEO标题</th>
                                        <td>
                                        <input type="text" name="SEO" required lay-verify="required" placeholder="" value="{{SEO}}" autocomplete="off" class="layui-input">
                                        </td></tr>
                                    <tr>
                                        <th>SEO关键字</th>
                                        <td>
                                        <input type="text" name="KEYWORD" required  placeholder="" value="{{KEYWORD}}" autocomplete="off" class="layui-input">
                                        </td></tr>
                                    <tr>
                                        <th>SEO描述</th>
                                        <td>
                                        <textarea name="DEPICT" text="textarea" required lay-verify="required" placeholder="请输入SEO描述" class="layui-textarea">{{DEPICT}}</textarea>
                                        </td></tr>
                                    <tr>
                                        <th>微信公众号二维码</th>
                                        <td>
                                        <img src="{{TWO_IMG}}" alt="" id="two">
                                        <button type="button" class="layui-btn" id="test2" tupian='2'>
                                        <i class="layui-icon">&#xe67c;</i>上传二维码
                                        </button>
                                        <!-- 隐藏域传老图片路径 -->
                                        <input type="hidden" name ="old_two" value="{{S_LOGO}}">
                                        </td></tr>
                                    <tr>
                                        <th>底部联系邮箱</th>
                                        <td>
                                        <input type="text" name="EMAIL" style="width:200px;" required lay-verify="required" placeholder="" value="{{EMAIL}}" autocomplete="off" class="layui-input">
                                        </td></tr>
                                    <tr>
                                        <th>采集随机点击次数</th>
                                        <td>
                                        <div class="layui-form-mid layui-word-aux">最小值:</div><input type="number" name="CLICK_MIN" min="1" max="10" style="width:50px;float: left;" required lay-verify="required" placeholder="" value="{{CLICK_MIN}}" autocomplete="off" class="layui-input">

                                        <div class="layui-form-mid layui-word-aux"> 最大值:</div>
                                        <input type="number" name="CLICK_MAX" min="1" max="10" style="width:50px;float: left;" required lay-verify="required" placeholder="" value="{{CLICK_MAX}}" autocomplete="off" class="layui-input">
                                        </td></tr>
                                    <tr>
                                    <tr>
                                        <th>采集随机评分</th>
                                        <td>
                                        <div class="layui-form-mid layui-word-aux">最小值:</div><input type="number" name="SCORE_MIN" min="1" max="10" style="width:50px;float: left;" required lay-verify="required" placeholder="" value="{{SCORE_MIN}}" autocomplete="off" class="layui-input">

                                        <div class="layui-form-mid layui-word-aux"> 最大值:</div>
                                        <input type="number" name="SCORE_MAX" min="1" max="10" style="width:50px;float: left;" required lay-verify="required" placeholder="" value="{{SCORE_MAX}}" autocomplete="off" class="layui-input">
                                        </td></tr>
                                    <tr>
                                        <th>统计代码</th>
                                        <td>
                                        <input type="text" name="COUNT" required  placeholder="" value="{{COUNT}}" autocomplete="off" class="layui-input">
                                        </td></tr>
                                    <tr>
                                        <th>ICP备案号</th>
                                        <td>
                                        <input type="text" name="ICP" required style="width:200px;" placeholder="" value="{{ICP}}" autocomplete="off" class="layui-input">
                                        </td></tr>
                                    <tr>
                                        <th>版权信息</th>
                                        <td>
                                        <textarea name="COPYRIGHT" text="textarea" required lay-verify="required" placeholder="请输入版权信心" class="layui-textarea">{{COPYRIGHT}}</textarea>
                                        </td></tr>
                                    <tr>
                                        <th>百度推送域名</th>
                                        <td>
                                        <input type="text" name="B_REALM" style="width:150px;float: left;" required  placeholder="" value="{{B_REALM}}" autocomplete="off" class="layui-input">
                                        <div class="layui-form-mid layui-word-aux"> &nbsp;&nbsp;&nbsp;&nbsp;如:eqcms.top</div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>百度推送密钥</th>
                                        <td>
                                        <input type="text" name="B_KEYT" style="width:200px;" required  placeholder="" value="{{B_KEYT}}" autocomplete="off" class="layui-input">
                                        </td></tr>
                                    <tr>
                                        <th>百度推送开关</th>
                                        <td>
                                        @if(B_SWITCH==1)
                                             <input type="radio" name="B_SWITCH" value="1" lay-filter="levelM" title="开启" checked>
                                            <input type="radio" name="B_SWITCH" value="2" lay-filter="levelM" title="关闭" >
                                        @else
                                            <input type="radio" name="B_SWITCH" value="1" lay-filter="levelM" title="开启" >
                                            <input type="radio" name="B_SWITCH" value="2" lay-filter="levelM" title="关闭" checked>
                                        @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <span style="color:#47A1F8;">
                                                百度推送设置说明:请到<a style="color:red;" href="https://ziyuan.baidu.com/dashboard/index" target="_blank">百度站长统计官网</a>进行注册并获取自己的网站TOKEN才可正常使用此项设置! 设置开启后,数据添加和修改页面将会出现是否推送此影片按钮,选中即可!
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>更新判断条件(默认按更新地址)</th>
                                        <td>
                                        @if(CONDITION==1)
                                             <input type="radio" name="CONDITION" lay-filter="levelM" value="1" title="更新时间" checked>
                                            <input type="radio" name="CONDITION" lay-filter="levelM" value="2" title="播放地址数量" >
                                        @else
                                            <input type="radio" name="CONDITION" lay-filter="levelM" value="1" title="更新时间" >
                                            <input type="radio" name="CONDITION" lay-filter="levelM" value="2" title="播放地址数量" checked>
                                        @endif


                                        </td></tr>
                                    <tr>
                                        <th>是否开启更新图片(废弃)</th>
                                        <td>
                                        @if(ISIMG==1)
                                             <input type="radio" name="ISIMG" value="1" lay-filter="levelM" title="开启" checked>
                                            <input type="radio" name="ISIMG" value="2" lay-filter="levelM" title="关闭" >
                                        @else
                                            <input type="radio" name="ISIMG" value="1" lay-filter="levelM" title="开启" >
                                            <input type="radio" name="ISIMG" value="2" lay-filter="levelM" title="关闭" checked>
                                        @endif
                                        <div class="layui-form-mid layui-word-aux">(开启后可能会影响采集速度)</div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>是否开启更新简介(废弃)</th>
                                        <td>
                                        @if(ISCONTENT==1)
                                             <input type="radio" name="ISCONTENT" lay-filter="levelM" value="1" title="开启" checked>
                                            <input type="radio" name="ISCONTENT" lay-filter="levelM" value="2" title="关闭" >
                                        @else
                                            <input type="radio" name="ISCONTENT" lay-filter="levelM" value="1" title="开启"" >
                                            <input type="radio" name="ISCONTENT" lay-filter="levelM" value="2" title="关闭" checked>
                                        @endif
                                         <div class="layui-form-mid layui-word-aux">(开启后可能会影响采集速度)</div>
                                        </td></tr>
                                    <tr>
                                        <th>是否开启后台选项卡刷新记忆</th>
                                        <td>
                                        @if(MEMORY==1)
                                             <input type="radio" name="MEMORY" lay-filter="levelM" value="1" title="开启" checked>
                                            <input type="radio" name="MEMORY" lay-filter="levelM" value="2" title="关闭" >
                                        @else
                                            <input type="radio" name="MEMORY" lay-filter="levelM" value="1" title="开启" >
                                            <input type="radio" name="MEMORY" lay-filter="levelM" value="2" title="关闭" checked>
                                        @endif
                                        </td></tr>
                                    <tr>
                                        <th>会员系统开关</th>
                                        <td>
                                        @if(H_SWITCH==1)
                                             <input type="radio" name="H_SWITCH" lay-filter="levelM" value="1" title="开启" checked>
                                            <input type="radio" name="H_SWITCH" lay-filter="levelM" value="2" title="关闭" >
                                        @else
                                            <input type="radio" name="H_SWITCH" lay-filter="levelM" value="1" title="开启" >
                                            <input type="radio" name="H_SWITCH" lay-filter="levelM" value="2" title="关闭" checked>
                                        @endif
                                        </td></tr>
                                    <tr>
                                        <th>用户评论开关</th>
                                        <td>
                                        @if(Y_SWITCH==1)
                                             <input type="radio" name="Y_SWITCH" lay-filter="levelM" value="1" title="开启" checked>
                                            <input type="radio" name="Y_SWITCH" lay-filter="levelM" value="2" title="关闭" >
                                        @else
                                            <input type="radio" name="Y_SWITCH" lay-filter="levelM" value="1" title="开启" >
                                            <input type="radio" name="Y_SWITCH" lay-filter="levelM" value="2" title="关闭" checked>
                                        @endif
                                        </td></tr>
                                    <tr>
                                        <th>用户留言审核开关</th>
                                        <td>
                                        @if(S_SWITCH==1)
                                             <input type="radio" name="S_SWITCH" lay-filter="levelM" value="1" title="开启" checked>
                                            <input type="radio" name="S_SWITCH" lay-filter="levelM" value="2" title="关闭" >
                                        @else
                                            <input type="radio" name="S_SWITCH" lay-filter="levelM" value="1" title="开启" >
                                            <input type="radio" name="S_SWITCH" lay-filter="levelM" value="2" title="关闭" checked>
                                        @endif
                                        <div class="layui-form-mid layui-word-aux">(开启后用户留言需要管理员审核后才能显示)</div>
                                        </td></tr>
                                    <tr>
                                        <th>广告开关</th>
                                        <td>
                                        @if(AD==1)
                                             <input type="radio" name="AD" lay-filter="levelM" value="1" title="开启" checked>
                                            <input type="radio" name="AD" lay-filter="levelM" value="2" title="关闭" >
                                        @else
                                            <input type="radio" name="AD" lay-filter="levelM" value="1" title="开启" >
                                            <input type="radio" name="AD" lay-filter="levelM" value="2" title="关闭" checked>
                                        @endif
                                        </td></tr>
                                    <tr>
                                        <th>是否开启评论</th>
                                        <td>
                                        @if(P_CODE==1)
                                             <input type="radio" name="P_CODE" lay-filter="levelM" value="1" title="开启" checked>
                                            <input type="radio" name="P_CODE" lay-filter="levelM" value="2" title="关闭" >
                                        @else
                                            <input type="radio" name="P_CODE" lay-filter="levelM" value="1" title="开启" >
                                            <input type="radio" name="P_CODE" lay-filter="levelM" value="2" title="关闭" checked>
                                        @endif
                                        </td></tr>
                                    <tr>
                                        <th>是否开启页面缓存</th>
                                        <td>
                                        @if(E_SWITCH==1)
                                             <input type="radio" name="E_SWITCH" lay-filter="levelM" value="1" title="开启" checked>
                                            <input type="radio" name="E_SWITCH" lay-filter="levelM" value="2" title="关闭" >
                                        @else
                                            <input type="radio" name="E_SWITCH" lay-filter="levelM" value="1" title="开启" >
                                            <input type="radio" name="E_SWITCH" lay-filter="levelM" value="2" title="关闭" checked>
                                        @endif
                                        <div class="layui-form-mid layui-word-aux">(需要Redis支持)</div>
                                        </td></tr>
                                        <th>是否开启邮件通知</th>
                                        <td>
                                        @if(MAIL==1)
                                             <input type="radio" name="MAIL" lay-filter="levelM" value="1" title="开启" checked>
                                            <input type="radio" name="MAIL" lay-filter="levelM" value="2" title="关闭" >
                                        @else
                                            <input type="radio" name="MAIL" lay-filter="levelM" value="1" title="开启" >
                                            <input type="radio" name="MAIL" lay-filter="levelM" value="2" title="关闭" checked>
                                        @endif
                                        </td></tr>
                                    <tr>
                                        <th>设置Redis缓存过期时间(分钟)</th>
                                        <td>
                                        <input type="number" name="E_TIME" min="5" max="1440" style="width:70px;" required lay-verify="required" placeholder="" value="{{E_TIME}}" autocomplete="off" class="layui-input">
                                        </td></tr>
                                    <tr>
                                        <th>采集数据间隔时间(分钟)</th>
                                        <td>
                                        <input type="number" name="CJ_TIME" min="1" max="20" style="width:50px; required lay-verify="required" placeholder="" value="{{CJ_TIME}}" autocomplete="off" class="layui-input">
                                        </td></tr>
                                        <th>注册赠送积分</th>
                                        <td>
                                        <input type="number" name="REGINTEGRAL" style="width:50px;  required lay-verify="required" placeholder="" value="{{REGINTEGRAL}}" autocomplete="off" class="layui-input">
                                        </td></tr>
                                    <tr>
                                        <th>签到赠送积分</th>
                                        <td>
                                        <input type="number" name="QDINTEGRAL" style="width:50px;  required lay-verify="required" placeholder="" value="{{QDINTEGRAL}}" autocomplete="off" class="layui-input">
                                        </td>
                                    </tr>

                                    <tr>
                                        <th>铜牌会员所需金额(元)</th>
                                        <td>
                                        <input type="number" name="LEVEL2" style="width:50px;  required lay-verify="required" placeholder="" value="{{LEVEL2}}" autocomplete="off" class="layui-input">
                                        </td>
                                    </tr>

                                    <tr>
                                        <th>银牌会员所需金额(元)</th>
                                        <td>
                                        <input type="number" name="LEVEL3" style="width:50px;  required lay-verify="required" placeholder="" value="{{LEVEL3}}" autocomplete="off" class="layui-input">
                                        </td>
                                    </tr>

                                    <tr>
                                        <th>金牌会员所需金额(元)</th>
                                        <td>
                                        <input type="number" name="LEVEL4" style="width:50px;  required lay-verify="required" placeholder="" value="{{LEVEL4}}" autocomplete="off" class="layui-input">
                                        </td>
                                    </tr>

                                    <tr>
                                        <th>钻石会员所需金额(元)</th>
                                        <td>
                                        <input type="number" name="LEVEL5" style="width:50px;  required lay-verify="required" placeholder="" value="{{LEVEL5}}" autocomplete="off" class="layui-input">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
  <div class="layui-form-item">
    <div class="layui-input-block">
      <!-- <button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button> -->
      <!-- <button type="reset" class="layui-btn layui-btn-primary">修改配置</button> -->
    </div>
  </div>
</form>
 </body>
<script>
var token = "{{csrf_token()}}";
//Demo
layui.use(['form', 'layer','upload'], function(){
  var form = layui.form;

  //监听提交
  form.on('submit(formDemo)', function(data){
    // layer.msg(JSON.stringify(data.field));
    return false;
  });
  //图片上传
  var upload = layui.upload; //实例化模块
  //执行实例
  var uploadInst = upload.render({
    elem: '#test1' //绑定元素
    ,url: '/admin/api/upload' //上传处理路由
    ,size: '1024*50' //上传大小
    ,data:{'_token':token}
    ,before: function(obj){ //obj参数包含的信息，跟 choose回调完全一致，可参见上文。
        layer.load(); //上传loading
    }
    ,choose: function(obj){
        //将每次选择的文件追加到文件队列
        var files = obj.pushFile();
        //预读本地文件，如果是多文件，则会遍历。(不支持ie8/9)
        obj.preview(function(index, file, result){
        // console.log(index); //得到文件索引
        // console.log(file); //得到文件对象
        // console.log(result); //得到文件base64编码，比如图片
        //obj.resetFile(index, file, 'logo.jpg'); //重命名文件名，layui 2.3.0 开始新增

        $("#logo").attr('src',result);
        });
    }
    ,progress: function(n){
        var percent = n + '%' //获取进度百分比
        element.progress('demo', percent); //可配合 layui 进度条元素使用
    }
    ,done: function(res){
      //上传完毕回调
      if(res==1){
          layer.msg("上传成功",{icon:1});
      }else{
        layer.msg("上传失败",{icon:2});
      }
      layer.closeAll('loading'); //关闭loading
    }
    ,error: function(){
      //请求异常回调
      layer.closeAll('loading'); //关闭loading
    }
  });
  var upload2 = layui.upload; //实例化模块
  var uploadInst = upload2.render({
    elem: '#test2' //绑定元素
    ,url: '/admin/api/upload2' //上传处理路由
    ,size: '1024*50' //上传大小
    ,accept:"images"
    ,data:{'_token':token}
    ,before: function(obj){ //obj参数包含的信息，跟 choose回调完全一致，可参见上文。
        layer.load(); //上传loading
    }
    ,choose: function(obj){
        //将每次选择的文件追加到文件队列
        var files = obj.pushFile();
        //预读本地文件，如果是多文件，则会遍历。(不支持ie8/9)
        obj.preview(function(index, file, result){
        // console.log(index); //得到文件索引
        // console.log(file); //得到文件对象
        // console.log(result); //得到文件base64编码，比如图片
        //obj.resetFile(index, file, 'logo.jpg'); //重命名文件名，layui 2.3.0 开始新增
        $("#two").attr('src',result);
        });
    }
    ,progress: function(n){
        var percent = n + '%' //获取进度百分比
        element.progress('demo', percent); //可配合 layui 进度条元素使用
    }
    ,done: function(res){
      //上传完毕回调
      if(res==1){
          layer.msg("上传成功",{icon:1});
      }else{
        layer.msg("上传失败",{icon:2});
      }
      layer.closeAll('loading'); //关闭loading
    }
    ,error: function(){
      //请求异常回调
      layer.closeAll('loading'); //关闭loading
    }
  });
  $ = layui.jquery;
    var form = layui.form,
    layer = layui.layer;

    //自定义验证规则
    form.verify({
     nikename: function(value) {
         if (value.length < 5) {
             return '昵称至少得5个字符啊';
        }
     },
        pass: [/(.+){6,12}$/, '密码必须6到12位'],
        repass: function(value) {
        if ($('#L_pass').val() != $('#L_repass').val()) {
             return '两次密码不一致';
        }
        }
    });
    form.on('radio(levelM)', function(data){
        var zhi=$(this).val();
        var name=$(this).attr("name");
        $.get("{{url('admin/jbconfig/edit')}}",{"zhi":zhi,"name":name},function(data){
            if(data==1){
           layer.msg("已修改",{icon:1})
        }
    });
   	});
});
//输入框
$("[type=text]").blur(function(){
    var zhi=$(this).val();
    var name=$(this).attr("name");
    $.get("{{url('admin/jbconfig/edit')}}",{"zhi":zhi,"name":name},function(data){
        if(data==1){
           layer.msg("已修改",{icon:1})
        }
    });
});
//输入框
$("[type=number]").blur(function(){
    var zhi=$(this).val();
    var name=$(this).attr("name");
    $.get("{{url('admin/jbconfig/edit')}}",{"zhi":zhi,"name":name},function(data){
        if(data==1){
           layer.msg("已修改",{icon:1})
        }
    });
});
//输入文本框
$("[text=textarea]").blur(function(){
    var zhi=$(this).val();
    var name=$(this).attr("name");
    $.get("{{url('admin/jbconfig/edit')}}",{"zhi":zhi,"name":name},function(data){
        if(data==1){
           layer.msg("已修改",{icon:1})
        }
    });
})
$("[type=radio]").click(function(){
    var zhi=$(this).val();
    var name=$(this).attr("name");
    $.get("{{url('admin/jbconfig/edit')}}",{"zhi":zhi,"name":name},function(data){
        if(data==1){
           layer.msg("已修改",{icon:1})
        }
    });
})
</script>
</html>
