<!DOCTYPE html>
<html class="x-admin-sm">
    <head>
        <meta charset="UTF-8">
        <title>EQCM后台管理系统 - 登录日志</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="/css/font.css">
        <link rel="stylesheet" href="/css/xadmin.css">
        <script src="/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/js/xadmin.js"></script>
        <!--[if lt IE 9]>
          <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
          <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="x-nav">
          <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
            <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i></a>
        </div>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-body ">
                            <form class="layui-form layui-col-space5">
                            </form>
                        </div>
                        <div class="layui-card-header">
                            <button class="layui-btn layui-btn-danger" onclick="delAll()"><i class="layui-icon"></i>清空日志文件</button>
                        </div>
                        <div class="layui-card-body ">
                            <table class="layui-table layui-form" id="test">
                              <thead>
                                <tr>
                                  <th>序号</th>
                                  <th>登录信息</th>
                                </tr>
                              </thead>
                              <tbody>
                                
                                @foreach($xx as $k=>$v)
                                <tr>
                                  <td>{{$k}}</td>
                                  <td>{{$v}}</td>
                                 </tr>
                                @endforeach
                                
                              </tbody>
                            </table>
                        </div>
                        <div class="layui-card-body ">
                        
                            <div class="page">
                                <div>
                                
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script>
      layui.use(['laydate','form','laypage'], function(){
        var laydate = layui.laydate;
        var form = layui.form;
        var laypage = layui.laypage;
        //执行一个laydate实例
        laydate.render({
          elem: '#start' //指定元素
        });

        laypage.render({
          elem: document.getElementsByClassName("page")[0]
          ,count: {{$count}} //数据总数，从服务端得到
          ,limit:20
          ,jump: function(obj, first){
            //obj包含了当前分页的所有参数，比如：
            // console.log(obj.curr); //得到当前页，以便向服务端请求对应页的数据。
            // console.log(obj.limit); //得到每页显示的条数
            
            //首次不执行
            if(!first){
              $.get("/admin/loginlog",{'page':obj.curr},function(data){
        
                  $("tbody").html(data);
                  $(document).scrollTop(0);
              });
            }
          }
        });

        //执行一个laydate实例
        laydate.render({
          elem: '#end' //指定元素
        });
      });

       /*用户-停用*/
      function member_stop(obj,id){
          layer.confirm('确认要停用吗？',function(index){

              if($(obj).attr('title')=='启用'){

                //发异步把用户状态进行更改
                $(obj).attr('title','停用')
                $(obj).find('i').html('&#xe62f;');

                $(obj).parents("tr").find(".td-status").find('span').addClass('layui-btn-disabled').html('已停用');
                layer.msg('已停用!',{icon: 5,time:1000});

              }else{
                $(obj).attr('title','启用')
                $(obj).find('i').html('&#xe601;');

                $(obj).parents("tr").find(".td-status").find('span').removeClass('layui-btn-disabled').html('已启用');
                layer.msg('已启用!',{icon: 5,time:1000});
              }

          });
      }

      /*用户-删除*/
      function member_del(obj,id){
          layer.confirm('确认要删除吗？',function(index){
              //发异步删除数据
              $(obj).parents("tr").remove();
              layer.msg('已删除!',{icon:1,time:1000});
          });
      }



      function delAll (argument) {
        $.get("{{url('admin/Dlrz/del')}}",function(data){
          if(data==1){
            layer.msg("已删除!",{icon:1},function(){
              location.reload();
            });
            // // xadmin.father_reload();
            // // window.parent.location.reload();//刷新父页面
            // // var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
            // // parent.layer.close(index);
            
            // layer.msg(data.msg, {icon:1,time:1000},function(){
            //   setTimeout('window.location.reload()',1000);
            // });
            
          }else{
            layer.msg('没有删除!',{icon:2});
          }
        });
      }
    </script></html>
