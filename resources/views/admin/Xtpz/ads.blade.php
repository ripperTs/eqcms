<!DOCTYPE html>
<html class="x-admin-sm">
    <head>
        <meta charset="UTF-8">
        <title>EQCMS后台管理系统 - 广告列表</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="/css/font.css">
        <link rel="stylesheet" href="/css/xadmin.css">
        <script src="/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/js/xadmin.js"></script>
        <!--[if lt IE 9]>
          <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
          <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="x-nav">
          <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
            <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i></a>
        </div>
        <!-- <div class="x-nav">
          <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
            <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i></ a>
        </div> -->
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-body layui-table-body layui-table-main">
                          <span style="color:#47A1F8;">
                            <p>考虑到后续模版的增加以及各个网站需求,暂不计划添加固定广告位设置!</p>
                            <p>您可以自行到模版中修改代码进行广告的新增!</p>
                            <p>默认模版路径:/resource/view/template/default 全部的模版文件都在这里了,一般情况下我们设置的广告显示文件有:</p>
                            <p> &nbsp;&nbsp;&nbsp;&nbsp;首页文件->index.blade.php</p>
                            <p> &nbsp;&nbsp;&nbsp;&nbsp;底部公用文件->foot.balde.php</p>
                            <p> &nbsp;&nbsp;&nbsp;&nbsp;顶部公用文件->header.balde.php</p>
                            <p> &nbsp;&nbsp;&nbsp;&nbsp;影片详情文件->details.balade.php</p>
                            <p> &nbsp;&nbsp;&nbsp;&nbsp;影片播放文件->play.blade.php</p>
                          </span>
                          <span style="color:red">
                            <p>修改时请注意,不要修改大括号中间的内容,这是模版分配的标签变量!</p>
                            <p>修改之前,注意备份,以免造成不必要的损失!</p>
                          </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script>
      layui.use(['laydate','form'], function(){
        var laydate = layui.laydate;
        var  form = layui.form;
        // 监听全选
        form.on('checkbox(checkall)', function(data){
          if(data.elem.checked){
            $('tbody input').prop('checked',true);
          }else{
            $('tbody input').prop('checked',false);
          }
          form.render('checkbox');
        });

        //执行一个laydate实例
        laydate.render({
          elem: '#start' //指定元素
        });

        //执行一个laydate实例
        laydate.render({
          elem: '#end' //指定元素
        });
        form.on('submit(add)',
            function(data) {
            parent.location.reload(); //刷新父页面
        });
      });
       /*用户-停用*/
      function member_stop(obj,id){
              if($(obj).attr('title')=='启用'){
                layer.confirm('确认要停用吗？',function(index){
                    $.get("{{url('admin/ad/jinyong')}}",{'id':id,'qb':'1'},function(data){
                    })
                    //发异步把用户状态进行更改
                    $(obj).attr('title','停用')
                    $(obj).find('i').html('&#xe62f;');

                    $(obj).parents("tr").find(".td-status").find('span').addClass('layui-btn-disabled').html('已停用');
                    layer.msg('已停用!',{icon: 5,time:1000});
                 });

              }else{
                // $.get("{{url('admin/ad/jinyong')}}",function(data){
                //         console.log(data);
                //     })
                $.get("{{url('admin/ad/jinyong')}}",{'id':id,'qb':'2'},function(data){
                        console.log(data);
                    })
                layer.confirm('确认要启用吗？',function(index){
                    $(obj).attr('title','启用')
                    $(obj).find('i').html('&#xe601;');

                    $(obj).parents("tr").find(".td-status").find('span').removeClass('layui-btn-disabled').html('已启用');
                    layer.msg('已启用!',{icon: 6,time:1000});
                });
              }


      }

      /*广告-删除*/
      function member_del(obj,id,ad_img){
          var oo = [];
          var tp= [];
          oo.push(id);
          tp.push(ad_img)
         console.log(tp);
          layer.confirm('确认要删除吗？',function(index){
              //发异步删除数据
              $.get("{{url('admin/ad/del')}}",{"id":oo,"tp":tp},function(data){
                    if(data==1){
                        $(obj).parents("tr").remove();
                        layer.msg('已删除!',{icon:1,time:1000});
                    }else{
                        layer.msg('删除失败!',{icon:1,time:1000});
                    }
                })

          });
      }


  /*广告-全选删除*/
      function delAll (argument) {
        var ids = [];
        var tp = [];
        // 获取选中的id
        $('tbody input').each(function(index, el) {
            if($(this).prop('checked')){
               ids.push($(this).parent().next().html());
               tp.push($(this).parent().next().next().next().next().html())
            }
        });
        layer.confirm('确认要删除吗？'+ids.toString(),function(index){
            //捉到所有被选中的，发异步进行删除
            $.get("{{url('admin/ad/del')}}",{"id":ids,"tp":tp},function(data){
                if(data==1){
                        layer.msg('删除成功', {icon: 1});
                         $(".layui-form-checked").not('.header').parents('tr').remove();
                    }else{
                        layer.msg('删除失败', {icon: 2});
                    }
            })

        });
      }

    </script>
</html>
