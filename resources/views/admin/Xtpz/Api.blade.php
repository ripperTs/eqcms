<!DOCTYPE html>
<html class="x-admin-sm">
    <head>
        <meta charset="UTF-8">
        <title>EQCM后台管理系统 - 接口配置列表</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="/css/font.css">
        <link rel="stylesheet" href="/css/xadmin.css">
        <script src="/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/js/xadmin.js"></script>
        <!--[if lt IE 9]>
          <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
          <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="x-nav">
          <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
            <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i></a>
        </div>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-body ">
                        </div>
                        <div class="layui-card-body ">
                            <table class="layui-table layui-form">
                            <caption><h2>手机验证码接口配置(云之讯专用)</h2></caption>
                              <thead>
                                <tr>
                                  <th>ID</th>
                                  <th>模板id(<span style="color:red">0表示关闭手机号注册</span>)</th>
                                  <th>账户id</th>
                                  <th>秘钥</th>
                                  <th>Appid</th>
                                  <th>操作</th>
                              </thead>
                              <tbody>
                              @foreach($phone as $v)
                                <tr>
                                  <td>{{$v->id}}</td>
                                  <td>{{$v->templateid}}</td>
                                  <td>{{$v->accountid}}</td>
                                  <td>{{$v->token}}</td>
                                  <td>{{$v->appid}}</td>
                                  <td class="td-manage">
                                    <a title="编辑"  onclick="xadmin.open('编辑手机验证码接口','/admin/apiconfig/phone?id={{$v->id}}',600,400)" href="javascript:;">
                                      <i class="iconfont layui-btn">&#xe69e; 编辑</i>
                                    </a>
                                  </td>
                                </tr>
                              @endforeach
                              </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="layui-card">
                        <div class="layui-card-body ">
                        </div>
                        <div class="layui-card-body ">
                            <table class="layui-table layui-form">
                            <caption><h2>易支付接口配置</h2></caption>
                              <thead>
                                <tr>
                                  <th>ID</th>
                                  <th>商户ID</th>
                                  <th>秘钥</th>
                                  <th>接口地址</th>
                                  <th>操作</th>
                              </thead>
                              <tbody>
                              @foreach($pay as $v2)
                                <tr>
                                  <td>{{$v2->id}}</td>
                                  <td>{{$v2->partner}}</td>
                                  <td>{{$v2->key}}</td>
                                  <td>{{$v2->apiurl}}</td>
                                  <td class="td-manage">
                                    <a title="编辑"  onclick="xadmin.open('编辑支付接口信息','/admin/apiconfig/pay?id={{$v2->id}}',600,400)" href="javascript:;">
                                      <i class="iconfont layui-btn">&#xe69e; 编辑</i>
                                    </a>
                                  </td>
                                </tr>
                              @endforeach
                              </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="layui-card">
                        <div class="layui-card-body ">
                          <p style="color:red"><b>易支付推荐接口(仅供参考):</b></p>
                          <div class="api">
                            <iframe src="https://wslmf.com/api/payapi.php" frameborder="0" scrolling="no" width="700"></iframe>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script>
      layui.use(['laydate','form'], function(){
        var laydate = layui.laydate;
        var form = layui.form;
        //执行一个laydate实例
        laydate.render({
          elem: '#start' //指定元素
        });

        //执行一个laydate实例
        laydate.render({
          elem: '#end' //指定元素
        });

      });



       /*用户-停用*/
      function member_stop(obj,id){
          layer.confirm('确认要停用吗？',function(index){

              if($(obj).attr('title')=='启用'){

                //发异步把用户状态进行更改
                $(obj).attr('title','停用')
                $(obj).find('i').html('&#xe62f;');

                $(obj).parents("tr").find(".td-status").find('span').addClass('layui-btn-disabled').html('已停用');
                layer.msg('已停用!',{icon: 5,time:1000});

              }else{
                $(obj).attr('title','启用')
                $(obj).find('i').html('&#xe601;');

                $(obj).parents("tr").find(".td-status").find('span').removeClass('layui-btn-disabled').html('已启用');
                layer.msg('已启用!',{icon: 5,time:1000});
              }

          });
      }

      /*用户-删除*/
      function member_del(obj,id){
          layer.confirm('确认要删除吗？',function(index){
              //发异步删除数据
              $(obj).parents("tr").remove();
              layer.msg('已删除!',{icon:1,time:1000});
          });
      }



      function delAll (argument) {

        var data = tableCheck.getData();

        layer.confirm('确认要删除吗？'+data,function(index){
            //捉到所有被选中的，发异步进行删除
            layer.msg('删除成功', {icon: 1});
            $(".layui-form-checked").not('.header').parents('tr').remove();
        });
      }
    </script></html>
