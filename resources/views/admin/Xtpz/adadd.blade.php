<!DOCTYPE html>
<html class="x-admin-sm">

    <head>
        <meta charset="UTF-8">
        <title>EQCMS管理系统 - 广告添加</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="/css/font.css">
        <link rel="stylesheet" href="/css/xadmin.css">
        <script type="text/javascript" src="/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/js/xadmin.js"></script>
        <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
        <!--[if lt IE 9]>
            <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
            <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="layui-fluid">
            <div class="layui-row">
                <form class="layui-form" id="photoForm" method="post" action="{{url('admin/ad/doadd')}}" enctype="multipart/form-data">
                {{csrf_field()}}
                    <div class="layui-form-item">
                        <label for="L_email" class="layui-form-label">
                            <span class="x-red">*</span>广告名</label>
                        <div class="layui-input-inline">
                            <input type="text" id="" name="ad_name" required="" lay-verify="required" autocomplete="off" class="layui-input"></div>
                        <div class="layui-form-mid layui-word-aux">
                           </div></div>
                           <div class="layui-form-item">
                        <label for="" class="layui-form-label">
                            <span class="x-red">*</span>广告连接</label>
                        <div class="layui-input-inline">
                            <input type="text" id="" name="ad_join" required="" lay-verify="required" autocomplete="off" class="layui-input"></div>
                        <div class="layui-form-mid layui-word-aux">
                           </div></div>
                    <div class="layui-form-item">
                        <label for="L_username" class="layui-form-label">
                            <span class="x-red">*</span>广告时间</label>
                        <div class="layui-input-inline">
                            <select name="ad_time" id="shipping">
                            <option value="1">1周</option>
                            <option value="2">2周</option>
                            <option value="3">3周</option>
                            <option value="4">4周</option>
                            <option value="5">5周</option>
                            </select>
                    </div>
                    <div class="layui-form-item">
                        <label for="L_pass" class="layui-form-label">
                            <span class="x-red">*</span>广告图片</label>
                        <div class="layui-input-inline">
                            <input type="file" id="L_pass" name="ad_img" required="" lay-verify="required" autocomplete="off" class="layui-file"></div>
                        <div class="layui-form-mid layui-word-aux"></div></div>
                    <div class="layui-form-item">
                        <label for="L_repass" class="layui-form-label">
                            <span class="x-red">*</span>广告描述</label>
                        <div class="layui-input-inline">
                        <textarea placeholder="请输入内容" id="desc" name="ad_depict" lay-verify="required" class="layui-textarea"></textarea></div>
                    </div>
                    <div class="layui-form-item">
                        <label for="L_repass" class="layui-form-label"></label>
                        <button class="layui-btn" lay-filter="add" lay-submit="">增加</button></div>
                </form>
            </div>
        </div>
        <script>layui.use(['form', 'layer','jquery'],
            function() {
                $ = layui.jquery;
                var form = layui.form,
                layer = layui.layer;

                //自定义验证规则
                form.verify({
                    nikename: function(value) {
                        if (value.length < 2) {
                            return '广告至少得2个字符啊';
                        }
                    },
                });

                //监听提交
                form.on('submit(add)',
                    function(data) {
                    layer.alert("增加成功", {
                        icon: 6
                    },
                    function() {
                    });
                     parent.location.reload(); //刷新父页面     
                });
            });</script>
    </body>

</html>
