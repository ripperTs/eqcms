<!DOCTYPE html>
<html class="x-admin-sm">
    <head>
        <meta charset="UTF-8">
        <title>安全配置</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="/css/font.css">
        <link rel="stylesheet" href="/css/xadmin.css">
        <link rel="stylesheet" href="/lib/layui/css/layui.css">
        <script src="/lib/layui/layui.js" charset="utf-8"></script>
        <script src="/js/jquery.min.js" charset="utf-8"></script>
        <script type="text/javascript" src="/js/xadmin.js"></script>
        <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
        <!--[if lt IE 9]>
          <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
          <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
    <div class="x-nav">
          <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
            <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i></a>
        </div>
<form class="layui-form" action="" enctype="multipart/form-data">
                        <div class="layui-card-body ">
                            <table class="layui-table">
                                <tbody>
                                  <tr>
                                        <th>网站开关</th>
                                        <td>
                                        @if(W_SWITCH==1)
                                             <input type="radio" name="W_SWITCH" value="1" lay-filter="levelM" title="开启" checked>
                                            <input type="radio" name="W_SWITCH" value="2" lay-filter="levelM" title="关闭" >
                                        @else
                                            <input type="radio" name="W_SWITCH" value="1" lay-filter="levelM" title="开启" >
                                            <input type="radio" name="W_SWITCH" value="2" lay-filter="levelM" title="关闭" checked>
                                        @endif
                                        <div class="layui-form-mid layui-word-aux">(关闭后用户将无法访问前台所有页面)</div>
                                        </td></tr>
                                    <tr>
                                        <th>网站关闭提示</th>
                                        <td>
                                        <input type="text" name="W_HINT" required lay-verify="required" placeholder="" value="{{W_HINT}}" autocomplete="off" class="layui-input">
                                        </td>
                                    </tr>
                                    <tr>
                                      <th>开启会员观看限制</th>
                                      <td>
                                        @if(U_WATCH==1)
                                             <input type="radio" name="U_WATCH" value="1" lay-filter="levelM" title="开启" checked>
                                            <input type="radio" name="U_WATCH" value="2" lay-filter="levelM" title="关闭" >
                                        @else
                                            <input type="radio" name="U_WATCH" value="1" lay-filter="levelM" title="开启" >
                                            <input type="radio" name="U_WATCH" value="2" lay-filter="levelM" title="关闭" checked>
                                        @endif
                                        <div class="layui-form-mid layui-word-aux">(开启后前台任何影片观看都需要注册会员)</div>
                                      </td>
                                    </tr>
                                      <tr>
                                        <th>禁止访问前台的IP(默认空不判断,多个IP使用|分隔)</th>
                                        <td>
                                        <textarea name="BANIP" text="textarea" required lay-verify="required" placeholder="请输入IP,多个IP使用|分隔!注意:不要换行填写" class="layui-textarea">{{BANIP}}</textarea>
                                        </td></tr>
                                      <tr>
                                        <th>过滤留言关键字(多个关键字以|分隔)</th>
                                        <td>
                                        <textarea name="C_WORDS" text="textarea" required lay-verify="required" placeholder="请输入关键字,多个关键字使用|分隔!注意:不要换行填写" class="layui-textarea">{{C_WORDS}}</textarea>
                                        </td></tr>
                                      <tr>
                                        <th>自动采集KEY (<span style="color:red;">请尽快更改</span>)</th>
                                        <td>
                                        <input type="text" name="CJ_TOKEN" style="width:200px;float:left;" required lay-verify="required" placeholder="" value="{{CJ_TOKEN}}" autocomplete="off" class="layui-input">
                                        <div class="layui-form-mid layui-word-aux">&nbsp;&nbsp;&nbsp;&nbsp;用于免登陆挂机采集</div>
                                        </td>
                                      </tr>

                                      <tr>
                                        <th>Redis缓存标识 (<span style="color:red;">搭建多个站点需更改</span>)</th>
                                        <td>
                                        <input type="text" name="MARK_REDIS" style="width:200px;float:left;" required lay-verify="required" placeholder="" value="{{MARK_REDIS}}"  class="layui-input">
                                        <div class="layui-form-mid layui-word-aux">&nbsp;&nbsp;&nbsp;&nbsp;用于快速清除对应网站缓存</div>
                                        </td>
                                      </tr>

                                      <tr>
                                        <th>网站登录入口 Token (<span style="color:red;">为空则关闭入口验证</span>)</th>
                                        <td>
                                        <input type="text" name="LOGIN_TOKEN" style="width:200px;float:left;"  placeholder="为空则关闭入口验证" value="{{LOGIN_TOKEN}}"  class="layui-input">
                                        <div class="layui-form-mid layui-word-aux">&nbsp;&nbsp;&nbsp;&nbsp;保护网站后台登录地址安全,开启后后台登录地址变成 http://{{S_REALM}}/admin?token={{LOGIN_TOKEN}}</div>
                                        </td>
                                      </tr>

                                </tbody>
                            </table>
                        </div>
  <div class="layui-form-item">
    <div class="layui-input-block">
      <!-- <button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button> -->
      <!-- <button type="reset" class="layui-btn layui-btn-primary">修改配置</button> -->
    </div>
  </div>
</form>
 </body>
<script>
var token = "{{csrf_token()}}";
//Demo
layui.use(['form', 'layer','upload'], function(){
  var form = layui.form;

  //监听提交
  form.on('submit(formDemo)', function(data){
    // layer.msg(JSON.stringify(data.field));
    return false;
  });
  //图片上传
  var upload = layui.upload; //实例化模块
  //执行实例
  var uploadInst = upload.render({
    elem: '#test1' //绑定元素
    ,url: '/admin/api/upload' //上传处理路由
    ,size: '1024*50' //上传大小
    ,data:{'_token':token}
    ,before: function(obj){ //obj参数包含的信息，跟 choose回调完全一致，可参见上文。
        layer.load(); //上传loading
    }
    ,choose: function(obj){
        //将每次选择的文件追加到文件队列
        var files = obj.pushFile();
        //预读本地文件，如果是多文件，则会遍历。(不支持ie8/9)
        obj.preview(function(index, file, result){
        // console.log(index); //得到文件索引
        // console.log(file); //得到文件对象
        // console.log(result); //得到文件base64编码，比如图片
        //obj.resetFile(index, file, 'logo.jpg'); //重命名文件名，layui 2.3.0 开始新增

        $("#logo").attr('src',result);
        });
    }
    ,progress: function(n){
        var percent = n + '%' //获取进度百分比
        element.progress('demo', percent); //可配合 layui 进度条元素使用
    }
    ,done: function(res){
      //上传完毕回调
      if(res==1){
          layer.msg("上传成功",{icon:1});
      }else{
        layer.msg("上传失败",{icon:2});
      }
      layer.closeAll('loading'); //关闭loading
    }
    ,error: function(){
      //请求异常回调
      layer.closeAll('loading'); //关闭loading
    }
  });
  var upload2 = layui.upload; //实例化模块
  var uploadInst = upload2.render({
    elem: '#test2' //绑定元素
    ,url: '/admin/api/upload2' //上传处理路由
    ,size: '1024*50' //上传大小
    ,accept:"images"
    ,data:{'_token':token}
    ,before: function(obj){ //obj参数包含的信息，跟 choose回调完全一致，可参见上文。
        layer.load(); //上传loading
    }
    ,choose: function(obj){
        //将每次选择的文件追加到文件队列
        var files = obj.pushFile();
        //预读本地文件，如果是多文件，则会遍历。(不支持ie8/9)
        obj.preview(function(index, file, result){
        // console.log(index); //得到文件索引
        // console.log(file); //得到文件对象
        // console.log(result); //得到文件base64编码，比如图片
        //obj.resetFile(index, file, 'logo.jpg'); //重命名文件名，layui 2.3.0 开始新增
        $("#two").attr('src',result);
        });
    }
    ,progress: function(n){
        var percent = n + '%' //获取进度百分比
        element.progress('demo', percent); //可配合 layui 进度条元素使用
    }
    ,done: function(res){
      //上传完毕回调
      if(res==1){
          layer.msg("上传成功",{icon:1});
      }else{
        layer.msg("上传失败",{icon:2});
      }
      layer.closeAll('loading'); //关闭loading
    }
    ,error: function(){
      //请求异常回调
      layer.closeAll('loading'); //关闭loading
    }
  });
  $ = layui.jquery;
    var form = layui.form,
    layer = layui.layer;

    //自定义验证规则
    form.verify({
     nikename: function(value) {
         if (value.length < 5) {
             return '昵称至少得5个字符啊';
        }
     },
        pass: [/(.+){6,12}$/, '密码必须6到12位'],
        repass: function(value) {
        if ($('#L_pass').val() != $('#L_repass').val()) {
             return '两次密码不一致';
        }
        }
    });
    form.on('radio(levelM)', function(data){
        var zhi=$(this).val();
        var name=$(this).attr("name");
        $.get("{{url('admin/jbconfig/edit')}}",{"zhi":zhi,"name":name},function(data){
            if(data==1){
           layer.msg("已修改",{icon:1})
        }
    });
   	});
});
//输入框
$("[type=text]").blur(function(){
    var zhi=$(this).val();
    var name=$(this).attr("name");
    $.get("{{url('admin/jbconfig/edit')}}",{"zhi":zhi,"name":name},function(data){
        if(data==1){
           layer.msg("已修改",{icon:1})
        }
    });
});
//输入文本框
$("[text=textarea]").blur(function(){
    var zhi=$(this).val();
    var name=$(this).attr("name");
    $.get("{{url('admin/jbconfig/edit')}}",{"zhi":zhi,"name":name},function(data){
        if(data==1){
           layer.msg("已修改",{icon:1})
        }
    });
})
$("[type=radio]").click(function(){
    var zhi=$(this).val();
    var name=$(this).attr("name");
    $.get("{{url('admin/jbconfig/edit')}}",{"zhi":zhi,"name":name},function(data){
        if(data==1){
           layer.msg("已修改",{icon:1})
        }
    });
})
</script>
</html>
