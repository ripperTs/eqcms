<!DOCTYPE html>
<html class="x-admin-sm">

  <head>
    <meta charset="UTF-8">
    <title>EQCMS管理系统 - 设置角色</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="stylesheet" href="/css/font.css">
    <link rel="stylesheet" href="/css/xadmin.css">
    <script type="text/javascript" src="/lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="/js/xadmin.js"></script>
    <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
    <!--[if lt IE 9]>
      <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
      <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
    <div class="layui-fluid">
        <div class="layui-row">
            <form action="/admin/admin-power-do" method="post" class="layui-form layui-form-pane">
            <div class="layui-form-item">
                <div class="layui-input-block" style="margin-left:0px">
                @foreach($row as $v)
                <input type="checkbox" name="pid" title="{{$v->name}}" value="{{$v->id}}" {{$v->checked}}>
                @endforeach
                </div>
                
            </div>
                <div class="layui-form-item">
                <button class="layui-btn" lay-submit="" lay-filter="add">设置</button>
              </div>
            </form>
        </div>
    </div>
    <script>layui.use(['form', 'layer','jquery'],
            function() {
                $ = layui.jquery;
                var form = layui.form,
                layer = layui.layer;

          //自定义验证规则
          form.verify({
            name: function(value){
              if(value.length > 16){
                return '昵称最多16个字符啊';
              }
            }
            ,describe: [/(.+){0,250}$/, '描述超出字数限制']
            ,repass: function(value){
                if($('#L_pass').val()!=$('#L_repass').val()){
                    return '两次密码不一致';
                }
            }
          });

          //监听提交
          form.on('submit(add)', function(data){
            //发异步，把数据提交给php
            var pid = new Array();
            $("input:checkbox[name='pid']:checked").each(function(){
                pid.push($(this).val());
            })
            // var json = {};
            // for (var i = 0; i < tid.length; i++) {
            //     json[i] = tid[i];
            // }
            // var myjson = JSON.stringify(json);
            $.post("{{url('admin/admin-power-do')}}",{'_token':"{{csrf_token()}}",'pid':pid,'id':"{{$id}}"},function(data){
                if(data == '1'){
                    layer.alert("设置成功", {
                        icon: 6
                    },
                    function() {
                        //关闭当前frame
                        xadmin.close();

                        // 可以对父窗口进行刷新
                        xadmin.father_reload();
                    
                    });
                }else if(data == '0'){
                    layer.alert("设置失败", {
                        icon: 5
                    },
                    function() {
                        //关闭当前frame
                        xadmin.close();

                        // 可以对父窗口进行刷新
                        xadmin.father_reload();
                    
                    });
                }
            });
            return false;
            
          });


        form.on('checkbox(father)', function(data){

            if(data.elem.checked){
                $(data.elem).parent().siblings('td').find('input').prop("checked", true);
                form.render();
            }else{
               $(data.elem).parent().siblings('td').find('input').prop("checked", false);
                form.render();
            }
        });


        });
    </script>
  </body>

</html>
