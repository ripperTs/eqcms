<!DOCTYPE html>
<html class="x-admin-sm">

  <head>
    <meta charset="UTF-8">
    <title>EQCMS管理系统 - 模块添加</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="stylesheet" href="/css/font.css">
    <link rel="stylesheet" href="/css/xadmin.css">
    <script type="text/javascript" src="/lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="/js/xadmin.js"></script>
    <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
    <!--[if lt IE 9]>
      <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
      <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
    <div class="layui-fluid">
        <div class="layui-row">
            <form action="/admin/menu-doadd" method="post" class="layui-form layui-form-pane">
                <div class="layui-form-item">
                    <label for="m_name" class="layui-form-label">
                        <span class="x-red">*</span>模块名
                    </label>
                    <div class="layui-input-inline">
                        <input type="text" id="m_name" name="m_name" required="" lay-verify="required|m_name" autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label for="en_name" class="layui-form-label">
                        <span class="x-red">*</span>英文名(路径)
                    </label>
                    <div class="layui-input-inline">
                        <input type="text" id="en_name" name="en_name" required="" lay-verify="required|en_name" autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">父级菜单</label>
                    <div class="layui-input-block">
                        <select name="pid" lay-verify="required">
                            <option value="0">顶级菜单</option>
                            @foreach($res as $v)
                            <option value="{{$v->id}}">{{$v->m_name}}</option>
                            @endforeach
                        </select>     
                    </div>
                </div>
            <div class="layui-form-item">
                <button class="layui-btn" lay-submit="" lay-filter="add">增加</button>
            </div>
            </form>
        </div>
    </div>
    <script>layui.use(['form', 'layer','jquery'],
            function() {
                $ = layui.jquery;
                var form = layui.form,
                layer = layui.layer;

          //自定义验证规则
          form.verify({
            m_name: function(value){
              if(value.length > 80){
                return '模块名超出字数限制';
              }
            }
            ,en_name: function(value){
              if(value.length > 80){
                return '模块英文名超出字数限制';
              }
            }
            ,describe: [/(.+){0,250}$/, '描述超出字数限制']
            ,repass: function(value){
                if($('#L_pass').val()!=$('#L_repass').val()){
                    return '两次密码不一致';
                }
            }
          });

          //监听提交
          form.on('submit(add)', function(data){
            //发异步，把数据提交给php
            $.post("{{url('admin/menu-doadd')}}",{'_token':"{{csrf_token()}}",'m_name':data.field.m_name,'en_name':data.field.en_name,'pid':data.field.pid},function(data){
                if(data == '1'){
                    layer.alert("添加成功", {
                        icon: 6
                    },
                    function() {
                        //关闭当前frame
                        xadmin.close();

                        // 可以对父窗口进行刷新
                        xadmin.father_reload();
                    
                    });
                }else if(data == '0'){
                    layer.alert("添加失败", {
                        icon: 5
                    },
                    function() {
                        //关闭当前frame
                        xadmin.close();

                        // 可以对父窗口进行刷新
                        xadmin.father_reload();
                    
                    });
                }
            });
            return false;
            
          });


        form.on('checkbox(father)', function(data){

            if(data.elem.checked){
                $(data.elem).parent().siblings('td').find('input').prop("checked", true);
                form.render();
            }else{
               $(data.elem).parent().siblings('td').find('input').prop("checked", false);
                form.render();
            }
        });


        });
    </script>
  </body>

</html>
