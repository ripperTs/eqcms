<!DOCTYPE html>
<html class="x-admin-sm">
    <head>
        <meta charset="UTF-8">
        <title>EQCMS后台管理系统 - 地图</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="/css/font.css">
        <link rel="stylesheet" href="/css/xadmin.css">
        <script src="/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/js/xadmin.js"></script>
        <!--[if lt IE 9]>
          <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
          <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="x-nav">

          <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload(true)" title="刷新">
            <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i></a>
        </div>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card">

                        <div class="layui-card-header">
                        <button class="layui-btn" onclick="xadmin.open('添加模块','/admin/menu-add',500,400)"><i class="layui-icon"></i>添加</button>
                            <form action="/admin/menulist" class="layui-form layui-col-space5" style="float:right;margin-right:50px">

                                <div class="layui-inline layui-show-xs-block">
                                    <input type="text" name="keywords"  placeholder="请输入模块名" autocomplete="off" class="layui-input">
                                </div>
                                <div class="layui-inline layui-show-xs-block">
                                    <button class="layui-btn"  lay-submit="" lay-filter="sreach"><i class="layui-icon">&#xe615;</i></button>
                                </div>
                            </form>

                        </div>
                        <div class="layui-card-body">
                            <table class="layui-table layui-form">
                                <thead>
                                  <tr>
                                    <th>ID</th>
                                    <th>模块名</th>
                                    <th>模块英文名(路径)</th>
                                    <th>是否隐藏</th>
                                    <th>操作</th></tr>
                                </thead>
                                <tbody class="x-cate">
                                @foreach($res as $v)
                                  @if ($v->pid == '0')
                                  <tr cate-id='{{$v->id}}' fid='{{$v->pid}}'>
                                    <td>{{$v->id}}</td>
                                    <td><i class="layui-icon x-show" status='true'>&#xe623;</i>{{$v->m_name}}</td>
                                    <td>{{$v->en_name}}</td>
                                    <td class="td-status">
                                      <span class="layui-btn layui-btn-normal layui-btn-mini
                                      @if($v->ishidden == '0')
                                      layui-btn-disabled
                                      @endif
                                          ">@if($v->ishidden == '0')隐藏
                                            @elseif($v->ishidden == '1')显示
                                            @endif</span></td>
                                    <td class="td-manage">
                                      <a onclick="member_stop(this,'10001')" href="javascript:;" uid="{{$v->id}}" title="@if($v->ishidden == '0')显示@elseif($v->ishidden == '1')隐藏@endif">
                                        <i class="layui-icon">@if($v->ishidden == '0')&#xe62f;@elseif($v->ishidden == '1')&#xe601;@endif</i>
                                      </a>

                                      <a onclick="xadmin.open('修改','/admin/menu-edit/{{$v->id}}',600,500)" title="修改" href="javascript:;">
                                        <i class="layui-icon">&#xe642;</i>
                                      </a>

                                      <a title="删除" onclick="member_del(this,'{{$v->id}}')" href="javascript:;">
                                        <i class="layui-icon">&#xe640;</i>
                                      </a>
                                    </td>
                                  </tr>
                                  @endif
                                  @foreach($res as $V)
                                  @if ($V->pid == $v->id)
                                  <tr cate-id='{{$V->id}}' fid='{{$V->pid}}'>
                                    <td>{{$V->id}}</td>
                                    <td>{{$V->m_name}}</td>
                                    <td>{{$V->en_name}}</td>
                                    <td class="td-status">
                                      <span class="layui-btn layui-btn-normal layui-btn-mini
                                      @if($V->ishidden == '0')
                                      layui-btn-disabled
                                      @endif
                                          ">@if($V->ishidden == '0')隐藏
                                            @elseif($V->ishidden == '1')显示
                                            @endif</span></td>
                                    <td class="td-manage">
                                      <a onclick="member_stop(this,'10001')" href="javascript:;" uid="{{$V->id}}" title="@if($V->ishidden == '0')显示@elseif($V->ishidden == '1')隐藏@endif">
                                        <i class="layui-icon">@if($V->ishidden == '0')&#xe62f;@elseif($V->ishidden == '1')&#xe601;@endif</i>
                                      </a>

                                      <a onclick="xadmin.open('修改','/admin/menu-edit/{{$V->id}}',600,500)" title="修改" href="javascript:;">
                                        <i class="layui-icon">&#xe642;</i>
                                      </a>

                                      <a title="删除" onclick="member_del(this,'{{$V->id}}')" href="javascript:;">
                                        <i class="layui-icon">&#xe640;</i>
                                      </a>
                                    </td>
                                  </tr>
                                  @endif
                                  @endforeach
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="layui-card-body ">
                            <div class="page">
                                <div>
                                    {{$res->appends($request)->render()}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script>
      layui.use(['laydate','form','jquery'], function(){
        $ = layui.jquery;
        var laydate = layui.laydate;
        var  form = layui.form;


        // 监听全选
        form.on('checkbox(checkall)', function(data){

          if(data.elem.checked){
            $('tbody input').prop('checked',true);
          }else{
            $('tbody input').prop('checked',false);
          }
          form.render('checkbox');
        });

        //执行一个laydate实例
        laydate.render({
          elem: '#start' //指定元素
        });

        //执行一个laydate实例
        laydate.render({
          elem: '#end' //指定元素
        });

        // 分类展开收起的分类的逻辑
          //
          $(function(){
            $("tbody.x-cate tr[fid!='0']").hide();
            // 栏目多级显示效果
            $('.x-show').click(function () {
                if($(this).attr('status')=='true'){
                    $(this).html('&#xe625;');
                    $(this).attr('status','false');
                    cateId = $(this).parents('tr').attr('cate-id');
                    $("tbody tr[fid="+cateId+"]").show();
               }else{
                    cateIds = [];
                    $(this).html('&#xe623;');
                    $(this).attr('status','true');
                    cateId = $(this).parents('tr').attr('cate-id');
                    getCateId(cateId);
                    for (var i in cateIds) {
                        $("tbody tr[cate-id="+cateIds[i]+"]").hide().find('.x-show').html('&#xe623;').attr('status','true');
                    }
               }
            })
          })

          var cateIds = [];
          function getCateId(cateId) {
              $("tbody tr[fid="+cateId+"]").each(function(index, el) {
                  id = $(el).attr('cate-id');
                  cateIds.push(id);
                  getCateId(id);
              });
          }
      });

       /*用户-隐藏*/
      function member_stop(obj,id){
          var title = $(obj).attr('title') == '显示'?'显示':'隐藏';
          var uid = $(obj).attr('uid');

          layer.confirm('确认要'+title+'吗？',function(index){
              var thiss = $(obj);
            $.post("{{url('admin/menu-banned')}}",{'_token':"{{csrf_token()}}",'ishidden':title,'id':uid},function(data){
                if(data == '0'){
                    layer.msg('显示失败',{icon: 5,time:1000});
                }else if(data == '1'){
                    layer.msg('隐藏失败',{icon: 5,time:1000});
                }else if(data == '2'){
                    thiss.attr('title','隐藏')
                    thiss.find('i').html('&#xe601;');

                    thiss.parents("tr").find(".td-status").find('span').removeClass('layui-btn-disabled').html('显示');
                    layer.msg('已显示!',{icon: 1,time:1000});
                }else if(data == '3'){
                    thiss.attr('title','显示')
                    thiss.find('i').html('&#xe62f;');

                    thiss.parents("tr").find(".td-status").find('span').addClass('layui-btn-disabled').html('隐藏');
                    layer.msg('已隐藏!',{icon: 1,time:1000});
                }
            });

          });
      }

      /*用户-删除*/
      function member_del(obj,id){
          layer.confirm('是否删除该模块?请注意该删除无法恢复!',function(index){
              //发异步删除数据
              $.post("{{url('admin/menu-del')}}",{'_token':"{{csrf_token()}}",'id':id},function(data){
                    if(data == '1'){
                        $(obj).parents("tr").remove();
                        layer.msg('已删除!',{icon:1,time:1000});
                    }else if(data == '2'){
                      layer.msg('请先删除该模块的子模块',{icon:5,time:2000});
                    }else{
                        layer.msg('删除失败',{icon:5,time:1000});
                    }
              });
          });
      }



      function delAll (argument) {
        var ids = [];

        // 获取选中的id
        $('tbody input').each(function(index, el) {
            if($(this).prop('checked')){
               ids.push($(this).val())
            }
        });

        layer.confirm('确认要删除吗？'+ids.toString(),function(index){
            //捉到所有被选中的，发异步进行删除
            layer.msg('删除成功', {icon: 1});
            $(".layui-form-checked").not('.header').parents('tr').remove();
        });
      }
    </script>
</html>
