
<!DOCTYPE html>
<html class="x-admin-sm">
    <head>
        <meta charset="UTF-8">
        <title>EQCMS后台管理系统 - 评论回复列表</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="/css/font.css">
        <link rel="stylesheet" href="/css/xadmin.css">
        <script src="/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/js/xadmin.js"></script>
        <!--[if lt IE 9]>
          <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
          <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="x-nav">
          <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload(true)" title="刷新">
            <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i></a>
        </div>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-body layui-table-body layui-table-main">
                            <table class="layui-table layui-form">
                                <thead>
                                  <tr>
                                    <th>用户ID</th>
                                    <th>回复内容</th>
                                    <th>回复时间</th>
                                    <th><i class="layui-icon layui-icon-praise"></i></th>
                                    <th><i class="layui-icon layui-icon-tread"></i></th>
                                    <th>操作</th>
                                  </tr>
                                </thead>
                                <tbody>
                                @foreach ($res as $v)
                                  <tr>
                                    <td>@if($v->uid != '0')<a title="用户详情" onclick="xadmin.open('用户详情','/admin/user-info/{{$v->uid}}',500,400)" href="javascript:;">{{$v->uid}}</a>@else游客@endif</td>
                                    <td>{!!$v->content!!}</td>
                                    <td>{{date('Y-m-d H:i:s',$v->time)}}</td>
                                    <td>{{$v->praise}}</td>
                                    <td>-{{$v->tread}}</td>
                                    <td class="td-manage">
                                        <a title="删除" onclick="member_del(this,'{{$v->id}}')" href="javascript:;">
                                            <span class="layui-btn layui-btn-danger"><i class="iconfont">&#xe69d;</i> 删除</span>
                                        </a>
                                    </td>
                                  </tr>
                                  @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script>
      layui.use(['laydate','form','laypage'], function(){
        var laydate = layui.laydate;
        var  form = layui.form;
        var laypage = layui.laypage;


        // 监听全选
        form.on('checkbox(checkall)', function(data){

          if(data.elem.checked){
            $('[name=id]').prop('checked',true);
          }else{
            $('[name=id]').prop('checked',false);
          }
          form.render('checkbox');
        });



      });


      /*删除*/
      function member_del(obj,id){
          layer.confirm('确认要删除吗？',function(index){
              //发异步删除数据
              $.post("{{url('admin/comments-del')}}",{'_token':"{{csrf_token()}}",'id':id},function(data){
                  if(data==1){
                    $(obj).parents("tr").remove();
                    layer.msg('已删除!',{icon:1,time:1000});
                  }else{
                    layer.msg('删除失败!',{icon:2,time:1000});
                  }
              });

          });
      }



      function delAll (argument) {
        var ids = [];

        // 获取选中的id
        $('[name=id]').each(function(index, el) {
            if($(this).prop('checked')){
               ids.push($(this).val())
            }
        });

        layer.confirm('确认要删除吗？',function(index){
            //捉到所有被选中的，发异步进行删除
            var url = "/admin/leave/"+ids.toString();
            $.post(url,{'type':'all','_method':"DELETE",'_token':token},function(data){
                  if(data==1){
                        layer.msg('删除成功', {icon: 1});
                        $(".layui-form-checked").not('.header').parents('tr').remove();
                        location.reload();
                  }else{
                        layer.msg('删除失败!',{icon:2,time:1000});
                  }
              });
        });
      }
    </script>
</html>
