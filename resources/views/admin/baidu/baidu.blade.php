<!DOCTYPE html>
<html class="x-admin-sm">
    <head>
        <meta charset="UTF-8">
        <title>EQCM后台管理系统 - 列表</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="/css/font.css">
        <link rel="stylesheet" href="/css/xadmin.css">
        <script src="/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/js/xadmin.js"></script>
        <!--[if lt IE 9]>
          <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
          <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="x-nav">
          <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
            <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i></a>
        </div>
        <div class="layui-fluid">
                   <div class="layui-row">
                <form class="layui-form layui-form layui-col-space5" action="/admin/mail/edit" method="post">
                   <div class="layui-form-item">
                      <label for="username" class="layui-form-label">
                          <span class="x-red"></span>smtp端口
                      </label>
                      <div style="width: 380px;margin-left: 110px;">
                          <input type="text" id="username" name="MAIL_PORT" required="" lay-verify="required"
                          autocomplete="off" class="layui-input" >
                      </div>
                  </div>
                  <div class="layui-form-item">
                      <label for="username" class="layui-form-label">
                          <span class="x-red"></span>smtp邮件地址(与账户名相同)
                      </label>
                      <div style="width: 380px;margin-left: 110px;">
                          <input type="text" id="username" name="MAIL_FROM_ADDRESS" required="" lay-verify="required"
                          autocomplete="off" class="layui-input" >
                      </div>
                  </div>
                   <div class="layui-form-item">
                      <label for="username" class="layui-form-label">
                          <span class="x-red"></span>smtp账户名
                      </label>
                      <div style="width: 380px;margin-left: 110px;">
                          <input type="text" id="username" name="MAIL_USERNAME" required="" lay-verify="required"
                          autocomplete="off" class="layui-input" >
                      </div>

                  </div>
                  <div class="layui-form-item">
                      <label for="username" class="layui-form-label">
                          <span class="x-red"></span>smtp发信名称
                      </label>
                      <div style="width: 380px;margin-left: 110px;">
                          <input type="text" id="username" name="MAIL_FROM_NAME" required="" lay-verify="required"MAIL_FROM_NAME
                          autocomplete="off" class="layui-input" >
                      </div>

                  </div>
                   <div class="layui-form-item">
                      <label for="username" class="layui-form-label">
                          <span class="x-red"></span>smtp密码(授权码)
                      </label>
                      <div style="width: 380px;margin-left: 110px;">
                          <input type="text" id="username" name="MAIL_PASSWORD" required="" lay-verify="required"
                          autocomplete="off" class="layui-input" >
                      </div>

                  </div>
                   <div class="layui-form-item">
                      <label for="username" class="layui-form-label">
                          <span class="x-red"></span>smtp加密方式(不允许改动)
                      </label>
                      <div style="width: 380px;margin-left: 110px;">
                          <input type="text" id="username" readonly name="MAIL_ENCRYPTION" required="" lay-verify="required"
                          autocomplete="off" class="layui-input" >
                      </div>

                  </div>


                  </div>
                  <div class="layui-form-item">
                      <label for="L_repass" class="layui-form-label">
                      </label>
                      <button  class="layui-btn" lay-filter="edit" onclick="abc()" lay-submit="">确定修改
                      </button>
                  </div>
              </form>

            </div>

                    </div>
                </div>
            </div>
        </div>
    </body>
<script>
layui.use('layer', function(){
  var layer = layui.layer;


});
function abc() {
    layer.msg('已修改',{icon:1});
}
</script>
    </html>
