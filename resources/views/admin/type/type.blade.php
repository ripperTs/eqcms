<!DOCTYPE html>
<html class="x-admin-sm">

    <head>
        <meta charset="UTF-8">
        <title>分类管理</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="/css/font.css">
        <link rel="stylesheet" href="/css/xadmin.css">
        <script src="/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/js/xadmin.js"></script>
    </head>

    <body>
        <div class="x-nav">
            <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
                <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i>
            </a>
        </div>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-body ">
                            <form class="layui-form layui-col-space5" action="/admin/type/level" method="get">
                                    <button class="layui-btn" onclick="xadmin.open('添加分类','/admin/type/add',400,600);return false;">
                                        <i class="layui-icon"></i>添加分类</button>
                                <a class="layui-btn layui-btn-primary">批量设置分类所有数据指定会员等级观看: 将分类</a>
                                <div class="layui-inline layui-show-xs-block">
                                    <select name="type" lay-filter="type">
{{--                                        <option>请选择类别...</option>--}}
                                        @foreach ($type as $v)
                                            <option value="{{$v->id}}">{{$v->t_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <a class="layui-btn layui-btn-primary">中的全部影片设置为:</a>
                                <div class="layui-inline layui-show-xs-block">
                                    <select name="level" lay-filter="type">
                                        <option value="">不限制</option>
                                        <option value="1">普通会员</option>
                                        <option value="2">铜牌会员</option>
                                        <option value="3">银牌会员</option>
                                        <option value="4">金牌会员</option>
                                        <option value="5">钻石会员</option>
                                    </select>
                                </div>
                                <a class="layui-btn layui-btn-primary">才能观看!</a>
                                <div class="layui-inline layui-show-xs-block">
                                    <button class="layui-btn layui-btn-normal" lay-submit="" lay-filter="sreach"><i class="iconfont">&#xe6ce;</i> 批量设置</button>
                                </div>
                            </form>
                        </div>
                        <div class="layui-card-body ">
                            <table class="layui-table layui-form">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>类别名</th>
                                        <th>父ID</th>
                                        <th>英文名</th>
                                        <th>分类标题</th>
                                        <th>创建时间</th>
                                        <th>是否隐藏</th>
                                        <th>会员观看限制</th>
                                        <th>操作</th></tr>
                                </thead>
                                <tbody>
                                @foreach($row as $v)
                                    <tr>
                                        <td>{{$v->id}}</td>
                                        <td>{{$v->t_name}}</td>
                                        <td>{{$v->p_id}}</td>
                                        <td>{{$v->tenname}}</td>
                                        <td>{{$v->title}}</td>
                                        <td>{{date('Y-m-d H:i',$v->time)}}</td>
                                        <td class="td-status">
                                            @if($v->ishidden==1)
                                            <span class="layui-btn layui-btn-normal layui-btn-mini">已显示</span></td>
                                            @else
                                            <span class="layui-btn layui-btn-normal layui-btn-mini layui-btn-disabled">已隐藏</span></td>
                                            @endif
                                            <td>{{$v->authority?'仅会员观看':'无限制'}}</td>
                                        <td class="td-manage">
                                        @if($v->ishidden==1)
                                        <a onclick="member_stop(this,{{$v->id}})" href="javascript:;"  title="启用">
                                            <i class="layui-icon">&#xe601;</i>
                                        </a>
                                        @else
                                        <a onclick="member_stop(this,{{$v->id}})" href="javascript:;"  title="隐藏">
                                            <i class="layui-icon">&#xe62f;</i>
                                        </a>
                                        @endif
                                            <a title="编辑" onclick="xadmin.open('编辑','/admin/type/edit/{{$v->id}}',400,600)" href="javascript:;">
                                              <i class="layui-icon">&#xe642;</i>
                                             </a>
                                             <a title="移动" onclick="xadmin.open('移动','/admin/type/yd/{{$v->id}}/{{$v->p_id}}',400,500)" href="javascript:;">
                                              <i class="icon iconfont">&#xe718;</i>
                                             </a>
                                            <a title="删除" onclick="member_del(this,{{$v->id}},{{$v->p_id}})" href="javascript:;">
                                                <i class="layui-icon">&#xe640;</i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script>layui.use(['laydate', 'form'],
        function() {
            var laydate = layui.laydate;

            //执行一个laydate实例
            laydate.render({
                elem: '#start' //指定元素
            });

            //执行一个laydate实例
            laydate.render({
                elem: '#end' //指定元素
            });
        });

        /*用户-停用*/
        function member_stop(obj,id){
              if($(obj).attr('title')=='启用'){
                layer.confirm('确认要隐藏吗？',function(index){
                    $.get("{{url('admin/type/xs')}}",{'id':id,'qb':'1'},function(data){
                    })
                    //发异步把用户状态进行更改
                    $(obj).attr('title','隐藏')
                    $(obj).find('i').html('&#xe62f;');

                    $(obj).parents("tr").find(".td-status").find('span').addClass('layui-btn-disabled').html('已隐藏');
                    layer.msg('已隐藏!',{icon: 5,time:1000});
                 });

              }else{
                layer.confirm('确认要显示吗？',function(index){
                    $.get("{{url('admin/type/xs')}}",{'id':id,'qb':'2'},function(data){
                    })
                    $(obj).attr('title','启用')
                    $(obj).find('i').html('&#xe601;');

                    $(obj).parents("tr").find(".td-status").find('span').removeClass('layui-btn-disabled').html('已显示');
                    layer.msg('已显示!',{icon: 6,time:1000});
                });
              }


      }


        /*用户-删除*/
        function member_del(obj, id,pid) {

            layer.confirm('确认要删除吗？',
            function(index) {
                $.post("/admin/type/del",{"id":id,"_token":"{{csrf_token()}}","pid":pid},function(data){
                    if(data==1){
                        layer.msg('请先删除子分类!', {
                            icon: 2,
                            time: 1000
                        });
                    }else if(data==2){
                        $(obj).parents("tr").remove();
                        layer.msg('已删除!', {
                            icon: 1,
                            time: 1000
                        });
                    }else{
                        layer.msg('删除失败!', {
                            icon: 2,
                            time: 1000
                        });
                    }
                });
                //发异步删除数据

            });
        }

        function delAll(argument) {

            var data = tableCheck.getData();

            layer.confirm('确认要删除吗？' + data,
            function(index) {
                //捉到所有被选中的，发异步进行删除
                layer.msg('删除成功', {
                    icon: 1
                });
                $(".layui-form-checked").not('.header').parents('tr').remove();
            });


        }</script>

</html>
