<!DOCTYPE html>
<html class="x-admin-sm">

    <head>
        <meta charset="UTF-8">
        <title>EQCMS管理系统 - 分类移动</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="/css/font.css">
        <link rel="stylesheet" href="/css/xadmin.css">
        <script type="text/javascript" src="/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/js/xadmin.js"></script>
        <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
        <!--[if lt IE 9]>
            <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
            <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="layui-fluid">
            <div class="layui-row">
                <form class="layui-form">
                    <div class="layui-form-item">
                        <label for="L_email" class="layui-form-label">
                            <span class="x-red">*</span>你要移动的</label>
                        <div class="layui-input-inline">
                            <select id="">
                            @foreach($row1 as $v)
                                <option value="">{{$v->t_name}}</option>
                            @endforeach
                            </select>
                        </div>
                        <div class="layui-form-mid layui-word-aux">
                          <span class="x-red">*</span>会将当前分支下的所有分支移走
                      </div>
                    </div>
                    <div class="layui-form-item">
                        <label for="L_repass" class="layui-form-label">
                            <span class="x-red">*</span>可以移动到的</label>
                        <div class="layui-input-inline">
                            <select name="id">
                            <option value="a">请选择</option>
                            @foreach($row2 as $v2)
                                <option value="{{$v2->id}}" lay-verify="required">{{$v2->t_name}}</option>
                            @endforeach
                            </select>
                            <input type="hidden" name="yid" value="{{$id}}">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label for="L_repass" class="layui-form-label"></label>
                        <button class="layui-btn" lay-filter="add" lay-submit="">移动</button></div>
                </form>
            </div>
        </div>
        <script>layui.use(['form', 'layer','jquery'],
            function() {
                $ = layui.jquery;
                var form = layui.form,
                layer = layui.layer;

                //自定义验证规则
                form.verify({
                    nikename: function(value) {
                        if (value.length < 5) {
                            return '昵称至少得5个字符啊';
                        }
                    },
                    pass: [/(.+){6,12}$/, '密码必须6到12位'],
                    repass: function(value) {
                        if ($('#L_pass').val() != $('#L_repass').val()) {
                            return '两次密码不一致';
                        }
                    }
                });

                //监听提交
                form.on('submit(add)',
                function(data) {
                    $.get("/admin/type/doyd",{"id":data.field.id,"yid":data.field.yid},function(data){
                        if(data==1){
                            layer.alert("移动成功", {
                                icon: 6
                            },
                            function() {
                                //关闭当前frame
                                xadmin.close();

                                // 可以对父窗口进行刷新
                                xadmin.father_reload();
                            }); 
                        }else if(data==2){
                            layer.alert("移动失败", {
                                icon: 5
                            },
                            function() {
                                //关闭当前frame
                                xadmin.close();

                                // 可以对父窗口进行刷新
                                xadmin.father_reload();
                            }); 
                        }else if(data==3){
                            
                            layer.alert("没有选择要移动到哪里", {
                                icon: 5
                            },
                            function() {

                                //关闭当前frame
                                xadmin.close();

                                // 可以对父窗口进行刷新
                                xadmin.father_reload();
                            }); 
                        }
                    })
                    return false;
                });

            });</script>
    </body>

</html>
