<!DOCTYPE html>
<html class="x-admin-sm">
    <head>
        <meta charset="UTF-8">
        <title>EQCM后台管理系统 - 列表</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="/css/font.css">
        <link rel="stylesheet" href="/css/xadmin.css">
        <script src="/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/js/xadmin.js"></script>
        <!--[if lt IE 9]>
          <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
          <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="x-nav">
          <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
            <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i></a>
        </div>
        <div class="layui-fluid">
                   <div class="layui-row layui-col-space15">
                    <div class="layui-col-md12">
                        <div class="layui-card">
                        <div class="layui-card-body ">
                <form class="layui-form">
                  @foreach($row as $v)
                  {{csrf_field()}}
                  <div class="layui-form-item">
                      <span class="layui-form-label x-red"></span>
                      微信平台对接API地址: {{$v->domain}}/wxapi.php
                  </div>
                  <div class="layui-form-item">
                      <label for="username" class="layui-form-label">
                          <span class="x-red"></span>网站域名
                      </label>
                      <div style="width: 380px;margin-left: 110px;">
                          <input type="text" id="username" name="domain" required="" lay-verify="required"
                          autocomplete="off" class="layui-input" value="{{$v->domain}}">
                           <span style="color:red;font-size: 10px;">*(http://你的域名)</span>
                      </div>
                  </div>


                   <div class="layui-form-item">
                      <label for="username" class="layui-form-label">
                          <span class="x-red"></span>TOKEN
                      </label>
                      <div style="width: 380px;margin-left: 110px;">
                          <input type="text" id="username" name="token" required="" lay-verify="required" value="{{$v->token}}"
                          autocomplete="off" class="layui-input">
                          <span style="color:red;font-size: 10px;">*需与公众号配置中的TOKEN一致</span>
                      </div>
                  </div>
                   <div class="layui-form-item">
                      <label for="username" class="layui-form-label">
                          <span class="x-red"></span>关注后回复消息
                      </label>
                      <div style="width: 380px;margin-left: 110px;">
                          <input type="text" id="username" name="focusmess" required="" lay-verify="required" value="{{$v->focusmess}}"
                          autocomplete="off" class="layui-input">
                      </div>

                  </div>

                   <div class="layui-form-item">
                      <label for="username" class="layui-form-label">
                          <span class="x-red"></span>消息回复数量
                      </label>
                      <div style="width: 380px;margin-left: 110px;">
                          <input type="number" min="5" id="username" name="num" required="" lay-verify="required" value="{{$v->num}}"
                          autocomplete="off" class="layui-input">
                      </div>
                  </div>
                  <div class="layui-form-item">
                      <label for="L_repass" class="layui-form-label">
                      </label>
                      <button  class="layui-btn" lay-filter="formDemo" lay-submit="">确定修改
                      </button>
                  </div>
                  @endforeach
              </form>
              </div>
              </div>
              </div>
            </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script>
      layui.use(['form'], function(){
      var form = layui.form;
      //监听提交
      form.on('submit(formDemo)', function(index){
          // $.post("/admin/wxconfig/add",{'index':index.field,'_token':"{{csrf_token()}}"},function(index){
console.log(index);
      $.post("/admin/wxconfig/add",{'domain':index.field.domain,'name':index.field.name,'token':index.field.token,'focusmess':index.field.focusmess,'nocontent':index.field.nocontent,'num':index.field.num,'_token':"{{csrf_token()}}"},function(data){

                        if(data==1){
                            //发异步，把数据提交给php
                            layer.alert("修改成功", {
                                icon: 6
                            },
                            function() {
                                //关闭当前frame
                               location.reload()
                            });

                        }else{
                            //发异步，把数据提交给php
                            layer.alert("修改失败", {
                                icon: 2
                            },
                            function() {
                                //关闭当前frame
                               location.reload()
                            });

                        }
                    });

                    return false;

      });

    });
      // $(.all).click(function(){
      // $(.didi).attr("checked",$(".all")[0].checked);

      // })
      </script>
    <script>
      layui.use(['laydate','form'], function(){
        var laydate = layui.laydate;
        var form = layui.form;

        //执行一个laydate实例
        laydate.render({
          elem: '#start' //指定元素
        });

        //执行一个laydate实例
        laydate.render({
          elem: '#end' //指定元素
        });
      });

       /*用户-停用*/
      // function member_stop(obj,id){
      //     layer.confirm('确认要停用吗？',function(index){

      //         if($(obj).attr('title')=='启用'){

      //           //发异步把用户状态进行更改
      //           $(obj).attr('title','停用')
      //           $(obj).find('i').html('&#xe62f;');

      //           $(obj).parents("tr").find(".td-status").find('span').addClass('layui-btn-disabled').html('已停用');
      //           layer.msg('已停用!',{icon: 5,time:1000});

      //         }else{
      //           $(obj).attr('title','启用')
      //           $(obj).find('i').html('&#xe601;');

      //           $(obj).parents("tr").find(".td-status").find('span').removeClass('layui-btn-disabled').html('已启用');
      //           layer.msg('已启用!',{icon: 5,time:1000});
      //         }

      //     });
      // }


      function delAll (argument) {

        var data = tableCheck.getData();

        layer.confirm('确认要删除吗？'+data,function(index){
            //捉到所有被选中的，发异步进行删除
            layer.msg('删除成功', {icon: 1});
            $(".layui-form-checked").not('.header').parents('tr').remove();
        });
      }
    </script></html>
