@foreach ($res as $v)
<tr>
<td>
    <input type="checkbox" name="id" value="{{$v->id}}" lay-skin="primary">
</td>
<td>{{$v->email}}</td>
<td>{{$v->d_name}}</td>
<td>{{$v->content}}</td>
<td>{{$v->ip}}</td>
<td>{{date('Y-m-d H:i',$v->time)}}</td>
<td>
    @if ($v->audit==1)
    <input type="checkbox" name="audit" value="{{$v->id}}" checked lay-filter="audit" lay-skin="switch" lay-text="已处理|未处理">
    @else
    <input type="checkbox" name="audit" value="{{$v->id}}" lay-filter="audit" lay-skin="switch" lay-text="已处理|未处理">
    @endif
</td>
<td class="td-manage">
    @if ($v->replay==1)
        <a title="查看处理内容" onclick="xadmin.open('查看处理结果','/admin/error-replay/?id={{$v->id}}',600,400)" href="javascript:;">
        <span class="layui-btn"><i class="iconfont">&#xe6e6;</i> 处理结果</span>
    @else
        <a title="回应处理" onclick="xadmin.open('回应处理','/admin/error/{{$v->id}}',600,400)" href="javascript:;">
        <span class="layui-btn layui-btn-normal"><i class="iconfont">&#xe69b;</i> 回应</span>
    @endif
    </a>
    <a title="删除" onclick="member_del(this,{{$v->id}})" href="javascript:;">
    <span class="layui-btn layui-btn-danger"><i class="iconfont">&#xe69d;</i> 删除</span>
    </a>
</td>

</tr>
@endforeach
