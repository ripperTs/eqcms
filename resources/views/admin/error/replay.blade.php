
<!DOCTYPE html>
<html class="x-admin-sm">

    <head>
        <meta charset="UTF-8">
        <title>EQCMS管理系统 - 问题处理</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <link rel="stylesheet" href="/css/font.css">
        <link rel="stylesheet" href="/css/xadmin.css">
        <script type="text/javascript" src="/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="/js/xadmin.js"></script>
        <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
        <!--[if lt IE 9]>
            <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
            <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="layui-fluid">
            <div class="layui-row">
                <form class="layui-form">
                    <div class="layui-form-item">
                        <label for="L_email" class="layui-form-label">
                            <span class="x-red">*</span>反馈邮箱</label>
                        <div class="layui-input-inline">
                            <input type="text" id="L_email" name="email" required="" autocomplete="off" value="{{$res->email}}" readonly class="layui-input"></div>
                        <div class="layui-form-mid layui-word-aux">
                            <span class="x-red">*</span>回复成功将自动向此邮箱发送通知</div></div>
                    <div class="layui-form-item">
                        <label for="L_username" class="layui-form-label">
                            <span class="x-red">*</span>影片名</label>
                        <div class="layui-input-inline">
                            <input type="text" id="L_username" readonly value="{{$res->d_name}}" name="d_name" required="" autocomplete="off" class="layui-input"></div>
                    </div>
                    <div class="layui-form-item">
                        <label for="L_pass" class="layui-form-label">
                            <span class="x-red">*</span>回复内容</label>
                        <div class="layui-input-inline">
                            <textarea name="rcontent" class="layui-textarea" lay-verify="nikename" id="" style="width:300px;" cols="30" rows="10"></textarea>
                        </div>
                        <input type="hidden" name="content" value="{{$res->content}}">
                        <input type="hidden" name="user" value="{{$user}}">
                        <input type="hidden" name="id" value="{{$res->d_id}}">
                        <input type="hidden" name="eid" value="{{$res->id}}">
                    <div class="layui-form-item">
                        <br><br>
                        <label for="L_repass" class="layui-form-label"></label>
                        <button class="layui-btn" lay-filter="add" lay-submit="">回复留言</button></div>
                </form>
            </div>
        </div>
        <script>
        var token = "{{csrf_token()}}";
        layui.use(['form', 'layer','jquery'],
            function() {
                $ = layui.jquery;
                var form = layui.form,
                layer = layui.layer;

                //自定义验证规则
                form.verify({
                    nikename: function(value) {
                        if (value.length < 5) {
                            return '至少得5个字符啊';
                        }
                    },
                    pass: [/(.+){6,12}$/, '密码必须6到12位'],
                    repass: function(value) {
                        if ($('#L_pass').val() != $('#L_repass').val()) {
                            return '两次密码不一致';
                        }
                    }
                });

                //监听提交
                form.on('submit(add)',
                function(data) {
                    var index = layer.load(3, {time: 15*1000}); //加载等待动画
                    //发异步，把数据提交给php
                    $.post("/admin/error",{'_token':token,'data':data.field},function(data){
                        if(data==1){
                            layer.close(index); //数据显示为完毕,关闭加载动画
                            layer.alert("回复成功", {
                                icon: 6
                            },
                            function() {
                                //关闭当前frame
                                xadmin.close();

                                // 可以对父窗口进行刷新
                                xadmin.father_reload();
                            });
                        }else if(data==2){
                            layer.close(index); //数据显示为完毕,关闭加载动画
                            layer.alert("回复失败", {
                                icon: 2
                            },
                            function() {
                                //关闭当前frame
                                xadmin.close();

                                // 可以对父窗口进行刷新
                                xadmin.father_reload();
                            });
                        }else{
                            layer.close(index); //数据显示为完毕,关闭加载动画
                            layer.alert("回复成功,但是通知邮件发送失败了!", {
                                icon: 1
                            },
                            function() {
                                //关闭当前frame
                                xadmin.close();

                                // 可以对父窗口进行刷新
                                xadmin.father_reload();
                            });
                        }
                    });

                    return false;
                });

            });</script>
    </body>

</html>
