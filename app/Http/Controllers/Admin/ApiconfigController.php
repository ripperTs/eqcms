<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
class ApiconfigController extends Controller
{
    //模板加载
    public function index(){
        //手机
        $phone = DB::table('eq_phoneapi')->get();
        //支付
        $pay = DB::table('eq_pay')->get();
        return view("/admin/Xtpz/Api",["phone"=>$phone,"pay"=>$pay]);
    }
     //手机修改模板
     public function phone(Request $request){
        $id=$request['id'];
        $phone = DB::table('eq_phoneapi')->where("id","=",$id)->get();
        return view("/admin/Xtpz/phone",["phone"=>$phone]);
    }
    //支付修改模板
    public function pay(Request $request){
        $id=$request['id'];
        $pay = DB::table('eq_pay')->where("id","=",$id)->get();
        return view("/admin/Xtpz/pay",["pay"=>$pay]);
    }
    //执行手机修改
    public function pedit(Request $request){
        $arr=$request['data'];
        $id=$arr['id'];
        unset($arr['id']);
        $row = DB::table('eq_phoneapi')->where('id',"=",$id)->update($arr);
        if($row){
            echo 1;
        }else{
            echo 2;
        }
    }
     //执行支付修改
     public function payedit(Request $request){
        $arr=$request['data'];
        $id=$arr['id'];
        unset($arr['id']);
        $row = DB::table('eq_pay')->where('id',"=",$id)->update($arr);
        if($row){
            echo 1;
        }else{
            echo 2;
        }
    }
}
