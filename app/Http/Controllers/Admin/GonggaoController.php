<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class GonggaoController extends Controller
{
	//查看
   public function gonggao(){
   	$row = DB::table("eq_gonggao")->get();
   	return view("admin.gonggao.gonggao",['row'=>$row,'count'=> count($row)]);

   }

   //修改
   public function edit($id){
   	//dd(13);
   		 $row = DB::table("eq_gonggao")->where('id','=',$id)->get();
   		 return view("admin.gonggao.gonggao-edit",['row'=>$row]);
   }

   //修改执行
   	public function editdo(Request $request ){
   		$a = $request['data'];
   		unset($a['_token']);
   		//一周秒数
   		$yz1 = 604800;

   		switch ($a['gg_time']) {
   			case '1':
   				$a['e_time'] = $a['s_time'] + $yz1;
   				break;
   			case '2':
   				$a['e_time'] = $a['s_time'] + $yz1*2;
   				break;
   			case '3':
   				$a['e_time'] = $a['s_time'] + $yz1*3;
   				break;
   			case '4':
   				$a['e_time'] = $a['s_time'] + $yz1*4;
   				break;
   			default:

   				break;
   		}
   		unset($a['gg_time']);
   		$res = DB::table("eq_gonggao")->where('id','=',$a['id'])->update($a);
   		if ($res) {
   			echo "1";
        } else {
        	echo "2";
        }
   		
   	}
  
    //添加
    public function store(){
   	//dd(13);
   		return view("admin.gonggao.gonggao-add");
   }

    public function doadd(Request $request){
    		//echo $request;
   		$arr = $request->except("_token");
   		$arr['s_time']  =  strtotime($arr['s_time']);
   		$yz = 604800;
   		switch ($arr['gg_time']) {
   			case '1':
   				$arr['e_time'] = $arr['s_time'] + $yz;
   				break;
   			case '2':
   				$arr['e_time'] = $arr['s_time'] + $yz*2;
   				break;
   			case '3':
   				$arr['e_time'] = $arr['s_time'] + $yz*3;
   				break;
   			case '4':
   				$arr['e_time'] = $arr['s_time'] + $yz*4;
   				break;
   			default:

   				break;
   		}
   		unset($arr['gg_time']);
   		//dd($arr);
   		$res = DB::table("eq_gonggao")->insert($arr);
   		if($res){
   			echo 1;
   		}else{
   			echo 2;
   		}
   		//return redirect('/admin/gglist');
   		//dd($arr);
   }
   public function del($id){
   		$row = DB::table("eq_gonggao")->where('id', '=', $id)->delete();
   		return redirect('/admin/gglist');
   }

}
