<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use DB;
use Hash;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $ip = $request->ip();
        if ($ip == '::1') {
            $ip = '127.0.0.1';
        }
        // 后台登录入口验证
        if ($request->input('token') != LOGIN_TOKEN) {
            return "<h1>请使用正确的入口登录网站后台</h1>如果您是网站所有者想关闭此处验证,<br>请修改<font color='red'>/app/Library/function.php</font>文件中LOGIN_TOKEN的值为空!<p style='color:red;'>注意：【关闭安全入口】将使您网站后台登录地址被直接暴露在互联网上，非常危险，请谨慎操作!</p>";
        }
        return view("admin.login", ['ip' => $ip]);
    }

    //登录处理
    public function dologin(Request $request)
    {
        //接取用户名和密码
        $user = $request->input("user");
        $passwd = $request->input("passwd");
        $adminip = $request->input("adminip");
        //先查询用户
        $row = DB::table("eq_admin")->where("user", "=", $user)->first();
        if ($row) {
            //用户名存在判断密码
            //判断加密
            if (Hash::check($passwd, $row->passwd)) {
                $time = date("Y-m-d H:i:s", time());
                $arr = ["user" => $user, "id" => $row->id];
                //存入session 权限判断用
                session(["admin" => $arr]);
                //把ip和登录时间写入日志开始
                $log = new Logger($user);
                $log->pushHandler(new StreamHandler('./log/rizhi.log', Logger::WARNING));
                $log->warning("用户ip:" . $adminip . "登录时间:" . $time);
                //结束
                echo "1";
            } else {
                echo "2";
            }
        } else {
            echo "2";
        }
    }

    //后台退出
    public function outlogin(Request $request)
    {
        //清除session
        $request->session()->pull("admin");
        $ip = $request->ip();
        if ($ip == '::1') {
            $ip = '127.0.0.1';
        }
        if($request->input('token') == LOGIN_TOKEN){
            return redirect("/admin/login?token=" . $request->input('token'));
        }else{
            return view("admin.login", ['ip' => $ip]);
        }

    }

    //修改密码
    public function updatepwd()
    {
        return view("admin.updatepwd", ['id' => session('admin')['id']]);
    }

    //执行修改密码
    public function doupdate(Request $request)
    {
        $data = $request['data'];
        $row = DB::table("eq_admin")->where("id", "=", $data['id'])->update(['passwd' => Hash::make($data['pass'])]);
        if ($row) {
            echo 1;
        } else {
            echo 2;
        }
    }
}
