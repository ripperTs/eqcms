<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Link;
use DB;
class LinksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $res = Link::orderBy("time","desc")->get();

        return view("admin.links.index",['res'=>$res,'count'=> count($res)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //添加页面

        return view("admin.links.add");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //执行友情链接添加操作
        $data = $request['data'];
        //格式化时间格式为时间戳
        $start=explode("-",$data['starttime']);
        $end = explode("-", $data['endtime']);
        $start = mktime(0,0,0,$start[0], $start[1], $start[2]);
        $end = mktime(0,0,0,$end[0], $end[1], $end[2]);
        //将所有要插入的数据生成数组
        $arr = array('name'=>$data['name'],'url'=>$data['url'], 'img'=>$data['img'], 'describe'=>$data['describe'], 'time'=>time(), 'starttime'=>$start,'endtime'=>$end);
        //将数组插入到数据库并返回插入成功的id
        $newId = Link::insertGetId($arr);
        if($newId){
            //新增友情链接成功 将数据同时存入到redis中
            // Redis::hmset("links:".$newId,$arr);
            // 用前台读取的方式存入缓存,到黑名单时再用此方式,同时id是字符串类型自增得到的
            echo 1;
        }else{
            echo 2;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //修改友情链接
        $res = Link::where("id","=",$id)->first();
        $start = date('m-d-Y',$res->starttime);
        $end = date('m-d-Y',$res->endtime);
        return view("admin.links.edit",['res'=>$res,'start'=>$start,'end'=>$end]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //友情链接修改
        // var_dump($request['data']);
        $data = $request['data'];
        $start = explode("-", $data['starttime']);
        $end = explode("-", $data['endtime']);
        $start = mktime(0, 0, 0, $start[0], $start[1], $start[2]);
        $end = mktime(0, 0, 0, $end[0], $end[1], $end[2]);
        $arr = array('name' => $data['name'], 'url' => $data['url'], 'img' => $data['img'], 'describe' => $data['describe'], 'time' => time(), 'starttime' => $start, 'endtime' => $end);
        $row = Link::where("id","=",$id)->update($arr);
        if($row){
            echo 1;
        }else{
            echo 2;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request , $id)
    {
        //友情链接删除
        $pic = Link::where("id","=",$id)->first();
        $pic = $pic->img;
        //删除友情链接
        $row = Link::where("id","=",$id)->delete();
        if($row){
            if(file_exists("{$pic}")){
                unlink("{$pic}");
            }
            echo 1;
        }else{
            echo 2;
        }
    }

    //状态改变
    public function status(Request $request){
        // var_dump($request['id']);
        $row  = Link::where("id", "=", $request['id'])->update(['ishidden' => $request['zhi'] == 'false' ? '0' : '1']);
        if ($row) {
            echo 1;
        } else {
            echo 2;
        }
    }

    //审核状态改变
    public function audit(Request $request)
    {
        // var_dump($request['id']);
        $row  = Link::where("id", "=", $request['id'])->update(['audit' => $request['zhi'] == 'false' ? '0' : '1']);
        if ($row) {
            echo 1;
        } else {
            echo 2;
        }
    }
}
