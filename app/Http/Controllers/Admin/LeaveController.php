<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Leave;
use App\Models\Repleave;
use Mail;
use DB;

class LeaveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $num = 20; //每页显示数量
        $count = count(Leave::get()); //总条数
        $pno = Intval(ceil($count / $num));  //总页数
        $p = isset($request['page']) ? $request['page'] : 1;
        $offset = ($p - 1)  * $num;
        $sql = "SELECT * FROM eq_leave ORDER BY id LIMIT {$offset},{$num}";
        $res = DB::select($sql);
        if($request->ajax()){
            return view("admin.leave.page", ['res' => $res]);
        }
        return view("admin.leave.index",['res'=>$res,'num'=>count($res),'count'=>$count,'numm'=>$num,'pno'=>$pno]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //回复留言
        $data = $request['data'];
        $email = $data['email'];
        // var_dump($data);
        $arr = array('l_id'=>$data['lid'], 'user'=>$data['user'], 'rcontent'=>$data['rcontent'],'rtime'=>time());
        $row = Repleave::insertGetId($arr);
        if($row){
            //修改留言回复字段为已回复
            Leave::where("id","=",$data['lid'])->update(['replay'=>1]); //已回复
            if(MAIL==1){
                //准备向指定用户邮箱发送邮件
                $view = 'admin.leave.email'; //邮件模版
                $data = array('content'=>$data['content'], 'rcontent' => $data['rcontent']); //模版分配变量
                $to = $email; //发送邮箱
                $subject = "【 " . S_NAME . " 】" . '您的留言已有管理员回复,请查看!'; //发送主题
                Mail::send($view, $data, function ($message) use ($to, $subject) {
                    $message->to($to)->subject($subject);
                });
                if (count(Mail::failures()) < 1) {
                    echo 1;
                } else {
                    echo 3;
                }
            }else{
                echo 1;
            }
        }else{
            echo 2;
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //回复留言
        $res = Leave::where("id","=",$id)->first();
        //取当前管理员账户名
        $user = session("admin")['user'];
        return view("admin.leave.replay",['res'=>$res,'user'=>$user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        //删除留言
        if($request['type']=='one'){
            //删除单条
            $row = Leave::where("id", '=', $id)->delete();
            if ($row) {
                echo 1;
            } else {
                echo 2;
            }
        }else{
            //批量删除
            $id = explode(",",$id);
            foreach ($id as $k => $v) {
                Leave::where("id", '=', $v)->delete();
            }
            echo 1;
        }

    }

    //审核状态改变
    public function audit(Request $request)
    {
        $row  = Leave::where("id", "=", $request['id'])->update(['audit' => $request['zhi'] == 'false' ? '0' : '1']);
        if ($row) {
            echo 1;
        } else {
            echo 2;
        }
    }

    //查看回复
    public function replay(Request $request){
        $id = $request['id'];
        $res = Repleave::where("l_id","=",$id)->first();
        if($res){
            return view("admin.leave.cxreplay",['res'=>$res]);
        }else{
            echo "暂无数据!";
            Leave::where("id","=",$id)->update(["replay"=>0]);
        }

    }

}
