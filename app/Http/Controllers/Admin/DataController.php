<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Banner;
use App\Models\Data;
use App\Models\Details;
use App\Models\Paddress;
use App\Models\Type;
use DB;

class DataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $type = Type::get();
        $num = 15; //每页显示数量
        $count = Data::count(); //总条数
        $pno = Intval(ceil($count / $num));  //总页数
        $p = isset($request['page']) ? $request['page'] : 1;
        $offset = ($p - 1)  * $num;
        $sql = "select eq_data.*,eq_type.t_name from eq_data,eq_type where eq_data.t_id=eq_type.id and eq_data.ishidden=1 order by eq_data.time desc limit {$offset},{$num}";
        $res = DB::select($sql);
        if ($request->ajax()) {
            //ajax传值
            return view("admin.data.page", ['res' => $res, 'pno' => $pno, 'count' => $count]);
        }
        return view("admin.data.index", ['res' => $res, 'pno' => $pno, 'count' => $count,'type'=>$type]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $type = Type::get();
        $lyname = DB::select("SELECT psorce FROM eq_paddress GROUP BY psorce");
        return view("admin.data.add",['type'=>$type,'lyname'=>$lyname]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //文件上传处理
        // 1.检查是否有文件上传
        $request->hasFile('file');
        // 2.设置文件上传路径
        $path = "./upload" . "/" . date("Y-m-d");
        // 3.设置文件名
        $fname = time() . rand(1000, 9999);
        // 4.获取文件上传的后缀名
        $tname = $request->file('file')->extension();
        // 5.拼接文件名+后缀名
        $name = $fname . '.' . $tname;
        // 6.移动文件到指定目录下
        $request->file('file')->move($path, $name);
        // 7.得到文件上传路径和文件名
        $path = rtrim($path, '/') . '/' . $name;
        $arr = array("status"=>1,'path'=>$path);
        $row = Details::where('id',"=",$request->input("id"))->update(['img'=>$path]);

        if($row || $request['type']=='add'){
            echo \json_encode($arr);
        }else{
            unlink($path);
        }

    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $res=DB::table('eq_data')->join('eq_details', 'eq_data.id', '=', 'eq_details.d_id')->where("eq_data.id",'=',$id)->first();
        $type = Type::get();
        $paddress = Paddress::where("r_id","=",$id)->get();
        $lyname = DB::select("SELECT psorce FROM eq_paddress WHERE r_id={$id} GROUP BY psorce");
        return view("admin.data.edit",['res'=>$res,'type'=>$type,'paddress'=> $paddress, 'lyname' => $lyname]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    //数据修改
    public function doedit(Request $request)
    {
        $data = $request['data'];
        $data['padnum'] = $request['padnum'];
        if(B_SWITCH==1 && $data['baidu']==1){
            //确认开关开启  并且单选主动 推送 才使用百度推送功能
            $api = "http://data.zz.baidu.com/urls?site=". B_REALM."&token=". B_KEYT;
            $url = "http://". S_REALM."/details/". $request['did'].".html";
            Baidu($api,$url); //开始推送
        }

        $eq_data = array('d_name'=>$data['d_name'], 't_id'=>$data['type'], 'note'=>$data['note'], 'ishidden'=>$data['ishidden'],'time'=>time(), 'passwd' => $data['passwd'], 'level'=>$data['level']);

        $eq_details = array('vod_name'=>$data['d_name'], 'director'=>$data['director'], 'strring'=>$data['strring'], 'num'=>$data['num'], 'score'=>$data['score'], 'region'=>$data['region'], 'language'=>$data['language'], 'year'=>$data['year'], 'plot'=>$data['plot']);

        $row = DB::table('eq_data')->where("id","=",$request['did'])->update($eq_data);
        $row1 = Details::where("id","=", $request['id'])->update($eq_details);


        for($i=0;$i<$data['padnum'];$i++){
            //判断是否存在这个播放地址
            $u = Paddress::where("r_id", "=", $request['did'])->where("psorce", "=", $data["psorce{$i}"])->count();
            if($u){
                //存在表示修改数据
                $row2 = Paddress::where("r_id", "=", $request['did'])->where("psorce", "=", $data["psorce{$i}"])->update(['playurl' => $data["playurl{$i}"]]);
            }else{
                //不存在表示新增播放数据和播放器来源
                $row2 = Paddress::insert(['r_id'=> $request['did'], 'psorce'=> $data["psorce{$i}"], 'playurl'=> str_replace("\n","#",$data["playurl{$i}"]), 'time'=>time()]);
            }

        }
        if($row || $row1 || $row2){
            echo 1;
        }else{
            echo 2;
        }
        // echo $request['did'];
    }

    //执行添加数据
    public function add(Request $request){
        $data = $request['data'];
        $eq_data=array("d_name"=>$data['d_name'], 't_id'=>$data['type'], 'note'=>$data['note'], 'lasttime'=>date('Y-m-d H:i:s'), 'ishidden'=>$data['ishidden'], 'time'=>time(),'passwd'=>$data['passwd'], 'level'=>$data['level']);

        //生成新的影片id
        $newId = Data::insertGetId($eq_data);
        if (B_SWITCH == 1 && $data['baidu'] == 1) {
            //确认开关开启  并且单选主动 推送 才使用百度推送功能
            $api = "http://data.zz.baidu.com/urls?site=" . B_REALM . "&token=" . B_KEYT;
            $url = "http://" . S_REALM . "/details/" . $newId . ".html";
            Baidu($api, $url); //开始推送
        }
        if($newId){
            $eq_details = array("d_id"=>$newId, 'vod_name'=>$data['d_name'], 'img'=>$data['img'], 'director'=>$data['director'], 'strring'=>$data['strring'], 'num'=>$data['num'], 'score'=>$data['score'], 'region'=>$data['region'], 'language'=>$data['language'], 'year'=>$data['year'], 'playly'=>$data['psorce'], 'plot'=>$data['plot'], 'time'=>date('Y-m-d H:i:s'));
            DB::table('eq_details')->insert($eq_details);
            $eq_paddress = array('r_id'=> $newId, 'psorce'=>$data['psorce'], 'playurl'=>str_replace("\n","#",$data['playurl']), 'time'=>time());
            Paddress::insert($eq_paddress);
            echo 1;
        }else{
            echo 2;
        }
        // $row = Details::where('id', "=", $request->input("id"))->update(['img' => $path]);

    }

    //执行隐藏数据
    public function yc(Request $request){
        $row = Data::where("id","=",$request['id'])->update(['ishidden'=>0]);
        if($row){
            echo "1";
        }else{
            echo "2";
        }
    }

    //隐藏数据列表
    public function hiddendata(Request $request){
        $type = Type::get();
        $num = 15; //每页显示数量
        $count = Data::where("ishidden","=",0)->count(); //总条数
        $pno = Intval(ceil($count / $num));  //总页数
        $p = isset($request['page']) ? $request['page'] : 1;
        $offset = ($p - 1)  * $num;
        $sql = "select eq_data.*,eq_type.t_name from eq_data,eq_type where eq_data.t_id=eq_type.id and eq_data.ishidden=0 order by eq_data.time desc limit {$offset},{$num}";
        $res = DB::select($sql);
        if ($request->ajax()) {
            //ajax传值
            return view("admin.data.page", ['res' => $res, 'pno' => $pno, 'count' => $count]);
        }
        return view("admin.data.yclist", ['res' => $res, 'pno' => $pno, 'count' => $count, 'type' => $type]);
    }

    //显示数据
    public function xs(Request $request){
        $row = Data::where("id", "=", $request['id'])->update(['ishidden' => 1]);
        if ($row) {
            echo "1";
        } else {
            echo "2";
        }
    }

    //删除数据操作
    public function del(Request $request){
        $row = Data::where("id","=",$request['id'])->delete();
        $row1 = Details::where("d_id","=",$request['id'])->delete();
        $row2 = Paddress::where("r_id","=",$request['id'])->delete();
        if($row || $row1 || $row2){
            echo 1;
        }else{
            echo 2;
        }
    }

    //批量删除数据
    public function delAll(Request $request){
        $ids = $request['ids'];
        $arr = explode(",",$ids);
        foreach($arr as $v){
            $row = Data::where("id", "=", $v)->delete();
            $row1 = Details::where("d_id", "=", $v)->delete();
            $row2 = Paddress::where("r_id", "=", $v)->delete();
        }
        echo 1;
    }

    //批量移动数据
    public function removeAll(Request $request){
        $typeid = $request['type'];
        $ids = $request['ids'];
        $arr = explode(",", $ids);
        foreach($arr as $v){
            Data::where("id","=",$v)->update(['t_id'=>$typeid]);
        }
        echo 1;
    }

    //搜索数据
    public function search(Request $request){
        $search = urldecode($request['search']);
        $type = Type::get();
        $num = 15; //每页显示数量
        $sqll = "select eq_data.*,eq_type.t_name from eq_data,eq_type where eq_data.t_id=eq_type.id and eq_data.ishidden=1 and d_name like '%{$search}%' order by eq_data.time desc";
        $count = count(DB::select($sqll));
        $pno = Intval(ceil($count / $num));  //总页数
        $p = isset($request['page']) ? $request['page'] : 1;
        $offset = ($p - 1)  * $num;
        $sql = "select eq_data.*,eq_type.t_name from eq_data,eq_type where eq_data.t_id=eq_type.id and eq_data.ishidden=1 and d_name like '%{$search}%' order by eq_data.time desc limit {$offset},{$num}";
        $res = DB::select($sql);
        if($request->ajax()){
            return view("admin.data.page", ['res' => $res, 'pno' => $pno, 'count' => $count, 'type' => $type, 'search' => $search]);
        }
        return view("admin.data.search", ['res' => $res, 'pno' => $pno, 'count' => $count, 'type' => $type,'search'=>$search]);

    }

    //添加新播放地址
    public function addPlay(Request $request){
        $id = $request['id'];
        $lyname = DB::select("SELECT psorce FROM eq_paddress GROUP BY psorce");
        $ly = DB::select("SELECT psorce FROM eq_paddress WHERE r_id={$id} GROUP BY psorce");
        $arr1 = [];
        $arr2 = [];
        foreach($lyname as $v){
            $arr1[]=$v->psorce;
        }
        foreach ($ly as $v1) {
            $arr2[] = $v1->psorce;
        }
        //判断已存在的播放来源不允许在往里面添加播放地址,执行到修改页面修改
        $arr = array_diff($arr1,$arr2);
        $res = Data::where("id","=",$id)->first();

        return view("admin.data.addPlay",['res'=>$res,'lyname'=>$arr]);
    }

    //执行新播放地址添加
    public function doAddPlay(Request $request){
        $data = $request['data'];
        $row = Paddress::insert(array('r_id'=>$data['id'], 'psorce'=>$data['psorce'], 'playurl'=>str_replace("\n","#",$data['playurl']), 'time'=>time()));
        if($row){
            echo 1;
        }else{
            echo 2;
        }
    }

    //执行删除播放来源
    public function delPlay(Request $request){
        $id = $request['id'];
        $lyname = $request['lyname'];
        $row = Paddress::where("r_id","=",$id)->where("psorce","=",$lyname)->delete();
        if($row){
            echo 1;
        }else{
            echo 2;
        }
    }

    //有密码数据列表
    public function pwddata(Request $request){

        $type = Type::get();
        $num = 15; //每页显示数量
        $count = Data::where("passwd", "!=", '')->count(); //总条数
        $pno = Intval(ceil($count / $num));  //总页数
        $p = isset($request['page']) ? $request['page'] : 1;
        $offset = ($p - 1)  * $num;
        $sql = "select eq_data.*,eq_type.t_name from eq_data,eq_type where eq_data.t_id=eq_type.id and eq_data.passwd!='' order by eq_data.time desc limit {$offset},{$num}";
        $res = DB::select($sql);
        if ($request->ajax()) {
            //ajax传值
            return view("admin.data.page", ['res' => $res, 'pno' => $pno, 'count' => $count]);
        }
        return view("admin.data.pwddata", ['res' => $res, 'pno' => $pno, 'count' => $count, 'type' => $type]);
    }

    //幻灯片列表
    public function bannerList(Request $request){
        $res = Banner::orderBy("sorting",'desc')->get();
        return view("admin.data.banner-list",['res'=>$res,'count'=>count($res)]);
    }

    //修改幻灯片状态
    public function bannerChange(Request $request){
        $row  = Banner::where("id","=",$request['id'])->update(['ishidden'=>$request['zhi']=='false'?'0':'1']);
        if($row){
            echo 1;
        }else{
            echo 2;
        }
    }

    //排序修改
    public function px(Request $request){
        $row  = Banner::where("id", "=", $request['id'])->update(['sorting' => $request['zhi']]);
        if ($row) {
            echo 1;
        } else {
            echo 2;
        }
    }

    //banner图修改
    public function bannerEdit(Request $request){
        $res = Banner::where("id","=",$request['id'])->first();
        return view("admin.data.banner-edit",['res'=>$res]);
    }

    //执行banner图修改
    public function doBannerEdit(Request $request){
        $data = $request['data'];
        $row = DB::table('eq_banner')->where("id","=",$request['id'])->update(['title'=>$data['title'],'b_url'=>$data['b_url'],'time'=>time(),'img'=>ltrim($data['img'],'.')]);
            if($row){
                echo 1;
            }else{
                echo 2;
            }

    }

    //添加幻灯
    public function addBanner(Request $request){
        return view("admin.data.banner-add");
    }

    //执行添加幻灯
    public function doBannerAdd(Request $request){
        $data = $request['data'];
        $newId = DB::table('eq_banner')->insertGetId(['title' => $data['title'], 'b_url' => $data['b_url'], 'time' => time(), 'img' => ltrim($data['img'], '.'),'sorting'=> 1]);
        $row = Banner::where("id","=",$newId)->update(['sorting'=>$newId]);
        if ($row) {
            echo 1;
        } else {
            echo 2;
        }
    }

    //删除幻灯
    public function delBanner(Request $request){
        $row = Banner::where("id","=",$request['id'])->delete();
        if ($row) {
            echo 1;
        } else {
            echo 2;
        }
    }


}
