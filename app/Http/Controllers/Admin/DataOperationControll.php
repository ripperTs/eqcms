<?php

/**
 * @Author: wslmf
 * @Date: 2020-02-19 09:36:35
 * @Desc: 工具新增功能
 */

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Paddress;
use App\Models\Data;
use App\Models\Details;
use App\Models\Type;
use DB;

class DataOperationControll extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view("admin.operaction.replace");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //执行数据替换
        $data = $request['data'];
        switch ($data['tihuan']) {
            case '1':
                $row = DB::update("update eq_details set img=REPLACE(img,'{$data['ytihuan']}','{$data['tihuanh']}') ");
                if ($row) {
                    $arr = array('status' => 1, 'row' => $row);
                    echo \json_encode($arr);
                }

                break;
            case '2':
                $row = DB::update("update eq_paddress set playurl=REPLACE(playurl,'{$data['ytihuan']}','{$data['tihuanh']}') ");
                if ($row) {
                    $arr = array('status' => 1, 'row' => $row);
                    echo \json_encode($arr);
                }
                break;
            case '3':
                $row = DB::update("update eq_details set num=REPLACE(num,'{$data['ytihuan']}','{$data['tihuanh']}') ");
                if ($row) {
                    $arr = array('status' => 1, 'row' => $row);
                    echo \json_encode($arr);
                }
                break;
            case '4':
                $row = DB::update("update eq_details set plot=REPLACE(plot,'{$data['ytihuan']}','{$data['tihuanh']}') ");
                if ($row) {
                    $arr = array('status' => 1, 'row' => $row);
                    echo \json_encode($arr);
                }
                break;
            case '5':
                $row = DB::update("update eq_details set vod_name=REPLACE(vod_name,'{$data['ytihuan']}','{$data['tihuanh']}') ");
                $row1 = DB::update("update eq_data set d_name=REPLACE(d_name,'{$data['ytihuan']}','{$data['tihuanh']}') ");
                if ($row && $row1) {
                    $arr = array('status' => 1, 'row' => ($row + $row1));
                    echo \json_encode($arr);
                }
                break;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    //文件权限检测
    public function checkperm()
    {
        return view('admin.operaction.checkperm');
    }

    //删除指定数据来源
    public function deldata()
    {
        $res = DB::select("SELECT psorce FROM eq_paddress GROUP BY psorce");
        return view("admin.operaction.deldata", ['res' => $res]);
    }

    //执行删除指定数据来源
    public function operationDel(Request $request)
    {
        $res = Paddress::where("psorce", '=', $request['data']['lyname'])->get();
        foreach ($res as $v) {
            Details::where("d_id", "=", $v->r_id)->delete();
            Data::where("id", "=", $v->r_id)->delete();
        }
        $row = Paddress::where("psorce", '=', $request['data']['lyname'])->delete();
        if ($row) {
            echo 1;
        } else {
            echo 2;
        }
    }

    //数据检测
    public function checkdata(Request $request)
    {
        $type = Type::get();
        $num = 15; //每页显示数量
        $data = Data::get();
        $details = Paddress::get(); //总条数
        $arr1 = [];
        foreach ($data as $v) {
            $arr1[] = $v->id;
        }
        $arr2 = [];
        foreach ($details as $v1) {
            $arr2[] = $v1->r_id;
        }
        $s = array_diff($arr1, $arr2);
        $datas = []; //数组
        foreach ($s as $v) {
            $datas[] = Data::where("id", "=", $v)->first();
        }
        $count = count($datas);
        $pno = Intval(ceil($count / $num));  //总页数
        $p = isset($request['page']) ? $request['page'] : 1;
        $offset = ($p - 1)  * $num;
        $aa = array_slice($datas, $offset, $num);

        if ($request->ajax()) {
            //ajax传值
            return view("admin.operaction.page", ['res' => $aa, 'pno' => $pno, 'count' => $count]);
        }
        return view("admin.operaction.checkdata", ['res' => $aa, 'pno' => $pno, 'count' => $count, 'type' => $type]);
    }

    //数据库语句操作
    public function mysqlTool(Request $request)
    {
        return view("admin.operaction.tool");
    }

    //数据库语句执行
    public function dosql(Request $request)
    {
        $data = $request['data'];
        $row = DB::statement(strval($data['sql']));
        if ($row) {
            echo 1;
        } else {
            echo 2;
        }
    }

    // 批量图片本地化展示页面
    public function imgBatchLocalization()
    {
        return view("admin.operaction.imgBatchLocalization");
    }

    // 获取远程图片url
    public function remoteImg()
    {
        $data = Details::get(['img'])->toArray();
        $num  = 0;
        $str = '';
        foreach ($data as $value) {
            $result = preg_match("/^http.*?/",$value['img']);
            if($result){
                $str .= $value['img']."\r\n";
                $num ++;
            }
        }
        return \json_encode(['code'=>200,'msg'=>'查询成功','num'=>$num,'data'=>$str]);
    }

    // 执行数据库变更
    public function doremoteImg()
    {
        // 批量操作数据库
        $data = Details::get()->toArray();
        foreach($data as $v){
            $result = preg_match("/^http.*?/", $v['img']);
            if ($result) {
                Details::where('id', '=', $v['id'])->update(['img' => $this->localImg($v['img'])]);
            }
        }
        return 200;
    }

    // 本地化图片路径
    private function localImg($fileName,$path = '../images/download/'){
        $fileName = pathinfo($fileName)['filename'];
        $imgInfo = $path . $fileName . '.jpg';
        return $imgInfo;
    }

    /**
     * @Author: wslmf
     * @Date: 2020-02-19 10:36:58
     * @Desc: 远程图片本地化(备用)
     * @param string 下载图片路径
     * @param string 指定文件名
     * @param string 保存路径
     * @return array 文件信息（【路径+文件名-returnFile】 【文件名-FileName】 【后缀名（如.jpeg）-extension】 【路径-path】）
     */
    private function downloadImageFromUrl($url, $fileName = '', $path = "../public/images/download/")
    {
        // 建立一个临时文件，用于保存
        $tmpFile = tempnam(sys_get_temp_dir(), 'image');
        # 文件下载 BEGIN #
        // 打开临时文件，用于写入（w),b二进制文件
        $resource = fopen($tmpFile, 'wb');
        // 初始化curl
        $curl = curl_init($url);
        // 设置输出文件为刚打开的
        curl_setopt($curl, CURLOPT_FILE, $resource);
        // 不需要头文件
        curl_setopt($curl, CURLOPT_HEADER, 0);
        // 执行
        curl_exec($curl);
        // 关闭curl
        curl_close($curl);
        // 关闭文件
        fclose($resource);
        # 文件下载 END #
        // 获取文件类型
        if (function_exists('exif_imagetype')) {
            // 读取一个图像的第一个字节并检查其签名(这里需要打开mbstring及php_exif)
            $fileType = exif_imagetype($tmpFile);
        } else {
            // 获取文件大小，里面第二个参数是文件类型 （这里后缀可以直接通过getimagesize($url)来获取，但是很慢）
            @$fileInfo = getimagesize($tmpFile);
            $fileType = $fileInfo[2];
        }
        // 根据文件类型获取后缀名
        $extension = image_type_to_extension($fileType);

        // 如果未指定文件名则使用MD5散列值
        if ($fileName == '') {
            // 计算指定文件的 MD5 散列值，作为保存的文件名，重复下载同一个文件不会产生重复保存，相同的文件散列值相同
            $md5FileName = md5_file($tmpFile);
        } else {
            $md5FileName = $fileName;
        }

        // 最终保存的文件
        $returnFile = $path . $md5FileName . $extension;
        // 检查传过来的路径是否存在，不存在就创建
        if (!is_dir($path)) {
            mkdir($path, 0777, true);
        }
        // 复制临时文件到最终保存的文件中
        copy($tmpFile, $returnFile);
        // 释放临时文件
        @unlink($tmpFile);
        // 返回文件信息
        return array('returnFile' => $returnFile, 'FileName' => $md5FileName, 'extension' => $extension, 'path' => $path);
    }


}
