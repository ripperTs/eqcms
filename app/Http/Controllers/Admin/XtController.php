<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
class XtController extends Controller
{
    //加载广告模块
    public function ad(){
        return view("admin.Xtpz.ads");
        $row = DB::table("eq_ad")->get();
        return view("admin.Xtpz.ad",["row"=>$row]);
    }
    //广告模块禁用
    public function jinyong(Request $request){
      //传过来的区别 1是禁用 2是启用
      $id=$request['id'];
      $qb=$request['qb'];
      if($qb==1){
        $row = DB::update("update eq_ad set ad_state=0 where id=".$id);
        if($row){
          echo 1;
        }else{
          echo 2;
        }
      }else{
        $row = DB::update("update eq_ad set ad_state=1 where id=".$id);
        if($row){
          echo 1;
        }else{
          echo 2;
        }
      }
    }
     //广告模块删除
     public function del(Request $request){
      $id = $request->input("id");
      $tp = $request->input("tp");
        foreach($id as $v){
            $row = DB::table("eq_ad")->where("id","=",$v)->delete();
        }
      if($row){
        foreach($tp as $v){
           unlink($v);
        }
          echo 1;
      }else{
          echo 2;
      }
    }
     //广告模块增加
     public function add(Request $request){
        return view("admin/Xtpz/adadd");
      }
      //处理广告模块增加
     public function doadd(Request $request){
       $arr = $request->except("_token");
       //判断文件上传
       if($request->hasFile("ad_img")){
        $path= "upload"."/".date("Y-m-d");
        $fname = time().rand(10000,99999);
        $hz = $request->file("ad_img")->extension();
        $tname = $fname.".".$hz;
        $request->file("ad_img")->move($path,$tname);
       $arr["ad_img"]=rtrim($path,'/').'/'.$tname;
       }
       $a=DB::table("eq_ad")->insert($arr);
       if($a){
           return redirect("admin/ad");
       }else{
        return redirect("admin/add");
       }
      }
      //广告模块修改
     public function edit($id){
        $row = DB::table("eq_ad")->where("id","=",$id)->get();
        return view("admin/Xtpz/adedit",["row"=>$row]);
      }
      //处理广告模块修改
     public function doedit(Request $request){
        $row = $request->except("_token");
        //判断文件上传
        if($request->hasFile("ad_img")){
          //有图片上传
          $path= "upload"."/".date("Y-m-d");
          $fname = time().rand(10000,99999);
          $hz = $request->file("ad_img")->extension();
          $tname = $fname.".".$hz;
          $request->file("ad_img")->move($path,$tname);
         $row["ad_img"]=rtrim($path,'/').'/'.$tname;
         $old = $row['old'];//获取原先文件名
         unset($row['old']);
         $res = DB::table('eq_ad')->where('id','=',$row['id'])->update($row);
         if($res){
            unlink($old);
            return redirect("admin/ad");
         }else{
            unlink( $row["ad_img"]);
            return redirect("admin/ad/edit/{$row['id']}");
         }
         }else{
          unset($row["ad_img"]);
          $row["ad_img"]=$row['old'];
          unset($row["old"]);
          $res = DB::table('eq_ad')->where('id','=',$row['id'])->update($row);
         if($res){
          return redirect("admin/ad");
         }else{
          return redirect("admin/ad");
         }
          // 没有图片上传
         }
        dd($row);
      }
}
