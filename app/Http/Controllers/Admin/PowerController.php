<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
class PowerController extends Controller
{
    //
    public function powerindex(Request $request)
    {
        $k = $request->input('keywords');
        $res = DB::table('eq_power')->where('name','like','%'.$k.'%')->orderBy('id','desc')->paginate(10);
        return view("admin.power.powerindex",["res"=>$res,'request'=>$request->all()]);
    }
    public function powertypeindex(Request $request)
    {
        $k = $request->input('keywords');
        $res = DB::table('eq_powertype')->select(DB::raw("*,concat(path,id) as c"))->where('name','like','%'.$k.'%')->orderBy('c')->paginate(100);
        foreach ($res as $k=>$v) {
            $arr = explode(",",$v->path);
            $chang = count($arr)-2;
            $res[$k]->name = str_repeat("└───＞",$chang).$v->name;
        }
        return view("admin.power.powertypeindex",["res"=>$res,'request'=>$request->all()]);
    }
    public function powermanageindex(Request $request)
    {
        $k = $request->input('keywords');
        $t = DB::table('eq_powertype')->get();
        foreach ($t as $K=>$V) {
            $T[$V->id] = $V->name;
        }
        $res = DB::table('eq_powermanage')->where('rule','like','%'.$k.'%')->orderBy('id','desc')->paginate(10);
        foreach ($res as $kk=>$vv) {
            $res[$kk]->tname = $T[$vv->tid];
        }
        return view("admin.power.powermanageindex",["res"=>$res,'request'=>$request->all()]);
    }
    public function powerbanned(Request $request)
    {
        $data = $request->input(['status']);
        $id = $request->input(['id']);
        if ($data == '启用') {
            if(DB::table('eq_power')->where('id','=',$id)->update(['status'=>1])){
                echo '2';
            }else{
                echo '0';
            }
        } elseif ($data == '停用') {
            if(DB::table('eq_power')->where('id','=',$id)->update(['status'=>0])){
                echo '3';
            }else{
                echo '1';
            }
        }
    }
    public function poweradd()
    {
        return view("admin.power.poweradd");
    }
    public function powerdoadd(Request $request)
    {
        $data = $request->except(['_token']);
        if(DB::table('eq_power')->insert($data)){
            echo '1';
        } else {
            echo '0';
        }
    }
    public function powerwxjtype($id)
    {
        //无限级分类
        //$res = $this->powerwxjtype(0);
        $res = DB::table('eq_powertype')->where('pid','=',$id)->orderBy('id','asc')->get();
        foreach ($res as $K=>$v) {
            $res[$K]->wxj = $this->powerwxjtype($v->id);
        }
        return $res;
    }
    public function poweredit($id)
    {
        $res = DB::table('eq_power')->where('id','=',$id)->first();
        $row = $this->powerwxjtype(0);
        $pt = DB::table('eq_power-type')->where('pid','=',$id)->get();
        if(DB::table('eq_power-type')->where('pid','=',$id)->count()){
            foreach($pt as $VVV){
                $PT[] = $VVV->tid;
            }
        } else {
            $PT = [];
        }
        foreach($row as $k=>$v){
            foreach($v->wxj as $K=>$V){
                if(in_array($V->id,$PT)) {
                    $row[$k]->wxj[$K]->checked = "checked";
                }else{
                    $row[$k]->wxj[$K]->checked = '';
                }
            }
        }
        return view("admin.power.poweredit",['res'=>$res,'row'=>$row,'id'=>$id]);
    }
    public function powerdoedit(Request $request)
    {
        
        $data = $request->only(['name','describe']);
        $id = $request->input('id');
        $tid = $request->input('tid');
        $c = 0;
        $b = DB::table('eq_power-type')->where('pid','=',$id)->delete();
        if($tid){
        foreach ($tid as $v) {
            $res[] = ['tid'=>$v,'pid'=>$id];
        }
        $c = DB::table('eq_power-type')->insert($res);
        }
        $a = DB::table('eq_power')->where('id','=',$id)->update($data);
        if ($a+$b+$c){
            echo '1';
        } else {
            echo '0';
        } 
    }
    public function powertypeedit($id)
    {
        $res = DB::table('eq_powertype')->where('id','=',$id)->first();
        return view("admin.power.powertypeedit",['res'=>$res]);
    }
    public function powertypedoedit(Request $request)
    {
        $data = $request->except(['_token','id']);
        $id = $request->input('id');
        
        if (DB::table('eq_powertype')->where('id','=',$id)->update($data)){
            echo '1';
        } else {
            echo '0';
        } 
    }
    public function powertypeadd()
    {
        $res = DB::table('eq_powertype')->where('pid','=','0')->get();
        return view("admin.power.powertypeadd",['res'=>$res]);
    }
    public function powertypedoadd(Request $request)
    {
        $data = $request->except(['_token']);
        if ($data['pid'] == '0') {
            $data['path'] = '0,';
        } else {
            $path = DB::table('eq_powertype')->where('id','=',$data['pid'])->value('path');
            $data['path'] = $path.$data['pid'].',';
        }
        if(DB::table('eq_powertype')->insert($data)){
            echo '1';
        } else {
            echo '0';
        }
    }
    public function powertypedel(Request $request)
    {
        $id = $request->input('id');
        if (DB::table('eq_powertype')->where('pid','=',$id)->count()) {
            echo '2';
        } else {
            if (DB::table('eq_powertype')->where('id','=',$id)->delete()) {
                DB::table('eq_power-type')->where('tid','=',$id)->delete();
                echo '1';
            } else {
                echo '0';
            }
        }
    }
    public function powerdel(Request $request)
    {
        $id = $request->input('id');
        if(DB::table('eq_power')->where('id','=',$id)->delete()){
            DB::table('eq_admin-power')->where('pid','=',$id)->delete();
            DB::table('eq_power-type')->where('pid','=',$id)->delete();
            DB::table('eq_power-menu')->where('pid','=',$id)->delete();

            echo '1';
        }else{
            echo '0';
        }
    }
    public function powermanageadd()
    {
        $row = $this->powerwxjtype(0);
        return view("admin.power.powermanageadd",['row'=>$row]);
    }
    public function powermanagedoadd(Request $request)
    {
        $data = $request->except(['_token']);
        if(DB::table('eq_powermanage')->insert($data)){
            echo '1';
        }else{
            echo '0';
        }
    }
    public function powermanageedit($id)
    {
        $row = $this->powerwxjtype(0);
        $res = DB::table('eq_powermanage')->where('id','=',$id)->first();
        return view("admin.power.powermanageedit",['row'=>$row,'res'=>$res]);
    }
    public function powermanagedoedit(Request $request)
    {
        $data = $request->except(['_token','id']);
        $id = $request->input('id');
        if(DB::table('eq_powermanage')->where('id','=',$id)->update($data)){
            echo '1';
        }else{
            echo '0';
        }
    }
    public function powermanagedel(Request $request)
    {
        $id = $request->input('id');
        if(DB::table('eq_powermanage')->where('id','=',$id)->delete()){
            echo '1';
        }else{
            echo '0';
        }
    }
}
