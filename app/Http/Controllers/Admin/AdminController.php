<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Hash;
class AdminController extends Controller
{
    //
    public function index(Request $request)
    {
        $k = $request->input('keywords');
        $res = DB::table('eq_admin')->where('id','!=','1')->where('user','like','%'.$k.'%')->orderBy('id','desc')->paginate(10);
        return view("admin.admin.index",["res"=>$res,'request'=>$request->all()]);
    }
    public function banned(Request $request)
    {
        $data = $request->input(['status']);
        $id = $request->input(['id']);
        if ($data == '启用') {
            if(DB::table('eq_admin')->where('id','=',$id)->update(['status'=>1])){
                echo '2';
            }else{
                echo '0';
            }
        } elseif ($data == '停用') {
            if(DB::table('eq_admin')->where('id','=',$id)->update(['status'=>0])){
                echo '3';
            }else{
                echo '1';
            }
        }
    }
    public function del(Request $request)
    {
        $id = $request->input('id');
        if(DB::table('eq_admin')->where("id",'=',$id)->delete()){
            DB::table('eq_admin-power')->where('uid','=',$id)->delete();
            echo '1';
        }else{
            echo '0';
        }
    }
    public function add()
    {
        return view("admin.admin.add");
    }
    public function doadd(Request $request)
    {
        $data = $request->except(['_token']);
        if(DB::table('eq_admin')->where('user','=',"{$data['user']}")->count()){
            echo '2';
        } else {
            $data['passwd'] = Hash::make($data['passwd']);
            $data['logintime'] = time();
            if(DB::table('eq_admin')->insert($data)){
                echo '1';
            } else {
                echo '0';
            }
        }
    }
    public function edit($id)
    {
        $res = DB::table('eq_admin')->where('id','=',$id)->first();
        return view("admin.admin.edit",['res'=>$res]);
    }
    public function doedit(Request $request)
    {
        $data = $request->except(['_token','id']);
        $id = $request->input('id');
        if ($data['passwd']) {
            $data['passwd'] = Hash::make($data['passwd']);
        } else {
            unset($data['passwd']);
        }
        if (DB::table('eq_admin')->where('id','=',$id)->update($data)){
            echo '1';
        } else {
            echo '0';
        }
    }
    public function adminpower($id)
    {
        $row = DB::table('eq_power')->where('status','=','1')->get();
        $ap = DB::table('eq_admin-power')->where('uid','=',$id)->get();
        if(DB::table('eq_admin-power')->where('uid','=',$id)->count()){
            foreach($ap as $VVV){
                $AP[] = $VVV->pid;
            }
        } else {
            $AP = [];
        }
        foreach($row as $k=>$v){
            if(in_array($v->id,$AP)) {
                $row[$k]->checked = "checked";
            }else{
                $row[$k]->checked = '';
            }
        }
        return view("admin.admin.adminpower",['id'=>$id,'row'=>$row]);
    }
    public function doadminpower(Request $request)
    {
        $uid = $request->input('id');
        $pid = $request->input('pid');
        $b = 0;
        $a = DB::table('eq_admin-power')->where('uid','=',$uid)->delete();
        if($pid){
        foreach ($pid as $k=>$v) {
            $data[] = ['pid'=>$v,'uid'=>$uid];
        }
        $b = DB::table('eq_admin-power')->insert($data);
        }
        if ($a+$b) {
            echo '1';
        } else {
            echo '0';
        }
    }
}
