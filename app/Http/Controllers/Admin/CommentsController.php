<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class CommentsController extends Controller
{
    //
    public function index(Request $request)
    {
        //admin/datalist/'+id+'/edit
        $k = $request->input('keywords');
        $res = DB::table('eq_comments')->where('did','like','%'.$k.'%')->where('cid','=','0')->orderBy('id','desc')->paginate(50);
        return view("admin.comments.index",["res"=>$res,'request'=>$request->all()]);
    }
    public function reply($id)
    {
        $res = DB::table('eq_comments')->where('cid','=',$id)->orderBy('id')->get();
        return view('admin.comments.reply',['res'=>$res]);
    }
    public function del(Request $request)
    {
        $id = $request->input('id');
        if (DB::table('eq_comments')->where('id','=',$id)->delete()) {
            DB::table('eq_comments')->where('cid','=',$id)->delete();
            echo '1';
        } else {
            echo '0';
        }
    }
}
