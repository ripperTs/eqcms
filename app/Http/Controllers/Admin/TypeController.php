<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Type;
use App\Models\Data;
use DB;

/**
 * 数据分类控制器
 * Class TypeController
 * @package App\Http\Controllers\Admin
 */
class TypeController extends Controller
{
    //分类
    public function index()
    {
        $type = Type::get();
        $row = DB::table('eq_type')->select(DB::raw(" *,concat(path,id) as pp"))->orderby("pp", "asc")->get();
        foreach ($row as $k => $v) {
            $arr = explode(",", $v->path);
            $chang = count($arr) - 2;
            $row[$k]->t_name = str_repeat("---", $chang) . $v->t_name;
        }
        return view("admin.type.type", ['row' => $row, 'type' => $type]);
    }

    /**
     * 批量设置会员等级观看限制
     * @param Request $request
     * @return bool|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\think\response\Redirect
     */
    public function level(Request $request)
    {
        $row = Data::where('t_id', '=', $request->type)->update(['level' => $request->level]);
        if ($row) {
            return redirect('/admin/type');
        } else {
            return false;
        }
    }

    //显示与隐藏
    public function xs(Request $request)
    {
        //传过来的区别 1是禁用 2是启用
        $id = $request['id'];
        $qb = $request['qb'];
        if ($qb == 1) {
            $row = DB::update("update eq_type set ishidden=0 where id=" . $id);
            if ($row) {
                echo 1;
            } else {
                echo 2;
            }
        } else {
            $row = DB::update("update eq_type set ishidden=1 where id=" . $id);
            if ($row) {
                echo 1;
            } else {
                echo 2;
            }
        }
    }

    public function del(Request $request)
    {
        $id = $request['id'];
        $pid = $request['pid'];
        //判断子级
        $row = DB::table("eq_type")->where("p_id", '=', $id)->get();
        if (count($row) > 0) {
            echo 1;
        } else {
            $row2 = DB::table("eq_type")->where("id", '=', $id)->delete();
            if ($row2) {
                echo 2;
            } else {
                echo 3;
            }
        }
    }

    public function add(Request $request)
    {
        $row = DB::select("select t_name,concat(path,id) as pp,id from eq_type order by pp");
        foreach ($row as $k => $v) {
            $arr = explode(",", $v->pp);
            $chang = count($arr) - 2;
            $row[$k]->t_name = str_repeat("---", $chang) . $v->t_name;
        }
        return view("admin.type.TypeAdd", ["row" => $row]);
    }

    public function doadd(Request $request)
    {
        $p_id = $request['data']['p_id'];
        $t_name = $request['data']['t_name'];
        //判断重名
        $cm = DB::table("eq_type")->where("t_name", "=", $t_name)->get();
        if (count($cm) > 0) {
            echo 3;
            die;
        }
        $arr = $request['data'];
        //获取path
        if ($p_id == 0) {
            //时间
            $arr["time"] = time();
            $arr["path"] = "0,";
            $row = DB::table("eq_type")->insert($arr);
            if ($row) {
                echo 1;
            } else {
                echo 2;
            }
        } else {
            $arr["time"] = time();
            $p = DB::table("eq_type")->where("id", "=", $p_id)->get();
            $arr["path"] = $p['0']->path . $p_id . ",";
            $row = DB::table("eq_type")->insert($arr);
            if ($row) {
                echo 1;
            } else {
                echo 2;
            }
        }
    }

    public function edit($id)
    {
        $row = DB::table("eq_type")->where("id", "=", $id)->get();
        $row2 = DB::select("select t_name,concat(path,id) as pp,id,p_id from eq_type order by pp");
        foreach ($row2 as $k => $v) {
            $arr = explode(",", $v->pp);
            $chang = count($arr) - 2;
            $row2[$k]->t_name = str_repeat("---", $chang) . $v->t_name;
        }
        return view("/admin/type/TypeEdit", ["row" => $row, "row2" => $row2]);
    }

    public function doedit(Request $request)
    {
        $arr = $request['data'];
        $id = $arr['id'];
        $old = $arr['old'];
        unset($arr['old']);
        //老名字 和 传过来的名字一样 就是没有修改名
        if ($old == $arr['t_name']) {
            unset($arr['id']);
            $arr["time"] = time();
            $row = DB::table("eq_type")->where("id", "=", $id)->update($arr);
            if ($row) {
                echo 1;
            } else {
                echo 2;
            }
        } else {
            // 判断重名 在数据库里有数据就是重名
            $cm = DB::table("eq_type")->where("t_name", "=", $arr['t_name'])->get();
            if (count($cm) > 0) {
                echo 3;
            } else {
                unset($arr['id']);
                $arr["time"] = time();
                $row = DB::table("eq_type")->where("id", "=", $id)->update($arr);
                if ($row) {
                    echo 1;
                } else {
                    echo 2;
                }
            }
        }
    }

    public function yd($id, $p_id)
    {
        //所有的要移动的
        $row1 = DB::select("select t_name from eq_type where id=" . $id . " or p_id =" . $id);
        //所有可以移动到的分支
        $row2 = DB::select("select t_name,id from eq_type where p_id <> " . $id . " and id <> " . $id . " and id <> " . $p_id);
        return view("/admin/type/TypeHb", ["row1" => $row1, "id" => $id, "row2" => $row2]);
    }

    public function doyd(Request $request)
    {
        //移到到哪个父级id下
        $id = $request['id'];
        //要移动的id
        $yid = $request['yid'];
        //没有传id
        if ($id == 'a') {
            echo 3;
            die;
        }
        $row = DB::select("update eq_type set p_id = " . $id . " where id=" . $yid . " or p_id = " . $yid);
        if ($row) {
            echo 2;
        } else {
            $fu = DB::table("eq_type")->where("id", '=', $id)->value("path");
            $path = $fu . $id . ",";
            $zi = DB::select("update eq_type set path = '{$path}' where id = " . $yid . " or p_id = " . $id);
            echo 1;
        }
    }
}
