<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class MenuController extends Controller
{
    //
    public function index(Request $request)
    {
        $k = $request->input('keywords');
        $res = DB::table('eq_menu')->where('m_name','like','%'.$k.'%')->orderBy('id')->paginate(1000);
        foreach ($res as $k=>$v) {
            $arr = explode(",",$v->path);
            $chang = count($arr)-2;
            $res[$k]->m_name = str_repeat("└───＞",$chang).$v->m_name;
        }
        return view("admin.menu.index",["res"=>$res,'request'=>$request->all()]);
    }
    public function banned(Request $request)
    {
        $data = $request->input(['ishidden']);
        $id = $request->input(['id']);
        if ($data == '显示') {
            if(DB::table('eq_menu')->where('id','=',$id)->update(['ishidden'=>1])){
                echo '2';
            }else{
                echo '0';
            }
        } elseif ($data == '隐藏') {
            if(DB::table('eq_menu')->where('id','=',$id)->update(['ishidden'=>0])){
                echo '3';
            }else{
                echo '1';
            }
        }
    }
    public function add()
    {
        $res = DB::table('eq_menu')->where('pid','=','0')->get();
        return view("admin.menu.add",['res'=>$res]);
    }
    public function doadd(Request $request)
    {
        $data = $request->except(['_token']);
        if ($data['pid'] == '0') {
            $data['path'] = '0,';
        } else {
            $path = DB::table('eq_menu')->where('id','=',$data['pid'])->value('path');
            $data['path'] = $path.$data['pid'].',';
        }
        if(DB::table('eq_menu')->insert($data)){
            echo '1';
        } else {
            echo '0';
        }
    }
    public function edit($id)
    {
        $res = DB::table('eq_menu')->where('id','=',$id)->first();
        return view("admin.menu.edit",['res'=>$res,'id'=>$id]);
    }
    public function doedit(Request $request)
    {
        $data = $request->except(['_token','id']);
        $id = $request->input('id');
        if (isset($data['ico'])) {
            $data['ico'] = rtrim($data['ico'],';').';';
        } else {
            unset($data['ico']);
        }
        if (DB::table('eq_menu')->where('id','=',$id)->update($data)) {
            echo '1';
        } else {
            echo '0';
        }
    }
    public function del(Request $request)
    {
        $id = $request->input('id');
        if (DB::table('eq_menu')->where('pid','=',$id)->count()) {
            echo '2';
        } else {
            if (DB::table('eq_menu')->where('id','=',$id)->delete()) {
                DB::table('eq_power-menu')->where('mid','=',$id)->delete();
                echo '1';
            } else {
                echo '0';
            }
        }
    }
    public function permissions(Request $request)
    {
        $k = $request->input('keywords');
        $res = DB::table('eq_power')->where('name','like','%'.$k.'%')->where('status','=','1')->orderBy('id')->paginate(10);
        return view("admin.menu.permissions",["res"=>$res,'request'=>$request->all()]);
    }
    public function einrichten($id)
    {
        $row = DB::table('eq_menu')->where('ishidden','=','1')->orderBy('id')->get();
        $pt = DB::table('eq_power-menu')->where('pid','=',$id)->get();
        if(DB::table('eq_power-menu')->where('pid','=',$id)->count()){
            foreach($pt as $VVV){
                $PT[] = $VVV->mid;
            }
        } else {
            $PT = [];
        }
        foreach($row as $k=>$v){
            if(in_array($v->id,$PT)){
                $row[$k]->checked = "checked";
            }else{
                $row[$k]->checked = '';
            }
        }
        // $row = $this->menuwxjtype(0);
        // foreach($row as $k=>$v){
        //     if(in_array($v->id,$PT)) {
        //         $row[$k]->checked = "checked";
        //     }else{
        //         $row[$k]->checked = '';
        //     }
        //     foreach($v->wxj as $K=>$V){
        //         if(in_array($V->id,$PT)) {
        //             $row[$k]->wxj[$K]->checked = "checked";
        //         }else{
        //             $row[$k]->wxj[$K]->checked = '';
        //         }
        //     }
        // }
        return view("admin.menu.einrichten",['row'=>$row,'id'=>$id]);
    }
    public function doeinrichten(Request $request)
    {
        $pid = $request->input('id');
        $mid = $request->input('mid');
        $a = 0;
        $b = DB::table('eq_power-menu')->where('pid','=',$pid)->delete();
        if($mid){
            foreach ($mid as $v) {
                $res[] = ['mid'=>$v,'pid'=>$pid];
            }
        $a = DB::table('eq_power-menu')->insert($res);
        }
        if($a+$b){
            echo '1';
        }else{
            echo '0';
        }
    }
    public function menuwxjtype($id)
    {
        //无限级分类
        //$res = $this->menuwxjtype(0);
        $res = DB::table('eq_menu')->where('pid','=',$id)->orderBy('id','asc')->get();
        foreach ($res as $K=>$v) {
            $res[$K]->wxj = $this->menuwxjtype($v->id);
        }
        return $res;
    }
}
