<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
class XtpzController extends Controller
{
    //显示基本配置页
    public function index(){
        $row = DB::table("eq_gconfig")->get();
        return view("admin/Xtpz/Jbpz");
    }
    //处理修改信息
    public function edit(Request $request){
        $zhi = $request["zhi"];
        $name= $request["name"];
        if($name=='COUNT'){
           $zhi=str_replace("\"","'",$zhi);
        }
        $arr = [$name=>$zhi];
        $r=constEdit("../app/Library/function.php",$arr);
        if($r){
            echo 1;
        }else{
            echo 2;
        }
    }
    public function imgedit(Request $request){
        // echo json_encode($request->hasFile('file'));
        // 1.检查是否有文件上传
        $request->hasFile('file');
        // 2.设置文件上传路径
        $path="./upload";
        // 3.设置文件名
        $fname='logo';
        // 4.获取文件上传的后缀名
        $tname=$request->file('file')->extension();
        // 5.拼接文件名+后缀名
        $name=$fname.'.png';
        // 6.移动文件到指定目录下
        $request->file('file')->move($path,$name);   
        // 7.得到文件上传路径和文件名
        rtrim($path,'/').'/'.$name;
        echo 1;
    }
    public function imgedit2(Request $request){
        // echo json_encode($request->hasFile('file'));
        // 1.检查是否有文件上传
        $request->hasFile('file');
        // 2.设置文件上传路径
        $path="./upload";
        // 3.设置文件名
        $fname='two';
        // 4.获取文件上传的后缀名
        $tname=$request->file('file')->extension();
        // 5.拼接文件名+后缀名
        $name=$fname.'.png';
        // 6.移动文件到指定目录下
        $request->file('file')->move($path,$name);   
        // 7.得到文件上传路径和文件名
        rtrim($path,'/').'/'.$name;
        echo 1;
    }
}
