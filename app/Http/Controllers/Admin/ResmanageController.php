<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Union;
use App\Models\Data;
use App\Models\Details;
use App\Models\Paddress;

class ResmanageController extends Controller
{
    public $ly;
    public $xiabiao;
    public $lyName;

    public function index(){
        $res = Union::get();
        return view("admin.caiji.caiji-res", ['res' => $res]);
    }

    //数据列表
    public function list(Request $request){
        $id = $request['id'];
        //设置最大超时时间
        set_time_limit(1800);
        $page = $request['page'];
        $union = Union::where("id", "=", $id)->first();
        if (count(explode(",", $union->ruletype)) <= 1) {
            echo "<font color='red'><b>请绑定分类,然后进行采集!</b></font><br/>";
            return;
        }
        $api = $union->u_api . "?ac=list&pg=" . $page;
        if($union->apitype==2){
            // json接口类型
            $data = json_decode(curl_get($api), true); //列表数据
            $pageCount = $data['pagecount']; //总页数
            $pageSize = $data['limit']; //每页显示条数
            $recordCount = $data['total']; //总共数据
        }else{
            // xml接口类型
            $data = curl_get($api);
            $xml = simplexml_load_string($data, 'SimpleXMLElement', LIBXML_NOCDATA); //得到xml
            $pageCount = $xml->list['pagecount']; //总页数
            $pageSize = $xml->list['pagesize']; //每页显示条数
            $recordCount = $xml->list['recordcount']; //总共数据
            $data = []; //最终用于存放数据列表数组
            $keys = ['vod_time', 'vod_id', 'type_id', 'vod_name', 'type_name', 'vod_play_from', 'vod_remarks'];  //用于批量更改键
            foreach ($xml->list->video as $k => $v) {
                $str = json_encode($v);
                $jsonArray = json_decode($str, true); //转换json为数组
                $rt = array_combine($keys, $jsonArray);
                $data['list'][] = $rt;
            }
        }

        if ($page > $pageCount) {
            return;
        }
        if($request->ajax()){
            return view("admin.caiji.caiji-respage", array('list' => $data['list']));
        }

        return view("admin.caiji.caiji-reslist",array('id'=>$id,'page'=>$page,'pageCount'=> $pageCount, 'pageSize'=> $pageSize, 'recordCount'=> $recordCount,'list'=> $data['list']));
    }

    public function search(Request $request){
        $keyword = $request['ss']; //获取搜索内容
        $id = $request['id'];
        $union = Union::where("id", "=", $id)->first();
        $api = $union->u_api . "?wd=" . $keyword;
        if($union->apitype==2){
            // json类型接口
            $data = json_decode(curl_get($api), true); //列表数据
        }else{
            // xml类型接口
            $data = curl_get($api);
            $xml = simplexml_load_string($data, 'SimpleXMLElement', LIBXML_NOCDATA); //得到xml
            $data = []; //最终用于存放数据列表数组
            $keys = ['vod_time', 'vod_id', 'type_id', 'vod_name', 'type_name', 'vod_play_from', 'vod_remarks'];  //用于批量更改键
            foreach ($xml->list->video as $k => $v) {
                $str = json_encode($v);
                $jsonArray = json_decode($str, true); //转换json为数组
                $rt = array_combine($keys, $jsonArray);
                $data['list'][] = $rt;
            }
        }

        return \json_encode($data);

    }


    //采集指定条数据
    public function ids($ids,$id){
        //设置最大超时时间
        set_time_limit(1800);
        $union = Union::where("id", "=", $id)->first();
        if (count(explode(",", $union->ruletype)) <= 1) {
            echo "<font color='red'><b>请绑定分类,然后进行采集!</b></font><br/>";
            return;
        }
        $api = $union->u_api . "?ac=list&ids=" . $ids;
        if($union->apitype==2){
            // json类型接口
            $data = json_decode(curl_get($api), true); //列表数据
            //取出播放来源,格式化
            if (isset($data['list'][0]['vod_play_from'])) {
                $this->ly = $ly = $data['list'][0]['vod_play_from']; //取出来源
            } else {
                $ly = $this->ly;
            }

            //分割来源取出指定来源
            $lys = explode('$$$', $ly);
            if (count($lys) == 1) {
                $lys = explode(',', $ly);
            }

            //指定下标
            if (playLy($lys) !== '') {
                $this->xiabiao =  $xiabiao = playLy($lys);
            } else {
                $xiabiao = $this->xiabiao;
            }

            //取出指定播放来源
            if (isset($lys["{$xiabiao}"])) {
                $this->lyName = $lyName = $lys["{$xiabiao}"];
            } else {
                $lyName = $this->lyName;
            }

            //取出分类绑定规则,并格式化分类绑定信息
            $str = $union->ruletype;
            $tarr = explode(",", $str);
            $groupId = "";
            $newIdGroup = ""; //新id组
            //循环列表数据
            foreach ($data['list'] as $v) {
                $d_name  =  Data::where("d_name", "=", $v['vod_name'])->first();
                if ($d_name) {
                    $newId = $d_name->id;
                    //更新显示集数
                    Data::where("id", "=", $newId)->update(['note' => $v['vod_remarks'], 'time' => time()]);
                    echo "数据存在,正在更新:  <font color='red'><b>" . $v['vod_name'] . "</b></font><br/>";
                } else {
                    $time = time(); //添加入库时间
                    $rearr = explodePro('_', $tarr); //分类绑定规则格式化
                    $tid = str_replace($rearr['left'], $rearr['right'], $v['type_id']);
                    if (!in_array($v['type_id'], $rearr['left'])) {
                        echo "数据:  <font color='red'><b>" . $v['vod_name'] . "</b></font> 未绑定分类,略过!</br>";
                        continue;
                    }
                    $newId = Data::insertGetId(array('d_name' => $v['vod_name'], 't_id' => $tid, 'typename' => $v['type_name'], 'note' => $v['vod_remarks'], 'lasttime' => $v['vod_time'], 'time' => $time));
                    echo "正在采集新数据:  <font color='red'><b>" . $v['vod_name'] . "</b></font></br>";
                }
                $newIdGroup .= $v['vod_id'] . '_' . $newId . ','; //格式化生成id,用于判断后面id与采集到的视频id

            }
            if ($newIdGroup == '') {
                echo "<font color='red'><b>全部任务已完成</b></font><br/>";
                return;
            }
            //生成id组
            $arrNewIdGroup = explode(",", rtrim($newIdGroup, ','));
            $apiDetail = $union->u_api . "?ac=detail&ids=" . $ids; //采集详情
            $data = json_decode(file_get_contents($apiDetail), true); //详情数据
            $play = [];
            // 循环详情数据
            foreach ($data['list'] as $v) {
                $rearr = explodePro('_', $arrNewIdGroup);
                if (count($rearr['left']) == count($rearr['right'])) {
                    $pid = str_replace($rearr['left'], $rearr['right'], $v['vod_id']);
                } else {
                    return  "<font color='red'><b>分类绑定转换错误,采集停止!</b></font><br/>";
                }

                $purl = explode('$$$', $v['vod_play_url']);
                if (count($purl) <= 1 || strlen("{$pid}") > 8) {
                    continue;
                }

                //判断播放地址,如果 为空则忽略本次采集
                if (!isset($purl["{$xiabiao}"])) {
                    continue;
                }
                $play["{$pid}"] = $purl["{$xiabiao}"]; //播放地址 字符串第01集$https://qq.com-ok-qq.com/20191001/23998_22dfae14/index.m3u8#第02集$ht..
                $d_name  =  Details::where("vod_name", "=", $v['vod_name'])->first();
                if ($d_name != '') {
                    //如果存在数据则删除全部播放地址以便后续更新
                    Paddress::where("r_id", "=", $pid)->where("psorce", "=", $lyName)->delete();
                    $row = Details::where("id", "=", $d_name->id)->update(['img' => $v['vod_pic']]);
                } else {
                    $clickNum = mt_rand(CLICK_MIN, CLICK_MAX);
                    $scoreNum = mt_rand(SCORE_MIN, SCORE_MAX);
                    $v['vod_content'] = isset($v['vod_content']) ? $v['vod_content'] : '';
                    $v['vod_director'] = isset($v['vod_director']) ? $v['vod_director'] : '';
                    $row = Details::insert(array('d_id' => $pid, 'vod_name' => $v['vod_name'], 'img' => $v['vod_pic'], 'director' => $v['vod_director'], 'strring' => $v['vod_actor'], 'num' => $clickNum, 'score' => $scoreNum, 'region' => $v['vod_area'], 'language' => $v['vod_lang'], 'year' => $v['vod_year'], 'playly' => $lyName, 'plot' => $v['vod_content'], 'time' => $v['vod_time']));
                }
            }

            foreach ($play as $k => $v) {
                $row = Paddress::insert(array('r_id' => $k, 'psorce' => $lyName, 'playurl' => $v, 'time' => time()));
            }

            echo "<font color='red'><b>全部任务已完成</b></font><br/>";
        }else{
            // xml类型接口
            $data = curl_get($union->u_api);
            $xml = json_encode(simplexml_load_string($data, 'SimpleXMLElement', LIBXML_NOCDATA)->list->video);
            $lyName = json_decode($xml, true)['dt']; //取出播放来源

            //准备开始采集
            $apiDetail = $union->u_api . "?ac=videolist&ids=" . $ids; //采集详情
            $data = curl_get($apiDetail);
            $xml = simplexml_load_string($data, 'SimpleXMLElement', LIBXML_NOCDATA); //得到xml
            $data = []; //最终用于存放数据列表数组
            $keys = ['vod_time', 'vod_id', 'type_id', 'vod_name', 'type_name', 'vod_pic', 'vod_lang', 'vod_area', 'vod_year', 'vod_state', 'vod_remarks', 'vod_actor', 'vod_director', 'vod_play_url', 'vod_content'];  //用于批量更改键
            foreach ($xml->list->video as $k => $v) {
                $str = json_encode($v);
                $jsonArray = json_decode($str, true); //转换json为数组
                $rt = array_combine($keys, $jsonArray);
                $data['list'][] = $rt;
            }
            //取出分类绑定规则,并格式化分类绑定信息
            $str = $union->ruletype;
            $tarr = explode(",", $str);
            $groupId = "";
            $newIdGroup = ""; //新id组
            //循环列表数据
            foreach ($data['list'] as $v) {
                $d_name  =  Data::where("d_name", "=", $v['vod_name'])->first();
                if ($d_name) {
                    $newId = $d_name->id;
                    //更新显示集数
                    Data::where("id", "=", $newId)->update(['note' => $v['vod_remarks'], 'time' => time()]);
                    echo "数据存在,正在更新:  <font color='red'><b>" . $v['vod_name'] . "</b></font><br/>";
                } else {
                    $time = time(); //添加入库时间
                    $rearr = explodePro('_', $tarr); //分类绑定规则格式化
                    $tid = str_replace($rearr['left'], $rearr['right'], $v['type_id']);
                    if (!in_array($v['type_id'], $rearr['left'])) {
                        echo "数据:  <font color='red'><b>" . $v['vod_name'] . "</b></font> 未绑定分类,略过!</br>";
                        continue;
                    }
                    $newId = Data::insertGetId(array('d_name' => $v['vod_name'], 't_id' => $tid, 'typename' => $v['type_name'], 'note' => $v['vod_remarks'], 'lasttime' => $v['vod_time'], 'time' => $time));
                    echo "正在采集新数据:  <font color='red'><b>" . $v['vod_name'] . "</b></font></br>";
                }
                $newIdGroup .= $v['vod_id'] . '_' . $newId . ','; //格式化生成id,用于判断后面id与采集到的视频id
            }
            if ($newIdGroup == '') {
                // 判断空数据
                echo "<font color='red'><b>全部任务已完成</b></font><br/>";
                return;
            }

            //生成id组
            $arrNewIdGroup = explode(",", rtrim($newIdGroup, ','));
            $play = [];

            // 循环详情数据
            foreach ($data['list'] as $v) {
                $rearr = explodePro('_', $arrNewIdGroup);
                if (count($rearr['left']) == count($rearr['right'])) {
                    $pid = str_replace($rearr['left'], $rearr['right'], $v['vod_id']);
                } else {
                    return  "<font color='red'><b>分类绑定转换错误,采集停止!</b></font><br/>";
                }

                $play["{$pid}"] = $v['vod_play_url']['dd']; //播放地址 字符串第01集$https://qq.com-ok-qq.com/20191001/23998_22dfae14/index.m3u8#第02集$ht..
                $d_name  =  Details::where("vod_name", "=", $v['vod_name'])->first();
                if ($d_name != '') {
                    //如果存在数据则删除全部播放地址以便后续更新
                    Paddress::where("r_id", "=", $pid)->where("psorce", "=", $lyName)->delete();
                    $row = Details::where("id", "=", $d_name->id)->update(['img' => $v['vod_pic']]);
                } else {
                    $clickNum = mt_rand(CLICK_MIN, CLICK_MAX);
                    $scoreNum = mt_rand(SCORE_MIN, SCORE_MAX);
                    $row = Details::insert(array('d_id' => $pid, 'vod_name' => $v['vod_name'], 'img' => $v['vod_pic'], 'director' => nullArrayConversion($v['vod_director']), 'strring' => nullArrayConversion($v['vod_actor']), 'num' => $clickNum, 'score' => $scoreNum, 'region' => nullArrayConversion($v['vod_area']), 'language' => nullArrayConversion($v['vod_lang']), 'year' => nullArrayConversion($v['vod_year']), 'playly' => $lyName, 'plot' => nullArrayConversion($v['vod_content']), 'time' => $v['vod_time']));
                }
            }
            //播放地址入库
            foreach ($play as $k => $v) {
                if (is_array($v)) {
                    $row = Paddress::insert(array('r_id' => $k, 'psorce' => $lyName, 'playurl' => $v[0], 'time' => time()));
                } else {
                    $row = Paddress::insert(array('r_id' => $k, 'psorce' => $lyName, 'playurl' => $v, 'time' => time()));
                }
            }
            echo "<font color='red'><b>全部任务已完成</b></font><br/>";

        }


    }




}
