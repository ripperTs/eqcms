<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Hash;
use DB;
class UserController extends Controller
{
    //
    public function index(Request $request)
    {
        $k = $request->input('keywords');
        $res = User::where('user','like','%'.$k.'%')->where("status","<>","0")->orderBy('id','desc')->paginate(10);
        return view("admin.user.index",["res"=>$res,'request'=>$request->all()]);
    }
    public function add()
    {
        return view("admin.user.add");
    }
    public function doadd(Request $request)
    {
        $data = $request->except(['_token']);
        if(User::where('user','=',"{$data['user']}")->count()){
            echo '2';
        } else {
            $data['passwd'] = Hash::make($data['passwd']);
            $data['time'] = time();
            $data['email'] = $data['user'];
            $data['activation'] = 1;
            if(User::create($data)){
                echo '1';
            } else {
                echo '0';
            }
        }
    }
    public function banned(Request $request)
    {
        $data = $request->input(['status']);
        $id = $request->input(['id']);
        if ($data == '启用') {
            if(User::where('id','=',$id)->update(['status'=>1])){
                echo '2';
            }else{
                echo '0';
            }
        } elseif ($data == '停用') {
            if(User::where('id','=',$id)->update(['status'=>0])){
                echo '3';
            }else{
                echo '1';
            }
        }
    }
    public function info($id)
    {
        $res = User::find($id);
        if($res){
        return view("admin.user.info",['res'=>$res]);
        }else{
            echo "<h1>该用户不存在</h1>";
        }
    }
    public function collection(Request $request)
    {
        $k = $request->input('keywords');
        $res = DB::table('eq_collection')->where('d_name','like','%'.$k.'%')->orderBy('id','desc')->paginate(10);
        return view("admin.user.collection",["res"=>$res,'request'=>$request->all()]);
    }
    public function backlist(Request $request)
    {
        $res = DB::table('eq_user')->where('status','=','0')->orderBy('id','desc')->paginate(10);
        return view("admin.user.backlist",["res"=>$res]);
    }
    public function deblocking(Request $request)
    {
        $id = $request->input('id');
        if(DB::table('eq_blacklist')->where("id",'=',$id)->delete()){
            echo '1';
        }else{
            echo '0';
        }
    }
    public function recharge(Request $request)
    {
        $k = $request->input('keywords');
        $res = DB::table('eq_recharge')->where('code','like','%'.$k.'%')->orderBy('id','desc')->paginate(10);
        return view("admin.user.recharge",["res"=>$res,'request'=>$request->all()]);
    }
}
