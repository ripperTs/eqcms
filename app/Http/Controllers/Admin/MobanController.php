<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MobanController extends Controller
{

    public function index() {
	    $dir = resource_path();
	    $dir = $dir."/views/template";
		$tem = explode("/",TEM)[1];
		$arr = $this->traverseDir($dir);
		return view("admin.moban.moban",['arr'=>$arr,'tem'=>$tem]);

	}

	public function switch(Request $request){
		$arr  = ['TEM'=> $request['tem']];
		constEdit("../app/Library/function.php",$arr);
		echo 1;
	}

	public function traverseDir($dir)
	{

		if ($dir_handle = @opendir($dir)) {
			$arr = array();
			while ($filename = readdir($dir_handle)) {
				if ($filename != "." && $filename != "..") {
					$subFile = $dir . DIRECTORY_SEPARATOR . $filename; //要将源目录及子文件相连
					if (is_dir($subFile)) { //若子文件是个目录
						$arr[] = $filename;

						//echo $filename; //输出该目录名称
						//traverseDir($subFile); //递归找出下级目录名称);
					}
				}
			}
			closedir($dir_handle);
		}
		return $arr;
	}

}
