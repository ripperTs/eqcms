<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\Redis;

class IndexController extends Controller
{
    //
    public function index(Request $request)
    {
        $id = session('admin')['id'];
        $res=DB::select('SELECT *,CONCAT(id,path) as pp FROM eq_menu WHERE ishidden=1 ORDER BY id');
        if ($id != '1') {
            $mid = DB::table('eq_admin-power')->join('eq_power','eq_admin-power.pid','=','eq_power.id')->join('eq_power-menu','eq_power.id','=','eq_power-menu.pid')->select('eq_power-menu.mid')->where('eq_admin-power.uid','=',$id)->where('eq_power.status','=','1')->get();
            $MID = array();
            foreach ($mid as $V) {
                $MID[] = $V->mid;
            }
            foreach ($res as $k=>$v) {
                if (in_array($v->id,$MID)) {
                    unset($res[$k]);
                }
            }
        }
        $admin_name=session("admin")['user'];
        return view("admin.index",["res"=>$res,"admin_name"=>$admin_name]);
    }

    public function welcome(Request $request)
    {
        $url = $_SERVER['HTTP_HOST'];
        $system = PHP_OS;
        $maxTime = get_cfg_var("max_execution_time")."秒";
        $zend = zend_version();
        $yclj = ini_get("allow_url_fopen") ? '支持' : '不支持';
        $maxFile = ini_get("post_max_size");
        $php_v = @phpversion();
        $mod = php_sapi_name(); //php运行方式
        $laravel = app()::VERSION; //框架版本

        $eq_data = DB::select("SELECT count(*) as num FROM eq_data");
        $dayTime = date("Y-m-d");
        $time = DB::select("select count(*) as num FROM eq_data WHERE time>unix_timestamp('{$dayTime}')");
        $userNum = DB::select("SELECT COUNT(*) AS num FROM eq_user");

        $leave = DB::select("SELECT COUNT(*) as num FROM eq_leave");
        $comment = DB::select("SELECT COUNT(*) as num FROM eq_comments");
        $backlist = DB::select("SELECT COUNT(*) as num FROM eq_blacklist");
        $admin_name=session("admin")['user'];
        return view("admin.welcome",array("admin_name"=>$admin_name,'backlist'=> $backlist[0]->num,'comment'=>$comment[0]->num,'leave'=> $leave[0]->num,'userNum'=>$userNum[0]->num,'dayTime'=>$time[0]->num,'cjz'=> $eq_data[0]->num,'url'=>$url,'system'=>$system,'maxTime'=>$maxTime,'zend'=>$zend,'yclj'=>$yclj,'maxFile'=>$maxFile,'php_v'=>$php_v,'mod'=>$mod,'laravel'=>$laravel,'systemType'=> php_uname('s')));
    }
    public function user(){
        return view("admin.member-del");
    }

    //清除缓存
    public function clear(){
        $res = Redis::keys(MARK_REDIS."*");  //EQ0INDEX
        foreach($res as $v){
            Redis::del($v);
        }
        echo "<script>alert('清除首页缓存成功!');location='/admin'</script>";
    }
}
