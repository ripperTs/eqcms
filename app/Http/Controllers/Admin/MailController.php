<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class MailController extends Controller
{
    public function mail(){
    	$file = "../.env"; //.env文件位置
		$str = file_get_contents($file); //读取文件内容
		$prg = "/MAIL_DRIVER=(.*?)MAIL_HOST=(.*?)MAIL_PORT=(.*?)MAIL_USERNAME=(.*?)MAIL_PASSWORD=(.*?)MAIL_ENCRYPTION=(.*?)MAIL_FROM_ADDRESS=(.*?)MAIL_FROM_NAME=(.*?)\n/s";
		preg_match_all($prg,$str,$arr);
		$host = $arr[2]; //smtp服务器地址
		$port= $arr[3]; //端口
		$username = $arr[4]; //smtp账户名
		$password = $arr[5]; //smtp密码(授权码)
		$encryption = $arr[6]; //加密方式(不允许改动)
		$address = $arr[7]; //smtp邮件地址 (与账户名相同)
		$name = $arr[8]; // smtp发信名称
    	return view("admin.email.email",['address'=>$address,'port'=>$port,'username'=>$username,'host'=>$host,'encryption'=>$encryption,'name'=>$name,'password'=>$password]);
	}

    public function edit(Request $request){
		$file = "../.env";
		saveEnv($file,$_POST);
		echo "<script>location='/admin/mail'</script>";
    }

}
