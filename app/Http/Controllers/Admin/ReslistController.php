<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Union;
use App\Models\Type;
use App\Models\Data;
use App\Models\Details;
use App\Models\Paddress;

class ReslistController extends Controller
{

    public $ly;
    public $xiabiao;
    public $lyName;

    public function index()
    {
        $res = Union::get();
        return view('admin.caiji.caiji-list', ['res' => $res]);
    }

    //分类绑定
    public function binding($id)
    {
        $union = Union::where("id", "=", $id)->first();
        $api = $union->u_api;
        $apitype = $union->apitype; //1:xml类型接口  2:json类型接口
        if ($apitype == 2) {
            // json类型接口
            $typeList = $api . "?ac=list";
            $ruletype = $union->ruletype;
            if ($ruletype == "") {
                $ruletype = ['0' => null];
            } else {
                $ruletype = explode(",", $ruletype);
            }
            $data = json_decode(curl_get($typeList), true); //列表数据
        } else if ($apitype == 1) {
            // xml类型接口
            $typeList = $api;
            $ruletype = $union->ruletype;
            if ($ruletype == "") {
                $ruletype = ['0' => null];
            } else {
                $ruletype = explode(",", $ruletype);
            }
            $data = curl_get($api);
            $xml = simplexml_load_string($data, 'SimpleXMLElement', LIBXML_NOCDATA); //得到xml
            $data = [];
            foreach ($xml->class->ty as $value) {
                $data['class'][] = [
                    'type_id' => strval($value['id']),
                    'type_name' => strval($value)
                ];
            }
        }
        $type = Type::get();
        return view("admin.caiji.caiji-binding", ['data' => $data, 'type' => $type, 'dataId' => $id, 'ruletype' => $ruletype]);
    }

    //执行分类绑定
    public function dobinding()
    {
        //避免错误绑定
        if ($_GET['typeid'] == "") {
            return;
        }
        $union = Union::where("id", "=", $_GET['id'])->first();
        //禁止采集
        if ($_GET['typeid'] == 'ban') {
            $notype = $union->notype;
            $tmp = explode(",", $notype);
            //判断是否存在禁止的分类id,如果存在则不添加
            if (array_search($_GET['apiid'], $tmp)  !== false) {
                echo "0";
                return;
            }
            $notype .= "," . $_GET['apiid'];
            $row = Union::where("id", "=", $_GET['id'])->update(['notype' => ltrim($notype, ',')]);
            if ($row) {
                echo "1";
            } else {
                echo "0";
            }
            //分类绑定数组
            $tmps = explode(",", $union->ruletype);
            foreach ($tmps as $v) {
                $arr = explode("_", $v);
                //如果当前要禁止采集的id已经在采集规则中,那么就删除这个采集规则
                if ($arr[1] == $_GET['apiid']) {
                    //返回已存在的采集规则获取
                    $ss = $arr[0] . "_" . $arr[1];
                    break;
                } else {
                    $ss = false;
                }
            }
            if ($ss) {
                $key = array_search($ss, $tmps);
                array_splice($tmps, $key, 1);
                $str = implode(",", $tmps);
                $row = Union::where("id", "=", $_GET['id'])->update(['ruletype' => $str]);
            }
        } else {
            $ruletype = $union->ruletype;
            $tmp = explode(",", $ruletype);
            //判断是否在禁止采集字段
            $tmps = explode(",", $union->notype);
            //判断是否存在禁止的分类id,如果存在则不添加
            if ($key = array_search($_GET['apiid'], $tmps)  !== false) {
                array_splice($tmps, ($key - 1), 1);
                $str = implode(",", $tmps);
                //如果数组小于等于1  直接删除数组
                if (count($tmps) <= 0) {
                    Union::where("id", "=", $_GET['id'])->update(['notype' => '']);
                } else {
                    Union::where("id", "=", $_GET['id'])->update(['notype' => $str]);
                }
            }
            //判断是否重复保存,如果是则替换
            if ($union->ruletype != '') {
                $tmpa = explode(",", $union->ruletype);
                foreach ($tmpa as $v) {
                    $arr = explode("_", $v);
                    //如果当前要禁止采集的id已经在采集规则中,那么就删除这个采集规则
                    if ($arr[1] == $_GET['apiid']) {
                        //返回已存在的采集规则获取
                        $ss = $arr[0] . "_" . $arr[1];
                        break;
                    } else {
                        $ss = false;
                    }
                }
                $ruletype .= "," . $_GET['apiid'] . "_" . $_GET['typeid'];
                $row = Union::where("id", "=", $_GET['id'])->update(['ruletype' => ltrim($ruletype, ',')]);
            } else {
                $ruletype .= "," . $_GET['apiid'] . "_" . $_GET['typeid'];
                $row = Union::where("id", "=", $_GET['id'])->update(['ruletype' => ltrim($ruletype, ',')]);
            }

            if ($row) {
                echo "1";
            } else {
                echo "0";
            }
        }
    }


    //执行资源库添加
    public function add()
    {
        return view("admin.caiji.caiji-add");
    }

    //执行资源库添加
    public function doadd(Request $request)
    {
        $arr = $request->except("_token");
        $arr['time'] = time();
        unset($arr['s']);
        $row = Union::insert($arr);
        if ($row) {
            echo 1;
        } else {
            echo 2;
        }
    }

    //资源库修改
    public function edit($id)
    {
        $res = Union::where("id", "=", $id)->first();
        return view("admin.caiji.caiji-edit", ['id' => $id, 'res' => $res]);
    }

    //执行资源库修改
    public function doedit(Request $request)
    {
        $arr = $request->except("_token");
        $arr['time'] = time();
        unset($arr['s']);
        $row = Union::where('id', '=', $arr['id'])->update($arr);
        if ($row) {
            echo 1;
        } else {
            echo 2;
        }
    }

    //执行资源库删除
    public function del(Request $request)
    {
        $row = Union::where("id", "=", $request['id'])->delete();
        if ($row) {
            echo 1;
        } else {
            echo 2;
        }
    }

    //清除分类绑定
    public function clear(Request $request)
    {
        $row = Union::where("id", "=", $request['id'])->update(['ruletype' => '']);
        if ($row) {
            echo 1;
        } else {
            echo 2;
        }
    }

    //采集内容
    public function action(Request $request)
    {
        $uid = session('admin')['id'];
        // 免登陆采集api判断token
        if (!isset($uid) && $request['token'] != CJ_TOKEN) {
            echo "<script>location='/404.html'</script>";
            return;
        }
        $log = "";
        $page = $request['page'];
        $id = $request['id'];
        $as = $request['type'];
        $union = Union::where("id", "=", $id)->first();
        // 判断分类绑定
        if (count(explode(",", $union->ruletype)) <= 1) {
            echo "<font color='red'><b>请绑定分类,然后进行采集!</b></font><br/>";
            return;
        }
        if ($union->apitype == 2) {
            // json类型接口

            if ($as == 'day') {
                $api = $union->u_api . "?ac=list&h=24&pg=" . $page;
            } elseif ($as == 'all') {
                $api = $union->u_api . "?ac=list&pg=" . $page;
            }
            $data = http_request($api); //列表数据

            //判断数据是否完整
            if ($data['pagecount'] == '') {
                Union::where("id", "=", $id)->update(['history' => "/admin/caiji-action/" . $request['page'] . "/" . $request['id'] . "/" . $as]); //存入本次采集记录
                return  "<font color='red'><b>接口信息获取失败,本次采集已记录,请重新采集即可续采!</b></font><br/>";
            }

            $pageCount = $data['pagecount']; //总页数
            $pageSize = $data['limit']; //每页显示条数
            $recordCount = $data['total']; //总共数据

            if ($page > $pageCount) {
                echo "<font color='red'><b>全部任务已完成</b></font><br/>";
                Union::where("id", "=", $id)->update(['history' => '']); //清空记录
                return;
            }
            $log .= "<font color='red'><b>资源库接口信息:</b></font><br/>";
            $log .= "当前页:" . $page . " 总页数:" . $pageCount . " 今日更新数据:" . $recordCount . "<br/><br/>";

            //取出播放来源,格式化
            if (isset($data['list'][5]['vod_play_from'])) {
                $this->ly = $ly = $data['list'][5]['vod_play_from']; //取出来源
            } else {
                $ly = $this->ly;
            }

            //分割来源取出指定来源
            $lys = explode('$$$', $ly);
            if (count($lys) == 1) {
                $lys = explode(',', $ly);
            }

            //指定下标
            if (playLy($lys) !== '') {
                $this->xiabiao =  $xiabiao = playLy($lys);
            } else {
                $xiabiao = $this->xiabiao;
            }

            //取出指定播放来源
            if (isset($lys["{$xiabiao}"])) {
                $this->lyName = $lyName = $lys["{$xiabiao}"];
            } else {
                $lyName = $this->lyName;
            }

            //取出分类绑定规则,并格式化分类绑定信息
            $str = $union->ruletype;
            $tarr = explode(",", $str);
            $groupId = "";
            $newIdGroup = ""; //新id组
            //循环列表数据
            foreach ($data['list'] as $v) {
                $d_name  =  Data::where("d_name", "=", $v['vod_name'])->first();
                if ($d_name) {
                    $newId = $d_name->id;
                    //更新显示集数
                    Data::where("id", "=", $newId)->update(['note' => $v['vod_remarks'], 'time' => time()]);
                    $log .= "数据存在,正在更新:  <font color='red'><b>" . $v['vod_name'] . "</b></font><br/>";
                } else {
                    $time = time(); //添加入库时间
                    $rearr = explodePro('_', $tarr); //分类绑定规则格式化
                    $tid = str_replace($rearr['left'], $rearr['right'], $v['type_id']);
                    if (!in_array($v['type_id'], $rearr['left'])) {
                        $log .= "数据:  <font color='red'><b>" . $v['vod_name'] . "</b></font> 未绑定分类,略过!</br>";
                        continue;
                    }
                    $newId = Data::insertGetId(array('d_name' => $v['vod_name'], 't_id' => $tid, 'typename' => $v['type_name'], 'note' => $v['vod_remarks'], 'lasttime' => $v['vod_time'], 'time' => $time));
                    $log .= "正在采集新数据:  <font color='red'><b>" . $v['vod_name'] . "</b></font></br>";
                }
                $newIdGroup .= $v['vod_id'] . '_' . $newId . ','; //格式化生成id,用于判断后面id与采集到的视频id
                $groupId .= $v['vod_id'] . ","; //拼接影片id

            }
            if ($newIdGroup == '') {
                $url = "/admin/caiji-action/" . ($request['page'] + 1) . "/" . $request['id'] . "/" . $as . "?token=" . CJ_TOKEN;
                echo "<script>location='{$url}'</script>";
            }
            //生成id组
            $arrNewIdGroup = explode(",", rtrim($newIdGroup, ','));
            $groupId = rtrim($groupId, ','); //得到要采集的id
            $apiDetail = $union->u_api . "?ac=detail&ids=" . $groupId; //采集详情
            $data = http_request($apiDetail);
            //判断数据是否完整
            if (count($data['list']) <= 0) {
                Union::where("id", "=", $id)->update(['history' => "/admin/caiji-action/" . $request['page'] . "/" . $request['id'] . "/" . $as]); //存入本次采集记录
                return  "<font color='red'><b>接口信息获取失败,本次采集已记录,请重新采集即可续采!</b></font><br/>";
            }
            $play = [];
            // 循环详情数据
            foreach ($data['list'] as $v) {
                $rearr = explodePro('_', $arrNewIdGroup);
                if (count($rearr['left']) == count($rearr['right'])) {
                    $pid = str_replace($rearr['left'], $rearr['right'], $v['vod_id']);
                } else {
                    return  "<font color='red'><b>分类绑定转换错误,采集停止!</b></font><br/>";
                }

                $purl = explode('$$$', $v['vod_play_url']);
                if (count($purl) <= 1 || strlen("{$pid}") > 8) {
                    continue;
                }

                //判断播放地址,如果 为空则忽略本次采集
                if (!isset($purl["{$xiabiao}"])) {
                    continue;
                }
                $play["{$pid}"] = $purl["{$xiabiao}"]; //播放地址 字符串第01集$https://qq.com-ok-qq.com/20191001/23998_22dfae14/index.m3u8#第02集$ht..
                $d_name  =  Details::where("vod_name", "=", $v['vod_name'])->first();
                if ($d_name != '') {
                    //如果存在数据则删除全部播放地址以便后续更新
                    Paddress::where("r_id", "=", $pid)->where("psorce", "=", $lyName)->delete();
                    // if (ISIMG == 1 && ISCONTENT == 1) {
                    //     Details::where("id", "=", $d_name->id)->update(['img' => $v['vod_pic'], 'plot' => $v['vod_content']]);
                    // } elseif (ISIMG == 0 && ISCONTENT == 1) {
                    //     Details::where("id", "=", $d_name->id)->update(['plot' => $v['vod_content']]);
                    // } elseif (ISIMG == 1 && ISCONTENT == 0) {
                    //     Details::where("id", "=", $d_name->id)->update(['img' => $v['vod_pic']]);
                    // }
                } else {
                    $clickNum = mt_rand(CLICK_MIN, CLICK_MAX);
                    $scoreNum = mt_rand(SCORE_MIN, SCORE_MAX);
                    $v['vod_content'] = isset($v['vod_content']) ? $v['vod_content'] : '';
                    $v['vod_director'] = isset($v['vod_director']) ? $v['vod_director'] : '';
                    $v['vod_actor'] = isset($v['vod_actor']) ? $v['vod_actor'] : '';
                    $v['vod_year'] = isset($v['vod_year']) ? $v['vod_year'] : '';
                    $v['vod_lang'] = isset($v['vod_lang']) ? $v['vod_lang'] : '';
                    $row = Details::insert(array(
                        'd_id' => $pid,
                        'vod_name' => $v['vod_name'],
                        'img' => $v['vod_pic'],
                        'director' => isset($v['vod_director']) ? $v['vod_director'] : '',
                        'strring' => isset($v['vod_actor']) ? $v['vod_actor'] : '',
                        'num' => $clickNum,
                        'score' => $scoreNum,
                        'region' => isset($v['vod_area']) ? $v['vod_area'] : '',
                        'language' => isset($v['vod_lang']) ? $v['vod_lang'] : '',
                        'year' => isset($v['vod_year']) ? $v['vod_year'] : '',
                        'playly' => $lyName,
                        'plot' => isset($v['vod_content']) ? $v['vod_content'] : '',
                        'time' => $v['vod_time']
                    ));
                }
            }
            // 播放地址入库
            foreach ($play as $k => $v) {
                $row = Paddress::insert(array('r_id' => $k, 'psorce' => $lyName, 'playurl' => $v, 'time' => time()));
            }
        } else {
            // xml类型接口
            $data = curl_xml($union->u_api)->list->video;
            $xml = json_encode($data);
            $lyName = json_decode($xml, true)['dt']; //取出播放来源
            //判断采集当天/全部
            if ($as == 'day') {
                $api = $union->u_api . "?ac=videolist&h=24&pg=" . $page;
            } elseif ($as == 'all') {
                $api = $union->u_api . "?ac=videolist&pg=" . $page;
            }
            //准备开始采集
            $xml = curl_xml($api); //得到xml
            $pageCount = $xml->list['pagecount']; //总页数
            $pageSize = $xml->list['pagesize']; //每页显示条数
            $recordCount = $xml->list['recordcount']; //总共数据
            //判断是否采集完毕
            if ($page > $pageCount) {
                echo "<font color='red'><b>全部任务已完成</b></font><br/>";
                Union::where("id", "=", $id)->update(['history' => '']); //清空记录
                return;
            }
            echo "<font color='red'><b>资源库接口信息:</b></font><br/>";
            echo "当前页:" . $page . " 总页数:" . $pageCount . " 今日更新数据:" . $recordCount . "<br/><br/>";

            $data = []; //最终用于存放数据列表数组
            $keys = ['vod_time', 'vod_id', 'type_id', 'vod_name', 'type_name', 'vod_pic', 'vod_lang', 'vod_area', 'vod_year', 'vod_state', 'vod_remarks', 'vod_actor', 'vod_director', 'vod_play_url', 'vod_content'];  //用于批量更改键
            foreach ($xml->list->video as $k => $v) {
                $str = json_encode($v);
                $jsonArray = json_decode($str, true); //转换json为数组
                $jsonArray = array(
                    'last' => $jsonArray['last'],
                    'id' => $jsonArray['id'],
                    'tid' => $jsonArray['tid'],
                    'name' => $jsonArray['name'],
                    'type' => $jsonArray['type'],
                    'pic' => $jsonArray['pic'],
                    'lang' => $jsonArray['lang'],
                    'area' => $jsonArray['area'],
                    'year' => $jsonArray['year'],
                    'state' => $jsonArray['state'],
                    'note' => $jsonArray['note'],
                    'actor' => $jsonArray['actor'],
                    'director' => $jsonArray['director'],
                    'dl' => $jsonArray['dl'],
                    'desc' => $jsonArray['des']
                );
                $rt = array_combine($keys, $jsonArray);
                $data['list'][] = $rt;
            }
            //取出分类绑定规则,并格式化分类绑定信息
            $str = $union->ruletype;
            $tarr = explode(",", $str);
            $groupId = "";
            $newIdGroup = ""; //新id组
            //循环列表数据
            foreach ($data['list'] as $v) {
                $d_name  =  Data::where("d_name", "=", $v['vod_name'])->first();
                if ($d_name) {
                    $newId = $d_name->id;
                    //更新显示集数
                    Data::where("id", "=", $newId)->update(['note' => nullArrayConversion($v['vod_remarks']), 'time' => time()]);
                    echo "数据存在,正在更新:  <font color='red'><b>" . $v['vod_name'] . "</b></font><br/>";
                } else {
                    $time = time(); //添加入库时间
                    $rearr = explodePro('_', $tarr); //分类绑定规则格式化
                    $tid = str_replace($rearr['left'], $rearr['right'], $v['type_id']);
                    if (!in_array($v['type_id'], $rearr['left'])) {
                        echo "数据:  <font color='red'><b>" . $v['vod_name'] . "</b></font> 未绑定分类,略过!</br>";
                        continue;
                    }
                    $newId = Data::insertGetId(array('d_name' => nullArrayConversion($v['vod_name']), 't_id' => $tid, 'typename' => nullArrayConversion($v['type_name']), 'note' => nullArrayConversion($v['vod_remarks']), 'lasttime' => $v['vod_time'], 'time' => $time));

                    echo "正在采集新数据:  <font color='red'><b>" . $v['vod_name'] . "</b></font></br>";
                }
                $newIdGroup .= $v['vod_id'] . '_' . $newId . ','; //格式化生成id,用于判断后面id与采集到的视频id
            }

            if ($newIdGroup == '') {
                // 判断空数据
                echo "<br/><font color='red'><b>正在准备采集第:" . ($page + 1) . " 页....</b></font><br/>";
                $url = "/admin/caiji-action/" . ($request['page'] + 1) . "/" . $request['id'] . "/" . $as . "?token=" . CJ_TOKEN;
                echo "<script>location='{$url}'</script>";
                return;
            }

            //生成id组
            $arrNewIdGroup = explode(",", rtrim($newIdGroup, ','));
            $play = [];

            // 循环详情数据
            foreach ($data['list'] as $v) {
                $rearr = explodePro('_', $arrNewIdGroup);
                if (count($rearr['left']) == count($rearr['right'])) {
                    $pid = str_replace($rearr['left'], $rearr['right'], $v['vod_id']);
                } else {
                    return  "<font color='red'><b>分类绑定转换错误,采集停止!</b></font><br/>";
                }

                $play["{$pid}"] = $v['vod_play_url']['dd']; //播放地址 字符串第01集$https://qq.com-ok-qq.com/20191001/23998_22dfae14/index.m3u8#第02集$ht..
                $d_name  =  Details::where("vod_name", "=", $v['vod_name'])->first();
                if ($d_name != '') {
                    //如果存在数据则删除全部播放地址以便后续更新
                    Paddress::where("r_id", "=", $pid)->where("psorce", "=", $lyName)->delete();
                    if (ISIMG == 1 && ISCONTENT == 1) {
                        Details::where("id", "=", $d_name->id)->update(['img' => $v['vod_pic'], 'plot' => $v['vod_content']]);
                    } elseif (ISIMG == 0 && ISCONTENT == 1) {
                        Details::where("id", "=", $d_name->id)->update(['plot' => $v['vod_content']]);
                    } elseif (ISIMG == 1 && ISCONTENT == 0) {
                        Details::where("id", "=", $d_name->id)->update(['img' => $v['vod_pic']]);
                    }
                } else {
                    $clickNum = mt_rand(CLICK_MIN, CLICK_MAX);
                    $scoreNum = mt_rand(SCORE_MIN, SCORE_MAX);
                    $row = Details::insert(array('d_id' => $pid, 'vod_name' => $v['vod_name'], 'img' => $v['vod_pic'], 'director' => nullArrayConversion($v['vod_director']), 'strring' => nullArrayConversion($v['vod_actor']), 'num' => $clickNum, 'score' => $scoreNum, 'region' => nullArrayConversion($v['vod_area']), 'language' => nullArrayConversion($v['vod_lang']), 'year' => nullArrayConversion($v['vod_year']), 'playly' => $lyName, 'plot' => nullArrayConversion($v['vod_content']), 'time' => $v['vod_time']));
                }
            }
            //播放地址入库
            foreach ($play as $k => $v) {
                if (is_array($v)) {
                    $row = Paddress::insert(array('r_id' => $k, 'psorce' => $lyName, 'playurl' => $v[0], 'time' => time()));
                } else {
                    $row = Paddress::insert(array('r_id' => $k, 'psorce' => $lyName, 'playurl' => $v, 'time' => time()));
                }
            }
        }

        $log .= "<br/><font color='red'><b>正在准备采集第:" . ($page + 1) . " 页....</b></font><br/>";
        Union::where("id", "=", $id)->update(['history' => "/admin/caiji-action/" . $request['page'] . "/" . $request['id'] . "/" . $as]); //存入本次采集记录
        echo $log;
        sleep(CJ_TIME); //采集间隔
        $url = "/admin/caiji-action/" . ($request['page'] + 1) . "/" . $request['id'] . "/" . $as . "?token=" . CJ_TOKEN;
        echo "<script>location='{$url}'</script>"; //自动跳转


    }



    //采集断点续采
    public function xucai(Request $request)
    {
        $id = $request['id'];
        $as = $request['type'];
        $res = Union::where("id", "=", $id)->first();
        //如果有采集历史,则执行断点续传采集
        if ($res->history != '') {
            return view("admin.caiji.caiji-xucai", ['id' => $id, 'as' => $as]);
        } else {
            return redirect("/admin/caiji-action/" . $request['page'] . "/" . $request['id'] . "/" . $as);
        }
    }

    //执行采集断点
    public function doxucai(Request $request)
    {
        $id = $request['id'];
        $type = $request['type'];
        $res = Union::where("id", "=", $id)->first();
        if ($type == 1) {
            //执行继续采集
            return redirect($res->history);
        } else {
            //执行取消记录
            $row = Union::where("id", "=", $id)->update(['history' => '']);
            if ($row) {
                echo "1";
            } else {
                echo "2";
            }
        }
    }
}
