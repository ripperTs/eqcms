<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Hash;
use Mail;
use App\Models\user;
class LoginController extends Controller
{
    //
    public function index(){
    	return view(TEM."login");
    }

	//注册页面
	public function reg(){
		$phone = DB::table('eq_phoneapi')->first();
		return view(TEM."reg",['phone'=>$phone]);
	}

    //退出登录
    public function tc(Request $request){
    	$request->session()->flush();
    	echo "<script>location.href='/';</script>";
	}

	//执行注册验证
	public function dologin(Request $request){
		switch ($request['type']){
			case 'user':
				$user  = $request['user'];
				$sql = "SELECT count(*) as num FROM eq_user WHERE email='{$user}' or phone='{$user}'";
				$row = DB::select($sql);
				if($row[0]->num){
					echo 1;
				}else{
					echo 2;
				}

			break;

			case 'yzm':
				$yzm = $request['yzm'];
				if(session('loginyzm')==$yzm){
					echo 1;
				}else{
					echo 2;
				}
			break;

			case "login":
				$data = $request['data'];
				$user  = $data['user'];
				$yzm = $data['vcode'];
				if($yzm  != session('loginyzm')){
					return;
				}
				$sql = "SELECT * FROM eq_user WHERE email='{$user}' or phone='{$user}'";
				$res = DB::select($sql)[0];
				if($res){
					if($res->num > 5 && $res->activation == 1){
						echo 701; //登陆错误次数过多
					}else if($res->status==1 && $res->activation==1){
						if (Hash::check($data['passwd'], $res->passwd)) {
							$arr = ["user" => $res->user, "id" => $res->id,'name'=>$res->name];
							//存入session 权限判断用
							session(["user" => $arr]);
							echo 200; //登陆成功
						} else {
							echo 201; //密码不正确
							user::where("id","=",$res->id)->increment("num");
						}
					}else if($res->status == 2 && $res->activation == 1){
						echo 301; //账户冻结
					}else if($res->status == 0 && $res->activation == 1){
						echo 401; //账户封禁
					}else{
						echo 501; //账户未激活
					}

				}else{
					echo 601; //登陆失败
				}
			break;

			case "clearNum":
				$sql = "SELECT num FROM eq_user WHERE num<>0";
				$res = DB::select($sql);
				foreach($res as $v){
					DB::table("eq_user")->where("id","=",$v->id)->update(['num'=>0]);
				}
				echo "清除成功!";
			break;
		}
	}

	//退出登陆
	public function out(Request $request){
		$request->session()->pull("user");
		echo "<script>alert('退出成功!');location='/'</script>";
	}





}
