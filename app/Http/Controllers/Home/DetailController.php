<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Data;
use App\Models\Details;
use App\Models\Paddress;
use DB;
use Cookie;
use Illuminate\Support\Facades\Redis;

/**
 * @Author: wslmf
 * @Date: 2020-02-22 10:48:25
 * @Desc: 新增redis缓存,优化性能
 */
class DetailController extends Controller
{

    public function index($id)
    {
        $tid = Data::where("id", "=", $id)->first()->t_id;
        $dayTime = mktime(0, 0, 0, date('m'), date('d'), date('Y'));

        // 取出当天更新
        if (E_SWITCH == 1 && Redis::get(MARK_REDIS . 'dayUpdate')) {
            $data = Redis::get(MARK_REDIS . 'dayUpdate');
            $day = json_decode($data);
        } else {
            $sql = "SELECT *,d.id AS id	FROM eq_data AS d,eq_details AS e WHERE d.id=e.d_id AND d.ishidden=1 AND d.time>{$dayTime} ORDER BY d.time DESC LIMIT 0,11";
            $day = DB::select($sql); //取出当天更前10条
            if (E_SWITCH == 1) {
                Redis::setex(MARK_REDIS . 'dayUpdate', (E_TIME * 60), json_encode($day));
            }
        }

        // 正在热播
        if (E_SWITCH == 1 && Redis::get(MARK_REDIS . 'now')) {
            $data = Redis::get(MARK_REDIS . 'now');
            $hot = json_decode($data);
        } else {
            $sql = "SELECT *,d.id AS id	FROM eq_data AS d,eq_details AS e WHERE d.id=e.d_id AND d.ishidden=1 AND d.t_id={$tid} ORDER BY d.time,e.num DESC LIMIT 0,5";
            $hot = DB::select($sql); //正在热播
            if (E_SWITCH == 1) {
                Redis::setex(MARK_REDIS . 'now', (E_TIME * 60), json_encode($hot));
            }
        }

        // 猜你喜欢
        if (E_SWITCH == 1 && Redis::get(MARK_REDIS . 'guessYouLike')) {
            $data = Redis::get(MARK_REDIS . 'guessYouLike');
            $like = json_decode($data);
        } else {
            $sql = "SELECT *,d.id AS id	FROM eq_data AS d,eq_details AS e WHERE d.id=e.d_id AND d.ishidden=1 AND d.t_id={$tid} ORDER BY d.time DESC LIMIT 0,6";
            $like = DB::select($sql); //猜你喜欢
            if (E_SWITCH == 1) {
                Redis::setex(MARK_REDIS . 'guessYouLike', (E_TIME * 60), json_encode($like));
            }
        }

        // 影片详情数据
        if (E_SWITCH == 1 && Redis::get(MARK_REDIS . 'details' . $id)) {
            $data = Redis::get(MARK_REDIS . 'details' . $id);
            $data = json_decode($data);
        } else {
            $sql = "SELECT *,d.id AS did,e.id AS eid,d.time AS time FROM eq_data AS d,eq_details AS e WHERE d.id={$id} AND e.d_id={$id}";
            $data = DB::select($sql); //总数据
            if (E_SWITCH == 1) {
                Redis::setex(MARK_REDIS . 'details' . $id, (E_TIME * 60), json_encode($data));
            }
        }
        $strring = explode(",", $data[0]->strring);

        // 播放地址列表
        if (E_SWITCH == 1 && Redis::get(MARK_REDIS . 'paddress' . $id)) {
            $paddress = Redis::get(MARK_REDIS . 'paddress' . $id);
            $paddress = json_decode($paddress);
        } else {
            $paddress = Paddress::where("r_id", "=", $id)->where('ishidden', "=", 1)->orderBy("id", "desc")->get();
            // psorce
            if (E_SWITCH == 1) {
                Redis::setex(MARK_REDIS . 'paddress' . $id, (E_TIME * 60), json_encode($paddress));
            }
        }

        // 播放地址
        if (E_SWITCH == 1 && Redis::get(MARK_REDIS . 'playurl' . $id)) {
            $playurl = Redis::get(MARK_REDIS . 'playurl' . $id);
            $playurl = json_decode($playurl,true);
        } else {
            $playurl = [];
            foreach ($paddress as $v) {
                $playurl["{$v->id}"] = explode("#", $v->playurl);
            }
            if (E_SWITCH == 1) {
                Redis::setex(MARK_REDIS . 'playurl' . $id, (E_TIME * 60), json_encode($playurl));
            }
        }

        return view(TEM . "detail", ['data' => $data[0], 'strring' => $strring, 'paddress' => $paddress, 'playurl' => $playurl, 'day' => $day, 'hot' => $hot, 'like' => $like]);
    }

    //ajax评分
    public function ajax(Request $request)
    {
        if ($request['action'] == 'score') {
            if ($request->cookie("pingfen") == '') {
                Details::where("d_id", "=", $request['id'])->increment('score', $request['score']);
                Details::where("d_id", "=", $request['id'])->increment('snum');
                Cookie::forever("pingfen", "value");
                echo 1;
            } else {
                echo 2;
            }
        } else if ($request['action'] == 'vpingfen') {
            $res = Details::where("d_id", "=", $request['id'])->first();
            echo round($res->score / $res->snum, 1);
        }
    }

    // 评论
    public function comments(Request $request)
    {
        $did = $request->input('did');
        $res = DB::table('eq_comments')->select('eq_comments.id as id', 'eq_comments.uid as uid', 'eq_comments.content as content', 'eq_comments.time as time', 'eq_comments.praise as praise', 'eq_comments.tread as tread', 'eq_comments.cid as cid', 'eq_comments.cname as cname', 'eq_user.user as user', 'eq_user.name as name', 'eq_user.pic as pic')->leftjoin('eq_user', 'eq_comments.uid', '=', 'eq_user.id')->where('did', '=', $did)->orderBy('eq_comments.id')->get();
        return view(TEM . "comments", ["res" => $res, 'did' => $did]);
    }

    public function reply(Request $request)
    {
        $data = $request->input('field');
        $data['content'] = $request->input('content');
        $data['did'] = $request->input('did');
        $data['time'] = time();
        $data['uid'] = session('user')['id'] ? session('user')['id'] : '0';
        if (DB::table('eq_comments')->insert($data)) {
            echo '1';
        } else {
            echo '0';
        }
    }

    public function praise(Request $request)
    {
        $id = $request->input('id');
        $praise = session('praise');
        if (!$praise) {
            $praise = array();
        }
        if (in_array($id, $praise)) {
            echo "2";
        } else {
            $num = DB::table('eq_comments')->where('id', '=', $id)->value('praise') + 1;
            if (DB::table('eq_comments')->where('id', '=', $id)->update(['praise' => $num])) {
                $request->session()->push('praise', $id);
                echo '1';
            } else {
                echo '0';
            }
        }
    }

    public function tread(Request $request)
    {
        $id = $request->input('id');
        $tread = session('tread');
        if (!$tread) {
            $tread = array();
        }
        if (in_array($id, $tread)) {
            echo "2";
        } else {
            $num = DB::table('eq_comments')->where('id', '=', $id)->value('tread') + 1;
            if (DB::table('eq_comments')->where('id', '=', $id)->update(['tread' => $num])) {
                $request->session()->push('tread', $id);
                echo '1';
            } else {
                echo '0';
            }
        }
    }
}
