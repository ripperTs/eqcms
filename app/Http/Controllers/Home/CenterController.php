<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Mail;
use Ucpaas;
use Hash;
use AlipayNotify;

class CenterController extends Controller
{
    //
    public function index(Request $request)
    {
        $id = session('user')['id'];
        $collection = DB::table('eq_collection')->where('uid','=',$id)->get();
        $res = DB::table('eq_user')->where('id','=',$id)->first();
        $recharge = DB::table('eq_recharge')->where('uid','=',$id)->where('hide','=','1')->get();
        return view(TEM.".center",['res'=>$res,'collection'=>$collection,'recharge'=>$recharge]);
    }

    //头像上传
    public function upload(Request $request){
        $uid = $request['uid'];
        $path = "./upload/userpic"; //用户头像路径
        $request->file('file')->move($path, $uid.'.png');
        $picPath = rtrim($path, '/') . '/' . $uid . '.png';
        $res = DB::table('eq_user')->where('id',"=",$uid)->update(['pic'=> $picPath]); //存入数据库
        echo 1;
    }

    //用户收藏
    public function collection(Request $request){
        $uid = $request['uid'];
        $vid = $request['vid'];
        $row = DB::table("eq_collection")->where("uid","=",$uid)->where("url","=", "http://" . S_REALM . "/details/" . $vid . ".html")->first();
        if($row){
            return 3; //判断是否收藏过该影片
        }
        $d_name = DB::table("eq_data")->where("id","=",$vid)->first()->d_name; //获取影片名
        $res  = DB::table("eq_collection")->insert(['uid'=>$uid, 'd_name'=>$d_name, 'url'=>"http://". S_REALM."/details/".$vid.".html", 'time'=>time()]);
        if($res){
            echo 1;
        }else{
            echo 2;
        }
    }

    public function email(Request $request)
    {
        $email = $request->input('email');
        $view = TEM.".center-email";
        $code = mt_rand(10000,99999);
        session(['email_code'=>[$email,$code]]);
        $subject = S_NAME.'您的邮箱验证码已发送,请查看!';
        Mail::send($view,['code'=>$code],function($message)use($email,$subject){
            $message->to($email)->subject($subject);
        });
        if(count(Mail::failures()) < 1){
        echo '1';
        }else{
        echo '0';
        }
    }

    public function phone(Request $request)
    {
        $phone = $request->input('phone');
        $res = $this->sendsphone($phone);
        $res = json_decode($res,true);
        if($res['code'] == '000000'){
            echo '1';
        }else{
            echo '0';
        }
    }

    public function sendsphone($p)
    {
        $oop = DB::table('eq_phoneapi')->first();
        $options['accountsid'] = $oop->accountid;
        $options['token'] = $oop->token;
        $ucpass = new Ucpaas($options);
        $appid = $oop->appid;
        $templateid = $oop->templateid;
        $param = mt_rand(10000,99999);
        session(['phone_code'=>[$p,$param]]);
        $mobile = $p;
        $uid = "";
        return $ucpass->SendSms($appid,$templateid,$param,$mobile,$uid);
    }

    public function centeremail(Request $request)
    {
        $data = $request->except('_token');
        $code = session('email_code');
        if ($code['0'] == $data['email'] and $code['1'] == $data['email_code']) {
            $pass = DB::table('eq_user')->where('id','=',$data['id'])->value('passwd');
            if (Hash::check($data['passwd'],$pass)) {
                if (DB::table('eq_user')->where('id','=',$data['id'])->update(['email'=>$data['email']])) {
                    echo '1';
                } else {
                    echo '0';
                }
            } else {
                echo '3';
            }
        } else {
            echo '2';
        }
        $request->session()->pull('email_code');
    }

    public function centerphone(Request $request)
    {
        $data = $request->except('_token');
        $code = session('phone_code');
        if ($code['0'] == $data['phone'] and $code['1'] == $data['phone_code']) {
            $pass = DB::table('eq_user')->where('id','=',$data['id'])->value('passwd');
            if (Hash::check($data['passwd'],$pass)) {
                if (DB::table('eq_user')->where('id','=',$data['id'])->update(['phone'=>$data['phone']])) {
                    echo '1';
                } else {
                    echo '0';
                }
            } else {
                echo '3';
            }
        } else {
            echo '2';
        }
        $request->session()->pull('phone_code');
    }

    public function centername(Request $request)
    {
        $data = $request->except('_token');
        if (DB::table('eq_user')->where('id','=',$data['id'])->update(['name'=>$data['name']])) {
            echo '1';
        } else {
            echo '0';
        }
    }

    public function cancel(Request $request)
    {
        $data = $request->except('_token');
        if (DB::table('eq_collection')->where('id','=',$data['id'])->delete()) {
            echo '1';
        } else {
            echo '0';
        }
    }

    public function rechargepay($id)
    {
        $row = DB::table('eq_recharge')->where('id','=',$id)->first();
        $id = session('user')['id'];
        if ($row->uid != $id) {
            return back();
        }
        $eqpay = DB::table('eq_pay')->first();
        $partner = $eqpay->partner;
        $key = $eqpay->key;
        $ordernum = $row->code;
        $orderming = $row->integral;
        $money = $row->money;
        $desc = '';
        echo pay($partner,$key,$ordernum,$orderming,$money,$desc);
    }

    public function pay(Request $request,$level)
    {
        $reInfo = explode('-',$level);
        $level = $reInfo[0];
        $payType = $reInfo[1];
        $id = session('user')['id'];
        if (DB::table('eq_user')->where('id','=',$id)->value('level') >= $level) {
            return back();
        }
        if (DB::table('eq_recharge')->where('uid','=',$id)->where('status','=','0')->count()) {
            echo "<script>alert('您还有未支付的订单,请到[购买记录]删除未付款订单后再下单..');location.href='/center';</script>";
            die();
        }

        $eqpay = DB::table('eq_pay')->first();
        $partner = $eqpay->partner;
        $apiurl = $eqpay->apiurl;
        $key = $eqpay->key;
        $ordernum = time().rand(100000,999999);

        if($level == 2){
            $orderming = "铜牌会员";
            $money = LEVEL2;
        }elseif($level == 3){
            $orderming = "银牌会员";
            $money = LEVEL3;
        }elseif($level == 4){
            $orderming = "金牌会员";
            $money = LEVEL4;
        }elseif($level == 5){
            $orderming = "钻石会员";
            $money = LEVEL5;
        }

        $desc = S_NAME;
        $data['code'] = $ordernum;
        $data['uid'] = $id;
        $data['money'] = $money;
        $data['integral'] = $orderming;
        $data['time'] = time();
        DB::table('eq_recharge')->insert($data); // 生成订单记录

        echo pay($partner,$key,$ordernum,$orderming,$money,$desc,$payType, $apiurl);

    }

    public function deletee(Request $request)
    {
        $id = $request->input('id');
        $row = DB::table('eq_recharge')->where('id','=',$id)->first();
        if ($row->status == '1') {
            if(DB::table('eq_recharge')->where('id','=',$id)->update(['hide'=>0])){
                echo '1';
            }else{
                echo '0';
            }
        } elseif ($row->status == '0') {
            if(DB::table('eq_recharge')->where('id','=',$id)->delete()){
                echo '1';
            }else{
                echo '0';
            }
        }
    }

    public function notifys(Request $request)
    {
        $eqpay = DB::table('eq_pay')->first();
        $partner = $eqpay->partner;
        $key = $eqpay->key;
        $apiurl = $eqpay->apiurl;
        //商户ID
        $alipay_config['partner']        = $partner;

        //商户KEY
        $alipay_config['key']            = $key;

        //签名方式 不需修改
        $alipay_config['sign_type']    = strtoupper('MD5');

        //字符编码格式 目前支持 gbk 或 utf-8
        $alipay_config['input_charset'] = strtolower('utf-8');

        //访问模式,根据自己的服务器是否支持ssl访问，若支持请选择https；若不支持请选择http
        $alipay_config['transport']    = 'http';

        //支付API地址
        $alipay_config['apiurl']    = $apiurl;
        echo pay($partner, $key, $request->input('out_trade_no'), $request->input('name'), $request->input('money'), '', $request->input('type'), $apiurl);
    }

    public function notifyy()
    {
        $eqpay = DB::table('eq_pay')->first();
        $partner = $eqpay->partner;
        $key = $eqpay->key;
        $apiurl = $eqpay->apiurl;
        //商户ID
        $alipay_config['partner']        = $partner;

        //商户KEY
        $alipay_config['key']            = $key;

        //签名方式 不需修改
        $alipay_config['sign_type']    = strtoupper('MD5');

        //字符编码格式 目前支持 gbk 或 utf-8
        $alipay_config['input_charset'] = strtolower('utf-8');

        //访问模式,根据自己的服务器是否支持ssl访问，若支持请选择https；若不支持请选择http
        $alipay_config['transport']    = 'http';

        //支付API地址
        $alipay_config['apiurl']    = $apiurl;

        //计算得出通知验证结果
        $alipayNotify = new AlipayNotify($alipay_config);
        $verify_result = $alipayNotify->verifyNotify();

        if ($verify_result) {

            $out_trade_no = $_GET['out_trade_no'];

            //彩虹易支付交易号

            $trade_no = $_GET['trade_no'];

            //交易状态
            $trade_status = $_GET['trade_status'];

            //支付方式
            $type = $_GET['type'];


            if ($_GET['trade_status'] == 'TRADE_SUCCESS') {


            }

            //——请根据您的业务逻辑来编写程序（以上代码仅作参考）——

            echo "success";
            $row = DB::table('eq_recharge')->where('code', '=', $out_trade_no)->first();
            if ($row->integral == '铜牌会员') {
                DB::table('eq_user')->where('id', '=', $row->uid)->update(['level' => 2]);
            } elseif ($row->integral == '银牌会员') {
                DB::table('eq_user')->where('id', '=', $row->uid)->update(['level' => 3]);
            } elseif ($row->integral == '金牌会员') {
                DB::table('eq_user')->where('id', '=', $row->uid)->update(['level' => 4]);
            } elseif ($row->integral == '钻石会员') {
                DB::table('eq_user')->where('id', '=', $row->uid)->update(['level' => 5]);
            }
            if ($row->status == '0') {
                $mtime = time();
                DB::table('eq_recharge')->where('id', '=', $row->id)->update(['status' => 1, 'mtime' => $mtime]);
            }
        } else {
            //验证失败
            echo "fail";
        }

    }


    public function returnn()
    {

        $eqpay = DB::table('eq_pay')->first();
        $partner = $eqpay->partner;
        $key = $eqpay->key;
        $apiurl = $eqpay->apiurl;
        //商户ID
        $alipay_config['partner']        = $partner;

        //商户KEY
        $alipay_config['key']            = $key;

        //签名方式 不需修改
        $alipay_config['sign_type']    = strtoupper('MD5');

        //字符编码格式 目前支持 gbk 或 utf-8
        $alipay_config['input_charset'] = strtolower('utf-8');

        //访问模式,根据自己的服务器是否支持ssl访问，若支持请选择https；若不支持请选择http
        $alipay_config['transport']    = 'http';

        //支付API地址
        $alipay_config['apiurl']    = $apiurl;

        //计算得出通知验证结果
        $alipayNotify = new AlipayNotify($alipay_config);
        $verify_result = $alipayNotify->verifyNotify();
        if ($verify_result) {

            //商户订单号

            $out_trade_no = $_GET['out_trade_no'];

            //支付宝交易号

            $trade_no = $_GET['trade_no'];

            //交易状态
            $trade_status = $_GET['trade_status'];

            //支付方式
            $type = $_GET['type'];


            if ($_GET['trade_status'] == 'TRADE_SUCCESS') {

            } else {
                echo "trade_status=" . $_GET['trade_status'];
            }

            $row = DB::table('eq_recharge')->where('code', '=', $out_trade_no)->first();
            if ($row->integral == '铜牌会员') {
                DB::table('eq_user')->where('id', '=', $row->uid)->update(['level' => 2]);
            } elseif ($row->integral == '银牌会员') {
                DB::table('eq_user')->where('id', '=', $row->uid)->update(['level' => 3]);
            } elseif ($row->integral == '金牌会员') {
                DB::table('eq_user')->where('id', '=', $row->uid)->update(['level' => 4]);
            } elseif ($row->integral == '钻石会员') {
                DB::table('eq_user')->where('id', '=', $row->uid)->update(['level' => 5]);
            }
            if ($row->status == '0') {
                $mtime = time();
                DB::table('eq_recharge')->where('id', '=', $row->id)->update(['status' => 1, 'mtime' => $mtime]);
            }
            echo "<script>alert('付款成功');location.href='/center'</script>";

        } else {
            //验证失败
            //如要调试，请看alipay_notify.php页面的verifyReturn函数
            echo "<script>alert('付款失败');location.herf='/center'</script>";
        }

    }
}
