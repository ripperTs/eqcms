<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Leave;
use App\Models\Repleave;
use DB;

class LeaveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $count = count(Leave::where("audit","=",1)->get()); //总数量
        $num = 10; //每页显示数量
        $p = isset($request['page']) ? $request['page'] : 1;
        $offset = ($p - 1)  * $num;
        $sql = "SELECT * FROM eq_leave WHERE audit=1 ORDER BY id limit {$offset},{$num}";
        $res = DB::select($sql);
        if($request->ajax()){
            return view(TEM."pages.leave",['res'=>$res]);
        }
        $words = explode("|", C_WORDS);
        // $words = \json_encode($words);
        // dd($words);
        return view(TEM."leave",['res'=>$res,'count'=>$count,'num'=>$num]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //添加留言
        $data = $request['data'];
        if($data['code']!=session($data['code_leave'])){
            return 3;
        }
        if(S_SWITCH==1){
            $row = Leave::insert(['email' => $data['email'], 'name' => $data['name'], 'title' => $data['title'], 'content' => $data['content'], 'ip' => $request->ip(), 'audit' => 2, 'time' => time()]);
        }else{
            $row = Leave::insert(['email' => $data['email'], 'name' => $data['name'], 'title' => $data['title'], 'content' => $data['content'], 'ip' => $request->ip(), 'audit' => 1, 'time' => time()]);
        }
        if($row){
            echo 1;
        }else{
            echo 2;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $res = Repleave::where("l_id","=",$id)->first();
        if(count($res)){
            $arr = array('msg' => 1, 'content' => $res->rcontent);
        }else{
            $arr = array('msg' => 2, 'content' => $res->rcontent);
        }
        echo \json_encode($arr);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
