<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Mail;
use Ucpaas;
use Hash;

class RegisterController extends Controller
{
    //
    public function doreg(Request $request)
    {

        switch ($request['type']) {
            case 'phone':
                $phone = $request['phone'];
                if ($this->phoneApi($phone) == '000000') {
                    echo 1;
                } else {
                    echo 2;
                }
                break;

            case 'yzm':
                $yzm = $request['yzm'];
                if ($yzm == cache('phoneyzm' . $request['phone'])) {
                    echo 1;
                } else {
                    echo 2;
                }
                break;

            case 'phones':
                $phone = $request['phone'];
                $row = DB::table('eq_user')->where('phone', '=', $phone)->first();
                if ($row) {
                    echo 2;
                } else {
                    echo 1;
                }
                break;

            case 'email':
                $email = $request['email'];
                $view = TEM . 'emailReg'; //邮件模版
                $yzm = mt_rand(1000, 9999);
                $data = array('email' => $email, 'yzm' => $yzm); //模版分配变量
                cache(['emailyzm' . $email => $yzm, 'emailreg' => $email], 3);
                $to = $email; //发送邮箱
                $subject =  S_NAME . '邮箱验证!'; //发送主题
                Mail::send($view, $data, function ($message) use ($to, $subject) {
                    $message->to($to)->subject($subject);
                });
                if (count(Mail::failures()) < 1) {
                    echo 1;
                } else {
                    echo 2;
                }
                break;

            case 'emailyzm':
                $yzm = $request['yzm'];
                $email = $request['email'];
                if ($yzm == cache('emailyzm' . $email)) {
                    echo 1;
                } else {
                    echo 2;
                }
                break;

            case 'emails':
                $email = $request['email'];
                $row = DB::table('eq_user')->where('email', '=', $email)->first();
                if ($row) {
                    echo 2;
                } else {
                    echo 1;
                }
                break;
        }
    }

    //执行手机号注册
    public function phoneReg(Request $request)
    {
        $data = $request['data'];
        if ($data['vcode'] != cache('phoneyzm' . $data['user'])) {
            return;
        }
        $row = DB::table('eq_user')->insert(['user' => $data['user'], 'name' => '新会员', 'passwd' => Hash::make($data['passwd']), 'pic' => '', 'phone' => $data['user'], 'integral' => REGINTEGRAL, 'status' => 1, 'activation' => 1, 'time' => time()]);
        if ($row) {
            echo 200;
        } else {
            echo 201;
        }
    }

    //点击获取验证码
    public function phoneApi($phone)
    {
        $phoneApi = DB::table("eq_phoneapi")->first();
        //填写在开发者控制台首页上的Account Sid
        $options['accountsid'] = $phoneApi->accountid;
        //填写在开发者控制台首页上的Auth Token
        $options['token'] = $phoneApi->token;
        $ucpass = new Ucpaas($options);
        $appid = $phoneApi->appid; //应用的ID，可在开发者控制台内的短信产品下查看
        $templateid = $phoneApi->templateid;    //可在后台短信产品→选择接入的应用→短信模板-模板ID，查看该模板ID
        $param = mt_rand(1000, 9999); // 验证码多个参数使用英文逗号隔开（如：param=“a,b,c”），如为参数则留空
        $mobile = $phone; //手机号
        $uid = "";
        cache(['phoneyzm' . $phone => $param, 'phonereg' => $phone], 3); // 写入缓存
        $data = $ucpass->SendSms($appid, $templateid, $param, $mobile, $uid);
        $data = json_decode($data, true);
        return $data['code'];
    }

    public function emailReg(Request $request)
    {
        $data = $request['data'];
        if ($data['vcode'] != cache('emailyzm' . $data['user'])) {
            return;
        }
        $email = $data['user'];
        $token = base64_encode($data['user'] . mt_rand(100, 999));
        $row = DB::table('eq_user')->insert(['user' => $email, 'name' => '新会员', 'passwd' => Hash::make($data['passwd']), 'pic' => '', 'email' => $email, 'integral' => REGINTEGRAL, 'status' => 1, 'activation' => 0, 'time' => time(), 'token' => $token]);

        $view = TEM . 'emailToken'; //邮件模版
        $data = array('email' => $data['user'], 'token' => $token); //模版分配变量
        $to = $email; //发送邮箱
        $subject =  S_NAME . '邮箱激活通知!'; //发送主题
        Mail::send($view, $data, function ($message) use ($to, $subject) {
            $message->to($to)->subject($subject);
        });
        if (count(Mail::failures()) < 1) {
            echo 1;
        } else {
            echo 2;
        }
    }

    public function emailjh(Request $request)
    {
        $res = DB::table("eq_user")->where("email", "=", $request['email'])->first();
        if ($res->token == '') {
            echo "<script>location='404.html'</script>";
            return;
        }
        if ($res->token == $request['token']) {
            DB::table("eq_user")->where("email", "=", $request['email'])->update(['activation' => 1, 'token' => '']);
            echo "<script>alert('账号激活成功,请登陆!');location='/login'</script>";
        } else {
            echo "<script>alert('账号激活失败');location='/'</script>";
        }
    }

    public function back(Request $request)
    {
        return view(TEM . "backpwd");
    }

    //找回手机号账号
    public function phoneBack(Request $request)
    {
        $data = $request['data'];
        //判断验证码
        if ($data['vcode'] != cache('phoneyzm' . $data['user'])) {
            return;
        }
        if (!$request['check']) {
            return;
        }
        $pass = $data['passwd'];
        $phone = $data['user'];
        //修改密码并重置错误次数
        $res = DB::table("eq_user")->where("phone", "=", $phone)->update(['passwd' => Hash::make($pass), 'num' => 0]);
        if ($res) {
            echo 200;
            cache(['phoneyzm' . $data['user'] => ''], 1);
        } else {
            echo 2;
        }
    }

    //找回邮箱账号
    public function emailBack(Request $request)
    {
        $data = $request['data'];
        // 判断验证码
        if ($data['vcode'] != cache('emailyzm'. $data['user'])) {
            return;
        }
        if (!$request['check']) {
            return;
        }
        $email = $data['user'];
        $pass = $data['passwd'];
        //修改密码并重置错误次数
        $res = DB::table("eq_user")->where("email", "=", $email)->update(['passwd' => Hash::make($pass), 'num' => 0]);
        if ($res) {
            echo 200;
            cache(['emailyzm' . $data['user'] => ''], 1);
        } else {
            echo 2;
        }
    }
}
