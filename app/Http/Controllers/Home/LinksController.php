<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
class LinksController extends Controller
{
    //
    public function index(){
        return view(TEM."link");
    }

    public function add(Request $request){
        $code = session("{$request['session_code']}");
        if($request['code'] != $code){
            echo "<script>alert('验证码不正确!');location='/tolink';</script>"; //验证码不正确
            return;
        }
        //查询影片数据
        $row = DB::table("eq_link")->insert(['name'=>$request['name'], 'url'=> $request['url'], 'describe'=>$request['describe'], 'ishidden'=>0 , 'audit'=>0, 'time'=>time()]);
        if($row){
            echo "友情链接申请成功,现在可以关闭此页面!";
        }else{
            echo "<script>alert('申请失败!');location='/tolink';</script>";
        }


    }
}
