<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Type;
use Illuminate\Support\Facades\Redis;
use DB;

class IndexController extends Controller
{
    public $urlParam = null;
    public $url = null;
    public $a = null;
    //首页
    public function __construct()
    {
        //获取当前的页的url地址
        $this->url = $_SERVER['PHP_SELF'];
        //调用获取url参数的方法
        $this->urlParam();
    }

    public function index()
    {
        //最新和最热电影 查询所有的子分类
        if (E_SWITCH == 1 && Redis::get(MARK_REDIS . 'typerow')) {
            $str = Redis::get(MARK_REDIS . 'typerow');
        } else {
            $row = DB::table("eq_type")->where("p_id", "=", "1")->get();
            //拼接条件 好查询电影类下的最新的电影
            $str = " and(";
            foreach ($row as $v) {
                $str .= "d.t_id=" . $v->id . " or ";
            }
            $str = rtrim($str, "or ") . ") ";
            if (E_SWITCH == 1) {
                Redis::setex(MARK_REDIS . 'typerow', (E_TIME * 360), $str);
            }
        }

        //最新电影
        if (E_SWITCH == 1 && Redis::get(MARK_REDIS . 'newcine')) {
            $row = Redis::get(MARK_REDIS . 'newcine');
            $row = json_decode($row);
        } else {
            $sql = "SELECT *,d.id as id FROM eq_data as d,eq_details as e WHERE d.id = e.d_id {$str} and d.ishidden = 1 ORDER BY d.time DESC LIMIT 0,6";
            $row = DB::select($sql);
            $json_row = json_encode($row);
            if (E_SWITCH == 1) {
                Redis::setex(MARK_REDIS . 'newcine', (E_TIME * 60), $json_row);
            }
        }
        //最热电影
        if (E_SWITCH == 1 && Redis::get(MARK_REDIS . 'heatcine')) {
            $rows = Redis::get(MARK_REDIS . 'heatcine');
            $rows = json_decode($rows);
        } else {
            $sqls = "SELECT *,d.id as id FROM eq_data as d,eq_details as e WHERE d.id = e.d_id {$str} and d.ishidden = 1 ORDER BY e.score DESC LIMIT 0,6";
            $rows = DB::select($sqls);
            $json_rows = json_encode($rows);
            if (E_SWITCH == 1) {
                Redis::setex(MARK_REDIS . 'heatcine', (E_TIME * 60), $json_rows);
            }
        }


        //最新和最热电视剧
        if (E_SWITCH == 1 && Redis::get(MARK_REDIS . 'typerow1')) {
            $str1 = Redis::get(MARK_REDIS . 'typerow1');
        } else {
            $row1 = DB::table("eq_type")->where("p_id", "=", "2")->get();
            //拼接条件 好查询电影类下的最新和最热的电影
            $str1 = " and(";
            foreach ($row1 as $v1) {
                $str1 .= "d.t_id=" . $v1->id . " or ";
            }
            $str1 = rtrim($str1, "or ") . ") ";
            if (E_SWITCH == 1) {
                Redis::setex(MARK_REDIS . 'typerow1', (E_TIME * 360), $str1);
            }
        }

        //最新查询
        if (E_SWITCH == 1 && Redis::get(MARK_REDIS . 'newTV')) {
            $row1 = Redis::get(MARK_REDIS . 'newTV');
            $row1 = json_decode($row1);
        } else {
            $sql1 = "SELECT *,d.id as id FROM eq_data as d,eq_details as e WHERE d.id = e.d_id {$str1} and d.ishidden = 1 ORDER BY d.time DESC LIMIT 0,6";
            $row1 = DB::select($sql1);
            $json_row1 = json_encode($row1);
            if (E_SWITCH == 1) {
                Redis::setex(MARK_REDIS . 'newTV', (E_TIME * 60), $json_row1);
            }
        }
        //最热查询
        if (E_SWITCH == 1 && Redis::get(MARK_REDIS . 'heatTV')) {
            $row1s = Redis::get(MARK_REDIS . 'heatTV');
            $row1s = json_decode($row1s);
        } else {
            $sql1s = "SELECT *,d.id as id FROM eq_data as d,eq_details as e WHERE d.id = e.d_id {$str1} and d.ishidden = 1 ORDER BY e.score DESC LIMIT 0,6";
            $row1s = DB::select($sql1s);
            $json_row1s = json_encode($row1s);
            if (E_SWITCH == 1) {
                Redis::setex(MARK_REDIS . 'heatTV', (E_TIME * 60), $json_row1s);
            }
        }

        //最新和最热综艺
        if (E_SWITCH == 1 && Redis::get(MARK_REDIS . 'typerow2')) {
            $str2 = Redis::get(MARK_REDIS . 'typerow2');
        } else {
            $row2 = DB::table("eq_type")->where("p_id", "=", "3")->get();
            //拼接条件 好查询电影类下的最新和最热的电影
            $str2 = " and(";
            foreach ($row2 as $v2) {
                $str2 .= "d.t_id=" . $v2->id . " or ";
            }
            $str2 = rtrim($str2, "or ") . ") ";
            if (E_SWITCH == 1) {
                Redis::setex(MARK_REDIS . 'typerow2', (E_TIME * 360), $str2);
            }
        }

        //最新查询
        if (E_SWITCH == 1 && Redis::get(MARK_REDIS . 'newvariety')) {
            $row2 = Redis::get(MARK_REDIS . 'newvariety');
            $row2 = json_decode($row2);
        } else {
            $sql2 = "SELECT *,d.id as id FROM eq_data as d,eq_details as e WHERE d.id = e.d_id {$str2} and d.ishidden = 1 ORDER BY d.time DESC LIMIT 0,6";
            $row2 = DB::select($sql2);
            $json_row2 = json_encode($row2);
            if (E_SWITCH == 1) {
                Redis::setex(MARK_REDIS . 'newvariety', (E_TIME * 60), $json_row2);
            }
        }
        //最热查询
        if (E_SWITCH == 1 && Redis::get(MARK_REDIS . 'heatvariety')) {
            $row2s = Redis::get(MARK_REDIS . 'heatvariety');
            $row2s = json_decode($row2s);
        } else {
            $sql2s = "SELECT *,d.id as id FROM eq_data as d,eq_details as e WHERE d.id = e.d_id {$str2} and d.ishidden = 1 ORDER BY e.score DESC LIMIT 0,6";
            $row2s = DB::select($sql2s);
            $json_row2s = json_encode($row2s);
            if (E_SWITCH == 1) {
                Redis::setex(MARK_REDIS . 'heatvariety', (E_TIME * 60), $json_row2s);
            }
        }

        //最新和最热动漫
        if (E_SWITCH == 1 && Redis::get(MARK_REDIS . 'typerow3')) {
            $str3 = Redis::get(MARK_REDIS . 'typerow3');
        } else {
            $row3 = DB::table("eq_type")->where("p_id", "=", "4")->get();
            //拼接条件 好查询电影类下的最新和最热的电影
            $str3 = " and(";
            foreach ($row3 as $v3) {
                $str3 .= "d.t_id=" . $v3->id . " or ";
            }
            $str3 = rtrim($str3, "or ") . ") ";
            if (E_SWITCH == 1) {
                Redis::setex(MARK_REDIS . 'typerow3', (E_TIME * 360), $str3);
            }
        }


        //最新查询
        if (E_SWITCH == 1 && Redis::get(MARK_REDIS . 'newmanga')) {
            $row3 = Redis::get(MARK_REDIS . 'newmanga');
            $row3 = json_decode($row3);
        } else {
            $sql3 = "SELECT *,d.id as id FROM eq_data as d,eq_details as e WHERE d.id = e.d_id {$str3} and d.ishidden = 1 ORDER BY d.time DESC LIMIT 0,6";
            $row3 = DB::select($sql3);
            $json_row3 = json_encode($row3);
            if (E_SWITCH == 1) {
                Redis::setex(MARK_REDIS . 'newmanga', (E_TIME * 60), $json_row3);
            }
        }
        //最热查询
        if (E_SWITCH == 1 && Redis::get(MARK_REDIS . 'heatmanga')) {
            $row3s = Redis::get(MARK_REDIS . 'heatmanga');
            $row3s = json_decode($row3s);
        } else {
            $sql3s = "SELECT *,d.id as id FROM eq_data as d,eq_details as e WHERE d.id = e.d_id {$str3} and d.ishidden = 1 ORDER BY e.score DESC LIMIT 0,6";
            $row3s = DB::select($sql3s);
            $json_row3s = json_encode($row3s);
            if (E_SWITCH == 1) {
                Redis::setex(MARK_REDIS . 'heatmanga', (E_TIME * 60), $json_row3s);
            }
        }
        //轮播图下的9个类型
        $row4 = DB::select("SELECT * FROM eq_type LIMIT 0,9");
        // 轮播图
        if (E_SWITCH == 1 && Redis::get(MARK_REDIS . 'banner')) {
            $banner = Redis::get(MARK_REDIS . 'banner');
            $banner = json_decode($banner);
        } else {
            $banner = DB::select("select * from eq_banner where ishidden=1 order by sorting desc limit 0,7");
            $json_banner = json_encode($banner);
            if (E_SWITCH == 1) {
                Redis::setex(MARK_REDIS . 'banner', (E_TIME * 60), $json_banner);
            }
        }
        // 今日推荐
        if (E_SWITCH == 1 && Redis::get(MARK_REDIS . 'today')) {
            $today = Redis::get(MARK_REDIS . 'today');
            $today = json_decode($today);
        } else {
            $today = DB::select("SELECT d.d_name,d.note,d.typename,d.lasttime,d.id,d.lasttime,e.* FROM eq_data as d,eq_details as e WHERE d.id=e.d_id and d.ishidden=1 ORDER BY d.time,e.num DESC LIMIT 0,8");
            $json_today = json_encode($today);
            if (E_SWITCH == 1) {
                Redis::setex(MARK_REDIS . 'today', (E_TIME * 60), $json_today);
            }
        }
        //今日新增的数据条数
        $dayTime = date("Y-m-d");
        $time = DB::select("SELECT count(*) as num FROM eq_data WHERE time>unix_timestamp('{$dayTime}')");
        //公告
        if (E_SWITCH == 1 && Redis::get(MARK_REDIS . 'notice')) {
            $notice = Redis::get(MARK_REDIS . 'notice');
            $notice = json_decode($notice);
        } else {
            $notice = DB::select("SELECT * FROM eq_gonggao WHERE status = '1' ");
            $json_notice = json_encode($notice);
            if (E_SWITCH == 1) {
                Redis::setex(MARK_REDIS . 'notice', (E_TIME * 60), $json_notice);
            }
        }
        //友情链接
        if (E_SWITCH == 1 && Redis::get(MARK_REDIS . 'link')) {
            $link = Redis::get(MARK_REDIS . 'link');
            $link = json_decode($link);
        } else {
            $link = DB::select("SELECT `name`,`img`,`url`,`endtime` FROM eq_link WHERE `ishidden`=1");
            $json_link = json_encode($link);
            if (E_SWITCH == 1) {
                Redis::setex(MARK_REDIS . 'link', (E_TIME * 60), $json_link);
            }
        }
        //全部影片数量
        $filmnum = DB::select("SELECT count(*) as filmnum FROM eq_data");
        return view(TEM . "/index", ["newcine" => $row, "newTV" => $row1, "newvariety" => $row2, "newmanga" => $row3, "newcines" => $rows, "newTVs" => $row1s, "newvarietys" => $row2s, "newmangas" => $row3s, "row4" => $row4, "banner" => $banner, "today" => $today, "dayUpdate" => $time[0]->num, "notice" => $notice, "link" => $link, "filmnum" => json_encode($filmnum[0])]);

    }




    //首页头部菜单主分类模板页
    public function list(Request $request, $tid)
    {
        //
        $aaa = $request->input();
        if (!empty($aaa['amp;region'])) {
            $request['region'] = $aaa['amp;region'];
        }
        if (!empty($aaa['amp;year'])) {
            $request['year'] = $aaa['amp;year'];
        }
        if (!empty($aaa['amp;qubie'])) {
            $request['qubie'] = $aaa['amp;qubie'];
        }
        if (!empty($aaa['amp;langAll'])) {
            $request['langAll'] = $aaa['amp;langAll'];
        }
        if (count($request['region']) > 0 && $request['region'] != "regionAll") {
            $regions = ' e.region = ' . "'{$request['region']}'" . " and";
            $regionOne = $request['region'];
        } else {
            $regions = " ";
            $regionOne = 'regionAll';
        }
        //年份
        if (count($request['year']) > 0 && $request['year'] != "yearAll") {
            $years = ' e.year = ' . "'{$request['year']}'" . " and";
            $yearOne = $request['year'];
        } else {
            $years = " ";
            $yearOne = 'yearAll';
        }
        //语言
        if (count($request['langAll']) > 0  && $request['langAll'] != "langAll") {
            $langAll = " e.language = " . "'{$request['langAll']}'" . " and";
            $langAllOne = $request['langAll'];
        } else {
            $langAll = " ";
            $langAllOne = 'langAll';
        }
        //排序方式
        if ($request['qubie'] == "time") {
            $ans = "ORDER BY d.time DESC";
            $ansOne = 'time';
        } else if ($request['qubie'] == "man") {
            $ans = "ORDER BY e.num DESC";
            $ansOne = 'man';
        } else if ($request['qubie'] == "grade") {
            $ans = "ORDER BY e.score DESC";
            $ansOne = 'grade';
        } else {
            $ans = "ORDER BY d.time DESC";
            $ansOne = "time";
        }
        $term = $regions . $years . $langAll;

        //每页多少条
        $num = 48;

        //判断点的是不是类型里的全部分类
        if ($tid == "typeAll") {
            $typeAll = Type::get();
        } else {
            $typeAll = Type::where("p_id", "=", $tid)->get();
        }
        //拼接条件
        $str = " and(";
        foreach ($typeAll as $v) {
            $str .= "d.t_id=" . $v->id . " or ";
        }
        $str = rtrim($str, "or ") . ") ";

        //条数
        if (count($typeAll) == 0) {
            $sqls = "SELECT *,d.id as id FROM eq_data as d,eq_details as e WHERE {$term} d.id = e.d_id and d.t_id = {$tid} and d.ishidden = 1 {$ans}";
        } else {
            $sqls = "SELECT *,d.id as id FROM eq_data as d,eq_details as e WHERE {$term} d.id = e.d_id {$str} and d.ishidden = 1 {$ans}";
        }

        $count = count(DB::select($sqls));
        $pno = Intval(ceil($count / $num));  //总页数
        $p = isset($request['page']) ? $request['page'] : 1;
        $offset = ($p - 1)  * $num;
        if (count($typeAll) == 0) {
            $sql = "SELECT *,d.id as id FROM eq_data as d,eq_details as e WHERE {$term} d.id = e.d_id  and d.t_id = {$tid} and d.ishidden = 1 {$ans} LIMIT {$offset},{$num}";
        } else {
            $sql = "SELECT *,d.id as id FROM eq_data as d,eq_details as e WHERE {$term} d.id = e.d_id {$str} and d.ishidden = 1 {$ans} LIMIT {$offset},{$num}";
        }
        //查询数据
        if (E_SWITCH == 1 && Redis::get(MARK_REDIS . "" . $request['region'] . $request['year'] . $request['langAll'] . $request['qubie'] . $p . "list" . $tid)) {
            $data = Redis::get(MARK_REDIS . "" . $request['region'] . $request['year'] . $request['langAll'] . $request['qubie'] . $p . "list" . $tid);
            $data = json_decode($data);
        } else {
            $data = DB::select($sql);
            $json_data = json_encode($data);
            if (E_SWITCH == 1) {
                Redis::setex(MARK_REDIS . "" . $request['region'] . $request['year'] . $request['langAll'] . $request['qubie'] . $p . "list" . $tid, (E_TIME * 60), $json_data);
            }
        }
        //查询前15条数据
        if (count($typeAll) == 0) {
            $sqlss = "SELECT *,d.id as id FROM eq_data as d,eq_details as e WHERE d.id = e.d_id and d.t_id = {$tid} and d.ishidden = 1 ORDER BY e.num DESC LIMIT 0,15";
        } else {
            $sqlss = "SELECT *,d.id as id FROM eq_data as d,eq_details as e WHERE d.id = e.d_id {$str} and d.ishidden = 1 ORDER BY e.num DESC LIMIT 0,15";
        }
        if (E_SWITCH == 1 && Redis::get(MARK_REDIS . 'list' . $tid)) {
            $clickTop = Redis::get(MARK_REDIS . 'list' . $tid);
            $clickTop = json_decode($clickTop);
        } else {
            $clickTop = DB::select($sqlss);
            $json_clickTop = json_encode($clickTop);
            if (E_SWITCH == 1) {
                Redis::setex(MARK_REDIS . 'list' . $tid, (E_TIME * 60), $json_clickTop);
            }
        }
        //遍历类别表
        $type = Type::get();
        if ($tid == "typeAll") {
            $typeOne = array('t_name' => '全部类别', 'title' => '', 'id' => 'typeAll');
            $typeOne = json_encode($typeOne);
            $typeOne = json_decode($typeOne);
        } else {
            $typeOne = Type::where("id", "=", $tid)->first();
        }
        //地区分组
        $region = array_slice(DB::select("select region from eq_details group by region limit 0,18"), 2);
        //语言
        $language = array_slice(DB::select("select language from eq_details group by language"), 3);
        //年份
        $year = array_slice(DB::select("select year from eq_details group by year order by year desc limit 0,16"), 2);
        //判断是否有ajax传递的参数

        if ($request->ajax()) {
            return view(TEM . "/pages/list", ['data' => $data]); //$t_id 就是你点击的父类id
        }
        return view(TEM . "list", ['data' => $data, 'typeOne' => $typeOne, 'type' => $type, 'region' => $region, 'language' => $language, 'year' => $year, 'count' => $count, 'pageNum' => $num, 'clickTop' => $clickTop, 'request' => $this->urlParam, 'regionOne' => $regionOne, 'yearOne' => $yearOne, "langAllOne" => $langAllOne, "ansOne" => $ansOne]); //$t_id 就是你点击的父类id
    }



    //获取当前页的参数
    public function urlParam()
    {
        foreach ($_GET as $k => $v) {
            if ($k != 'page' && $k != '') {
                $this->urlParam .= '&' . $k . '=' . $v;
            }
        }
    }

    public function search(Request $request)
    {
        $terms = $request['terms'];

        $nums = DB::select("SELECT d.d_name,d.id,d.typename,e.strring,e.* FROM eq_data as d,eq_details as e,eq_type as t WHERE (d.d_name LIKE '%{$terms}%' OR e.region LIKE '%{$terms}%' OR e.strring LIKE '%{$terms}%' OR e.director LIKE '%{$terms}%') and (d.id=e.d_id) and (d.ishidden = 1) and(d.t_id = t.id)");
        $num = 10;
        $count = count($nums);
        $pno = Intval(ceil($count / $num));  //总页数
        $p = isset($request['page']) ? $request['page'] : 1;
        $offset = ($p - 1)  * $num;
        $row = DB::select("SELECT d.d_name,d.id,d.typename,e.strring,t.id as tid,e.* FROM eq_data as d,eq_details as e,eq_type as t WHERE (d.d_name LIKE '%{$terms}%' OR e.strring LIKE '%{$terms}%' OR e.director LIKE '%{$terms}%') and(d.id=e.d_id) and(d.ishidden = 1) and(d.t_id = t.id) LIMIT {$offset},{$num}");
        //演员
        foreach ($row as $k => $v) {
            $arr[$k] = explode(",", $v->strring);
        }
        //导演
        foreach ($row as $k => $v) {
            $dir[$k] = explode(",", $v->director);
        }
        if (count($row) == 0) {
            $judge = 0;
            $arr = [];
            $dir = [];
        } else {
            $judge = 1;
        }
        if ($request->ajax()) {
            return view(TEM . "/pages/search", ['row' => $row, 'arr' => $arr, "dir" => $dir]); //$t_id 就是你点击的父类id
        }
        return view(TEM . "search", ["row" => $row, "terms" => $terms, "judge" => $judge, 'count' => $count, 'pageNum' => $num, 'arr' => $arr, "dir" => $dir]);
    }

    //公告详情
    public function gonggao($id)
    {
        if (E_SWITCH == 1 && Redis::get(MARK_REDIS . 'gonggao')) {
            $data = Redis::get(MARK_REDIS . 'gonggao');
            $res = json_decode($data);
        } else {
            $res = DB::table("eq_gonggao")->where("id", "=", $id)->first();
            if (E_SWITCH == 1) {
                Redis::setex(MARK_REDIS . 'gonggao', (E_TIME * 60), $res);
            }
        }
        return view(TEM . "gonggao", ['res' => $res]);
    }

    public function switch()
    {
        return view(TEM . "switch");
    }

    //前台禁止访问ip
    public function banip()
    {
        return view(TEM . "banip");
    }
}
