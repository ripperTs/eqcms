<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Gregwar\Captcha\CaptchaBuilder;
use Gregwar\Captcha\PhraseBuilder;
use App\Models\Error;
use App\Models\Data;
use Mail;

class ErrorController extends Controller
{
    //
    public function index(Request $request)
    {
        //提交报错信息框
        $id = $request['id'];
        return view(TEM . "error", ['id' => $id]);
    }

    //验证码方法
    public function code($name)
    {
        // 限制验证码位数和验证码内容仅数字类型
        $phraseBuilder = new PhraseBuilder(4, '0123456789');
        $builder = new CaptchaBuilder(null, $phraseBuilder);
        $builder->build($width = 100, $height = 40, $font = null);
        $phrase = $builder->getPhrase();
        session([$name => $phrase]);
        //生成图片
        header("Cache-Control: no-cache, must-revalidate");
        header('Content-Type: image/jpeg');
        $builder->output();
    }

    public function add(Request $request)
    {
        $code = session("{$request['session_code']}");
        if ($request['code'] != $code) {
            echo "<script>alert('验证码不正确!');location='/error?id={$request['did']}';</script>"; //验证码不正确
            return;
        }
        //查询影片 数据
        $data = Data::where("id", "=", $request['did'])->first();
        $ip = str_replace("::1", "127.0.0.1", $request->ip());
        $row = Error::insert(['email' => $request['email'], 'd_name' => $data->d_name, 'd_id' => $request['did'], 'content' => $request['content'], 'ip' => $ip, 'audit' => 2, 'time' => time()]);
        if ($row) {
            // 如果开启邮件发送功能
            if (MAIL == 1) {
                //准备向指定用户邮箱发送邮件
                $view = 'admin.error.homeerror'; //邮件模版
                $data = array('content' => $request['content'], 'ip' => $ip, 'id' => $data['id'], 'd_name' => $data['d_name']); //模版分配变量
                $to = EMAIL; //发送邮箱
                $subject = '有新的影片报错!请注意查看并修复'; //发送主题
                Mail::send($view, $data, function ($message) use ($to, $subject) {
                    $message->to($to)->subject($subject);
                });
                if (count(Mail::failures()) < 1) {
                    echo "报错成功,等待管理员修复后会有邮件通知!<br><font color='red'>同时已自动发送邮件通知给管理员!</font><br>您现在可以关闭此页面!";
                } else {
                    echo "报错信息提交成功!但是邮件通知管理员失败,请放心关闭此页面.稍后修复完毕会有邮件通知到您的邮箱,请注意查收
                    !";
                }
            } else {
                echo "报错成功,等待管理员修复后会有邮件通知!您现在可以关闭此页面!";
            }
        } else {
            echo "<script>alert('报错失败!');location='/error?id={$request['did']}';</script>";
        }


    }
}
