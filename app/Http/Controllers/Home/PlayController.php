<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Data;
use App\Models\Details;
use App\Models\Paddress;
use App\Models\Type;
use DB;
use Illuminate\Support\Facades\Redis;


/**
 * @Author: wslmf
 * @Date: 2020-02-22 10:48:02
 * @Desc: 新增redis缓存,优化性能
 */
class PlayController extends Controller
{
    public function index($ids)
    {
        //分割id组,取出指定id
        $arrId = explode("-", $ids);
        $did = $arrId[0]; //影片id
        $pid = $arrId[1]; //播放地址id
        $blues = $arrId[2];  //当前播放集数id (显示5 页面上是第6集  无关紧要)
        @$quanxian = $arrId[3];
        //点击增加点击次数
        Details::where("d_id", "=", $did)->increment('num');
        $tid = Data::where("id", "=", $did)->first()->t_id;

        // 查询分类是否限制会员观看 0 无限制 1限制
        $authority = Type::where('id', '=', $tid)->value('authority');

        $dayTime = mktime(0, 0, 0, date('m'), date('d'), date('Y'));

        // 取出当天更新
        if (E_SWITCH == 1 && Redis::get(MARK_REDIS . 'dayUpdate')) {
            $data = Redis::get(MARK_REDIS . 'dayUpdate');
            $day = json_decode($data);
        } else {
            $sql = "SELECT *,d.id AS id	FROM eq_data AS d,eq_details AS e WHERE d.id=e.d_id AND d.ishidden=1 AND d.time>{$dayTime} ORDER BY d.time DESC LIMIT 0,11";
            $day = DB::select($sql); //取出当天更前10条
            if (E_SWITCH == 1) {
                Redis::setex(MARK_REDIS . 'dayUpdate', (E_TIME * 60), json_encode($day));
            }
        }

        // 正在热播
        if (E_SWITCH == 1 && Redis::get(MARK_REDIS . 'now')) {
            $data = Redis::get(MARK_REDIS . 'now');
            $hot = json_decode($data);
        } else {
            $sql = "SELECT *,d.id AS id	FROM eq_data AS d,eq_details AS e WHERE d.id=e.d_id AND d.ishidden=1 AND d.t_id={$tid} ORDER BY d.time,e.num DESC LIMIT 0,5";
            $hot = DB::select($sql); //正在热播
            if (E_SWITCH == 1) {
                Redis::setex(MARK_REDIS . 'now', (E_TIME * 60), json_encode($hot));
            }
        }

        // 猜你喜欢
        if (E_SWITCH == 1 && Redis::get(MARK_REDIS . 'guessYouLike')) {
            $data = Redis::get(MARK_REDIS . 'guessYouLike');
            $like = json_decode($data);
        } else {
            $sql = "SELECT *,d.id AS id	FROM eq_data AS d,eq_details AS e WHERE d.id=e.d_id AND d.ishidden=1 AND d.t_id={$tid} ORDER BY d.time DESC LIMIT 0,6";
            $like = DB::select($sql); //猜你喜欢
            if (E_SWITCH == 1) {
                Redis::setex(MARK_REDIS . 'guessYouLike', (E_TIME * 60), json_encode($like));
            }
        }

        // 影片详情数据
        if (E_SWITCH == 1 && Redis::get(MARK_REDIS . 'details' . $did)) {
            $data = Redis::get(MARK_REDIS . 'details' . $did);
            $data = json_decode($data);
        } else {
            $sql = "SELECT *,d.id AS did,e.id AS eid,d.time AS `time` FROM eq_data AS d,eq_details AS e WHERE d.id={$did} AND e.d_id={$did}";
            $data = DB::select($sql); //总数据
            if (E_SWITCH == 1) {
                Redis::setex(MARK_REDIS . 'details' . $did, (E_TIME * 60), json_encode($data));
            }
        }
        $strring = explode(",", $data[0]->strring);

        // 同主演推荐
        if (E_SWITCH == 1 && Redis::get(MARK_REDIS . 'withStar' . $did)) {
            $strrTop = Redis::get(MARK_REDIS . 'withStar' . $did);
            $strrTop = json_decode($strrTop);
        } else {
            $sqls = "SELECT *,d.id AS id FROM eq_data AS d,eq_details AS e WHERE d.id=e.d_id AND d.ishidden=1 AND e.strring='{$data[0]->strring}' ORDER BY d.time,e.num DESC LIMIT 0,10";
            $strrTop = DB::select($sqls); //同主演推荐
            if (E_SWITCH == 1) {
                Redis::setex(MARK_REDIS . 'withStar' . $did, (E_TIME * 60), json_encode($strrTop));
            }
        }

        // 播放地址列表
        if (E_SWITCH == 1 && Redis::get(MARK_REDIS . 'paddress' . $did)) {
            $paddress = Redis::get(MARK_REDIS . 'paddress' . $did);
            $paddress = json_decode($paddress);
        } else {
            $paddress = Paddress::where("r_id", "=", $did)->where('ishidden', "=", 1)->orderBy("id", "desc")->get();
            if (E_SWITCH == 1) {
                Redis::setex(MARK_REDIS . 'paddress' . $did, (E_TIME * 60), json_encode($paddress));
            }
        }

        // 播放地址
        if (E_SWITCH == 1 && Redis::get(MARK_REDIS . 'playurl' . $did)) {
            $playurl = Redis::get(MARK_REDIS . 'playurl' . $did);
            $playurl = json_decode($playurl, true);
        } else {
            $playurl = [];
            foreach ($paddress as $v) {
                $playurl["{$v->id}"] = explode("#", $v->playurl);
            }
            if (E_SWITCH == 1) {
                Redis::setex(MARK_REDIS . 'playurl' . $did, (E_TIME * 60), json_encode($playurl));
            }
        }

        $numm = Paddress::where('id', "=", $pid)->first();
        $numm = count(explode("#", $numm->playurl));

        $uid = session('user')['id'];
        if (!isset($uid)) {
            $ulevel = '';
        } else {
            $ulevel = DB::table("eq_user")->where('id', "=", $uid)->first()->level;
        }

        return  view(TEM . "play", ['authority' => $authority, 'strring' => $strring, 'paddress' => $paddress, 'data' => $data[0], 'day' => $day, 'hot' => $hot, 'like' => $like, 'blues' => $blues, 'vid' => $did, 'vfrom' => $pid, 'vpart' => $blues, 'playurl' => $playurl, 'strrTop' => $strrTop, 'numm' => $numm, 'quanxian' => $quanxian, 'ulevel' => $ulevel]);
    }

    //处理观看密码
    public function watchPasswd(Request $request)
    {
        $ids = $request->input("ids");
        $pwd = $request->input("password");
        $id = explode("-", $ids)[0];
        $passwd = Data::where("id", "=", $id)->first()->passwd;
        if ($pwd == $passwd) {
            $shouquan = MD5($pwd);
            $ids .= "-" . $shouquan;
            return redirect("/play/{$ids}.html");
        } else {
            echo "<script>alert('观看密码不正确!');location='/play/{$ids}.html'</script>";
        }
    }


    //收藏管理
    public function collection(Request $request)
    {
        if ($request['uid'] == '') {
            echo "err";
            return;
        }
        $res = table("eq_data")->where("id", "=", $request['id'])->first();
        $url = "http://" . S_REALM . "/details/" . $request['id'] . ".html";
        $row = DB::table("eq_collection")->insert(['uid' => $request['uid'], 'd_name' => $res->d_name, 'url' => $url, 'time' => time()]);
        if ($row) {
            echo 1;
        } else {
            echo 'err';
        }
    }
}
