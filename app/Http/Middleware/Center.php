<?php

namespace App\Http\Middleware;

use Closure;

class Center
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(H_SWITCH==2){
            echo "<script>alert('会员中心模块已被关闭!');location='/'</script>";
            return;
        }
        $id = session('user')['id'];
        if(!isset($id)){
            return redirect("/login");
        }
        return $next($request);
    }
}
