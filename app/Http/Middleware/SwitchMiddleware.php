<?php

namespace App\Http\Middleware;

use Closure;

class SwitchMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //前台总开关
        if(W_SWITCH == 1){
            $arr = explode("|", BANIP);
            $ip = str_replace("::1", "127.0.0.1", $request->ip());
            //前台禁止访问ip
            if (BANIP != '' && in_array($ip, $arr)) {
                return redirect("/banip.html");
            } else {
                return $next($request);
            }
        }else{
            return redirect("/switch.html");
        }



    }
}
