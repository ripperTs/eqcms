<?php

namespace App\Http\Middleware;

use Closure;
use DB;

class PowerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $id = session('admin')['id'];
        if (!isset($id)){
            // 登录入口 token 验证
            if(empty($request->input('token'))){
                return redirect("/admin/login");
            }else{
                return redirect("/admin/login?token=" . $request->input('token'));
            }
        } else if ($id != '1') {
            $action = \Route::current()->getActionName();
            list($class,$method) = explode('@',$action);
            $class = substr(strrchr($class,'\\'),1);
            $way = $class.'@'.$method;
            $RULE = DB::table('eq_admin-power')->join('eq_power','eq_admin-power.pid','=','eq_power.id')->join('eq_power-type','eq_power.id','=','eq_power-type.pid')->join('eq_powermanage','eq_power-type.tid','=','eq_powermanage.tid')->select('eq_powermanage.rule')->where('eq_admin-power.uid','=',$id)->where('eq_power.status','=','1')->get();
            $rule = array();
            foreach ($RULE as $V) {
                $rule[] = $V->rule;
            }
            $rule[] = "IndexController@index";
            $rule[] = "IndexController@welcome";
            if (in_array($way,$rule)) {
                return $next($request);
            } else {
                return redirect("/admin/unpower.html");
            }
        } else {
            return $next($request);
        }

    }
}
