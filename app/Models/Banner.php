<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    //
    protected $table = 'eq_banner'; //关联表
    public $timestamps = false; //自动维护时间
    protected $primaryKey = "id"; //主键
}
