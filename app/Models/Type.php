<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    //
    protected $table = 'eq_type'; //关联表
    public $timestamps = false; //自动维护时间
    protected $primaryKey = "id"; //主键
}
