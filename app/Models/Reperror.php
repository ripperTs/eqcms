<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reperror extends Model
{
    protected $table = 'eq_reperror'; //关联表
    public $timestamps = false; //自动维护时间
    protected $primaryKey = "id"; //主键
}
