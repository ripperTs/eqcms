<?php

    /*
        将一个数组中对应的值以字符分割成两个数据
        @param 分割符
        @param 数组
        @return 二维数组
        */
    function explodePro($str, $arr)
    {
        foreach ($arr as $v) {
            $am = explode($str, $v);
            $array['left'][] = $am[0];
            $array['right'][] = $am[1];
        }
        return $array;
    }

    //返回播放来源id
    //参数1:数组
    //返回值 下标
    function playLy(array $arr)
    {
        foreach ($arr as $k => $v) {
            if (strstr($v, 'm3u8')) {
                return $k;
            }
        }
    }

?>
