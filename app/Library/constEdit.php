<?php

    /*
    @param 常量文件
    @param 修改数组(常量名=>常量值)
    @return 失败返回false  成功修改常量文件
    */
    function constEdit($file, $arr)
    {
        $info = file_get_contents($file);
        foreach ($arr as $k => $v) {
            $info = preg_replace("/define\(\"{$k}\",\".*?\"\)/", "define(\"{$k}\",\"{$v}\")", $info);
        }
        return file_put_contents($file, $info);
    }

    /*
    @param 邮件配置修改
    @param 修改数组(常量名=>常量值)
    @return 失败返回false  成功修改常量文件
    */
    function saveEnv($file, $arr)
    {
        $info = file_get_contents($file);
        foreach ($arr as $k => $v) {
            $info = preg_replace("/{$k}=.*?\n/", "{$k}={$v}\n", $info);
        }
        return file_put_contents($file, $info);
    }

    /*
    空数组转换空字符串
    @param array
    @return string
    */
    function nullArrayConversion($data){
        if (is_array($data)) {
            return '';
        }
        return $data;
    }
