<?php
    // 开关1开  2关
    define("TEM","template/default/");//模板
    define("SWITCH","1");//开关
    define("S_REALM","yy8848.top");//网站域名
    define("S_NAME","8848私人影院");//网站名
    define("S_LOGO","/upload/logo.svg");//网站LOGO
    define("S_LOGO2","/upload/logo2.ico");//网站LOGO
    define("SEO","最新电影,好看的电影,高清影院为您提供最新、最全、最流畅的在线电影免费观看,看最新电影、好看的电影就上8848私人影院");//seo标题
    define("DEPICT","最新电影,好看的电影,高清影院为您提供最新、最全、最流畅的在线电影免费观看,看最新电影、好看的电影就上8848私人影院");//seo描述
    define("KEYWORD","最新电影,好看的电影,高清影院为您提供最新、最全、最流畅的在线电影免费观看,看最新电影、好看的电影就上8848私人影院");//seo关键字 默认空
    define("TWO_IMG","/upload/two.png");//二维码图片
    define("EMAIL","wangyifani@foxmail.com");//联系邮箱
    define("COUNT","");//统计代码
    define("ICP","");//ICP备案号
    define("COPYRIGHT","本站提供的最新电影和电视剧资源均系收集于各大视频网站,本站只提供web页面服务,并不提供影片资源存储,也不参与录制、上传 若本站收录的节目无意侵犯了贵司版权，请给网页底部邮箱地址来信,我们会及时处理和回复,谢谢！");//版权信息
    define("H_SWITCH","1");//会员开关
    define("Y_SWITCH","1");//用户评论开关
    define("S_SWITCH","2");//用户留言审核开关
    define("AD","1");//广告开关
    define("P_CODE","1");//评论验证码开关
    define("E_TIME","30");//缓存过期时间默认10秒
    define("E_SWITCH","1");//缓存开关
    define("W_SWITCH","1");//网站开关 默认1开(安全)
    define("W_HINT","网站正在维护中，请稍后再试！");//网站关闭提示(安全)
    define("C_WORDS","SB|草");//过滤留言关键字(安全)
    define("CJ_TOKEN","8P2CA382C0FABE202A4");//自动采集key(尽快更改红色字体提示)(安全)
    define("B_SWITCH","1");//百度推送功能开关
    define("B_REALM","yy8848.top");//百度推送域名
    define("B_KEYT","gA0sQkszqalfNHQ1");//百度推送密钥
    define("ADMINIP","127.0.0.2");//允许访问后台ip判断默认是空的不判断(安全)
    define("BANIP","");//禁止访问前台ip判断默认是空的不判断(安全)
    define("CJ_TIME","3");//采集间隔时间 默认3秒
    define("CLICK_MIN","2");//随机点击次数 最小1
    define("CLICK_MAX","4");//随机点击次数 最大10
    define("SCORE_MIN","4");//随机评分 最小1
    define("SCORE_MAX","7");//随机评分 最大5
    define("CONDITION","2");//更新判断条件 默认是2 1：更新时间  2：播放地址数量
    define("ISIMG","2");//是否更新图片 默认是1 0：不更新  1：更新呢
    define("ISCONTENT","2");//是否更新简介 默认是1 0：不更新  1：更新呢
    define("INTERVAL","3");//更新间隔时间 默认是3 单位是秒
    define("REGINTEGRAL","10");//注册送积分 默认0
    define("QDINTEGRAL","2");//签到送积分 默认0
    define("MEMORY","1");//是否开启刷新记忆功能 默认2 关闭状态
    define("MAIL","1");//是否开启邮件通知 1是开启 2是关闭 默认关闭
    define("LEVEL2","6");//铜牌会员价格
    define("LEVEL3","9.3");//银牌会员价格
    define("LEVEL4","13.5");//金牌会员价格
    define("LEVEL5","18.8");//钻石会员价格
    define("U_WATCH","2");//是否开启会员观看限制
    define("LOGIN_TOKEN","123456");//后台登录 token 令牌
    define("MARK_REDIS","EQ0INDEX0");//Redis缓存标识
