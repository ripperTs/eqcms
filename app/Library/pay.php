<?php
//支付函数
function pay($partner, $key, $ordernum, $orderming, $money, $desc, $payType,$apiurl)
{

	/* *
 * 功能：即时到账交易接口接入页
 *
 * 以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
 * 该代码仅供学习和研究支付宝接口使用，只是提供一个参考。
 */

	require_once("lib/epay_submit.class.php");
	//↓↓↓↓↓↓↓↓↓↓请在这里配置您的基本信息↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
	//商户ID
	$alipay_config['partner']		= $partner;

	//商户KEY
	$alipay_config['key']			= $key;

	//↑↑↑↑↑↑↑↑↑↑请在这里配置您的基本信息↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑


	//签名方式 不需修改
	$alipay_config['sign_type']    = strtoupper('MD5');

	//字符编码格式 目前支持 gbk 或 utf-8
	$alipay_config['input_charset'] = strtolower('utf-8');

	//访问模式,根据自己的服务器是否支持ssl访问，若支持请选择https；若不支持请选择http
	$alipay_config['transport']    = 'http';

	//支付API地址
	$alipay_config['apiurl']    = $apiurl;
	/**************************请求参数**************************/
	$notify_url = 'http://' . S_REALM . '/notify_url';
	//需http://格式的完整路径，不能加?id=123这类自定义参数

	//页面跳转同步通知页面路径
	$return_url = 'http://' . S_REALM . '/return_url';
	//需http://格式的完整路径，不能加?id=123这类自定义参数，不能写成http://localhost/


	/************************************************************/

	//构造要请求的参数数组，无需改动
	$parameter = array(
		"pid" => $partner,
		"type" => $payType,
		"notify_url"	=> $notify_url,
		"return_url"	=> $return_url,
		"out_trade_no"	=> $ordernum,
		"name"	=> $orderming == '钻石会员'?'顶级会员' : $orderming,
		"money"	=> $money,
		"sitename"	=> $desc
	);


	//建立请求
	$alipaySubmit = new AlipaySubmit($alipay_config);
	$html_text = $alipaySubmit->buildRequestForm($parameter);
	echo $html_text;

}
