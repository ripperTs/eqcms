<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use DB;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $row = DB::table("eq_type")->where("ishidden","=","1")->get();
        $sql = "SELECT d.d_name,d.id FROM	eq_data AS d,eq_details AS e WHERE d.id=e.d_id ORDER BY e.num LIMIT 0,5";
        $dataTop = DB::select($sql); //热门搜索
        view()->share(['header'=>$row,'dataTop'=>$dataTop]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
