<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    
    protected $table = 'eq_user';
    public $timestamps = false;
    
    protected $guarded = [];
    //这个不能随便改,不然切换状态不好使
    public function getStatusAttribute($value){
        $status = [0=>'停用',1=>'启用',2=>'冻结'];
        return $status[$value];
    }
    //下面这些都是可以改的
    public function getLevelAttribute($value){
        $level = [0=>'你就是个弟弟',1=>'普通会员',2=>'铜牌会员',3=>'银牌会员',4=>'金牌会员',5=>'钻石会员'];
        return $level[$value];
    }
    public function getActivationAttribute($value){
        $activation = [0=>'未激活',1=>'已激活'];
        return $activation[$value];
    }
}
