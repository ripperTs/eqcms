#!/bin/bash
#API获取步骤:后台->采集管理->[你要采集的资源库]->编辑->自动采集API,复制出来的API如下
#http://v.eqcms.top/admin/caiji-action/1/10/day?token=8P2C6322CHJ0PFAsdBE204

#请修改下面的采集API,例如:
web_site="http://v.eqcms.top/admin/caiji-action/"

#请修改下面项内容为后台设置的采集TOKEN
web_pwd="改成后台取到的token值,如:8P2C6322CHJ0PFAsdBE204"

#下面项内容为资源站ID,可在后台->采集管理->资源库列表中查看,也可以在上方采集API中取到
#注意格式必须是/+ID 如:/10
web_api_id="/10"

#下面内容不要修改
#start
web_page="1"
web_type="/day?token="
web_ua="Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)
Chrome/76.0.3809.100 Safari/537.36 seacmsbot/1.2;"
#end

#采集单页
function get_content() {
  echo  "正在采集第$page页..."
  #echo  " get_content: --->url:--->$1"
  cResult=$(curl  --connect-timeout 10 -m 20  -k -s   -L -A "$web_ua"  "$1" )
  echo $cResult | grep -q "采集"
  #echo  -e  "$1\n$cResult"
 if [ "$?" = "0" ]; then
     next_content "$cResult"
  else
      echo  -e "采集失败,请检查设置!\n失败链接-->$1\n返回信息-->$cResult\n采集结束，共0页"
  fi
}

#采集下页
function next_content() {
    #统计数据
     Result=$(echo "$1" | tr "<br>" "\n")
     a=$(echo "$Result" | grep -c  "正在采集新数据")
     b=$(echo "$Result" | grep -c  "数据存在")
     c=$(echo "$Result" | grep -c  "无需更新")
     d=$(echo "$Result" | grep -c  "略过")
     e=$(echo "$Result" | grep -c  "全部任务已完成")
     echo "采集成功-->已更$c部,新增$a部,更新$b部,跳过$d部"
     let add+=$a
     let update+=$b
     let none+=$c
     let jmp+=$d
     let end+=$e

    #检测并采集下页
    if [ "$end" = "0" ]
     then
      let page++
      get_content "$web_site$page$web_api_id$web_type$web_pwd"
    else
      echo "采集结束!"
    fi


}

#脚本入口
echo "EQCMS自动采集脚本开始执行 版本：v1.0"
starttime=$(date +%s)
update=0  #更新
add=0     #新增
none=0  #无变化
jmp=0  # 跳过
end=0 #已完成

web_param="$web_site$web_page$web_api_id$web_type$web_pwd"
page="$web_page"
echo "开始采集：$web_param"
get_content $web_param

endtime=$(date +%s)
echo "============================"
echo "入库-->$add部"
echo "更新-->$update部"
echo "跳过-->$jmp部(未绑定分类或链接错误)"
echo "今日-->$[none+add+update]部"
echo "============================"
echo  "全部采集结束,耗时$[endtime - starttime]秒"
